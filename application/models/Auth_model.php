<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth_model extends CI_Model
{
	protected $table_transactions = 'transactions';
	protected $table_entreprise = 'entreprise';

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('UTC');
	}

	public function getPrimes($mobile)
	{	
		$vie = array();
		$iard = array();

		$mobile = '+'.$mobile;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => '197.159.217.53/abi/getListePolicesVies.php',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 400,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'TELEASSU='.$mobile,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));
		$response = json_decode(curl_exec($curl), true);
		curl_close($curl);

		if ($response['code'] == 1)
		{	
			if (isset($response['data'])) 
			{
		        $vie = $response['data']['polices'];
			}
		}

		$ch = curl_init();
		curl_setopt_array($ch, array(
		  CURLOPT_URL => '197.159.217.53/abi/getListePolicesNonVies.php',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 600,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'TELEASSU='.$mobile,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));
		$resultat = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if ($resultat['code'] == 1)
		{	
			if (isset($resultat['data'])) 
			{
		        $iard = $resultat['data']['quittances'];
			}
		}

		// $iard[] = array('code' => '1010-40000074', 'prime' => 'FLOTTE AUTOMOBILE', "assure" => " ATLANTIC BUSINESS INTERNATIONAL", 'type' => 'IARD', 'montant' => 53864);
		// $iard[] = array('code' => '1002-41003773', 'prime' => 'VEHICULE DE TOURISME PERSONNE PHYSIQUE', "assure" => " ADOMON KOUACOU PIERRE", 'type' => 'IARD', 'montant' => 27854);
		// $iard[] = array('code' => '1012-41000053', 'prime' => 'VEHICULE DE TOURISME PERSONNE PHYSIQUE', "assure" => " JAFFAR EPSE MAKKE SANDRA", 'type' => 'IARD', 'montant' => 266247);
		// $iard[] = array('code' => '1003-41100456', 'prime' => 'VEHICULE DE TOURISME PERSONNE MORALE', "assure" => " IVOIRE AUTO SERVICES", 'type' => 'IARD', 'montant' => 495043);
		// $iard[] = array('code' => '3274-40000025', 'prime' => 'FLOTTE AUTOMOBILE', "assure" => "DECOTEK INSAAT GIDA TEKSTIL ORMAN", 'type' => 'IARD', 'montant' => 2308707);
		// $iard[] = array('code' => '3274-60300005', 'prime' => 'RC Entreprise PME/PMI', "assure" => "INSTAFRIC ELEC", 'type' => 'IARD', 'montant' => 641200);


		return array("vie" => $vie, "iard" => $iard);

	}

	/**
	 * function permettant d'enregistrer un abonne
	 */
	public function insertTrans($NUMEAVEN, $NUMEQUIT, $nombre_choisis, $assure_code_inte, $assure_num_polices, $libelle_produit,
	$montant_trans, $atlantis_ref, $ent_fk, $pays_fk, $assure, $type_concat, $provider)
	{			
			if (empty($pays_fk)) {
				$pays_fk = $this->session->userdata('pays_id');
			}

			if ($type_concat !== 'VIE') 
			{
				$nombre_choisis = NULL;
			}

			       $this->db->set('montant_trans', $montant_trans)
							->set('atlantis_ref', $atlantis_ref)
							->set('nombre_choisis', $nombre_choisis)
							->set('NUMEAVEN', $NUMEAVEN)
							->set('LIBPRODUIT', $libelle_produit)
							->set('NUMEQUIT', $NUMEQUIT)
							->set('ent_fk', $ent_fk)
							->set('assure_num_polices', $assure_num_polices)
							->set('assure_code_inte', $assure_code_inte)
							->set('assureID', $this->session->userdata('id_assure'))
							->set('assureMobile', $this->session->userdata('mobile_assure'))
							->set('pays_fk', $pays_fk)
							->set('assure_trans', $assure)
							->set('produit_trans', $type_concat)
							->set('provider', $provider)
							->set('statut_trans', 'P')
							->set('date_create_trans', date("Y-m-d H:i:s"))
							->insert($this->table_transactions);
	 	    return $this->db->insert_id();	
	}

	public function insertTrans2($NUMEAVEN, $NUMEQUIT, $nombre_choisis, $assure_code_inte, $assure_num_polices, $libelle_produit,
	$montant_trans, $atlantis_ref, $ent_fk, $pays_fk, $assure, $type_concat, $provider, $mobile_trans)
	{
			if (empty($pays_fk)) {
				$pays_fk = $this->session->userdata('pays_id');
			}

			if ($type_concat !== 'VIE')
			{
				$nombre_choisis = NULL;
			}

			       $this->db->set('montant_trans', $montant_trans)
							->set('atlantis_ref', $atlantis_ref)
							->set('nombre_choisis', $nombre_choisis)
							->set('NUMEAVEN', $NUMEAVEN)
							->set('LIBPRODUIT', $libelle_produit)
							->set('NUMEQUIT', $NUMEQUIT)
							->set('ent_fk', $ent_fk)
							->set('assure_num_polices', $assure_num_polices)
							->set('assure_code_inte', $assure_code_inte)
							->set('assureID', $this->session->userdata('id_assure'))
							->set('assureMobile', $this->session->userdata('mobile_assure'))
							->set('pays_fk', $pays_fk)
							->set('assure_trans', $assure)
							->set('produit_trans', $type_concat)
							->set('provider', $provider)
							->set('mobile_trans', $mobile_trans)
							->set('statut_trans', 'P')
							->set('date_create_trans', date("Y-m-d H:i:s"))
							->insert($this->table_transactions);
	 	    return $this->db->insert_id();
	}

	public function insertTrans3($NUMEAVEN, $NUMEQUIT, $nombre_choisis, $assure_code_inte, $assure_num_polices, $libelle_produit,
	$montant_trans, $atlantis_ref, $ent_fk, $pays_fk, $assure, $type_concat, $provider, $mobile_trans, $assureID, $assureMobile)
	{

			if ($type_concat !== 'VIE')
			{
				$nombre_choisis = NULL;
			}

			       $this->db->set('montant_trans', $montant_trans)
							->set('atlantis_ref', $atlantis_ref)
							->set('nombre_choisis', $nombre_choisis)
							->set('NUMEAVEN', $NUMEAVEN)
							->set('LIBPRODUIT', $libelle_produit)
							->set('NUMEQUIT', $NUMEQUIT)
							->set('ent_fk', $ent_fk)
							->set('assure_num_polices', $assure_num_polices)
							->set('assure_code_inte', $assure_code_inte)
							->set('assureID', $assureID)
							->set('assureMobile', $assureMobile)
							->set('pays_fk', $pays_fk)
							->set('assure_trans', $assure)
							->set('produit_trans', $type_concat)
							->set('provider', $provider)
							->set('mobile_trans', $mobile_trans)
							->set('statut_trans', 'P')
							->set('date_create_trans', date("Y-m-d H:i:s"))
							->insert($this->table_transactions);
	 	    return $this->db->insert_id();
	}

	public function modifier($reference_syca, $atlantis_ref, $mobile_trans, $statut_trans, $operatorTrans)
	{		 
			if ($statut_trans == '0') 
			{
				$statut_trans = 'S';
			}
			elseif ($statut_trans == '-200') 
			{
				$statut_trans = 'P';
			}
			else
			{
				$statut_trans = 'E';
			}

			 return $this->db->set('reference_syca', $reference_syca)
							 ->set('provider', $operatorTrans)
							 ->set('mobile_trans', $mobile_trans)
							 ->set('statut_trans',  $statut_trans)
							 ->set('date_maj_statut', date("Y-m-d H:i:s"))
							 ->where('atlantis_ref', $atlantis_ref)
							 ->update($this->table_transactions);
	}

	public function is_existe($atlantis_ref)
	{
		return $this->db->select("*")
					->from($this->table_transactions)
					->where('atlantis_ref', $atlantis_ref)
					->get()
					->row();

	}

	public function isCommande($atlantis_ref, $montant_trans)
	{
		return $this->db->select("*")
					->from($this->table_transactions)
					->where('montant_trans', $montant_trans)
					->where('atlantis_ref', $atlantis_ref)
					->get()
					->row();
	}

	public function getEntByDomaine($domaine_activite)
	{	
		$pays_id = $this->session->userdata('pays_id');
		return $this->db->select("*")
					->from($this->table_entreprise)
					->like('domaine_activite', $domaine_activite)
					->where('pays_id', $pays_id)
					->get()
					->row();
	}

	public function getEntByDomaineAndPays($domaine_activite, $pays_id)
	{
		return $this->db->select("*")
					->from($this->table_entreprise)
					->like('domaine_activite', $domaine_activite)
					->where('pays_id', $pays_id)
					->get()
					->row();
	}

	public function Guid($prefix)
	{
		do {
		$lettre = strtoupper(uniqid($prefix,FALSE));
		} while ($this->is_existe($lettre));

		return $lettre;
	}
}