<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Entreprise_model extends CI_Model
{
	protected $table_entreprise = 'entreprise';
	

	public function __construct()
	{
		parent::__construct();
	}

	public function getAllEntreprises()
	{
		$query = $this->db->select("*")
					  ->from($this->table_entreprise)
					  ->get();
		return $query->result();

	}

}