<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transactions_model extends CI_Model
{
	protected $table_transactions = 'transactions';
	protected $table_callback = 'callback';
	

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('UTC');
	}

	public function getReferenceSycapay($ReferenceSycapay)
	{
		$query = $this->db->select("*")
						->from($this->table_transactions)
						->join("pays p", "pays_fk = p.pays_id", "left")
						->join("entreprise e", "ent_fk = e.id_ent", "left")
						->where('transactions.reference_syca', $ReferenceSycapay)
						->get();
		return $query->result();
	}

	public function getTransactions()
	{
		$id_assure = $this->session->userdata('id_assure');
		$query = $this->db->select("*")
						->from($this->table_transactions)
						->where('assureID', $id_assure)
						->order_by("date_create_trans","desc")
						->get();
		return $query->result();
	}

	public function getAllTransactions()
	{
		$query = $this->db->select("*")
						->from($this->table_transactions)
						->order_by("date_create_trans","desc")
						->get();
		return $query->result();
	}

	public function getAssure()
	{
		$query = $this->db->select("*")
						->from("user")
						->get();
		return $query->result();
	}

	public function modifierTermesListes($atlantis_ref, $NUMEENCA, $NUMEQUIT, $MOISTERM, $DATEEFFE, $DATEECHE)
	{		 
			 return $this->db->set('NUMEENCA', $NUMEENCA)
							 ->set('NUMEQUIT', $NUMEQUIT)
							 ->set('MOISTERM',  $MOISTERM)
							 ->set('DATEEFFE', $DATEEFFE)
							 ->set('DATEECHE',  $DATEECHE)
							 ->where('atlantis_ref', $atlantis_ref)
							 ->update($this->table_transactions);
	}

	/**
	 * function permettant d'enregistrer un abonne
	 */
	public function insert_trans_temp($montant, $allianz_ref)
	{
		date_default_timezone_set('UTC');
		$today = date("Y-m-d H:i:s");
		return $this->db->set('declencheur_transaction', "WEB")
				->set('allianz_ref', $allianz_ref)
				->set('syca_ref', null)
				->set('montant_transaction', $montant)
				->set('date_transaction', $today)
				->set('statut', "P")
				->insert($this->table_transactions);
	}

	public function insert_trans_def($syca_ref, $provider, $allianz_ref, $statut, $mobile_transaction)
	{
		date_default_timezone_set('UTC');
		$today = date("Y-m-d H:i:s");
		$this->db->set('syca_ref', $syca_ref);
		$this->db->set('mobile_transaction', $mobile_transaction);
		$this->db->set('provider', $provider);
		$this->db->set('date_changement_statut', $today);
		$this->db->set('statut', $statut);
		$this->db->where('allianz_ref', $allianz_ref);
		return $this->db->update($this->table_transactions);
	}

	public function getTransByRefPaie($referencePay)
	{
		date_default_timezone_set('UTC');
		$today = date("Y-m-d H:i:s");

		return $this->db->select("*")
						->from($this->table_transactions)
						->join("pays p", "pays_fk = p.pays_id", "left")
						->join("entreprise e", "ent_fk = e.id_ent", "left")
						->where('reference_syca', $referencePay)
						->or_where('atlantis_ref', $referencePay)
						->get()
						->row();
	}

	public function majStatusEchecs($atlantis_ref)
	{
		return $this->db->set('statut_trans',  'E')
						 ->set('date_maj_statut', date("Y-m-d H:i:s"))
						 ->where('atlantis_ref', $atlantis_ref)
						 ->update($this->table_transactions);
	}

}