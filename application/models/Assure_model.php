<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Assure_model extends CI_Model
{
	protected $table_assures = 'assures';
	

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * function permettant d'enregistrer un abonne
	 */
	public function inscrire($pays_id, $mobile, $nom, $prenoms, $sexe, $password)
	{
			  $this->db->set('pays_id', $pays_id)
							->set('mobile_assure', $mobile)
							->set('nom_assure', $nom)
							->set('prenoms_assure', $prenoms)
							->set('sexe_assure', $sexe)
							->set('password_assure', md5($password))
							->set('etat_assure', 'A')
							->set('date_enregistrement_assure', date("Y-m-d H:i:s"))
							->insert($this->table_assures);
	 	    return $this->db->insert_id();
	}

	public function updatePass($mobile, $password)
	{
			  return $this->db->set('password_assure', md5($password))
							->where('mobile_assure', $mobile)
							->update($this->table_assures);
	}

	public function getAssureById($id_assure)
	{
			  $ret = $this->db->select('*')
							->from($this->table_assures)
							->where('id_assure', $id_assure)
							->get()
				  			->row();
	 	    return $ret;
	}

	public function getAssure($mobile, $password)
	{
			  $ret = $this->db->select('*')
							->from($this->table_assures)
							->where('mobile_assure', $mobile)
							->where('password_assure', md5($password))
							->get()
				  			->row();
	 	    return $ret;
	}

	public function getAssure2($mobile)
	{
			  $ret = $this->db->select('*')
							->from($this->table_assures)
							->where('mobile_assure', $mobile)
							->get()
				  			->row();
	 	    return $ret;
	}

}