<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	/*
	| -------------------------------------------------------------------
	| Assets HELPER
	| -------------------------------------------------------------------
	| Cette fonction me permettra de récuperer plus rapidement les 
	|URL des fichiers contenus  dans le dossier assets
	|
	*/

	if ( ! function_exists('css_url'))
	{
		function css_url($nom)
		{
			return base_url() . 'assets/css/' . $nom . '.css';
		}
	}

	if ( ! function_exists('js_url'))
	{
		function js_url($nom)
		{
			return base_url() . 'assets/js/' . $nom . '.js';
		}
	}

	if ( ! function_exists('img_url'))
	{
		function img_url($nom)
		{
			return base_url() . 'assets/img/' . $nom;
		}
	}

	if ( ! function_exists('libraries_url'))
	{
		function libraries_url($nom)
		{
			return base_url() . 'assets/libraries/' . $nom;
		}
	}

	if ( ! function_exists('plugins_url'))
	{
		function plugins_url($nom)
		{
			return base_url() . 'assets/plugins/' . $nom;
		}
	}



    if ( ! function_exists('bowers_url'))
    {
        function bowers_url()
        {
            return base_url() . 'assets/atlantis_assets/bower_components/';
        }
    }



    if ( ! function_exists('decontaminate_data'))
    {
        function decontaminate_data($post){
            foreach($post as $key => $value){
                $data = trim($value);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                $post[$key] = $data;
            }

            return $post;
        }
    }



