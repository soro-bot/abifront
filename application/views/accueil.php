<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
<style>
.buttonload {
  background-color: #e67902; /* Green background */
  border: none; /* Remove borders */
  color: white; /* White text */
  padding: 12px 24px; /* Some padding */
  font-size: 16px; /* Set a font-size */
}
</style>
</head>
<body >

<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h3 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirm_modal_titre'); ?></h3>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="active item one"></div>
        <div class="item two"></div>
        <div class="item three"></div>
    </div>
</div>

<div class="container">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('tpl/header'); ?>
    <!-- END HEADER -->

    
    <div class="tab-block3" style="">
        <div class="buttonload" style="text-align: center; display: none;">
            <i class="fa fa-refresh fa-spin"></i> Chargement ...
        </div>
        <!--  onSubmit="return confirm('Voulez Vous Faire Ce Paiement ?')" -->
        <!-- <?php echo site_url('Accueil/payment') ?> -->
        <!-- <form id="regForm" action="https://secure.sycapay.net/checkresponsive" enctype="multipart/form-data"
              method="post" role="form" style="display:none;"> -->
        <form id="regForm" action="https://secure.sycapay.net/checkresponsive" enctype="multipart/form-data"
              method="post" role="form">
            <div class="form-body" style="position: center;">
                <!-- One "tab" for each step in the form: -->
                <?php if ($this->session->userdata("prenoms_assure")) : ?>
                <div class="tab" style="padding: 10px; background-color: white">
                    <h2 style="margin-bottom: 30px">
                        <center>Bienvenue <b style="color: #491c05;"><?php echo $this->session->userdata("prenoms_assure") ?>,</b></center>
                    </h2>
                    <h3 class="text-center">Que voulez vous faire ?</h3>
                    <div class="row">
                        <div class="col col-md-4 text-center">
                            <div id="block_button" style="height: 140px; width: 100%; position: absolute; display: none"></div>
                            <a class="myLongChargement" style="cursor: pointer" onclick="nextPrev(1); $('#block_button').fadeIn();">
                            <img src="<?php echo img_url("insurance.png") ?>" style="height: 100px;">
                            <h4 >Payer ma prime</h4>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                            <a style="cursor: pointer" href="javascript:void(0)" data-toggle="modal" data-target="#historique-modal">
                            <img src="<?php echo img_url("history.png") ?>" style="height: 100px;">
                            <h4 >Consulter mon historique</h4>
                            </a>
                        </div>
                        <div class="col col-md-4 text-center">
                            <a style="cursor: pointer" href="javascript:void(0)" data-toggle="modal" data-target="#forgot-pass-modal">
                            <img src="<?php echo img_url("password.png") ?>" style="height: 100px;">
                            <h4 >Modifier mon mot de passe</h4>
                            </a>
                        </div>
                    </div>
                </div>


                    <!--Login, Signup, Forgot Password Modal -->
                    <div id="forgot-pass-modal" class="modal fade" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">

                            <!-- signup modal content -->
                            <div class="modal-content" id="signup-modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"><span class="glyphicon glyphicon-lock"></span> Modifier mon mot de passe</h4>
                                </div>

                                <div class="modal-body">
                                    <p>Veuillez renseigner les infos ci dessous pour procéder à la modification de votre mot de passe</p>
                                    <form method="post" id="forgot-pass-form" role="form">

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-time"></span></div>
                                                <input onpaste="return false" name="password" id="old_mdp" type="password" class="form-control input-lg" placeholder="Entrez votre ancien mot de passe" required data-parsley-type="email">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                                <input onpaste="return false" name="password" id="passwd" type="password" class="form-control input-lg" placeholder="Entrez le nouveau mot de passe" required data-parsley-length="[6, 10]" data-parsley-trigger="keyup">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                                <input onpaste="return false" name="password" id="confirm-passwd" type="password" class="form-control input-lg" placeholder="Retaper le nouveau mot de passe" required data-parsley-equalto="#passwd" data-parsley-trigger="keyup">
                                            </div>
                                        </div>

                                        <div class="form-group " style="margin-top: 15px">
                                            <p> </p>
                                        </div>


                                        <button type="submit" id="btn_update_pass" class="btn btn-warning btn-block btn-lg">Modifier mot de passe</button>
                                    </form>
                                </div>

                            </div>
                            <!-- signup modal content -->




                            <!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>
                    <!--Forgot Password Modal -->

                <!--Login, Signup, Forgot Password Modal -->
                <div id="historique-modal" class="modal fade" tabindex="-1" role="dialog" style="width:100%">
                    <div class="modal-dialog modal-lg" role="document">

                        <!-- signup modal content -->
                        <div class="modal-content" id="signup-modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"><span class="glyphicon glyphicon-time"></span> Historique des transactions</h4>
                            </div>

                            <div class="modal-body">
                                    <table id="example" class="table table-striped table-bordered" style="width:100%; font-size: 15px">
                                    <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <!-- <th>Ref Syca</th> -->
                                        <th><?php echo $this->lang->line('montant_label'); ?></th>
                                        <th><?php echo $this->lang->line('produit_label'); ?></th>
                                        <th>Payé via</th>
                                        <th>Statut</th>
                                        <th>Date</th>
                                        <th><i class="fa fa-download" aria-hidden="true"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($getTransactions) { ?>
                                    <?php foreach ($getTransactions as $trans) : ?>
                                    <tr>
                                        <td><?php echo $trans->atlantis_ref; ?></td>
                                        <!-- <td><?php echo $trans->reference_syca; ?></td> -->
                                        <td><?php echo $trans->montant_trans; ?></td>
                                        <td><?php echo $trans->LIBPRODUIT; ?></td>
                                        <td><?php echo $trans->provider; ?><?php echo isset($trans->mobile_trans) ? "(".$trans->mobile_trans.")" : ""; ?></td>
                                        <?php if ($trans->statut_trans == 'P') { ?>
                                        <td>En attente</td>
                                        <?php }elseif ($trans->statut_trans == 'E') { ?>
                                        <td style="color: red;">Echec</td>
                                        <?php }else{ ?>
                                        <td style="color: green;">Succes</td>
                                        <?php }; ?>
                                        <td><?php echo $trans->date_create_trans; ?></td>
                                        <td>
                                        <?php if ($trans->statut_trans == 'S') { ?>
                                        <a title="Download" class="btn btn-xs btn-success" href="<?php echo base_url('assets/generatepdf')."/".$trans->reference_syca.".pdf";?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
                                        <?php }; ?>
                                       </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php }; ?>
                                    </tbody>
                                   </table>
                                </div>

                            </div>
                            <!-- signup modal content -->




                            <!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>
                    <!--Forgot Password Modal -->

                <?php else : ?>

                <div class="tab" style="padding: 10px; background: #f8ddb4;">
                    <h2 style="margin-bottom: 30px">
                        <center>Identifiez-vous</center>
                    </h2>
                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php echo $this->lang->line('pays'); ?> :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-home"></i>
                                </div>

                                <select class="selectpicker select2 form-control" id="pays_id" required="">

                                    <option value="CI" selected="" data-indicatif="225"
                                            data-content="<img class='' width='25' src='<?php echo img_url("flags/ci.png") ?>' />   Côte d'Ivoire">
                                        Côte d'Ivoire
                                    </option>
                                    <option value="TG" data-indicatif="228"
                                            data-content="<img class='' width='25' src='<?php echo img_url("flags/tg.png") ?>' />   Togo">
                                        Togo
                                    </option>
                                </select>

                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-row hide" style="display: none">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php echo $this->lang->line('entreprise'); ?>
                                :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-home"></i>
                                </div>

                                <select class="selectpicker select2 form-control" id="ent_id" required="">
                                    <?php
                                    foreach ($entreprises as $ent)
                                        echo '<option data-pays="' . $ent->pays_id . '" data-content="' . $ent->ent_raison . '  (<img class=\' \' width=\'25\' src=\'' . img_url("flags/" . $ent->pays_id . ".png") . '\' />)" value="' . $ent->code_ent . '">' . $ent->ent_raison . ' (' . $ent->pays_id . ')</option>';

                                    ?>
                                </select>

                            </div><!-- /.input group -->
                        </div>
                    </div>
                 
                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mobile :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <span id="indicatif">+225</span>
                                </div>
                                <input onpaste="return false" type="text" class="form-control" placeholder="Mobile" required id="mobile" name="mobile" maxlength="10" value=""/>
                            </div><!-- /.input group -->
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mot de passe:</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input onpaste="return false" type="password" autocomplete="on" class="form-control" placeholder="Votre mot de passe" required name="password" id="password"/>
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" style="text-align: right">
                            <span>Mot de passe oublié ? <a href="" data-toggle="modal" data-target="#getDivOffPassWord" style="text-decoration: underline; color: black">Cliquez ici</a></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" style="text-align: right">
                            <span>Vous n'avez pas de compte ? <a href="<?php echo site_url("Accueil/inscription") ?>" style="text-decoration: underline; color: black">Inscrivez vous</a></span>
                        </div>
                    </div>
                </div>


                <?php endif; ?>

                <!-- <div class="tab"> -->
                <div class="tab">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" style="border-radius: 6px 0 0 0;" href="#vie">VIE</a></li>
                        <li><a data-toggle="tab" href="#iard" style="border-radius: 0 6px 0 0;">IARD</a></li>
                    </ul>

                    <div class="tab-content" style="">
                        <div id="vie" class="tab-pane fade in active" style="padding-top: 1px;">
                            <center><?php echo "Liste des impayés VIE"; ?></center>
                            <div class="form-row">
                                <div class="scroll">
                                    <div class="row row-primes hide">
                                        <div class="col col-md-1 check">
                                            <input type="checkbox" />
                                        </div>
                                        <div class="col col-md-7 title">
                                            <h4>Accident corporel</h4>
                                            <span>(Souscription : <b>200 000F</b>)</span>
                                            <h5>N° police</h5>
                                        </div>
                                        <div class="col col-md-4 montant">
                                            <p><span>15 000</span> <sup>FCFA</sup></p>
                                            <p>nbr mois <select></select></p>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-bordered" style="display: none;">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col"><?php echo $this->lang->line('produit_label'); ?></th>
                                        <th scope="col"><?php echo $this->lang->line('montant_label'); ?></th>
                                        <th scope="col"><?php echo "Durée"; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody class="tr-bim bim1">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="iard" class="tab-pane fade" style="padding-top: 1px;">
                            <center><?php echo "Liste des impayés IARD"; ?></center>
                            <div class="form-row">

                                <div class="scroll">
                                    <div class="row row-primes hide">
                                        <div class="col col-md-1 check">
                                            <input type="checkbox" />
                                        </div>
                                        <div class="col col-md-7 title">
                                            <h4>Accident corporel</h4>
                                            <h5>N° police</h5>
                                        </div>
                                        <div class="col col-md-4 montant">
                                            <p>15 000 <sup>FCFA</sup></p>
                                        </div>
                                    </div>
                                    <div class="row row-primes">
                                        <div class="col col-md-1 check">
                                            <input type="checkbox" />
                                        </div>
                                        <div class="col col-md-7 title">
                                            <h4>Accident corporel</h4>
                                            <h5>N° police</h5>
                                        </div>
                                        <div class="col col-md-4 montant">
                                            <p>15 000 <sup>FCFA</sup></p>
                                        </div>
                                    </div>
                                    <div class="row row-primes">
                                        <div class="col col-md-1 check">
                                            <input type="checkbox" />
                                        </div>
                                        <div class="col col-md-7 title">
                                            <h4>Accident corporel</h4>
                                            <h5>N° police</h5>
                                        </div>
                                        <div class="col col-md-4 montant">
                                            <p>15 000 <sup>FCFA</sup></p>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-bordered hide">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col"><?php echo $this->lang->line('produit_label'); ?></th>
                                        <th scope="col"><?php echo $this->lang->line('montant_label'); ?></th>
                                        <th scope="col"><?php echo "Durée"; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody class="tr-bim bim2">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="tab">
                    <h3><center><?php echo "Resumé règlement"; ?></center></h3>
                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nom</label>
                            <div class="col-sm-8 input-group">
                                <h4><b id="nom"></b></h4>
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <label
                                class="col-sm-6 control-label"><?php echo "Police(s) à régler" ?></label>
                            <div class="col-sm-8 input-group">
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group liste_primes" id="liste_primes_vie">
                            <div class="scroll">
                                <div class="row row-primes">
                                    <div class="col col-md-1 check">
                                    </div>
                                    <div class="col col-md-7 title">
                                        <h4>Accident corporel</h4>
                                        <h5>N° police</h5>
                                    </div>
                                    <div class="col col-md-4 montant">
                                        <p>15 000 <sup>FCFA</sup></p>
                                    </div>
                                </div>
                                <div class="row row-primes">
                                    <div class="col col-md-1 check">
                                    </div>
                                    <div class="col col-md-7 title">
                                        <h4>Accident corporel</h4>
                                        <h5>N° police</h5>
                                    </div>
                                    <div class="col col-md-4 montant">
                                        <p>15 000 <sup>FCFA</sup></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group liste_primes" id="liste_primes_iard">
                            <div class="scroll">
                                <div class="row row-primes">
                                    <div class="col col-md-1 check">
                                    </div>
                                    <div class="col col-md-7 title">
                                        <h4>Accident corporel</h4>
                                        <h5>N° police</h5>
                                    </div>
                                    <div class="col col-md-4 montant">
                                        <p>15 000 <sup>FCFA</sup></p>
                                    </div>
                                </div>
                                <div class="row row-primes">
                                    <div class="col col-md-1 check">
                                    </div>
                                    <div class="col col-md-7 title">
                                        <h4>Accident corporel</h4>
                                        <h5>N° police</h5>
                                    </div>
                                    <div class="col col-md-4 montant">
                                        <p>15 000 <sup>FCFA</sup></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-4">
                            <div class="form-row" style="border-top: 1px solid black; padding-top: 10px">
                                    <label class="col-sm-4 control-label" style="text-align: right;"><?php echo "Total"; ?></label>
                                    <div class="col-sm-8">
                                        <p style="border-bottom: 0; text-align: right"><span id="total">150 000</span> <sup>FCFA</sup></p>
                                    </div><!-- /.input group -->
                            </div>
                        </div><!-- /.input group -->
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-4">
                            <div class="form-row">
                                    <label class="col-sm-4 control-label" style="text-align: right;"><?php echo "Frais"; ?></label>
                                    <div class="col-sm-8">
                                        <p style="border-bottom: 0; text-align: right"><span id="frais">150 000</span> <sup>FCFA</sup></p>
                                    </div><!-- /.input group -->
                            </div>
                        </div><!-- /.input group -->
                    </div>
                    <div class="row" style="margin-bottom: 20px">
                        <div class="form-group">
                        <div class="col-sm-10 col-md-offset-2">
                            <div class="form-row" style="border-top: 1px solid black; padding-top: 10px">
                                    <label class="col-sm-6 control-label" style="text-align: right;"><?php echo "Montant à régler"; ?></label>
                                    <div class="col-sm-6">
                                        <p style="border-bottom: 0; padding-top: 10px; text-align: right"><span id="montant_total">150 000</span> <sup>FCFA</sup></p>
                                    </div><!-- /.input group -->
                            </div>
                        </div><!-- /.input group -->
                        </div>
                    </div>

                    <!-- /.Information de SYCAPAY -->
                    <input type="hidden" name="token" id="token">
                    <input type="hidden" name="code_int_concat" id="code_int_concat">
                    <input type="hidden" name="num_police_concat" id="num_police_concat">
                    <input type="hidden" name="label_concat" id="label_concat">
                    <input type="hidden" name="nombre_mois_concat" id="nombre_mois_concat">
                    <input type="hidden" name="num_quittance_concat" id="num_quittance_concat">
                    <input type="hidden" name="num_avenant_concat" id="num_avenant_concat">
                    <input type="hidden" name="mont" id="mont_concat">
                    <input type="hidden" name="type_concat" id="type_concat">
                    <input type="hidden" name="assure" id="assure">
                    <input type="hidden" name="amount" class="total">
                    <input type="hidden" name="currency" value="XOF">
                    <input type="hidden" name="telephone" value="<?php echo $this->session->userdata('mobile_assure') ?>">
                    <input type="hidden" name="name" value="<?php echo $this->session->userdata('nom_assure') ?>">
                    <input type="hidden" name="urlnotif" value="<?php echo site_url('Accueil/getRetours') ?>">
                    <input type="hidden" name="numcommande" id="commande">
                    <input type="hidden" name="merchandid" value="<?php echo $this->session->userdata('merchandid') ?>">
                    <!-- <input type="hidden" name="typpaie" value="payement"> -->
                </div>

                <div id="foot_login" class="hide">
                    <!-- <div style="overflow:auto; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
                        <div style="text-align:center;">
                            <a type="button" class="btn btn-danger" href="<?php echo site_url("Accueil/logout") ?>" >Me deconnecter</a>
                        </div>
                    </div> -->

                    <div style="text-align:center; background-color: rgba(255,255,255,0.68); padding-top: 10px; padding-bottom: 10px;">
                        <span class="" style="color: black; font-size: 12px">Copyright @ Syca SAS - 2021 </span>
                    </div>
                </div>
                <div id="foot_nologin" class="hide">
                    <div style="overflow:auto; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
                        <div style="text-align:center;">
                            <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous
                            </button>
                            <button type="button" class="myChargement btn btn-primary" id="nextBtn" onclick="nextPrev(1);">Next</button>
                        </div>
                    </div>

                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
                        <span class="step" style="background: #EA9617;"></span>
                        <span class="step" style="background: #EA9617;"></span>
                        <span class="step" style="background: #EA9617;"></span>
                    </div>
                </div>

            </div>
        </form>



<div id="myOperatorsCheckBox" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center"><b style="color: red;">CHOISIR UN MOYEN DE PAIEMENT</b></h4>
      </div>
      <form id="modePaiementFormulaire" action="" method="post" role="form">
      <input type="hidden" name="token" required="" id="tokenCard" />
      <input type="hidden" name="numpayeur" id="numpayeurCard" /> 
      <input type="hidden" name="amount" required="" id="amountCard" />
      <input type="hidden" name="commande" required="" id="commandeCard" />
      <input type="hidden" name="urls" value="<?php echo site_url('Accueil/getRetoursPaiements') ?>" />
      <input type="hidden" name="urlc" value="<?php echo site_url('Accueil/getRetoursPaiements') ?>" /> 
      <input type="hidden" name="telephone" value="<?php echo $this->session->userdata('mobile_assure'); ?>" />
      <input type="hidden" name="name" value="<?php echo $this->session->userdata('nom_assure'); ?>" /> 
      <input type="hidden" name="pname" value="<?php echo $this->session->userdata('prenoms_assure'); ?>" />
      <input type="hidden" name="emailpayeur" value="sales@sycapay.com" />
      <input type="hidden" name="merchandid" value="<?php echo $this->session->userdata('merchandid') ?>" required="" />
      <input type="hidden" name="currency" value="XOF"/>
      <input type="hidden" name="typpaie" value="payement"/> 

      <div class="modal-body">
        <div class="form-group"> 
            <label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="MO" checked></label> 
            <div class="col-sm-11"> 
            <img height="25" width="25" src="<?php echo img_url("MO.png") ?>"/> MOBILE MONEY
            </div> 
        </div> 
        <div class="form-group" style="margin-top: 50px;"> 
            <label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="VI"></label> 
            <div class="col-sm-11"> 
            <img height="25" width="25" src="<?php echo img_url("VI.png") ?>"/> VISA
            </div> 
        </div> 
        <div class="form-group" style="margin-top: 100px;"> 
            <label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="MC"></label> 
            <div class="col-sm-11"> 
            <img height="25" width="25" src="<?php echo img_url("MC.png") ?>"/> MASTERCARD
            </div> 
        </div> 
      </div>
      <div class="modal-footer" style="text-align: center; margin-top: 25px;">
        <a class="btn btn-default" data-dismiss="modal" class="form-control" >Annuler</a>
        <button id="confirmerModePaiement" type="button" class="btn btn-primary">Valider</button>
      </div>
     </form>
    </div>
  </div>
</div>

<div id="getDivOffPassWord" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title text-center"><b style="color: #e67902;">MOT DE PASSE OUBLIE ?</b></h4>
      </div>
      <form action="<?php echo site_url('Accueil/sendPassword');?>" method="post" role="form" autocomplete="off" onSubmit="return confirm('Voulez Vous Faire Cette Réinitialisation ?')">
      <div class="modal-body"> 

        <div class="form-group row">
          <label for="text" class="col-sm-4 control-label">Pays</label> 
          <div class="col-sm-8"> 
          <select class="selectpicker select2 form-control" id="myContryLieu" required="">
            <option value="CI" selected="" data-indicatif="225"
                    data-content="<img class='' width='25' src='<?php echo img_url("flags/ci.png") ?>' />   Côte d'Ivoire">
                Côte d'Ivoire
            </option>
            <option value="TG" data-indicatif="228"
                    data-content="<img class='' width='25' src='<?php echo img_url("flags/tg.png") ?>' />   Togo">
                Togo
            </option>
        </select>
          </div> 
        </div> 

        <div class="form-group row">
            <label for="text" class="col-sm-4 control-label">Mobile</label> 
            <div class="col-sm-8"> 
            <input onpaste="return false" type="text" class="form-control" name="telephoneAssure" value="225" required="" maxlength="13" autocomplete="off" id="telephoneAssure" />
            </div> 
        </div>  
        <div class="form-group row" style="text-align: center;">
            <b style="color: #e67902;">NB: Vous allez recevoir votre nouveau mot de passe par SMS !</b>
         </div>

      </div>
      <div class="modal-footer" style="text-align: center; margin-top: 25px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Valider</button>
      </div>
     </form>
    </div>
  </div>
</div>

</div>


<?php $this->load->view('tpl/footer') ?>
</div>
<div class="md-overlay"></div>
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
    function processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, OTP, mobile, token, code_int_concat, num_police_concat, label_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider){
        var alerte = $.confirm({
            boxWidth: '800px',
            boxHeight: '800px',
            title: "Paiement en cours",
            content: 'Veuillez patienter votre paiement est en cours...',
            buttons: {
                logoutUser: false,
                cancel: false,
                confirm: false
            }
        });
        //$("#foot_nologin").addClass("hide");
        var provider = "Mobile Money";
        $.ajax({
            url: "<?php echo site_url('Accueil/paiement')?>",
            type: "POST",
            data: {num_avenant_concat: num_avenant_concat, num_quittance_concat: num_quittance_concat, nombre_mois_concat: nombre_mois_concat, OTP: OTP, mobile: mobile, token: token, code_int_concat: code_int_concat, num_police_concat: num_police_concat, label_concat: label_concat, commande: commande, ent_id: ent_id, pays_id: pays_id,montant: mont_concat, assure: assure, type_concat: type_concat, provider: provider},
            success: function (datas) {
                try {
                    alerte.close();
                } catch (e) {
                    console.log(e);
                }
                console.log(datas);
                if (datas !== "")
                {
                    datas = JSON.parse(datas);
                    if (datas.code == 0) {
                        //$("#foot_nologin").removeClass("hide");
                        var code = datas.transactionId;
                        //console.log(datas);
                        //$.alert(datas.message);
                        $.confirm({
                            boxWidth: '800px',
                            boxHeight: '800px',
                            title: datas.message,
                            content: 'Votre paiement sera annulé dans 2 minutes ou cliquez sur attendre.',
                            autoClose: 'logoutUser|120000',
                            buttons: {
                                logoutUser: {
                                    text: 'Annuler le paiement !',
                                    btnClass: 'btn-danger',
                                    action: function () {

                                        document.location.href = '<?php echo site_url('Accueil/getAnnulations') . '/'?>' + commande;
                                    }
                                },
                                cancel:
                                    {
                                        text: 'Attendre le paiement !',
                                        btnClass: 'btn-success',
                                        function() {
                                            $.alert('Prière confirmer le paiement sur votre téléphone SVP !');
                                        },
                                    }
                            }
                        });
                        var setIntervalId = setInterval(function () {
                            $.ajax({
                                url: "<?php echo site_url('Accueil/getStatus')?>",
                                type: "POST",
                                data: {code: code},
                                success: function (rep) {
                                    var code = datas.code;
                                    var json = JSON.parse(rep);
                                    if (json == null) {
                                        console.log("Erreur diverse");
                                    } else if (json.statut) {
                                        document.location.href = '<?php echo site_url('Accueil/getFatures') . '/'?>' + json.data;
                                    } else if (!json.statut && json.code === -200) {
                                        //$.alert('Veuillez patientez SVP !');
                                        //return false;
                                        var commande = $("#commande").val();
                                        console.log(commande);
                                        //$.confirm({
                                        //   boxWidth: '800px',
                                        //   boxHeight: '800px',
                                        //   title: datas.message,
                                        //    content: 'Votre paiement sera annulé dans 10 secondes ou cliquez sur attendre.',
                                        //    autoClose: 'logoutUser|10000',
                                        //    buttons: {
                                        //        logoutUser: {
                                        //            text: 'Annuler le paiement !',
                                        //            btnClass: 'btn-danger',
                                        //            action: function () {
                                        //
                                        //                document.location.href = '<?php //echo site_url('Accueil/getAnnulations').'/'?>//'+commande;
                                        //            }
                                        //        },
                                        //        cancel:
                                        //        {
                                        //            text: 'Attendre le paiement !',
                                        //            btnClass: 'btn-success',
                                        //            function () {
                                        //                $.alert('Prière confirmer le paiement sur votre téléphone SVP !');
                                        //            },
                                        //        }
                                        //    }
                                        //});
                                    } else{
                                        console.log(json);
                                    }
                                }
                            });
                        }, 10000);

                    } else {
                        $.alert(datas.message);
                        currentTab = 2;
                        showTab(currentTab);
                    }
                }
                else
                {
                    $.alert('Paiement impossible - Reprendre SVP !');
                    currentTab = 2;
                    showTab(currentTab);
                }
            }
        });
    }

$(document).on('click', "#confirmerModePaiement", function() {
    // $('#myOperatorsCheckBox').hide();
    $('#myOperatorsCheckBox').modal('hide');
    var modePaiementChoose = $("input[name='optradioVilain']:checked").val();

    var commande = $("#commande").val();
    var ent_id = $("#ent_id").val();
    var pays_id = $("#pays_id").val();
    var assure = $("#assure").val();
    var token = $("#token").val();
    var type_concat = $("#type_concat").val();
    var code_int_concat = $("#code_int_concat").val();
    var num_police_concat = $("#num_police_concat").val();
    var label_concat = $("#label_concat").val();
    var nombre_mois_concat = $("#nombre_mois_concat").val();
    var num_avenant_concat = $("#num_avenant_concat").val();
    var num_quittance_concat = $("#num_quittance_concat").val();
    var total = $("#montant_total").text();
    var mont_concat = $("#mont_concat").text();

    if(modePaiementChoose == 'MO') 
    {
        var firstTime = true;
        var confirm_box = $.confirm({
            theme: 'material',
            boxWidth: '1500px',
            title: 'Numéro de Téléphone!',
            content: '' +
            '<form action="" class="formName" autocomplete="off">' +
            '<div class="form-group">' +
            '<label>Montant à payer</label>' +
            '<input type="text" placeholder="Montant" value="'+total+'" class="form-control" /></div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label>Saisir Numéro de Paiement</label>' +
            '<input onpaste="return false" type="text" placeholder="Téléphone" class="name form-control" required id="target" autocomplete="off" maxlength="10" value="<?php echo substr($this->session->userdata("mobile_assure"), 3, strlen($this->session->userdata("mobile_assure"))); ?>"/>' +
            '</div>' +
            '<div class="form-group" id="getOTP" style="display:none;">' +
            '<label><b>Prière générer un code temporaire au #144*8*2#.</b></label>' +
            '<input onpaste="return false" type="text" placeholder="Saisir OTP ICI" class="OTP form-control" autocomplete="off" maxlength="4"/>' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Valider',
                    btnClass: 'btn-blue',
                    action: function () {
                        //console.log(this.text);
                        var otpShowed = false;
                        var OTP = this.$content.find('.OTP').val();
                        var mobile = this.$content.find('.name').val();
                        if(!mobile)
                        {
                            $.alert('Veuillez renseigner le numéro de téléphone !');
                            return false;
                        }
                        else if (mobile.length != 8 && mobile.length != 10)
                        {
                            $.alert('Le numéro de téléphone doit être 08 ou 10 chiffres !');
                            return false;
                        }
                        else
                        {
                            if (firstTime) {
                                //Verifier le réseau et afficher l'OTP si orange la première fois
                                $($(".jconfirm-buttons")[0]).prepend("<div id=\"buttonload\" style=\"text-align: center; display: none;\">\n" +
                                    "            <i class=\"fa fa-refresh fa-spin\"></i> Chargement ...\n" +
                                    "        </div>");
                                $("#buttonload").show();
                                $($(".jconfirm-buttons")[0]).find("button.btn-blue").hide();

                                var pays_id = "<?php echo $this->session->userdata("pays_id") ?>";

                                // if ()

                                $.post("<?php echo site_url("Accueil/getCheckOperateurs") ?>", {mobile: mobile})
                                    .done(function (data) {
                                        console.log(data);
                                        if (data != "") {
                                            firstTime = false;
                                            $("#buttonload").hide();
                                            if (data === "OK") {
                                                otpShowed = true;
                                                $($(".jconfirm-buttons")[0]).find("#buttonload").hide();
                                                $($(".jconfirm-buttons")[0]).find("button.btn-blue").show();

                                                $('#getOTP').show();
                                                $('.retourOperateurs').show();
                                                return false;
                                            } else if (data === "NO") {
                                                $('#getOTP').hide();
                                                $('.retourOperateurs').show();
                                                otpShowed = false;
                                                confirm_box.close();
                                                processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, null, mobile, token, code_int_concat, num_police_concat, label_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider);
                                            }

                                        } else {
                                            $.alert("Impossible de détecter l'opérateur !");
                                            $($(".jconfirm-buttons")[0]).find("#buttonload").hide();
                                            $($(".jconfirm-buttons")[0]).find("button.btn-blue").show();
                                            return false;
                                        }
                                    })
                                    .fail(function (error) {
                                        console.log(error);
                                    });
                                return false;
                            } else {
                                // Proceder au paiement si ce n'est pas la première fois dans le cas d'orange
                                if (OTP.length == 0) {
                                    $.alert('Le code d\'autorisation doit être saisi !');
                                    return false;
                                } else {
                                    processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, OTP, mobile, token, code_int_concat, num_police_concat, label_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider);
                                }
                            }
                        }
                    }
                },
                cancel: 
                {
                text: 'Retour',
                btnClass: 'btn-default',
                function () {
                    $.alert('Prière Reprendre SVP !!');
                },
              }
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });

    } 
    else 
    {
        var alerte = $.confirm({
            boxWidth: '800px',
            boxHeight: '800px',
            title: "Paiement en cours",
            content: 'Veuillez patienter votre paiement est en cours...',
            buttons: {
                logoutUser: false,
                cancel: false,
                confirm: false
            }
        });
        //$("#foot_nologin").addClass("hide");
        var provider = "";
        if(modePaiementChoose == 'VI')
        {
            provider = "VISA";
        }
        else
        {
            provider = "MASTERCARD";
        }
        $.ajax({
            url: "<?php echo site_url('Accueil/paiement_cb')?>",
            type: "POST",
            data: {num_avenant_concat: num_avenant_concat, num_quittance_concat: num_quittance_concat, nombre_mois_concat: nombre_mois_concat, token: token, code_int_concat: code_int_concat, num_police_concat: num_police_concat, label_concat: label_concat, commande: commande, ent_id: ent_id, pays_id: pays_id,montant: mont_concat, assure: assure, type_concat: type_concat, provider: provider},
            success: function (datas) {
                try {
                    alerte.close();
                } catch (e) {
                    console.log(e);
                }

                if (datas !== "")
                {

                    if(modePaiementChoose == 'VI')
                    {
                        // $("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.com/checkvisadir');
                        $("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.net/checkvisadir.php');
                    }
                    else
                    {
                        // $("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.com/checkmasterdir');
                        $("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.net/checkvisadir.php');
                    }

                    $( "#modePaiementFormulaire" ).submit();

                }
                else
                {
                    $.alert('Paiement impossible - Reprendre SVP !');
                    currentTab = 2;
                    showTab(currentTab);
                }
            }
        });
    } 
});

$(document).on('click', ".myChargement", function() {
$('.buttonload').show();
setTimeout(function () {
$('.buttonload').hide();
document.getElementById("regForm").style.display = "block";
}, 5000);
});

$(document).on('click', ".myLongChargement", function() {
$('.buttonload').show();
setTimeout(function () {
    $('#block_button').hide();
$('.buttonload').hide();
document.getElementById("regForm").style.display = "block";
}, 20000);
});

$(document).on('change', "#myContryLieu", function() {
    var that = this;
    var myContryLieu = $(this).val();
    console.log(myContryLieu);
    if(myContryLieu !== 'CI') 
    {   
        $('#telephoneAssure').attr('value','228');
        $('#telephoneAssure').attr('maxlength','11');
    } 
    else 
    {   
        $('#telephoneAssure').attr('value','225');
        $('#telephoneAssure').attr('maxlength','13');
    } 
});

$('#sendNewPassword').confirm({
    theme: 'material',
    title: 'CONFIRMATION !',
    content: 'Voulez vous faire cette action ?',
    buttons: {
        confirm: {
        text: 'Valider',
        btnClass: 'btn-blue',
        action: function () {
            $("#sendingPassword").submit();
          }
        },
        cancel: {
        text: 'Retour',
        btnClass: 'btn-default',
        function () {
            document.location.href = '<?php echo site_url('Accueil');?>';
          }
      }
    }
});


/*$(document).on("keypress", "#target", function () {
//function getCheckOperateurs() {
    $('#getOTP').hide();
    $('.retourOperateurs').hide();
    var mobile = $('#target').val();
    if (mobile.length === 8 || mobile.length === 10)
    {

        $.post("<?php echo site_url("Accueil/getCheckOperateurs") ?>", {mobile : mobile})
            .done(function (data) {
                console.log(data);
                if(data === "OK")
                {
                    $('#getOTP').show();
                    $('.retourOperateurs').show();
                }
                else if(data === "NO")
                {
                   $('#getOTP').hide();
                   $('.retourOperateurs').show();
                }
                else
                {
                   $.alert("Impossible de détecter l'opérateur !");
                   return false;
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    }
});*/

jQuery(document).ready(function () {

    var table;
    $(document).ready(function() {
        //datatables
        table = $('#example').DataTable({
            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": false, //Feature control DataTables' server-side processing mode.
            "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [
                { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
                { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
            ],
            "columnDefs": [ { orderable: false, targets: [0] } ],
            "language" : {
                "sEmptyTable":     "<?php echo $this->lang->line('sEmptyTable'); ?>",
                "sInfo":           "<?php echo $this->lang->line('sInfo'); ?>",
                "sInfoEmpty":      "<?php echo $this->lang->line('sInfoEmpty'); ?>",
                "sInfoFiltered":   "<?php echo $this->lang->line('sInfoFiltered'); ?>",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "<?php echo $this->lang->line('sLengthMenu'); ?>",
                "sLoadingRecords": "<?php echo $this->lang->line('sLoadingRecords'); ?>",
                "sProcessing":     "<?php echo $this->lang->line('sProcessing'); ?>",
                "sSearch":         "<?php echo $this->lang->line('sSearch'); ?>",
                "sZeroRecords":    "<?php echo $this->lang->line('sZeroRecords'); ?>",
                "oPaginate": {
                    "sFirst":    "<?php echo $this->lang->line('sFirst'); ?>",
                    "sLast":     "<?php echo $this->lang->line('sLast'); ?>",
                    "sNext":     "<?php echo $this->lang->line('sNext'); ?>",
                    "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>"
                },
                "oAria": {
                    "sSortAscending":  "<?php echo $this->lang->line('sSortAscending'); ?>",
                    "sSortDescending": "<?php echo $this->lang->line('sSortDescending'); ?>"
                },
                "select": {
                    "rows": {
                        "_": "<?php echo $this->lang->line('_'); ?>",
                        "0": "<?php echo $this->lang->line('0'); ?>",
                        "1": "<?php echo $this->lang->line('1'); ?>"
                    }
                }
            }
        });

    });


    window.ParsleyValidator.setLocale('fr');
        $('#old_mdp').parsley();
        $('#passwd').parsley();
        $('#confirm-passwd').parsley();

        <?php if ($this->session->userdata("prenoms_assure")) : ?>
            $("#foot_login").removeClass("hide");
            $("#foot_nologin").addClass("hide");
        <?php else : ?>
            $("#foot_login").addClass("hide");
            $("#foot_nologin").removeClass("hide");
        <?php endif; ?>


        $('#btn_update_pass').click(function (e) {
            e.preventDefault();

            var old_mdp = $('#old_mdp');
            var passwd = $('#passwd');
            var confirm_passwd = $('#confirm-passwd');

            if (passwd.val() != confirm_passwd.val()){
                confirm_passwd.addClass("invalid");
                alert("Les Mots de passe ne sont pas conformes")
            } else {

                //var montant = parseInt(total);


                $.ajax({
                    url: "<?php echo site_url('Accueil/update_pass')?>",
                    type: "POST",
                    data: {oldpassword: old_mdp.val(), password: passwd.val()},
                    success: function (datas) {
                        data = JSON.parse(datas);
                        if (data.statut) {
                            $.alert(data.msg);
                            location.href = "<?php echo site_url('Accueil/')?>";
                        }
                        else 
                        {
                            $.alert(data.msg);
                            location.href = "<?php echo site_url('Accueil/')?>";
                        }
                    }
                });
            }
        });


        $('.carousel').carousel({interval: 20000});

        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            };
        }(jQuery));

        $(document).on('keydown', function (e) {
            if (e.keyCode == 13 && $('#mobile').is(":focus") && $('#mobile').val().length < 4) {
                e.preventDefault();
            }
        });

        $(document).on('change', "#pays_id", function (e) {
            $("#indicatif").text("+" + $(this).find("option:selected").attr("data-indicatif"));

        });

//$('.myRadio').click(function() {
        var pinId ;
        $("#mobile").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        });

        $(document).on("change", ".optradio", function () {

            var prime = $(this).attr("data-label");
            var montant = $(this).val();
            var code = $(this).attr("data-id");

            var all = $(".optradio:checked");
            console.log(all);


            var type = $(this).attr("data-type");

            var change = false;

            $("#liste_primes_vie").find("div.scroll").empty();
            $("#liste_primes_iard").find("div.scroll").empty();
            $("#liste_primes_iard").fadeIn();
            $("#liste_primes_vie").fadeIn();

            $.each(all, function (index, value) {
                var type_new = $(this).attr("data-type");

                if (type != type_new){
                    remove_all(type);
                }
                console.log(value);

            });

            all = $(".optradio:checked");

            $.each(all, function (index, value) {
                var type_new = $(this).attr("data-type");
                var prime = $(this).attr("data-label");
                var police = $(this).attr("data-num-police");
                var montant = $(this).val();

                var code = $(this).attr("data-id");
                var assure = $(this).attr("data-assure");
                var codeInterv = $(this).attr("data-code-inter");

                if (type_new == "VIE"){

                    var nb = $($(this).closest("div.row").find("input[type='number']")[0]).val();
                    if (nb == '0') 
                    {
                       var nb = 1;     
                    }

                    $($("#liste_primes_vie").find("div.scroll")[0]).append("" +
                        '<div id='+police+' class="row row-primes" style="margin-left: 20px; height: auto important!; width: auto important!;">' +
                        '<div class="col col-md-5 title">' +
                        '   <h4>' + prime + '</h4>' +
                        '   <h5>' + code + ' - ' + codeInterv + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-3 title">' +
                        '   <h5> '+ nb +' x ' + Math.floor(parseInt(montant) / parseInt(nb)) + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-4 montant">' +
                        '   <p>' + numStr(montant) + ' <sup>FCFA</sup></p>' +
                        '</div>' +
                        '</div><hr>' +
                        ""
                    );

                    $("#liste_primes_iard").hide();
                }

                if (type_new == "IARD"){

                    var nb = 1;  

                    $($("#liste_primes_iard").find("div.scroll")[0]).append("" +
                        '<div class="row row-primes" style="margin-left: 20px; height: auto important!; width: auto important!;">' +
                        '<div class="col col-md-5 title">' +
                        '   <h4>' + prime + '</h4>' +
                        '   <h5>' + code + ' - ' + codeInterv + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-3 title">' +
                        '   <h5> '+ nb +' x ' + Math.floor(parseInt(montant) / parseInt(nb)) + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-4 montant">' +
                        '   <p>' + numStr(montant) + ' <sup>FCFA</sup></p>' +
                        '</div>' +
                        '</div><hr>' +
                        ""
                    );

                    $("#liste_primes_vie").hide();
                }

                $("#nom").text(assure);
            });

            var pays_id = $("#pays_id").val();

        });

        function remove_all (type){
            if (type == "VIE"){
                $("#iard").find('.optradio:checked').each(function(i){
                    $(this).prop('checked', false);
                });
            }
            if (type == "IARD"){
                $("#vie").find('.optradio:checked').each(function(i){
                    $(this).prop('checked', false);
                });
            }
        }


        $(document).on("change", ".slider", function () {

            var nb_mois = parseInt($(this).val());
            var montant = parseInt($(this).attr("data-montant"));
            var prime = $($(this).closest("div.row").find("input[type=checkbox]")[0]).attr("data-label");
            var police = $($(this).closest("div.row").find("input[type=checkbox]")[0]).attr("data-num-police");
            var code = $($(this).closest("div.row").find("input[type=checkbox]")[0]).attr("data-id");
            var codeInterv = $($(this).closest("div.row").find("input[type=checkbox]")[0]).attr("data-code-inter");


            var mt_total = nb_mois * montant;


            $($(this).parent().parent().find(".mt_tot")[0]).text(numStr(mt_total));

            $($(this).closest("div.row").find("input[type=checkbox]")[0]).val(mt_total);

            $("#"+police).html(
                '<div class="col col-md-5 title">' +
                '   <h4>' + prime + '</h4>' +
                '   <h5>' + code + ' - ' + codeInterv + '</h5>' +
                '</div>' +
                '<div class="col col-md-3 title">' +
                '   <h5> '+ nb_mois +' x ' + montant + '</h5>' +
                '</div>' +
                '<div class="col col-md-4 montant">' +
                '   <p>' + numStr(Math.floor(parseInt(montant)) * parseInt(nb_mois)) + ' <sup>FCFA</sup></p>' +
                '</div>'
            );
        });


        $(document).on("click", "#resendOtp", function () {

            var pinId = $("#pinId").val();

            $.ajax({
                url: "<?php echo site_url('Accueil/resendOtp')?>",
                type: "POST",
                data: {pinId: pinId},
                success: function (datas) {

                    data = JSON.parse(datas);
                    if (data != null && data.status == true) {
                        //nextPrev(1);
                        $("#mob").parent().text("L'OTP a bien été renvoyé au numero suivant : +225 " + mobile);
                        //currentTab = 0;
                        //$("#pinId").val(data);
                        //pinId = data;


                    }
                    else {
                        //currentTab = 0;
                        //nextPrev(-1);
                        //currentTab=0;
                        //showTab(currentTab);
                        $.alert("OTP non envoyé veuillez tenter un nouvel envoi");
                    }
                }
            });


        });


    <?php
        if ($this->session->flashdata("success")) {
            echo "toast_success('" . $this->session->flashdata("success") . "');";
        }
        ?>
        <?php
        if ($this->session->flashdata("error")) {
            echo "toast_error('" . $this->session->flashdata("error") . "');";
        }
        ?>
        <?php
        if ($this->session->flashdata("token")) {
            echo "toast_success('" . $this->session->flashdata("token") . "');";
        }
        ?>
    });

    // Current tab is set to be the first tab (0)
    var currentTab = 0;
    // Display the current tab
    showTab(currentTab);


    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        //if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        //x[currentTab].style.display = "none";

        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        //$(".tab").hide();

        // if you have reached the end of the form...

        //Afficher/masquer le bas prev/next

        if (currentTab == 0 && n == -1){
            $("#foot_login").removeClass("hide");
            $("#foot_nologin").addClass("hide");
        } else {
            $("#foot_login").addClass("hide");
            $("#foot_nologin").removeClass("hide");
        }

        if (currentTab >= x.length) {
            // ... the form gets submitted:
            //document.getElementById("regForm").submit();
            //return false;
            $.confirm({
                theme: 'material',
                title: 'CONFIRMATION !',
                content: 'Voulez vous faire cette action ?',
                buttons: {
                    confirm: {
                        text: '<?php echo $this->lang->line('confirm_modal_valider_btn'); ?>',
                        btnClass: 'btn-blue',
                        action: function () {

                            //$('#myOperatorsCheckBox').show();

                            var commande = $("#commande").val();
                            var ent_id = $("#ent_id").val();
                            var pays_id = $("#pays_id").val();
                            var assure = $("#assure").val();
                            var token = $("#token").val();
                            var type_concat = $("#type_concat").val();
                            var code_int_concat = $("#code_int_concat").val();
                            var num_police_concat = $("#num_police_concat").val();
                            var label_concat = $("#label_concat").val();
                            var nombre_mois_concat = $("#nombre_mois_concat").val();
                            var num_avenant_concat = $("#num_avenant_concat").val();
                            var num_quittance_concat = $("#num_quittance_concat").val();
                            var total = $("#montant_total").text();
                            var mont_concat = $("#mont_concat").text();
                            //var montant = parseInt(total);

                            console.log(total);


                              $("#tokenCard").val(token);
                              $("#telephoneCard").val('+22555835666');
                              $("#nameCard").val(assure);
                              $("#pnameCard").val(assure);
                              $("#emailpayeurCard").val('sales@sycapay.com');
                              $("#numpayeurCard").val(commande);
                              $("#amountCard").val(total);
                              $("#commandeCard").val(commande);


                              $('#myOperatorsCheckBox').modal('show');

                        }
                    },
                    cancel: {
                        text: '<?php echo $this->lang->line('confirm_modal_annuler_btn'); ?>',
                        btnClass: 'btn-default',
                        action: function () {
                            //document.location.reload(true);
                            //return false;
                        }
                    }
                }
            });

        }


        //Login de l'utilisateur


        <?php if ($this->session->userdata("prenoms_assure")) : ?>

        else if (currentTab == 1 && n !== -1) {
                document.getElementById("foot_nologin").style.display = "none";
                //$('#loaderDIV').show();
                $.get("<?php echo site_url("Accueil/getPrimes") ?>")
                    .done(function (data) {
                        $('.buttonload').hide();
                        data = JSON.parse(data);
                        if (data != null && data.statut == true) {
                            $('#foot_nologin').show();

                            $("#mob").text("+ <?php echo $this->session->userdata("mobile_assure") ?>");

                            $("div#vie").find("div.form-row").find("div.scroll").empty();
                            $("div#iard").find("div.form-row").find("div.scroll").empty();
                            $(".tr-bim.bim1").empty();
                            $(".tr-bim.bim2").empty();
                            //console.log(data.vie[0]);
                             $.each(data.vie, function (index, value) {
                                 console.log(value);
                                 var periode = "";

                                 switch (value.CODEPERI) {
                                    case "A":
                                        periode = "Année(s)";
                                        break;
                                    case "S":
                                        periode = "Semestre(s)";
                                        break;
                                    case "T":
                                        periode = "Trimestre(s)";
                                        break;
                                    case "M":
                                        periode = "Mois";
                                        break;
                                 }

                                $(".tr-bim.bim1").append('<tr><td>' + value.NUMEPOLI + '</td><td>' + value.LIBECATE + ' (VIE)</td><td>' + value.MONTGARA + '</td><td><input class="slider" type="range" min="1" max="10" value="1" data-montant="' + value.MONTGARA + '">(<span class="nb_mois">10 mois</span>)</td><td><input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU+ '" data-label="' + value.LIBECATE + '" data-type="VIE" value="' + value.MONTGARA + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.NUMEPOLI + '""></a></td></tr>');
                                $("div#vie").find("div.form-row").find("div.scroll").append('' +
                                    '<div class="row row-primes" style="height:auto !important; width: auto !important;">' +
                                    '<div class="col col-md-1 col-sm-2 col-xs-2 check">' +
                                    '<input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU+ '" data-label="' + value.LIBECATE + '" data-type="VIE" value="' + value.montant_tot + '" data-code-int="' + value.CODEINTE + '" data-num-police="' + value.NUMEPOLI + '" name="optradio" class="optradio" data-nombre-mois="' + value.NB_COTISATION + '" data-code-inter="' + value.CODEINTE + '">' +
                                    '</div>' +
                                    '<div class="col col-md-7 col-sm-10 col-xs-10 title">' +
                                    '<h4>' + value.LIBECATE + '</h4>' +
                                    '<span>(Souscription : <b>' + numStr(value.MONTGARA) + '</b> FCFA)</span>' +
                                    '<h5 style="color: black; text-decoration: none">Période impayée : <b>' + value.NB_P_IMPAYE + '</b> '+ periode +'</h5>' +
                                    '<h5>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</h5>' +
                                    '</div>' +
                                    '<div class="col col-md-4 col-sm-12 col-xs-12 montant">' +
                                    '<p style="text-align: right"><span class="mt_tot">' + value.montant_tot + '</span> <sup>FCFA</sup></p>' +
                                    '<p style="text-align: right">Période à payer <input type="number" style="text-align: right" value="' + value.NB_P_IMPAYE + '" min="1" class="slider getNombreMois" data-montant="' + value.MONTGARA + '" data-num-quittance="" data-num-avenant=""/> ' + periode + '</p>' +
                                    '</div>' +
                                    '</div>' +
                                    ''
                                );
                                //Sor@y@200401
                                $("#nom").text(data.NOM_ASSU);


                            });

                            $.each(data.iard, function (index, value) {
                                $(".tr-bim.bim2").append('<tr><td>' + value.NUMEPOLI + '</td><td>' + value.LIBTYPAV + ' (IARD)</td><td>' + value.PRIMTOTA + '</td><td><input class="slider" type="range" min="1" max="10" value="1" data-montant="' + value.PRIMTOTA + '">(<span class="nb_mois">10 mois</span>)</td><td><input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU+ '" data-label="' + value.LIBTYPAV + '" data-type="IARD" value="' + value.PRIMTOTA + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.NUMEPOLI + '""></a></td></tr>');
                                $("div#iard").find("div.form-row").find("div.scroll").append('' +
                                    '<div class="row row-primes" style="height:auto !important; width: auto !important;">' +
                                    '<div class="col col-md-1 col-sm-2 col-xs-2 check">' +
                                    '<input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU+ '" data-label="' + value.REFERISQ + '" data-type="IARD" value="' + value.PRIMTOTA + '" data-code-int="' + value.CODEINTE + '" data-num-police="' + value.NUMEPOLI + '" name="optradio" class="optradio" data-nombre-mois="' + value.RELIQUAT + '" data-code-inter="' + value.CODEINTE + '" data-num-quittance="' + value.NUMEQUIT + '" data-num-avenant="' + value.NUMEAVEN + '">' +
                                    '</div>' +
                                    '<div class="col col-md-7 col-sm-10 col-xs-10 title">' +
                                    '<h4>' + value.LIBTYPAV + '</h4>' +
                                    '<h5 style="text-decoration:none !important;color:black !important;"><b>'+ value.REFERISQ +'</h5>' +
                                    '<h5>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</h5>' +
                                    '</div>' +
                                    '<div class="col col-md-4 col-sm-12 col-xs-12 montant">' +
                                    '<h4><span class="mt_tot">' + numStr(value.PRIMTOTA) + '</span> <sup>FCFA</sup></h4>' +
                                    // '<p><span class="mt_tot">' + value.PRIMTOTA + '</span> <sup>FCFA</sup></p>' +
                                    '<h5>' + value.DATEEFFE + ' - ' + value.DATEECHE + '</h5>' +
                                    '</div>' +
                                    '</div>' +
                                    ''
                                );

                                $("#nom").text(data.NOM_ASSU);

                            });

                            $(".slider").change();
                            showTab(currentTab);

                        }
                        else 
                        {
                            //currentTab = 0;
                            //nextPrev(-1);
                            currentTab = 0;
                            showTab(currentTab);
                            $.alert("Aucune prime retrouvée");
                        }
                    })
                    .fail(function (error) {
                        currentTab = 0;
                        //nextPrev(-1);
                        $('.buttonload').hide();

                        showTab(currentTab);
                        $.alert("Aucune prime retrouvée !");
                    });

        }

        <?php else: ?>

        else if (currentTab == 1 && n !== -1) {
            var mobile = $('#mobile');
            var password = $('#password');
            var indicatif = $("#indicatif").text().replace("+", "");
            $("#mob").text("+" + indicatif + " " + mobile.val());
            if (mobile.val() == ""){
                currentTab=0;
                showTab(currentTab);
                mobile.addClass("invalid");
                alert("Le mobile n'a pas été renseigné !")
            } else if (password.val() == ""){
                currentTab=0;
                showTab(currentTab);
                password.addClass("invalid");
                alert("Le mot de passe n'a pas été renseigné !")
            //} else if (mobile.val().length < 11 || mobile.val().length > 13){
            } else if (indicatif == "228" && mobile.val().length != 8){
                currentTab=0;
                showTab(currentTab);
                mobile.addClass("invalid");
                alert("Le mobile doit être de 8 chiffres !")
            } else if (indicatif == "225" && mobile.val().length != 10){
                currentTab=0;
                showTab(currentTab);
                mobile.addClass("invalid");
                alert("Le mobile doit être de 10 chiffres !")
            } else {
                document.getElementById("nextBtn").style.display = "none";
                $.post("<?php echo site_url("Accueil/login") ?>", {mobile: indicatif + mobile.val(), password: password.val()})
                    .done(function (data) {
                        data = JSON.parse(data);
                        if (data != null && data.statut == true) {

                            location.reload();

                        }
                        else {
                            //currentTab = 0;
                            //nextPrev(-1);
                            currentTab = 0;
                            showTab(currentTab);
                            //$.alert("Mobile / Mot de passe incorrect");
                            //return false;
                            location.reload();
                        }
                    })
                    .fail(function (error) {
                        currentTab = 0;
                        //nextPrev(-1);

                        showTab(currentTab);
                        $.alert("Le code OTP n'a pas pu être envoyé, veuillez réessayer !");
                        //return false;
                        location.reload();
                    });
            }
        }

        <?php endif; ?>

        else if(currentTab == 5 && n === 1){
            var otp = $("#otp").val();
            if (otp == ""){
                currentTab=2;
                $("#otp").addClass("invalid");
                alert("L'otp n'a pas été renseigné")
            } else {

                var pinId = $("#pinId").val();
                mobile = $('#mobile').val();

                $.ajax({
                    url: "<?php echo site_url('Accueil/ajax_confirmation')?>",
                    type: "POST",
                    data: {code: otp, pinId: pinId, mobile: mobile},
                    success: function (datas) {

                        if (datas !== "") {
                            datas = JSON.parse(datas);


                            $("div#vie").find("div.form-row").find("div.scroll").empty();
                            $("div#iard").find("div.form-row").find("div.scroll").empty();
                            $(".tr-bim.bim1").empty();
                            $(".tr-bim.bim2").empty();
                            $.each(datas.vie, function (index, value) {
                                $(".tr-bim.bim1").append('<tr><td>' + value.code + '</td><td>' + value.prime + ' (' + value.type + ')</td><td>' + value.montant + '</td><td><input class="slider" type="range" min="1" max="10" value="1" data-montant="' + value.montant + '">(<span class="nb_mois">10 mois</span>)</td><td><input type="checkbox" data-id="' + value.code + '" data-assure="' + value.assure+ '" data-label="' + value.prime + '" data-type="' + value.type + '" value="' + value.montant + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.code + '""></a></td></tr>');
                                $("div#vie").find("div.form-row").find("div.scroll").append('' +
                                    '<div class="row row-primes">' +
                                    '<div class="col col-md-1 col-sm-2 col-xs-2 check">' +
                                    '<input type="checkbox" data-id="' + value.code + '" data-assure="' + value.assure+ '" data-label="' + value.prime + '" data-type="' + value.type + '" value="' + value.montant + '" name="optradio" class="optradio">' +
                                    '</div>' +
                                    '<div class="col col-md-7 col-sm-10 col-xs-10 title">' +
                                    '<h4>' + value.prime + '</h4>' +
                                    '<span>(Souscription : <b>' + value.montant + '</b> FCFA)</span>' +
                                    '<h5>' + value.code + '</h5>' +
                                    '</div>' +
                                    '<div class="col col-md-4 col-sm-12 col-xs-12 montant">' +
                                    '<p style="text-align: right"><span class="mt_tot">' + value.montant_tot + '</span> <sup>FCFA</sup></p>' +
                                    '<p style="text-align: right">nbr mois <select class="slider" data-montant="' + value.montant + '"><option value="1">1</option><option value="2">2</option><option value="3" selected>3</option></select></p>' +
                                    '</div>' +
                                    '</div>' +
                                    ''
                                );

                                $("#nom").text(datas.assure);


                            });

                            $.each(datas.iard, function (index, value) {
                                $(".tr-bim.bim2").append('<tr><td>' + value.code + '</td><td>' + value.prime + ' (' + value.type + ')</td><td>' + value.montant + '</td><td><span class="nb_mois">6 mois</span></td><td><input type="checkbox" data-id="' + value.code + '" data-assure="' + value.assure+ '" data-label="' + value.prime + '" data-type="' + value.type + '" value="' + value.montant + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.code + '""></a></td></tr>');
                                $("div#iard").find("div.form-row").find("div.scroll").append('' +
                                    '<div class="row row-primes">' +
                                    '<div class="col col-md-1 col-sm-2 col-xs-2 check">' +
                                    '<input type="checkbox" data-id="' + value.code + '" data-assure="' + value.assure+ '" data-label="' + value.prime + '" data-type="' + value.type + '" value="' + value.montant + '" name="optradio" class="optradio">' +
                                    '</div>' +
                                    '<div class="col col-md-7 col-sm-10 col-xs-10 title">' +
                                    '<h4>' + value.prime + '</h4>' +
                                    '<h5>' + value.code + '</h5>' +
                                    '</div>' +
                                    '<div class="col col-md-4 col-sm-12 col-xs-12 montant">' +
                                    '<p>' + value.montant + ' <sup>FCFA</sup></p>' +
                                    '</div>' +
                                    '</div>' +
                                    ''
                                );

                                $("#nom").text(datas.assure);

                            });

                            $(".slider").change();


                            //currentTab = 2;
                            currentTab = 2;
                            showTab(currentTab);
                            //$.alert('Identification succès !');
                        }
                        else {
                            currentTab = 1;
                            //currentTab = 1;
                            showTab(currentTab);
                            $.alert('Echec, Mauvais code OTP !');
                        }
                    }
                });


            }
        } else if (currentTab == 2 && n !== -1){
            var all = $(".optradio:checked");
            if (all.length == 0){
                currentTab=1;
                //$("#otp").addClass("invalid");
                $.alert("Veuillez selectionner au moins une prime");
                return false;
            } else {
                // Do something interesting here
                var code = $('input[name=optradio]:checked', '#regForm').attr("data-id");
                var label = $('input[name=optradio]:checked', '#regForm').attr("data-label");
                var type = $('input[name=optradio]:checked', '#regForm').attr("data-type");
                var assure = $('input[name=optradio]:checked', '#regForm').attr("data-assure");
                var code_int = $('input[name=optradio]:checked', '#regForm').attr("data-code-int");
                var num_police = $('input[name=optradio]:checked', '#regForm').attr("data-num-police");
                var prime_montant = 0;
                var nb = 0;
                var mont_concat = "";
                var type_concat = "";
                var code_int_concat = "";
                var num_police_concat = "";
                var label_concat = "";
                var nombre_mois_concat = "";
                var num_quittance_concat = "";
                var num_avenant_concat = "";

                $('input[name=optradio]:checked').each(function (i) {
                    nb++;
                    prime_montant += parseInt($(this).val());
                    mont_concat += $(this).val() + "|";
                    code_int_concat += $(this).attr("data-code-int") + "|";
                    num_police_concat += $(this).attr("data-num-police") + "|";
                    label_concat += $(this).attr("data-label") + "|";
                    type_concat += $(this).attr("data-type") + "|";
                    //nombre_mois_concat += $(this).attr("data-nombre-mois") + "|";
                    nombre_mois_concat += $($(this).closest("div.row").find("input[type='number']")[0]).val() + "|";
                    num_quittance_concat += $(this).attr("data-num-quittance") + "|";
                    num_avenant_concat += $(this).attr("data-num-avenant") + "|";
                });

                mont_concat = mont_concat.slice(0, -1);
                type_concat = type_concat.slice(0, -1);
                code_int_concat = code_int_concat.slice(0, -1);
                num_police_concat = num_police_concat.slice(0, -1);
                label_concat = label_concat.slice(0, -1);
                nombre_mois_concat = nombre_mois_concat.slice(0, -1);
                num_avenant_concat = num_avenant_concat.slice(0, -1);
                num_quittance_concat = num_quittance_concat.slice(0, -1);

                //var frais = parseInt(100);
                var frais = parseInt(0);
                var total = parseInt(prime_montant) + parseInt(frais);
                var chiffre = parseInt(total);

                $("#total").text(prime_montant);
                $("#frais").text(frais);
                $("#montant_total").text(numStr(total));      

                $.post("<?php echo site_url("Accueil/getTokens") ?>", {
                    chiffre: chiffre,
                    types: type_concat,
                    labels: label_concat,
                    nb_primes: nb
                })
                    .done(function (data) {
                        $('.buttonload').hide();

                        data = JSON.parse(data);
                        console.log(data);
                        if (data) {
                            //var json = JSON.parse(data);
                            $(".id").val(code);
                            $("#token").val(data.token);
                            $("#commande").val(data.commande);
                            $("#frais").val(frais);
                            $("#label").val(label);
                            $("#mont_concat").text(mont_concat);
                            $("#assure").val(assure);
                            $("#type_concat").val(type_concat);
                            $("#code_int_concat").val(code_int_concat);
                            $("#num_police_concat").val(num_police_concat);
                            $("#label_concat").val(label_concat);
                            $("#nombre_mois_concat").val(nombre_mois_concat);
                            $("#num_quittance_concat").val(num_quittance_concat);
                            $("#num_avenant_concat").val(num_avenant_concat);
                            $(".total").val(total);
                            $("#montant").val(prime_montant);

                            showTab(currentTab);

                        }
                        else 
                        {
                            $.alert('Token impossible');
                            location.href = "<?php echo site_url('Accueil/')?>";
                        }

                    })
                    .fail(function (error) {
                        $.alert('Token impossible');
                        $('.buttonload').hide();
                    });
            }
        } else {
            showTab(currentTab);
        }


        // Otherwise, display the correct tab:
        //showTab(currentTab);
    }

    function showTab(n) {
        // This function will display the specified tab of the form...
        $(".tab").hide();
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";

        if (n == (x.length - 1)) {


            //document.getElementById("nextBtn").innerHTML = "Confirmer";
            document.getElementById("nextBtn").innerHTML = "<?php echo $this->lang->line('payer_btn'); ?>";
        } else {
            //document.getElementById("nextBtn").innerHTML = "Suivant";
            document.getElementById("nextBtn").innerHTML = "<?php echo $this->lang->line('next_btn'); ?>";
        }

        if (n == 0){
            $(".tab").closest(".tab-block").css("margin-top", "100px");
            document.getElementById("nextBtn").innerHTML = "Me connecter";
        } else if (n == 2) {
            //$(".tab").closest(".tab-block").css("margin-top", "-30px")
            $(".tab").closest(".tab-block").css("margin-top", "100px");
            //$(".tab").closest(".tab-block").css("margin-top", "80px")
        } else {
            //$(".tab").closest(".tab-block").css("margin-top", "0px")
            $(".tab").closest(".tab-block").css("margin-top", "100px");
            //$(".tab").closest(".tab-block").css("margin-top", "80px")
        }
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {

            document.getElementById("prevBtn").style.display = "inline";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:

        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }


function numStr(a, b) {
  a = '' + a;
  b = b || ' ';
  var c = '',
      d = 0;
  while (a.match(/^0[0-9]/)) {
    a = a.substr(1);
  }
  for (var i = a.length-1; i >= 0; i--) {
    c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
    d++;
  }
  return c;
}


</script>
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
</body>
</html>