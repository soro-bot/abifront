<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>

<!-- <style>
.tab {
    padding: 10px;
}

#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 120px;
  height: 120px;
  margin: -76px 0 0 -76px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #491c05;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

#myDiv {
  display: none;
  text-align: center;
}
</style> -->
    <style>
        .tooltip
        {
            display: inline;
            position: relative;
            text-decoration: none;
            top: 0px;
            left: 4px;
        }

        .tooltip:hover:after
        {
            background: #333;
            background: rgba(0,0,0,.8);
            border-radius: 5px;
            top: -5px;
            color: #fff;
            content: attr(alt);
            left: 160px;
            padding: 5px 15px;
            position: absolute;
            z-index: 98;
            width: 150px;
        }

        .tooltip:hover:before
        {
            border: solid;
            border-color: transparent black;
            border-width: 6px 6px 6px 0;
            bottom: 20px;
            content: "";
            left: 155px;
            position: absolute;
            z-index: 99;
            top: 3px;
        }
    </style>
</head>
<!-- <body onload="myFunction()"> -->
<body>

<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h3 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirm_modal_titre'); ?></h3>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="active item one"></div>
        <div class="item two"></div>
        <div class="item three"></div>
    </div>
</div>

<div class="container">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('tpl/header'); ?>
    <!-- END HEADER -->




    <div class="tab-block3" style="">
        <!--  onSubmit="return confirm('Voulez Vous Faire Ce Paiement ?')" -->
        <!-- <div style="margin-top: 200px !important;" id="loaderDIV">
            <div id="loader"></div>
        </div> -->
        <!-- <?php echo site_url('Accueil/payment') ?> -->
        <form id="regForm" action="https://secure.sycapay.net/checkresponsive" enctype="multipart/form-data"
              method="post" role="form">
            <div class="form-body" style="position: center;">
                <!-- One "tab" for each step in the form: -->
                <div class="tab" style="padding: 10px;">
                    <h2 style="margin-bottom: 30px">
                        <center>Inscription</center>
                    </h2>
                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php echo $this->lang->line('pays'); ?> :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-home"></i>
                                </div>

                                <select class="selectpicker select2 form-control" id="pays_id" name="pays_id" required="">

                                    <option value="CI" selected="" data-indicatif="225"
                                            data-content="<img class='' width='25' src='<?php echo img_url("flags/ci.png") ?>' />   Côte d'Ivoire">
                                        Côte d'Ivoire
                                    </option>
                                    <option value="TG" data-indicatif="228"
                                            data-content="<img class='' width='25' src='<?php echo img_url("flags/tg.png") ?>' />   Togo">
                                        Togo
                                    </option>
                                </select>

                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-row hide">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php echo $this->lang->line('entreprise'); ?>
                                :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-home"></i>
                                </div>

                                <select class="selectpicker select2 form-control" id="ent_id" required="">
                                    <?php
                                    foreach ($entreprises as $ent)
                                        echo '<option data-pays="' . $ent->pays_id . '" data-content="' . $ent->ent_raison . '  (<img class=\' \' width=\'25\' src=\'' . img_url("flags/" . $ent->pays_id . ".png") . '\' />)" value="' . $ent->code_ent . '">' . $ent->ent_raison . ' (' . $ent->pays_id . ')</option>';

                                    ?>
                                </select>

                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <!--
            <div class="form-row">
              <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('pays'); ?> :</label>
                <div class="col-sm-8 input-group">
                    <div class="input-group-addon">
                         <i class="fa fa-home"></i>
                    </div>
                   <select class="select2 form-control" id="pays_id" required="">
                    <option value="BJ">Benin</option>
                    <option value="CI" selected="">Côte d'Ivoire</option>
                    <option value="TG">Togo</option>
                 </select>
                </div> 
              </div>
            </div>-->
                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mobile :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" placeholder="Mobile" required id="mobile" name="mobile" maxlength="13" value="225"/>
                            </div><!-- /.input group -->

                        </div>
                    </div>

                    <div class="form-body" style="position: center;">
                        <input name="brand" type="hidden" value="ABI">
                    </div>

                    <div class="form-row">
                        <div class="form-group" style="text-align: right">
                            <span>Vous avez déjà un compte ? <a href="<?php echo site_url("Accueil") ?>" style="text-decoration: underline; color: black">Connectez vous</a></span>
                        </div>
                    </div>
                </div>

                <div class="tab">
                    <h2>
                        <center>Renseignez l'OTP</center>
                    </h2>
                    <p style="text-align: center; margin: 10px">Un code à 4 chiffres a été envoyé au numéro suivant <b id="mob">+225 49541652</b>, veuillez le renseigner dans le champ ci-dessous</p>

                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">OTP :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-code"></i>
                                </div>
                                <input type="hidden" class="form-control" required id="pinId"/>
                                <input type="text" class="form-control" placeholder="Code OTP ici" required id="otp"/>
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">

                            <p style="text-align: right; font-size: .8em">Vous n'avez pas reçu le code OTP ? <a href="#" id="resendOtp">Renvoyez-le</a> .</p>

                        </div>
                    </div>
                </div>

                <div class="tab">
                    <h2>
                        <center>Renseignez les infos complémentaires</center>
                    </h2>


                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Vous êtes :</label>
                            <div class="col-sm-8">
                                <label class="label-radio"><input type="radio" checked required name="sexe" value="M">Un homme</label>
                                <label class="label-radio" style="margin-left: 50px"><input type="radio" required name="sexe" value="F">Une femme</label>
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nom :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" class="form-control" placeholder="Votre nom" required name="nom" id="nom"/>
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Prénoms :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" class="form-control" placeholder="Votre(vos) Prénom(s)" required name="prenoms" id="prenoms"/>
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="form-row" style="margin-top: 30px">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mot de passe :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" autocomplete="on" class="form-control" placeholder="Votre mot de passe" required name="password" id="password"/>

                            </div><!-- /.input group -->
                            <span class="help-block col-sm-8">Le mot de passe doit être composé : <br> d'au moins une majuscule<br>d'au moins une minuscule<br>d'au moins un chiffre<br>d'au moins un caractère spécial<br>doit être long de 8 caractères au moins</span>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirmation mot de passe :</label>
                            <div class="col-sm-8 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" autocomplete="on" class="form-control" placeholder="Confirmez votre mot de passe" required id="c_password" name="c_password"/>
                            </div><!-- /.input group -->
                            <span class="help-block col-sm-8"></span>

                        </div>
                    </div>
                    <br/>
                </div>

                <div style="overflow:auto; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
                    <div style="text-align:center;">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous
                        </button>
                        <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    </div>
                </div>

                <!-- Circles which indicates the steps of the form: -->
                <div style="text-align:center; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
                    <span class="step" style="background: #EA9617;"></span>
                    <span class="step" style="background: #EA9617;"></span>
                    <span class="step" style="background: #EA9617;"></span>
                </div>

            </div>
        </form>
    </div>

    <?php $this->load->view('tpl/footer') ?>
</div>
<div class="md-overlay"></div>
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
// var myVar;
// function myFunction() {
//   myVar = setTimeout(showPage, 10000);
// }
// function showPage() {
//   $('#loaderDIV').hide();
//   document.getElementById("loader").style.display = "none";
//   document.getElementById("regForm").style.display = "block";
// }

    jQuery(document).ready(function () {

        $('.carousel').carousel({interval: 20000});

        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            };
        }(jQuery));

        $(document).on('keydown', function (e) {
            if (e.keyCode == 8 && $('#mobile').is(":focus") && $('#mobile').val().length < 4) {
                e.preventDefault();
            }
        });

        $(document).on('change', "#pays_id", function (e) {
            $("#mobile").val($(this).find("option:selected").attr("data-indicatif"));
        });

//$('.myRadio').click(function() {
        var pinId ;
        $("#mobile").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        });

        $(document).on("change", ".optradio", function () {

            var prime = $(this).attr("data-label");
            var montant = $(this).val();
            var code = $(this).attr("data-id");

            var all = $(".optradio:checked");
            console.log(all);


            var type = $(this).attr("data-type");

            var change = false;

            $("#liste_primes_vie").find("div.scroll").empty();
            $("#liste_primes_iard").find("div.scroll").empty();
            $("#liste_primes_iard").fadeIn();
            $("#liste_primes_vie").fadeIn();

            $.each(all, function (index, value) {
                var type_new = $(this).attr("data-type");

                if (type != type_new){
                    remove_all(type);
                }
                console.log(value);

            });

            all = $(".optradio:checked");

            $.each(all, function (index, value) {
                var type_new = $(this).attr("data-type");
                var prime = $(this).attr("data-label");
                var montant = $(this).val();
                var code = $(this).attr("data-id");
                var assure = $(this).attr("data-assure");
                var nb = $($(this).closest("div.row").find("select")[0]).val();


                if (type_new == "VIE"){
                    $($("#liste_primes_vie").find("div.scroll")[0]).append("" +
                        '<div class="row row-primes">' +
                        '<div class="col col-md-1 check">' +
                        '</div>' +
                        '<div class="col col-md-5 title">' +
                        '   <h4>' + prime + '</h4>' +
                        '   <h5>' + code + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-2 title">' +
                        '   <h5> '+ nb +' x ' + parseInt(montant) / parseInt(nb) + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-4 montant">' +
                        '   <p>' + montant + ' <sup>FCFA</sup></p>' +
                        '</div>' +
                        '</div>' +
                        ""
                    );

                    $("#liste_primes_iard").hide();
                }

                if (type_new == "IARD"){

                    $($("#liste_primes_iard").find("div.scroll")[0]).append("" +
                        '<div class="row row-primes">' +
                        '<div class="col col-md-1 check">' +
                        '</div>' +
                        '<div class="col col-md-7 title">' +
                        '   <h4>' + prime + '</h4>' +
                        '   <h5>' + code + '</h5>' +
                        '</div>' +
                        '<div class="col col-md-4 montant">' +
                        '   <p>' + montant + ' <sup>FCFA</sup></p>' +
                        '</div>' +
                        '</div>' +
                        ""
                    );

                    $("#liste_primes_vie").hide();
                }

                $("#nom").text(assure);
            });

            var pays_id = $("#pays_id").val();


        });

        function remove_all (type){
            if (type == "VIE"){
                $("#iard").find('.optradio:checked').each(function(i){
                    $(this).prop('checked', false);
                });
            }
            if (type == "IARD"){
                $("#vie").find('.optradio:checked').each(function(i){
                    $(this).prop('checked', false);
                });
            }
        }


        $(document).on("change", ".slider", function () {

            var nb_mois = parseInt($(this).val());
            var montant = parseInt($(this).attr("data-montant"));


            var mt_total = nb_mois * montant;


            $($(this).parent().parent().find(".mt_tot")[0]).text(mt_total);


            $($(this).closest("div.row").find("input[type=checkbox]")[0]).val(mt_total);



        });


        $(document).on("click", "#resendOtp", function () {

            var pinId = $("#pinId").val();
            mobile = $('#mobile').val();

            $.ajax({
                url: "<?php echo site_url('Accueil/resendOtp')?>",
                type: "POST",
                data: {pinId: pinId},
                success: function (datas) {

                    data = JSON.parse(datas);
                    if (data != null && data.status == true) {
                        mobile = mobile.substring(3, mobile.length); 
                        $("#mob").parent().text("L'OTP a bien été renvoyé au numero suivant : +225 " + mobile);

                    }
                    else 
                    {

                        $.alert("OTP non envoyé veuillez tenter un nouvel envoi");
                    }
                }
            });


        });


    <?php
        if ($this->session->flashdata("success")) {
            echo "toast_success('" . $this->session->flashdata("success") . "');";
        }
        ?>
        <?php
        if ($this->session->flashdata("error")) {
            echo "toast_error('" . $this->session->flashdata("error") . "');";
        }
        ?>
        <?php
        if ($this->session->flashdata("token")) {
            echo "toast_success('" . $this->session->flashdata("token") . "');";
        }
        ?>
    });

    // Current tab is set to be the first tab (0)
    var currentTab = 0;
    // Display the current tab
    showTab(currentTab);


    function showTab(n) {
        // This function will display the specified tab of the form...
        $(".tab").hide();
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";

        if (n == 0 || n == 1){
            $(".tab").closest(".tab-block").css("margin-top", "100px");
        } else if (n == 2) {
            //$(".tab").closest(".tab-block").css("margin-top", "10px")
            $(".tab").closest(".tab-block").css("margin-top", "100px");
        } else if (n == 3) {
            //$(".tab").closest(".tab-block").css("margin-top", "-30px")
            $(".tab").closest(".tab-block").css("margin-top", "100px");
        } else {
            //$(".tab").closest(".tab-block").css("margin-top", "0px")
            $(".tab").closest(".tab-block").css("margin-top", "100px");
        }
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {


            //document.getElementById("nextBtn").innerHTML = "Confirmer";
            document.getElementById("nextBtn").innerHTML = "<?php echo "m'inscrire"; ?>";
        } else {
            //document.getElementById("nextBtn").innerHTML = "Suivant";
            document.getElementById("nextBtn").innerHTML = "<?php echo $this->lang->line('next_btn'); ?>";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        //if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        $(".tab").hide();
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            currentTab = 2;
            // ... the form gets submitted:
            //document.getElementById("regForm").submit();
            //return false;
            $.confirm({
                theme: 'material',
                title: 'CONFIRMATION !',
                content: 'Voulez vous confirmer votre inscription ?',
                buttons: {
                    confirm: {
                        text: '<?php echo $this->lang->line('confirm_modal_valider_btn'); ?>',
                        btnClass: 'btn-blue',
                        action: function () {

                            let strengthBadge = document.getElementById('StrengthDisp');
                            let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})')
                            let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))')

                            var password = document.getElementById('password');
                            var c_password = document.getElementById('c_password');

                            if ($(password).val() !== $(c_password).val()){
                                $(password).closest(".form-group").addClass("has-error");
                                $($(password).closest(".form-group").find(".help-block")).text("Les mots de passes ne sont pas conformes");
                                $(c_password).closest(".form-group").addClass("has-error");
                                $($(c_password).closest(".form-group").find(".help-block")).text("Les mots de passes ne sont pas conformes");
                            } else if(!strongPassword.test($(password).val())){
                                $(password).closest(".form-group").addClass("has-error");
                                $($(password).closest(".form-group").find(".help-block")).text("Le mot de passe doit être composé : <br> d'au moins une majuscule<br>d'au moins une minuscule<br>d'au moins un chiffre<br>d'au moins un caractère spécial<br>doit être long de 8 caractères au moins");
                                $(c_password).closest(".form-group").removeClass("has-error");
                                $($(c_password).closest(".form-group").find(".help-block")).text("");
                            } else {
                                $(password).closest(".form-group").removeClass("has-error");
                                $($(password).closest(".form-group").find(".help-block")).text("");
                                $(c_password).closest(".form-group").removeClass("has-error");
                                $($(c_password).closest(".form-group").find(".help-block")).text("");
                                password.setCustomValidity("");
                                c_password.setCustomValidity("");



                                $("#regForm").attr("action", "<?php echo site_url("Accueil/inscrire") ?>");
                                $("#regForm").submit();
                            }


                        }
                    },
                    cancel: {
                        text: '<?php echo $this->lang->line('confirm_modal_annuler_btn'); ?>',
                        btnClass: 'btn-default',
                        action: function () {
                            //document.location.reload(true);
                        }
                    }
                }
            });

        }


        //Envoie de l'OTP
        if (currentTab == 1 && n !== -1) {
            var mobile = $('#mobile').val();
            var brand = $('#brand').val();
            $("#mob").text("+" + mobile);

            mobile = mobile.substring(3, mobile.length);
            if (mobile == ""){
                currentTab=0;
                showTab(currentTab);
                $('#mobile').addClass("invalid");
                alert("Le mobile n'a pas été renseigné !")
            } else if (mobile.length < 8 || mobile.length > 10){
                currentTab=0;
                showTab(currentTab);
                $('#mobile').addClass("invalid");
                alert("Le mobile doit être de 10 chiffres !")
            } else {

                $.post("<?php echo site_url("Accueil/getVerification") ?>", {mobile: $('#mobile').val(), brand:'ABI'})
                    .done(function (data) {
                        data = JSON.parse(data);
                        if (data != null && data.status == true) {
                            //nextPrev(1);
                            $("#mob").text("+225 " + mobile);
                            //currentTab = 0;
                            $("#pinId").val(data.pinId);
                            //pinId = data;


                        }
                        else {
                            //currentTab = 0;
                            //nextPrev(-1);
                            currentTab = 0;
                            showTab(currentTab);
                            if (data.result){
                                $.alert(data.message+ " : " + data.result.smsStatus);
                            } else {
                                $.alert(data.message);
                            }
                        }
                    })
                    .fail(function (error) {
                        currentTab = 0;
                        //nextPrev(-1);

                        showTab(currentTab);
                        $.alert("Le code OTP n'a pas pu être envoyé, veuillez réessayer !");
                    });
            }
        }

        if(currentTab == 2 && n === 1){
            var otp = $("#otp").val();
            if (otp == ""){
                currentTab=2;
                $("#otp").addClass("invalid");
                alert("L'otp n'a pas été renseigné")
            } else {

                var pinId = $("#pinId").val();
                mobile = $('#mobile').val();
                mobile = mobile.substring(3, mobile.length);

                $.ajax({
                    url: "<?php echo site_url('Accueil/ajax_confirmation')?>",
                    type: "POST",
                    data: {code: otp, pinId: pinId, mobile: mobile},
                    success: function (datas) {

                        if (datas !== "") {
                            datas = JSON.parse(datas);

                            //currentTab = 2;
                            currentTab = 2;
                            showTab(currentTab);
                           // $.alert('Identification succès !');
                        }
                        else {
                            currentTab = 2;
                            //currentTab = 1;
                            showTab(currentTab);
                         //   $.alert('Echec, Mauvais code OTP !');
                        }
                    }
                });


            }
        }

        if (currentTab == 3 && n !== -1){

            let strengthBadge = document.getElementById('StrengthDisp');
            let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})')
            let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))')

            var password = document.getElementById('password');
            var c_password = document.getElementById('c_password');

            if ($(password).val() !== $(c_password).val()){
                password.setCustomValidity("Les mots de passes ne sont pas conformes");
                c_password.setCustomValidity("Les mots de passes ne sont pas conformes");
            } else if(!strongPassword.test($(password).val())){
                password.setCustomValidity("Le mot de passe doit être composé d'au moins d'une majuscule, d'une minuscule, d'un chiffre, d'un caractère spécial et doit être long de 8 caractères au moins");
            } else {
                password.setCustomValidity("");
                c_password.setCustomValidity("");

                $("#inscription").submit();
            }

        }


        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:

        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }



</script>
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
</body>
</html>