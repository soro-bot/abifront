
<div class="header mb30">
    <div class="menu-bar">
        <div class="row" style="margin: 0">
            <div class="col-sm-5 col-md-offset-1 logo">

            </div>
            <div class="col-sm-12" style="text-align: right">
                <?php if ($this->session->userdata("prenoms_assure")) { ?>
                    <a style="margin-top: 10px;" id="btn_logout" class="btn btn-danger" href="<?php echo site_url("Accueil/logout") ?>" >Me deconnecter</a>
                <?php } ; ?>
                <select style="width: auto; float: right; margin-top: 10px;" class="form-control" onchange="javascript:window.location.href='<?php echo site_url(); ?>/LanguageSwitcher/switchLang/'+this.value;">
                    <option value="francais" <?php if($this->session->userdata('site_lang') == "francais") echo 'selected="selected"'; ?>><?php echo $this->lang->line('francais'); ?></option>
                    <option value="english" <?php if($this->session->userdata('site_lang') == "english") echo 'selected="selected"'; ?>><?php echo $this->lang->line('anglais'); ?></option>
                </select>
            </div>
        </div>
    </div>
</div>