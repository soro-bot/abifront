<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo js_url('jquery-3.6.0.min')?>" type="text/javascript"></script>
<script src="<?php echo js_url('bootstrap.min')?>" type="text/javascript"></script>
<script src="<?php echo js_url('jquery-filestyle.min')?>" type="text/javascript"></script>
<script src="<?php echo js_url('jquery.toast')?>" type="text/javascript"></script>
<!-- <script src="<?php echo js_url('libraries/bootstrap-datepicker-1.6.4/js/bootstrap-datepicker.min')?>" type="text/javascript"></script> -->
<script src="<?php echo js_url('rs-plugin/js/jquery.themepunch.tools.min')?>" type="text/javascript"></script>
<script src="<?php echo js_url('rs-plugin/js/jquery.themepunch.revolution.min')?>" type="text/javascript"></script>
<script src="<?php echo js_url('autoNumeric-1.9.18')?>" type="text/javascript"></script>
<!-- DataTables -->
<script src="<?php echo bowers_url(); ?>DataTables/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/parsley.min.js" type="text/javascript"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.4.4/parsley.min.js" type="text/javascript"></script>-->
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/i18n/fr.js" type="text/javascript"></script>
<script src="<?php echo js_url('form-validation')?>" type="text/javascript"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
<script src="<?php echo plugins_url('confirms.js')?>" type="text/javascript"></script>

<!-- END CORE TO SEND PLUGINS -->
<script>
function toast_warning(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Attention', // Optional heading to be shown on the toast
                icon: 'warning', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_information(texte){
        $.toast({
                text: "texte", // Text that is to be shown in the toast
                heading: 'Information', // Optional heading to be shown on the toast
                icon: 'info', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_success(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Succès', // Optional heading to be shown on the toast
                icon: 'success', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_error(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Erreur', // Optional heading to be shown on the toast
                icon: 'error', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}


</script>

<!-- END JAVASCRIPTS -->