<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $page_title ; ?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300" rel="stylesheet">
<link href="<?php echo css_url("bootstrap.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("font-awesome/css/font-awesome.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("slider-revolution-slider/rs-plugin/css/settings")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("slider-revolution-slider/css/style")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("jquery.toast")?>" rel="stylesheet" type="text/css" />

<!-- Librairie -->
<link href="<?php echo css_url("libraries/bootstrap-datepicker-1.6.4/css/bootstrap-datepicker.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("libraries/bootstrap-datepicker-1.6.4/css/bootstrap-datepicker.standalone.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("libraries/bootstrap-datepicker-1.6.4/css/bootstrap-datepicker3.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("libraries/bootstrap-datepicker-1.6.4/css/bootstrap-datepicker3.standalone.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("jquery-filestyle.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("style")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url("jquery-ui")?>" rel="stylesheet" type="text/css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">


<!-- Widzard to SEND et logo -->
<link rel="stylesheet" href="<?php echo plugins_url('confirms.css'); ?>">
<link href="<?php echo base_url() ?>assets/wizard/style.css" rel="stylesheet" type="text/css"/>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo bowers_url(); ?>DataTables/datatables.min.css">
<link rel="shortcut icon" href="<?php echo base_url("assets/img/logo.png"); ?>" style="width: 100%; height: 100%;"/>


