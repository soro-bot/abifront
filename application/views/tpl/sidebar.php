<?php
/**
 * User: Abou KONATE
 * Date: 01/08/2020
 */ 
?>
<!-- Left side column. contains the logo and sidebar -->
<div id="sidebar" class="sidebar responsive ace-save-state sidebar-fixed sidebar-scroll">
<script type="text/javascript">
    try{ace.settings.loadState('sidebar')}catch(e){}
</script>

<div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
        <a class="btn btn-success" href="<?php echo site_url('Dashboard');?>" title="<?php echo($this->lang->line('dashboard')); ?>">
            <i class="ace-icon fa fa-home"></i>
        </a>
        <?php if($this->session->userdata('site_lang') == "francais") { ?>
        <a class="btn btn-info" href="<?php echo site_url('LanguageSwitcher/switchLang/english');?>"  title="Changer la langue">
            <img src="<?php echo img_url(); ?>gallery/en.png" alt="en" width="25" height="18"/>
        </a>
        <?php }else{ ?>
        <a class="btn btn-primary" href="<?php echo site_url('LanguageSwitcher/switchLang/francais');?>" title="Change language">
            <img src="<?php echo img_url(); ?>gallery/fr.png" alt="fr" width="25" height="18"/>
        </a>
        <?php }; ?>
        <a class="btn btn-danger" href="<?php echo site_url('Profil');?>" title="Profil">
            <i class="ace-icon fa fa-user"></i>
        </a>
       <!--  <button class="btn btn-danger">
            <i class="ace-icon fa fa-cogs"></i>
        </button> -->
    </div>

    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
        <span class="btn btn-success"></span>

        <span class="btn btn-info"></span>

        <span class="btn btn-warning"></span>

        <span class="btn btn-danger"></span>
    </div>
</div><!-- /.sidebar-shortcuts -->

<ul class="nav nav-list">
    <li class="<?php if($page == 'dashboard'){echo "active";} ?>">
        <a href="<?php echo site_url('Dashboard');?>">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> <?php echo($this->lang->line('dashboard')); ?> </span>
        </a>
        <b class="arrow"></b>
    </li>

    <li class="<?php if($page == 'agence'){echo "active";} ?>">
    <a href="<?php echo site_url('Agence');?>">
        <i class="menu-icon fa fa-bank"></i>
        <span class="menu-text"> <?php echo($this->lang->line('agences')); ?> </span>
    </a>
        <b class="arrow"></b>
    </li>

    <li class="<?php if($page == 'transactions'){echo "active";} ?>">
        <a href="<?php echo site_url('Transactions');?>">
            <i class="menu-icon fa fa-money"></i>
            <span class="menu-text"> <?php echo($this->lang->line('transactions')); ?> </span>
        </a>
        <b class="arrow"></b>
    </li>

    <li class="<?php if($page == 'users'){echo "active";} ?>">
    <a href="<?php echo site_url('Users');?>">
        <i class="menu-icon fa fa-users"></i>
        <span class="menu-text"> <?php echo($this->lang->line('utilisateurs')); ?> </span>
    </a>
        <b class="arrow"></b>
    </li>

    <li class="" style="display: none;">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Pays et entreprises </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="tables.html">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Simple &amp; Dynamic
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="jqgrid.html">
                    <i class="menu-icon fa fa-caret-right"></i>
                    jqGrid plugin
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
        </li>


        </ul>
    </li>
</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>
</div>