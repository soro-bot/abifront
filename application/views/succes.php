<!DOCTYPE html>
<html lang="fr">
<head>
    <?php $this->load->view('tpl/css_files'); ?>
</head>
<body>

    <div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h3 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirm_modal_titre'); ?></h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- BEGIN HEADER -->
        <!-- ?php $this->load->view('tpl/header'); ?> -->
        <!-- END HEADER -->

        <div class="col-md-offset-3 col-md-5"  align="center">
          <div id="getFacturations">
            <div style="">
            <img src="<?php echo img_url("logo.png") ?>" width="80px" >

            </div>

            <div class="clearfix"><center><?php echo "Résumé de paiement"; ?></center><br>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Assuré :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-pencil"></i>
                      </div>
                    <b><label class="form-control" id="assure"><?php echo $assure ?></label></b>
                  </div><!-- /.input group -->
              </div>
              <div class="form-group col-md-6">
                  <label>Société :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="societe">ABI</label></b>
                  </div><!-- /.input group -->
              </div>
              </div>

              <div class="form-row">
              <div class="form-group col-md-6">
                <label>Produit :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-pencil"></i>
                      </div>
                      <b><label class="form-control" id="produit"><?php echo $produit ?></label></b>
                  </div><!-- /.input group -->
              </div>
              <div class="form-group col-md-6">
                  <label>Nombre de primes :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="nbre_mois"><?php echo $nbre_mois ?></label></b>
                  </div><!-- /.input group -->
              </div>
              </div>

              <div class="form-row">
              <div class="form-group col-md-6">
                <label>Montant réglé :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-money"></i>
                      </div>
                      <b><label class="form-control" id="montant"><?php echo $montant ?></label></b>
                  </div><!-- /.input group -->
              </div>
              <div class="form-group col-md-6">
                  <label>Ref. paiement :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-lock"></i>
                      </div>
                      <b><label class="form-control" id="refpaie"><?php echo $refpaie ?></label></b>
                  </div><!-- /.input group -->
                  
              </div>
              </div>

              <div class="form-row">
              <div class="form-group col-md-6">
                <label>Date paiement :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="datepaie"><?php echo $datepaie ?></label></b>
                  </div><!-- /.input group -->
              </div>
              <div class="form-group col-md-6">
                  <label>Liste des quittances :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="listeDesPolices"><?php echo $listeDesPolices ?></label></b>
                  </div><!-- /.input group -->
              </div>
              </div>

              <div class="form-row">
              <div class="form-group col-md-6">
                <label>Mois Termes :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="moisTermes"><?php echo $moisTermes ?></label></b>
                  </div><!-- /.input group -->
              </div>
              <div class="form-group col-md-6">
                  <label>Numéros Encaissés :</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                           <i class="fa fa-home"></i>
                      </div>
                      <b><label class="form-control" id="numeEncais"><?php echo $numeEncais ?></label></b>
                  </div><!-- /.input group -->
              </div>
              </div>

            </div>
                <br>
                <div class="form-row">
                    <div class="form-group">
                        <p class="col-sm-12 control-label">* Ce reçu ne fait pas office de pièce de réclamation mais est juste utilisé comme un reçu </p>
                    </div>
                </div>
                <br>
                </div>
                <br>
                <div class="form-row text-center">

                            <button class="btn btn-success" id="btn_mail">Envoyer par mail</button>
    
                            <!-- <button class="btn btn-danger" onclick="window.print()">Imprimer/Télécharger</button> -->
                            <a title="Download" class="btn btn-dangers" href="<?php echo base_url('assets/generatepdf')."/".$refpaie.".pdf";?>" download><button class="btn btn-danger">Télécharger</button></a>

                            <a class="btn btn-default" href="<?php echo site_url("Accueil/") ?>" >Retour à mon compte</a>
                    </div>
                

            </div>

        </div>

        <?php $this->load->view('tpl/footer') ?>
    </div>
<div class="md-overlay"></div>
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
jQuery(document).ready(function() {
$('#btn_mail').click(function() {
    $.confirm({
        theme: 'material',
        title: 'Votre paiement est un succès',
        content: '<p>Voulez-vous recevoir votre reçu par email ?</p>' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '' +
        '<input type="email" placeholder="Email" class="name form-control email" required/>' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Recevoir mon reçu',
                btnClass: 'btn-blue',
                action: function () {
                    var email = $(this.$content.find('.email')[0]).val();
                    if (!email) {
                        $.alert('Veuillez renseigner votre email!');
                        return false;
                    }
                    else {

                        $("html").find(".jconfirm.jconfirm-material.jconfirm-open").remove();
                        $("#btn_mail").closest("div.form-row").hide();
                        var mail = $("html").html();
                        var refpaie = $("#refpaie").text();
                        var montant = $("#montant").text();
                        var produit = $("#produit").text();
                        var societe = $("#societe").text();
                        var assure = $("#assure").text();
                        var listeDesPolices = $("#listeDesPolices").text();
                        var datepaie = $("#datepaie").text();
                        var content = $("#getFacturations").text();

                        $.ajax({
                            url: "<?php echo site_url('Accueil/send_email')?>",
                            type: "POST",
                            data: {content: content, datepaie: datepaie, listeDesPolices: listeDesPolices, email: email, mail: mail, refpaie:refpaie, montant:montant, produit:produit, assure:assure, societe:societe},
                            success: function (datas) {
                                //datas = JSON.parse(datas);
                                if (datas) {

                                    setTimeout(function () {
                                        location.href = "<?php echo site_url('Accueil/')?>";
                                    }, 3000);

                                    $.alert('Email envoyé avec succès !');
                                    return true;

                                }
                                else {
                                    $.alert('Erreur lors de l\'envoir de l\'email, veuillez reessayer svp! ');
                                    return false;
                                }
                            }
                        });

                        return false;

                    }
                }
            },
            cancel: {
                text: 'Non, merci',
                btnClass: 'btn-default',
                action: function () {
                    //location.href = "<?php echo site_url('Accueil/')?>";
                },
            }
        },
        onContentReady: function () {
            // bind to events
            currentTab = 1;
        }
    });
})
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }
  ?>
  <?php
      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>
  <?php
      if ($this->session->flashdata("token")){
          echo "toast_success('".$this->session->flashdata("token")."');";
      }
  ?>
});


function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:

  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
    
</script>
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
</body>
</html>