<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Accueil_title'] = 'Atlantique || Welcome';
$lang['saisir_otp'] = 'Enter OPT code';
$lang['choix_prime'] = 'CHOOSE PRIME TO PAY';
$lang['choice_label'] = 'Choice';
$lang['pays'] = 'Country';
$lang['next_btn'] = 'Next';
$lang['previous_btn'] = 'Previous';
$lang['anglais'] = 'English';
$lang['francais'] = 'French';
$lang['produit_label'] = 'Product';
$lang['num_contrat_label'] = 'Contract Number :';
$lang['montant_label'] = 'Amount';
$lang['taux_label'] = 'Tax';
$lang['total_label'] = 'Total';
$lang['num_quittance_label'] = 'Receipt Number :';
$lang['payer_btn'] = 'Buy';
$lang['verif_btn'] = 'My insurance amount';
$lang['confirm_modal_titre'] = 'PAYEMENT CONFIRMATION';
$lang['confirm_modal_text'] = 'Do you confirm the information below ?';
$lang['confirm_modal_annuler_btn'] = 'Cancel';
$lang['confirm_modal_valider_btn'] = 'Yes I confirm';
$lang['notif_transaction_annulee'] = 'Your transaction has been canceled';
$lang['notif_transaction_succes'] = 'Your transaction was successful';
$lang['prefix_label'] = 'Prefix';
$lang['police_label'] = 'Police number';
$lang['mobile_payeur'] = "Payer's Mobile";
$lang['entreprise'] = 'Compagny';
$lang['saisir_email'] = 'Type your email';
$lang['notif_succes'] = 'Your operation was successful';

