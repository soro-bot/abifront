<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Accueil_title'] = 'Atlantique || Bienvenu';
$lang['saisir_otp'] = 'Saisir le code OTP';
$lang['choix_prime'] = 'CHOISIR UNE PRIME A PAYER';
$lang['choice_label'] = 'Choisir';
$lang['pays'] = 'Pays';
$lang['next_btn'] = 'Suivant';
$lang['previous_btn'] = 'Retour';
$lang['anglais'] = 'Anglais';
$lang['francais'] = 'Français';
$lang['produit_label'] = 'Produit';
$lang['num_contrat_label'] = 'Numero contrat :';
$lang['montant_label'] = 'Montant';
$lang['taux_label'] = 'Frais';
$lang['total_label'] = 'Total';
$lang['num_quittance_label'] = 'Numero quittance :';
$lang['payer_btn'] = 'Payer';
$lang['verif_btn'] = 'Demander la prime';
$lang['confirm_modal_titre'] = 'PAIEMENT DE LA PRIME';
$lang['confirm_modal_text'] = 'Confirmez-vous les informations ci-dessous ?';
$lang['confirm_modal_annuler_btn'] = 'Annuler';
$lang['confirm_modal_valider_btn'] = 'Oui, je confirme';
$lang['notif_transaction_annulee'] = 'Votre transaction a été annulée';
$lang['notif_auth'] = 'Authentification';
$lang['notif_transaction_succes'] = 'Votre transaction s\'est effectuée avec succès';
$lang['notif_transaction_succes'] = 'Votre transaction s\'est effectuée avec succès';
$lang['prefix_label'] = 'Préfixe';
$lang['police_label'] = 'Numero police';
$lang['mobile_payeur'] = 'Mobile payeur';
$lang['entreprise'] = 'Entreprise';
$lang['saisir_email'] = 'Saisir votre email';
$lang['notif_succes'] = 'Votre opération est effectuée avec succès';
