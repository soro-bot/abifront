<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class NuSoap_lib {
    function Nusoap_lib(){
        require_once('lib/nusoap'.EXT);
       // require_once(str_replace("\\","/",APPPATH).'libraries/nusoap/lib/nusoap'.EXT); //If we are executing this script on a Windows server
          }

    function soaprequest($api_url, $service, $params)
    {
        if ($api_url != '' && $service != '' && count($params) > 0)
        {
            $wsdl = $api_url."?wsdl";
            $client = new nusoap_client($wsdl, 'wsdl');
            //$client->setCredentials($api_username,$api_password);
            $error = $client->getError();
            if ($error)
            {
                echo "\nSOAP Error\n".$error."\n";
                return false;
            }
            else
            {
                try {
                    $result = $client->call($service, $params);
                    if ($client->fault) {
                        print_r($result);
                        return false;
                    } else {
                        //$aux = $result->recupererListeBureauResponse->return;
                        $result_arr = json_decode($result, true);
                        //print_r($result);
                        $aux = $result_arr ['DATA'];
                        //print_r($aux);
                        $return_array = $aux;
                        return $return_array;
                    }
                } catch (SoapFault $e) {echo $e->getMessage();}
            }
        }
    }
      }
?>