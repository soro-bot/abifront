<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

public function __construct()
{
	parent::__construct();
	$this->load->model('Auth_model', 'authModel');
	$this->load->model('Assure_model', 'assureModel');
	$this->load->model('Transactions_model', 'transactionsModel');
	$this->load->model('Entreprise_model', 'entrepriseModel');
}

public function index()
{	
	$data['page_title'] = $this->lang->line('Accueil_title');
	//$data['entreprises'] = $this->entrepriseModel->getAllEntreprises();
	$data['entreprises'] = '';
	$data['getTransactions'] = $this->transactionsModel->getTransactions();
	$this->load->view('accueil', $data);
}

public function sendPassword()
{
	$mobile = $this->input->post('telephoneAssure');
	if (empty($mobile)) 
	{	
		$this->session->set_flashdata('error', 'Veuillez renseigner le numéro de téléphone !');
		redirect(site_url("Accueil"));
	}
	else
	{	
		
		$query = $this->assureModel->getAssure2($mobile);
		if ($query) 
		{	
			$client_name = $query->nom_assure;
			$passClient = substr(time(), -3, -1).strtoupper(substr($client_name, 0, 2)).'#'.substr(uniqid(), 2, 4);
			$id = $this->assureModel->updatePass($mobile, $passClient);
			if ($id) 
			{	
				$mobile = str_replace(array("225", "228"), "", $mobile);
				$url = "http://secure.sycapay.net/smssendapi";
                $customer_id = "C_5894A04C65BDD";
                $headers = array('X-IDENTIFIANT: ' . $customer_id);
                $sendername = "ABI";
                $corps_message = "Votre nouveau mot de passe ABI est $passClient";

                $parametre = array(
                  'contact' => $mobile,
                  'emmeteur' => $sendername,
                  'identifiant' => $customer_id,
                  'message' => urlencode($corps_message),
                  'infobip' => true
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $donnee = http_build_query($parametre);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $donnee);
                $result = json_decode(curl_exec($ch), true);
                if ($result["code"] == 0) 
                {
                	$this->session->set_flashdata('success', "Mot de passe mis à jour, Veuillez consulter votre téléphone SVP!");
				    redirect(site_url("Accueil"));
                }
                else
                {
                	$this->session->set_flashdata('error', "Impossibe d envoyer SMS, prière contacter le support !");
				    redirect(site_url("Accueil"));
                }
			}
			else
			{
				$this->session->set_flashdata('error', "Opération impossible, prière reprendre plus tard !");
				redirect(site_url("Accueil"));
			}
			
		}
		else
		{	
			$this->session->set_flashdata('error', 'Ce numéro de téléphone est inconnu !');
			redirect(site_url("Accueil"));
		}
	}


}

public function getCheckOperateurs()
{     
    $mobile = $this->input->post('mobile');
    $curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Topup/accessToken',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_HTTPHEADER => array(
	    'Cookie: ci_session=l7i1isuogd6kp2rrs6naptrg802dlq5o'
	  ),
	));
	$response = json_decode(curl_exec($curl), true);
	curl_close($curl);
	if ($response['status'] == true)
	{
		$toakenOP = $response['token'];
		$ch = curl_init();
		curl_setopt_array($ch, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Topup/CheickMobileOperators',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'client_auth='.$toakenOP.'&client_mobile='.$mobile.'&iso_code='.$this->session->userdata("pays_id"),
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=l7i1isuogd6kp2rrs6naptrg802dlq5o'
		  ),
		));

		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if ($response['status'] == true)
		{	
			if (strpos($response['OperatorName'], "Orange") !== 0) 
			{
				echo "NO";
			}
			else
			{
				echo "OK";
			}
		}
		else
		{
			echo "";
		}
	}
	else
	{
		echo "";
	}
   
}

	public function getFatures($Sycapay_ref)
	{	

		if (!empty($Sycapay_ref))
		{	
			$nbre_mois = 0;
			$montant_trans = 0;
			$montant_trans = "";
			$assure_trans = "";
			$produit = "";
			$policesConcatees = "";
			$moisTermes = "";
			$numeEncais = "";
			$datepaie = date("Y-m-d H:i:s");

			$trans = $this->transactionsModel->getReferenceSycapay($Sycapay_ref);
			if (!empty($trans))
			{	

				foreach ($trans as $valors) 
				{	
					$nbre_mois = $nbre_mois + 1; 
					if ($nbre_mois > 1)
					{
						$policesConcatees .= "|";
						$moisTermes .= "|";
						$numeEncais .= "|";
					}

					$policesConcatees .= $valors->assure_num_polices;
					$moisTermes .= $valors->MOISTERM;
					$numeEncais .= $valors->NUMEENCA;
					$montant_trans += $valors->montant_trans;
					$assure_trans = $valors->assure_trans;
					$ent_raison = $valors->ent_raison;
					$produit = $valors->produit_trans;
					$datepaie = $valors->date_create_trans;
				}

				//$this->session->set_userdata(array("getFatures" => $trans));
				$data['page_title'] = $this->lang->line('Accueil_title');
				$data['assure'] = $assure_trans;
				$data['societe'] = $ent_raison;
				$data['produit'] = $produit;
				$data['nbre_mois'] = $nbre_mois;
				$data['montant'] = $montant_trans;
				$data['refpaie'] = $Sycapay_ref;
				$data['datepaie'] = $datepaie;
				$data['moisTermes'] = $moisTermes;
				$data['numeEncais'] = $numeEncais;
				$data['listeDesPolices'] = $policesConcatees;

				$this->load->view('succes', $data);
			}
			else
			{
				$this->session->set_flashdata('error', 'Ce paiement est inexistant');
				redirect(site_url("Accueil"));
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Ce paiement est inexistant');
			redirect(site_url("Accueil"));
		}

	}

	public function getAnnulations($commande)
	{	
		$commande = str_replace("%7C", "|", $commande);
		if ($commande) 
		{
			$commandes = explode("|", $commande);


			foreach($commandes as $com)
			{
				$id = $this->transactionsModel->majStatusEchecs($com);
			}


			$this->session->set_flashdata('error', 'Paiement annulé !');
			redirect(site_url("Accueil"));
		}
		else
		{
			$this->session->set_flashdata('error', 'Paiement annulé !');
			redirect(site_url("Accueil"));
		}
    }

	public function inscription()
	{
		$data['page_title'] = $this->lang->line('Accueil_title');

		$data['entreprises'] = '';
		//$data['entreprises'] = $this->entrepriseModel->getAllEntreprises();

		$this->load->view('inscription', $data);
	}

	public function inscrire()
	{
		$pays_id = $this->input->post('pays_id');
		$mobile = $this->input->post('mobile');
		$nom = $this->input->post('nom');
		$prenoms = $this->input->post('prenoms');
		$sexe = $this->input->post('sexe');
		$password = $this->input->post('password');

		$query = $this->assureModel->getAssure2($mobile);
		if ($query)
		{	
			$this->session->set_flashdata('error', 'Ce numéro de téléphone existe déjà !');
			//echo 'Erreur inscription';
			redirect(site_url("Accueil"));
		}
		else
		{	
			if ($pays_id || $mobile || $nom || $prenoms || $sexe || $password) 
			{

				$id = $this->assureModel->inscrire($pays_id, $mobile, $nom, $prenoms, $sexe, $password);
				$this->session->set_flashdata('success', $this->lang->line("notif_succes"));
				redirect(site_url("Accueil"));
			}
			else
			{	
				$this->session->set_flashdata('error', 'Prière renseigner la liste des champs !');
				//echo 'Erreur inscription';
				redirect(site_url("Accueil"));
			}
		}


	}

	public function login()
	{
        header("Access-Control-Allow-Origin: *");
        $mobile = $this->input->post('mobile');
		$password = $this->input->post('password');
		$query = true;
		if ($mobile || $password) 
		{
			$ret = $this->assureModel->getAssure($mobile, $password);
			if ($ret) 
			{
				$userdata = array('afficheToken' => 'afficheToken');
				$this->session->set_userdata((array)$ret);
				//$query = $this->authModel->getPrimes($mobile);
				echo json_encode(array(
					"statut" => true,
					"data" => $ret,
					//"vie" => $query["vie"],
					//"iard" => $query["iard"]
				));
			}
			else
			{	
				$this->session->set_flashdata('error', 'Login / Mot de passe incorrect');
				echo json_encode(array(
				"statut" => false,
				"data" => "Login / Mot de passe incorrect"
				));
			}
			
		}
		else
		{	
			$this->session->set_flashdata('error', 'Login / Mot de passe obligatoire');
			echo json_encode(array(
				"statut" => false,
				"data" => "Login / Mot de passe obligatoire"
			));
		}


	}

	public function update_pass()
	{
        header("Access-Control-Allow-Origin: *");

        $mobile = $this->session->userdata('mobile_assure');
		$oldpassword = $this->input->post('oldpassword');
		$password = $this->input->post('password');


		//$query = $this->authModel->isCommande($purchaseinfo, $montant);
		$query = true;

		if ($mobile || $oldpassword || $password) {


			//$query = $query && $this->authModel->is_existe($com);
			$ret = $this->assureModel->getAssure($mobile, $oldpassword);

            if ($ret){
                $query = $this->assureModel->updatePass($mobile, $password);

                if ($query){

                	$this->session->set_flashdata('success', 'Mise à jour effectuée avec succès');
                    echo json_encode(array(
                        "statut" => true,
                        "msg" => "Mise à jour effectuée avec succès",
                    ));

                } else {

                	$this->session->set_flashdata('error', 'Mise à jour impossible');
                    echo json_encode(array(
                        "statut" => false,
                        "msg" => "Mise à jour impossible",
                    ));
                }

            } else {

            	$this->session->set_flashdata('error', 'Ancien mot de passe incorrect');
                echo json_encode(array(
                    "statut" => false,
                    "msg" => "Ancien mot de passe incorrect",
                ));
            }

		}
		else
		{	
			$this->session->set_flashdata('error', 'Utilisateur inconnu, déconnectez vous et revenez svp!');
			echo json_encode(array(
				"statut" => false,
				"msg" => "Login / Mot de passe incorrect"
			));
		}


	}

	public function getPrimes()
	{
        header("Access-Control-Allow-Origin: *");
        $mobile = $this->session->userdata("mobile_assure");
		if ($mobile) 
		{
			$query = $this->authModel->getPrimes($mobile);
			echo json_encode(array(
				"statut" => true,
				"vie" => $query["vie"],
				"iard" => $query["iard"]
			));
		}
		else
		{
			echo json_encode(array(
				"statut" => false,
				"data" => "Aucune prime"
			));
		}
	}

	public function succes()
	{
		$data['page_title'] = $this->lang->line('Accueil_title');
		$this->load->view('succes', $data);
	}

	
	public function paiement()
	{
		header("Access-Control-Allow-Origin: *");

		$chiffre = 0;
		$commande = $this->input->post('commande');
		$montant = $this->input->post('montant');
        $ent_id = $this->input->post('ent_id');
        $pays_id = $this->input->post('pays_id');
        $assure = $this->input->post('assure');
        $type_concat = $this->input->post('type_concat');

        $code_int_concat = $this->input->post('code_int_concat');
        $num_police_concat = $this->input->post('num_police_concat');
        $nombre_mois_concat = $this->input->post('nombre_mois_concat');

        $num_avenant_concat = $this->input->post('num_avenant_concat');
        $num_quittance_concat = $this->input->post('num_quittance_concat');

		$commandes = explode("|", $commande);
		$montants = explode("|", $montant);
		$type_concats = explode("|", $type_concat);

		$code_int_concats = explode("|", $code_int_concat);
		$num_police_concats = explode("|", $num_police_concat);
		$nombre_mois_concats = explode("|", $nombre_mois_concat);

		$num_avenant_concats = explode("|", $num_avenant_concat);
		$num_quittance_concats = explode("|", $num_quittance_concat);

		$query = true;
		if ($query)
		{
			$id = false;
			foreach($commandes as $key => $com){
				$getEnt = $this->authModel->getEntByDomaine($type_concats[$key]);
				if ($getEnt)
				{
					$ent_id = $getEnt->id_ent;
					$pays_id = $getEnt->pays_id;
				}

				$chiffre += $montants[$key];
				$id = $this->authModel->insertTrans($num_avenant_concats[$key], $num_quittance_concats[$key], 
				$nombre_mois_concats[$key], $code_int_concats[$key], $num_police_concats[$key], $montants[$key], $com, 
				$ent_id, $pays_id, $assure, $type_concats[$key]);
			}

			if ($id)
			{
				$paramtoken = array(
					"montant" => str_replace(" ", "", $chiffre),
					//"montant" => 5,
					"currency" =>"XOF"
				);

		        $headers = array (
					'X-SYCA-MERCHANDID: C_6033A4C37BA52',
					'X-SYCA-APIKEY: pk_syca_aaa56629b509320441d3483a04147af521cc7978',
					'X-SYCA-REQUEST-DATA-FORMAT: JSON',
					'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
				);
		        $url = "https://dev.sycapay.com/login.php";
		        $ch = curl_init();
		        curl_setopt($ch, CURLOPT_URL,$url);
		        curl_setopt($ch, CURLOPT_VERBOSE, 1);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		        curl_setopt($ch, CURLOPT_POST, 1);
		        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        $data_string = json_encode($paramtoken);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		        $response = json_decode(curl_exec($ch),TRUE);
		        curl_close($ch);
		        if($response['code'] == 0)
		        {
		           $token = $response['token'];
		        }

				$mobileSend = $this->input->post('mobile');
				if (substr($mobileSend, 0, 2) == '07') 
				{	
					$OTP = $this->input->post('OTP');
					$paramsend = array(
						"montant" => $chiffre,
						//"montant" => 10,
						"telephone" => $mobileSend,
						"name" => $this->session->userdata('nom_assure'),
						"pname" => $this->session->userdata('prenoms_assure'),
						"marchandid" => "C_6033A4C37BA52",
						"urlnotif" => site_url('Accueil/getRetours'),
						"numcommande" => $this->input->post('commande'),
						//"token" => $this->input->post('token'),
						"token" => $token,
						"currency" =>"XOF",
						"otp" => $OTP,
					);

					$messageToRead = "Patientez SVP, vous serez notifié dans un instant !";

				}
				else
				{
					$paramsend = array(
						"montant" => $chiffre,
						//"montant" => 5,
						"telephone" => $mobileSend,
						"name" => $this->session->userdata('nom_assure'),
						"pname" => $this->session->userdata('prenoms_assure'),
						"marchandid" => "C_6033A4C37BA52",
						"urlnotif" => site_url('Accueil/getRetours'),
						"numcommande" => $this->input->post('commande'),
						"token" => $token,
						"currency" =>"XOF",
					);

					$messageToRead = "Paiement en cours, consulter votre téléphone pour valider SVP !";
				}

		        //$url = "https://dev.sycapay.net/api/checkoutpay.php";
		        $url = "https://dev.sycapay.com/checkoutpay.php";
		        $ch = curl_init();
		        curl_setopt($ch, CURLOPT_URL,$url);
		        curl_setopt($ch, CURLOPT_VERBOSE, 1);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		        curl_setopt($ch, CURLOPT_POST, 1);
		        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json" ));
		        $data_string = json_encode($paramsend);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		        $response = json_decode(curl_exec($ch),TRUE);
		        curl_close($ch);
		        if($response['code'] == 0)
		        {	
				   $codeSycapay = $response['transactionId'];
		           $retourResponse = ['code' => $codeSycapay, 'message' => $messageToRead];
		           echo json_encode($retourResponse);
		        }
		        else
				{
					echo "";
				}
			}
			else
			{				
				echo " ";
			}
		}
		else
		{
			echo "";
		}

	}

	public function GetStatus()
	{
		header("Access-Control-Allow-Origin: *");
		$ref = $this->input->post('code');

		$paramsend = array("ref" => $ref);
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://dev.sycapay.com/GetStatus.php',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($paramsend),
		  CURLOPT_HTTPHEADER => array("Content-Type: application/json" ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);

		if ($response)
		{	
			if ($response->code == -200)
			{
				echo 'PE';
			}
			elseif ($response->code == 0)
			{
				$purchaseinfo = $response->orderId;
				$montant = $response->montant;
		        $status = $response->code;
		        $reference = $response->transactionID;
		        $contact = $response->mobile;
		        $operatorTrans = $response->operator;
		        $policesConcatees = "";
		        $moisTermes = "";
				$numeEncais = "";

                if ($operatorTrans == "MarterCardCI"){
                    $operatorTrans = "MASTERCARD";
                } elseif ($operatorTrans == "EcobCI"){
                    $operatorTrans = "VISA";
                }

				if ($purchaseinfo)
				{
					$commandes = explode("|", $purchaseinfo);
					$id = false;
					$nbre_mois = 0;
					foreach($commandes as $com)
					{	
						$nbre_mois = $nbre_mois + 1; 
						$id = $this->authModel->modifier($reference, $com, $contact, $status);
						if (isset($status)) 
				        {	
				        	if ($status == '0') 
				        	{
								$getTrans = $this->transactionsModel->getTransByRefPaie($com);
								if ($getTrans) 
								{	
									//Pour la génération du pdf
									$produit = $getTrans->produit_trans;
									$assure = $getTrans->assure_trans;
									$societe = $getTrans->ent_raison;
									$datepaie = $getTrans->date_create_trans;

									if ($nbre_mois > 1){
										$policesConcatees .= "|";
										$moisTermes .= "|";
									    $numeEncais .= "|";
									}

									$policesConcatees .= $getTrans->assure_num_polices;
									$MOBILEMONEY = strtoupper(str_replace(array("CI", " ", "TG"), "", $operatorTrans));
									$TELEASSU = '+'.$getTrans->assureMobile;

									if ($getTrans->produit_trans == 'VIE') 
									{
										$curl = curl_init();
										curl_setopt_array($curl, array(
										  CURLOPT_URL => '197.159.217.53/abi/getListeTerme.php',
										  CURLOPT_RETURNTRANSFER => true,
										  CURLOPT_ENCODING => '',
										  CURLOPT_MAXREDIRS => 10,
										  CURLOPT_TIMEOUT => 0,
										  CURLOPT_FOLLOWLOCATION => true,
										  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										  CURLOPT_CUSTOMREQUEST => 'POST',
										  CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&NOMBPERI=1&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY,
										  CURLOPT_HTTPHEADER => array(
										    'Content-Type: application/x-www-form-urlencoded'
										  ),
										));

										$response = json_decode(curl_exec($curl), true);
										curl_close($curl);
										if ($response['code'] == 1)
										{	
											if (isset($response['data'])) 
											{
										        $quittances = $response['data']['quittances'];

										        if ($quittances) 
										        {
													foreach ($quittances as $gets) 
													{
														$NUMEENCA = $gets['NUMEENCA'];
														$NUMEQUIT = $gets['NUMEQUIT'];
														$MOISTERM = $gets['MOISTERM'];
														$DATEEFFE = $gets['DATEEFFE'];
														$DATEECHE = $gets['DATEECHE'];
													}

													$moisTermes .= $MOISTERM;
												    $numeEncais .= $NUMEENCA;

													$this->transactionsModel->modifierTermesListes($com, $NUMEENCA, 
													$NUMEQUIT, $MOISTERM, $DATEEFFE, $DATEECHE);
										        }

											}

										}
									}
									else
									{
										$curl = curl_init();
										curl_setopt_array($curl, array(
										  CURLOPT_URL => '197.159.217.53/abi/getListeQuittancesDirectes.php',
										  CURLOPT_RETURNTRANSFER => true,
										  CURLOPT_ENCODING => '',
										  CURLOPT_MAXREDIRS => 10,
										  CURLOPT_TIMEOUT => 0,
										  CURLOPT_FOLLOWLOCATION => true,
										  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										  CURLOPT_CUSTOMREQUEST => 'POST',
										  CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY.'&NUMEQUIT='.$getTrans->NUMEQUIT.'&NUMEAVEN='.$getTrans->NUMEAVEN,
										  CURLOPT_HTTPHEADER => array(
										    'Content-Type: application/x-www-form-urlencoded'
										  ),
										));
										$response = json_decode(curl_exec($curl), true);
										curl_close($curl);
										if ($response['code'] == 1)
										{	
											if (isset($response['data'])) 
											{
										        $NUMEENCA = $response['data']['NUMEENCA'];
										        if ($NUMEENCA) 
										        {
												    $numeEncais .= $NUMEENCA;
													$this->transactionsModel->modifierTermesListes($com, $NUMEENCA, 
													$getTrans->NUMEQUIT, NULL, NULL, NULL);
										        }

											}

										}
									}

								}
							}
						}

					}

					if ($reference) 
					{
						require APPPATH . 'libraries/fpdf182/fpdf.php';

						$refpaie = $reference;
						$listeDesPolices = $policesConcatees;
						$moisTermes = $moisTermes;
						$numeEncais = $numeEncais;
						$trans_status =  "Succes";
				        // create file name
						$path = 'assets/generatepdf/';
						if (!is_dir($path))
						{ 
							//create the folder if it's not already exists
							mkdir($path, 0755, TRUE);
				            $path = $path;
				        }

				        $fichiers_def = $refpaie.'.pdf';
				        $html = '<p style="text-align:center";><a href="http://www.sycapay.net">Confirmation de paiement</a></p><br><br><br><br>       	     
								 <span style="text-align:center";>REFERENCE : '.$refpaie.'</span><br><br><br>
								 <span style="text-align:center";>MONTANT : '.$montant.'</span><br><br><br>
								 <span style="text-align:center";>ASSURE : '.$assure.'</span><br><br><br>
								 <span style="text-align:center";>LISTE DES QUITTANCES : '.$listeDesPolices.'</span><br><br><br>
								 <span style="text-align:center";>STATUS PAIEMENT: '.$trans_status.'</span><br><br><br>
								 <span style="text-align:center";>DATE DU PAIEMENT : '.$datepaie.'</span><br><br><br><br>
								 <span style="text-align:center";>SOCIETE : ABI</span><br><br><br>
								 <span style="text-align:center";>PRODUIT : '.$produit.'</span><br><br><br>
								 <span style="text-align:center";>MOIS TERMES : '.$moisTermes.'</span><br><br><br>
								 <span style="text-align:center";>NUMERO ENCAISSEMENT : '.$numeEncais.'</span><br><br><br>
								 ';

						$pdf = new FPDF();
						// Première page
						$pdf->AddPage();
						$pdf->SetFont('Arial','',14);
						$pdf->WriteHTML($html);
						$pdf->Output('F', $path.$fichiers_def);

						echo $refpaie;

					}
					else
					{
						echo $refpaie;
					}

				}
				else
				{
					echo "";
				}
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}

	}

	public function getTokens()
	{
		header("Access-Control-Allow-Origin: *");
		$chiffre = $this->input->post('chiffre');
		$types = $this->input->post('types');
		$nb_primes = intval($this->input->post('nb_primes'));

		$types = explode("|", $types);

		//var_dump($types);

        $paramtoken = array(
			"montant" => str_replace(" ", "", $chiffre),
			//"montant" => 5,
			"currency" =>"XOF"
		);

        $headers = array (
			'X-SYCA-MERCHANDID: C_6033A4C37BA52',
			'X-SYCA-APIKEY: pk_syca_aaa56629b509320441d3483a04147af521cc7978',
			'X-SYCA-REQUEST-DATA-FORMAT: JSON',
			'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
		);

        //$url = "https://dev.sycapay.net/api/login.php";
        $url = "https://dev.sycapay.com/login.php";
        //$url = "https://secure.sycapay.net/login";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data_string = json_encode($paramtoken);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $response = json_decode(curl_exec($ch),TRUE);
        curl_close($ch);

        if($response['code'] == 0)
        {
           $token = $response['token'];
        }

		if ($token)
		{
			$commandes = "";
			for($i = 0; $i < $nb_primes ; $i++){
				$prefix = "";
				if ($types[$i] == "VIE"){
					$prefix = "V_";
				} else if ($types[$i] == "IARD"){
					$prefix = "I_";
				} else {
					exit("");
				}

				$commande = $this->authModel->Guid($prefix);

				if ($i > 0){
					$commandes .= "|";
				}

				$commandes .= $commande;
			}
			//echo $response->pinId;
			echo json_encode(array(
				"token" => $token,
				"commande" => $commandes
				));
		}
		else
		{
			echo "";
		}

	}

	public function getVerification()
	{	
		header("Access-Control-Allow-Origin: *");
		$mobile = $this->input->post('mobile');

		$query = $this->assureModel->getAssure2($mobile);
		if ($query)
		{	
			echo json_encode(array("status" => false));
		}
		else
		{
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/universal_sender',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => 'number='.$mobile.'&brand=ABI',
			  CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/x-www-form-urlencoded',
			    'Cookie: ci_session=582spmfkama1pkam3ehav1588dsiah0p'
			  ),
			));

			$response = json_decode(curl_exec($curl));
			$err = curl_error($curl);
			curl_close($curl);
			if ($response)
			{	
				if ($response->status == TRUE) 
				{
					echo json_encode($response);
				}
				else
				{
					echo json_encode($response);
				}
			}
			else
			{
				echo json_encode(array("status" => false));
			}
		}

	}

	public function resendOtp()
	{
		header("Access-Control-Allow-Origin: *");
		$pinId = $this->input->post('pinId');

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/resend',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'id='.$pinId,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=2044k3c96gk2vjh1vrmma9e8hl3qc0d0'
		  ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);
		if ($response)
		{
			if ($response->status == TRUE)
			{
				echo json_encode($response);
			}
			else
			{
				echo json_encode($response);
			}
		}
		else
		{
			echo "";
		}

	}

	public function ajax_confirmation()
	{
		header("Access-Control-Allow-Origin: *");

		$this->session->sess_destroy();
		$pinId = $this->input->post('pinId');
		$code = $this->input->post('code');
		$mobile = $this->input->post('mobile');

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/match',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  //CURLOPT_POSTFIELDS => 'pinID=1C5D32ACFCAE594BD55D56578F4B4044&code=6391',
		  CURLOPT_POSTFIELDS => 'pinID='.$pinId.'&code='.$code,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=17mf4qh7b36pmh3bn7av23e8ts5opn93'
		  ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);

		if ($response)
		{
			if ($response->status == TRUE)
			{
				$userdata = array('afficheToken' => 'afficheToken');
		        $this->session->set_userdata($userdata);
				//$query = $this->authModel->getPrimes($mobile);
				echo json_encode($response);
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}

	}


	public function value_in($element_name, $xml, $content_only = true) {
		if ($xml == false) {
			return false;
		}
		$found = preg_match('#<'.$element_name.'(?:\s+[^>]+)?>(.*?)'.
			'</'.$element_name.'>#s', $xml, $matches);
		if ($found != false) {
			if ($content_only) {
				return $matches[1];  //ignore the enclosing tags
			} else {
				return $matches[0];  //return the full pattern match
			}
		}
		// No match found: return false.
		return false;
	}


	public function send_email()
	{
		header("Access-Control-Allow-Origin: *");

		require APPPATH . 'libraries/fpdf182/fpdf.php';
		$this->load->library("Mailjet");

		$email = $this->input->post("email");
		$corps = $this->input->post("mail");
		$refpaie = $this->input->post("refpaie");
		$montant = $this->input->post("montant");
		$produit = $this->input->post("produit");
		$listeDesPolices = $this->input->post("listeDesPolices");
		$assure = $this->input->post("assure");
		$societe = $this->input->post("societe");
		$datepaie = $this->input->post("datepaie");
		$content = $this->input->post("content");
		$trans_status =  "Succes";

		$objet = "Confirmation de paiement";
		$this->load->library('PHPExcel');

        $fichiers_def = $refpaie.'.pdf';
		$headers = array ('Content-Type: application/json');
	    $paramsend = [
	          'Messages' => [
	              [
	                  'From' => [
	                      'Email' => "noreply@bfree-ci.com",
	                      'Name' => "ABI CUSTOMER SERVICE"
	                  ],
	                  'To' => [
	                      [
                                'Email' => $email,
                          ]
                      ],
	                  'Subject' => $objet,
	                  'TextPart' => $corps,
	                  'HTMLPart' => $corps,
					  "Attachments" => [
						[
							"ContentType" => "application/pdf",
							"Filename" => $fichiers_def,
							"Base64Content" => base64_encode(file_get_contents(base_url().$path.$fichiers_def)),
						]
					],
				]
              ]
           ];

                      
       $url = "https://api.mailjet.com/v3.1/send";
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_VERBOSE, 1);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_USERPWD, "a6be9ae172aebc0bcea021cf75d5a9cf:4f8d8ed39af0aa99ac63fa5a52e8f0bc");
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($paramsend));
       $response = json_decode(curl_exec($ch));
       $err = curl_error($ch);
       curl_close ($ch);
	   echo $email;
	}


	public function logout(){
	    $this->session->sess_destroy();
	    redirect("Accueil");
    }
}
