<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model', 'authModel');
		$this->load->model('Assure_model', 'assureModel');
		$this->load->model('Transactions_model', 'transactionsModel');
		$this->load->model('Entreprise_model', 'entrepriseModel');
	}

	public function index()
	{	
		$data['page_title'] = $this->lang->line('Accueil_title');
		//$data['entreprises'] = $this->entrepriseModel->getAllEntreprises();
		$data['entreprises'] = '';
		$data['getTransactions'] = $this->transactionsModel->getTransactions();
		$this->load->view('accueil', $data);
	}

	public function getFatures($atlantis_ref)
	{	
		if ($atlantis_ref)
		{	
			$nbre_mois = 1;
			$trans = $this->transactionsModel->getTransByRefPaie($atlantis_ref);
			if ($trans)
			{	
				$this->session->set_userdata(array("getFatures" => $trans));
				$data['page_title'] = $this->lang->line('Accueil_title');
				$data['assure'] = $trans->assure_trans == "" || is_null($trans->assure_trans) ? "N/A" : $trans->assure_trans;
				$data['societe'] = $trans->ent_raison == "" || is_null($trans->ent_raison) ? "N/A" : $trans->ent_raison;
				$data['produit'] = $trans->produit_trans == "" || is_null($trans->produit_trans) ? "N/A" : $trans->produit_trans;
				//$data['nbre_mois'] = $trans->nbre_mois == "" || is_null($trans->nbre_mois) ? "N/A" : $trans->nbre_mois;
				$data['nbre_mois'] = $nbre_mois;
				$data['montant'] = $trans->montant_trans;
				$data['refpaie'] = $atlantis_ref;
				$data['datepaie'] = $trans->date_create_trans;

				$this->load->view('succes', $data);
			}
			else
			{
				$this->session->set_flashdata('error', 'Ce paiement est inexistant');
				redirect(site_url("Accueil"));
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Ce paiement est inexistant');
			redirect(site_url("Accueil"));
		}

	}

	public function inscription()
	{
		$data['page_title'] = $this->lang->line('Accueil_title');

		$data['entreprises'] = '';
		//$data['entreprises'] = $this->entrepriseModel->getAllEntreprises();

		$this->load->view('inscription', $data);
	}

	public function inscrire()
	{
		$pays_id = $this->input->post('pays_id');
		$mobile = $this->input->post('mobile');
		$nom = $this->input->post('nom');
		$prenoms = $this->input->post('prenoms');
		$sexe = $this->input->post('sexe');
		$password = $this->input->post('password');

		$query = true;
		if ($pays_id || $mobile || $nom || $prenoms || $sexe || $password) 
		{

			$id = $this->assureModel->inscrire($pays_id, $mobile, $nom, $prenoms, $sexe, $password);
			$this->session->set_flashdata('success', $this->lang->line("notif_succes"));
			redirect(site_url("Accueil"));
		}
		else
		{
			echo 'Erreur inscription';
		}


	}

	public function login()
	{
        header("Access-Control-Allow-Origin: *");
        $mobile = $this->input->post('mobile');
		$password = $this->input->post('password');
		$query = true;
		if ($mobile || $password) 
		{
			$ret = $this->assureModel->getAssure($mobile, $password);
			if ($ret) 
			{
				$userdata = array('afficheToken' => 'afficheToken');
				$this->session->set_userdata((array)$ret);
				$query = $this->authModel->getPrimes($mobile);
				echo json_encode(array(
					"statut" => true,
					"data" => $ret,
					"vie" => $query["vie"],
					"iard" => $query["iard"]
				));
			}
			else
			{
				echo json_encode(array(
				"statut" => false,
				"data" => "Login / Mot de passe incorrect"
				));
			}
			
		}
		else
		{
			echo json_encode(array(
				"statut" => false,
				"data" => "Login / Mot de passe obligatoire"
			));
		}


	}

	public function update_pass()
	{
        header("Access-Control-Allow-Origin: *");

        $mobile = $this->session->userdata('mobile_assure');
		$oldpassword = $this->input->post('oldpassword');
		$password = $this->input->post('password');


		//$query = $this->authModel->isCommande($purchaseinfo, $montant);
		$query = true;

		if ($mobile || $oldpassword || $password) {


			//$query = $query && $this->authModel->is_existe($com);
			$ret = $this->assureModel->getAssure($mobile, $oldpassword);

            if ($ret){
                $query = $this->assureModel->updatePass($mobile, $password);

                if ($query){

                    echo json_encode(array(
                        "statut" => true,
                        "msg" => "Mise à jour effectuée avec succès",
                    ));

                } else {

                    echo json_encode(array(
                        "statut" => false,
                        "msg" => "Mise à jour impossible",
                    ));
                }

            } else {

                echo json_encode(array(
                    "statut" => false,
                    "msg" => "Ancien mot de passe incorrect",
                ));
            }

		}
		else
		{
			echo json_encode(array(
				"statut" => false,
				"msg" => "Login / Mot de passe incorrect"
			));
		}


	}

	public function getPrimes()
	{
        header("Access-Control-Allow-Origin: *");

        $mobile = $this->session->userdata("mobile_assure");


		//$query = $this->authModel->isCommande($purchaseinfo, $montant);
		$query = true;

		if ($mobile) {


			//$query = $query && $this->authModel->is_existe($com);
			$ret = $this->assureModel->getAssure2($mobile);


			$userdata = array('afficheToken' => 'afficheToken');
			$this->session->set_userdata((array)$ret);
			$query = $this->authModel->getPrimes($mobile);

			echo json_encode(array(
				"statut" => true,
				"data" => $ret,
				"vie" => $query["vie"],
				"iard" => $query["iard"]
			));
		}
		else
		{
			echo json_encode(array(
				"statut" => false,
				"data" => "Login / Mot de passe incorrect"
			));
		}


	}

	public function succes()
	{
		$data['page_title'] = $this->lang->line('Accueil_title');

		$this->load->view('succes', $data);
	}

	public function getRetours()
	{	
		$purchaseinfo = $this->input->post('purchaseinfo');
		$montant = $this->input->post('montant');
        $status = $this->input->post('status');
        $reference = $this->input->post('reference');
        $contact = $this->input->post('contact');
        $policesConcatees = "";

		if ($purchaseinfo)
		{
			$commandes = explode("|", $purchaseinfo);
			$id = false;
			$nbre_mois = 0;
			foreach($commandes as $com)
			{	
				$nbre_mois = $nbre_mois + 1; 
				$id = $this->authModel->modifier($reference, $com, $contact, $status);
				if (isset($status)) 
		        {	
		        	if ($status == '0') 
		        	{
						$getTrans = $this->transactionsModel->getTransByRefPaie($com);
						if ($getTrans) 
						{	

							if ($nbre_mois > 1){
								$policesConcatees .= "|";
							}
							$policesConcatees .= $getTrans->assure_num_polices;

							$MOBILEMONEY = strtoupper(str_replace(array("CI", " ", "TG"), "", $this->input->post('provider')));

							$TELEASSU = '+'.$getTrans->assureMobile;
							$curl = curl_init();
							curl_setopt_array($curl, array(
							  CURLOPT_URL => '197.159.217.53/abi/getListeTerme.php',
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => '',
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 0,
							  CURLOPT_FOLLOWLOCATION => true,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => 'POST',
							  CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&NOMBPERI=%201&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY,
							  CURLOPT_HTTPHEADER => array(
							    'Content-Type: application/x-www-form-urlencoded'
							  ),
							));

							$response = json_decode(curl_exec($curl), true);
							curl_close($curl);
							if ($response['code'] == 1)
							{	
								if (isset($response['data'])) 
								{
							        $quittances = $response['data']['quittances'];

							        if ($quittances) 
							        {
										foreach ($quittances as $gets) 
										{
											$NUMEENCA = $gets['NUMEENCA'];
											$NUMEQUIT = $gets['NUMEQUIT'];
											$MOISTERM = $gets['MOISTERM'];
											$DATEEFFE = $gets['DATEEFFE'];
											$DATEECHE = $gets['DATEECHE'];
										}

										$this->transactionsModel->modifierTermesListes($com, $NUMEENCA, $NUMEQUIT, 
										$MOISTERM, $DATEEFFE, $DATEECHE);
							        }

								}

							}

						}
					}
				}

			}

			if ($id)
			{
				$trans = $this->transactionsModel->getTransByRefPaie($reference);
				if ($trans)
				{	
					$ret = $this->assureModel->getAssure2($trans->assureMobile);
					$userdata = array('afficheToken' => 'afficheToken');
					$this->session->set_userdata((array)$ret);

					$data['page_title'] = $this->lang->line('Accueil_title');
					$data['assure'] = $trans->assure_trans == "" || is_null($trans->assure_trans) ? "N/A" : $trans->assure_trans;
					$data['societe'] = $trans->ent_raison == "" || is_null($trans->ent_raison) ? "N/A" : $trans->ent_raison;
					$data['produit'] = $trans->produit_trans == "" || is_null($trans->produit_trans) ? "N/A" : $trans->produit_trans;
					//$data['nbre_mois'] = $trans->nbre_mois == "" || is_null($trans->nbre_mois) ? "N/A" : $trans->nbre_mois;
					$data['nbre_mois'] = $nbre_mois;
					$data['montant'] = $montant;
					$data['refpaie'] = $reference;
					$data['datepaie'] = $trans->date_create_trans;
					$data['listeDesPolices'] = $policesConcatees;

					$this->load->view('succes', $data);
				}
				else
				{
					$this->session->set_flashdata('error', 'Ce paiement est inexistant');
					redirect(site_url("Accueil"));
				}
			}
			else
			{				
				//echo 'Impossible de changer le status';
				$this->session->set_flashdata('error', 'Impossible de changer le status');
			    redirect(site_url("Accueil"));
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Ce paiement est inexistant');
			redirect(site_url("Accueil"));
		}

	}

	public function paiement()
	{
		header("Access-Control-Allow-Origin: *");

		$commande = $this->input->post('commande');
		$montant = $this->input->post('montant');
        $ent_id = $this->input->post('ent_id');
        $pays_id = $this->input->post('pays_id');
        $assure = $this->input->post('assure');
        $type_concat = $this->input->post('type_concat');

        $code_int_concat = $this->input->post('code_int_concat');
        $num_police_concat = $this->input->post('num_police_concat');

		$commandes = explode("|", $commande);
		$montants = explode("|", $montant);
		$type_concats = explode("|", $type_concat);

		$code_int_concats = explode("|", $code_int_concat);
		$num_police_concats = explode("|", $num_police_concat);

		$query = true;
		if ($query)
		{
			$id = false;
			foreach($commandes as $key => $com){
				$getEnt = $this->authModel->getEntByDomaine($type_concats[$key]);
				if ($getEnt)
				{
					$ent_id = $getEnt->id_ent;
					$pays_id = $getEnt->pays_id;
				}
				$id = $this->authModel->insertTrans($code_int_concats[$key], $num_police_concats[$key], $montants[$key], $com, $ent_id, $pays_id, $assure, $type_concats[$key]);
			}
			if ($id)
			{
				echo ($id);
			}
			else
			{				
				echo "Erreur insertion";
			}
		}
		else
		{
			echo "";
		}

	}

	public function getTokens()
	{
		header("Access-Control-Allow-Origin: *");
		$chiffre = $this->input->post('chiffre');
		$types = $this->input->post('types');
		$nb_primes = intval($this->input->post('nb_primes'));

		$types = explode("|", $types);

		//var_dump($types);

        $paramtoken = array(
			"montant" => (int)$chiffre,
			"curr" =>"XOF"
		);

        $headers = array (
			'X-SYCA-MERCHANDID: C_5696ADED4C7FD',
			'X-SYCA-APIKEY: pk_syca_c29210c11f69e50e16a0ba472638b9d05641ae24',
			'X-SYCA-REQUEST-DATA-FORMAT: JSON',
			'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
		);

        $url = "https://secure.sycapay.com/login";
        //$url = "https://secure.sycapay.net/login";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data_string = json_encode($paramtoken);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $response = json_decode(curl_exec($ch),TRUE);
        curl_close($ch);
        if($response['code'] == 0)
        {
           $token = $response['token'];
        }

		if ($token)
		{
			$commandes = "";
			for($i = 0; $i < $nb_primes ; $i++){
				$prefix = "";
				if ($types[$i] == "VIE"){
					$prefix = "V_";
				} else if ($types[$i] == "IARD"){
					$prefix = "I_";
				} else {
					exit("");
				}

				$commande = $this->authModel->Guid($prefix);

				if ($i > 0){
					$commandes .= "|";
				}

				$commandes .= $commande;
			}
			//echo $response->pinId;
			echo json_encode(array(
				"token" => $token,
				"commande" => $commandes
				));
		}
		else
		{
			echo "";
		}

	}

	public function getVerification()
	{	
		header("Access-Control-Allow-Origin: *");
		$mobile = $this->input->post('mobile');
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/universal_sender',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'number='.$mobile.'&brand=ABI',
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=582spmfkama1pkam3ehav1588dsiah0p'
		  ),
		));


		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => 'https://apis.bnifree.net/V1.0/bnixpressapi/index.php/api/Otp/create',
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => 'number='.$mobile,
		//   CURLOPT_HTTPHEADER => array(
		//     "Accept: */*",
		//     "Accept-Encoding: gzip, deflate",
		//     "Cache-Control: no-cache",
		//     "Connection: keep-alive",
		//     "Content-Length: 15",
		//     "Content-Type: application/x-www-form-urlencoded",
		//     "cache-control: no-cache"
		//   ),
		// ));
		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);
		if ($response)
		{	
			if ($response->status == TRUE) 
			{
				echo json_encode($response);
			}
			else
			{
				echo json_encode($response);
			}
		}
		else
		{
			echo json_encode(array("status" => false));
		}

	}

	public function resendOtp()
	{
		header("Access-Control-Allow-Origin: *");
		$pinId = $this->input->post('pinId');
		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => 'http://apis.bnifree.net/V1.0/bnixpressapi/index.php/api/Otp/resend',
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => 'id='.$pinId,
		//   CURLOPT_HTTPHEADER => array(
		//     'Content-Type: application/x-www-form-urlencoded'
		//   ),
		// ));

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/resend',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'id='.$pinId,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=2044k3c96gk2vjh1vrmma9e8hl3qc0d0'
		  ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);
		if ($response)
		{
			if ($response->status == TRUE)
			{
				echo json_encode($response);
			}
			else
			{
				echo json_encode($response);
			}
		}
		else
		{
			echo "";
		}

	}

	public function ajax_confirmation()
	{
		header("Access-Control-Allow-Origin: *");

		$this->session->sess_destroy();
		$pinId = $this->input->post('pinId');
		$code = $this->input->post('code');
		$mobile = $this->input->post('mobile');

  		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => "apis.bnifree.net/V1.0/bnixpressapi/index.php/api/Otp/match",
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => 'pinID='.$pinId.'&code='.$code,
		//   CURLOPT_HTTPHEADER => array(
		//     'Content-Type: application/x-www-form-urlencoded'
		//   ),
		// ));

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Otp/match',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  //CURLOPT_POSTFIELDS => 'pinID=1C5D32ACFCAE594BD55D56578F4B4044&code=6391',
		  CURLOPT_POSTFIELDS => 'pinID='.$pinId.'&code='.$code,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded',
		    'Cookie: ci_session=17mf4qh7b36pmh3bn7av23e8ts5opn93'
		  ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);

		if ($response)
		{
			if ($response->status == TRUE)
			{
				$userdata = array('afficheToken' => 'afficheToken');
		        $this->session->set_userdata($userdata);
				//$query = $this->authModel->getPrimes($mobile);
				echo json_encode($response);
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}

	}


	public function value_in($element_name, $xml, $content_only = true) {
		if ($xml == false) {
			return false;
		}
		$found = preg_match('#<'.$element_name.'(?:\s+[^>]+)?>(.*?)'.
			'</'.$element_name.'>#s', $xml, $matches);
		if ($found != false) {
			if ($content_only) {
				return $matches[1];  //ignore the enclosing tags
			} else {
				return $matches[0];  //return the full pattern match
			}
		}
		// No match found: return false.
		return false;
	}


	public function send_email(){

		header("Access-Control-Allow-Origin: *");

		$email = $this->input->post("email");
		$corps = $this->input->post("mail");
		$refpaie = $this->input->post("refpaie");
		$montant = $this->input->post("montant");
		$produit = $this->input->post("produit");
		$listeDesPolices = $this->input->post("listeDesPolices");
		$assure = $this->input->post("assure");
		$societe = $this->input->post("societe");

		$objet = "Confirmation de paiement";
		$this->load->library("Mailjet");

		$this->load->library('PHPExcel');
	        // create file name
			$path = 'assets/generatepdf/';
			if (!is_dir($path))
			{ 
				//create the folder if it's not already exists
				mkdir($path, 0755, TRUE);
                $path = $path;
            }

	          $fileName = 'ABI-'.time();  
	          // load excel library
	          $objPHPExcel = new PHPExcel();
	          $objPHPExcel->setActiveSheetIndex(0);
	          // set Header
	          $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Reference');
	          $objPHPExcel->getActiveSheet()->SetCellValue('B1', $this->lang->line('montant_label'));
	          $objPHPExcel->getActiveSheet()->SetCellValue('C1', $this->lang->line('produit_label'));
	          $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Status');
	          $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ListePolices'); 
	          $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date'); 
	          $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Assure'); 
	          $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Societe');       
	          // set Row      
	          // set Row
              $objPHPExcel->getActiveSheet()->SetCellValue('A2', $refpaie);
              $objPHPExcel->getActiveSheet()->SetCellValue('B2', $montant);
              $objPHPExcel->getActiveSheet()->SetCellValue('C2', $produit);

              $trans_status =  "Succès";
 
              $objPHPExcel->getActiveSheet()->SetCellValue('D2', $trans_status);
              $objPHPExcel->getActiveSheet()->SetCellValue('E2', $listeDesPolices);
              $objPHPExcel->getActiveSheet()->SetCellValue('F2', date("Y-m-d H:i:s"));
              $objPHPExcel->getActiveSheet()->SetCellValue('G2', $assure);
              $objPHPExcel->getActiveSheet()->SetCellValue('H2', $societe);

	          $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	          $getData = $objWriter->save($path.$fileName .'.xlsx');
	          
			  $headers = array ('Content-Type: application/json');
		      $paramsend = [
		          'Messages' => [
		              [
		                  'From' => [
		                      'Email' => "noreply@bfree-ci.com",
		                      'Name' => "ABI CUSTOMER SERVICE"
		                  ],
		                  'To' => [
		                      [
	                                'Email' => $email,
	                          ]
	                      ],
		                  'Subject' => $objet,
		                  'TextPart' => $corps,
		                  'HTMLPart' => $corps,
						  "Attachments" => [
							[
								"ContentType" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
								"Filename" => "document.xlsx",
								"Base64Content" => base64_encode(file_get_contents(base_url().$path.$fileName .'.xlsx')),
							]
						],
					]
	              ]
	           ];
                      
       $url = "https://api.mailjet.com/v3.1/send";
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_VERBOSE, 1);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_USERPWD, "a6be9ae172aebc0bcea021cf75d5a9cf:4f8d8ed39af0aa99ac63fa5a52e8f0bc");
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($paramsend));
       $response = json_decode(curl_exec($ch));
       $err = curl_error($ch);
       curl_close ($ch);
	   echo $email;
	}

	public function logout(){
	    $this->session->sess_destroy();
	    redirect("Accueil");
    }
}
