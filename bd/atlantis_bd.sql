-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 05 fév. 2021 à 12:39
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `atlantis_bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `affectation`
--

CREATE TABLE `affectation` (
  `id_affectation` int(11) NOT NULL,
  `date_affectation` datetime NOT NULL,
  `etat_affectation` varchar(2) DEFAULT NULL,
  `users_fk` int(11) DEFAULT NULL,
  `agences_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `affectation`
--

INSERT INTO `affectation` (`id_affectation`, `date_affectation`, `etat_affectation`, `users_fk`, `agences_fk`) VALUES
(1, '2020-09-04 13:30:18', 'I', 4, 3),
(2, '2020-09-04 13:56:02', 'A', 4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `agence`
--

CREATE TABLE `agence` (
  `id_agence` int(11) NOT NULL,
  `nom_agence` varchar(250) CHARACTER SET utf8 NOT NULL,
  `etat_agence` varchar(2) CHARACTER SET utf8 NOT NULL COMMENT 'A=Actif, I=Inactif',
  `localisation_agence` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `date_create_agence` datetime NOT NULL,
  `entreprise_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `agence`
--

INSERT INTO `agence` (`id_agence`, `nom_agence`, `etat_agence`, `localisation_agence`, `date_create_agence`, `entreprise_fk`) VALUES
(1, 'ABIDJAN', 'A', 'PLATEAU RUE DU COMMERCE', '2020-09-03 22:07:00', 1),
(2, 'BOUAKE', 'A', 'IMMEUBLE WATAO', '2020-09-03 22:08:56', 1),
(3, 'YAMOUSSOKRO', 'A', 'PRES DE L\'INHB', '2020-09-03 22:10:49', 2),
(4, 'DALOA', 'A', 'APRES LE GRAND MARCHE', '2020-09-03 22:11:47', 2),
(5, 'KOROGHO', 'A', 'RÉSIDENCE AGC', '2020-09-03 22:37:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `assures`
--

CREATE TABLE `assures` (
  `id_assure` int(11) NOT NULL,
  `nom_assure` varchar(50) DEFAULT NULL,
  `prenoms_assure` varchar(100) DEFAULT NULL,
  `mobile_assure` varchar(50) DEFAULT NULL,
  `sexe_assure` varchar(10) DEFAULT NULL,
  `pays_id` varchar(11) DEFAULT NULL,
  `password_assure` varchar(150) DEFAULT NULL,
  `date_enregistrement_assure` datetime DEFAULT NULL,
  `etat_assure` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assures`
--

INSERT INTO `assures` (`id_assure`, `nom_assure`, `prenoms_assure`, `mobile_assure`, `sexe_assure`, `pays_id`, `password_assure`, `date_enregistrement_assure`, `etat_assure`) VALUES
(1, 'test', 'test', NULL, 'M', NULL, '21232f297a57a5a743894a0e4a801fc3', '2021-01-12 18:55:13', 'A'),
(2, 'test', 'test', NULL, 'M', NULL, '21232f297a57a5a743894a0e4a801fc3', '2021-01-12 18:57:12', 'A'),
(3, 'GNALY', 'Philippe', '22890029922', 'M', 'TG', 'ab4f63f9ac65152575886860dde480a1', '2021-01-12 18:58:02', 'A'),
(4, 'Konate', 'Abou', '22507941295', 'M', 'CI', 'fe01ce2a7fbac8fafaed7c982a04e229', '2021-02-04 10:33:56', 'A'),
(5, 'Kone', 'Sally', '22508321387', 'F', 'CI', 'ab4f63f9ac65152575886860dde480a1', '2021-02-04 12:50:23', 'A');

-- --------------------------------------------------------

--
-- Structure de la table `demandes`
--

CREATE TABLE `demandes` (
  `id_demande` int(11) NOT NULL,
  `mobile_demande` varchar(255) DEFAULT NULL,
  `montant_demande` varchar(255) DEFAULT NULL,
  `details_demande` varchar(255) DEFAULT NULL,
  `type_demande` varchar(255) DEFAULT NULL,
  `beneficiaire` varchar(255) DEFAULT NULL,
  `numero_sinistre` varchar(50) DEFAULT NULL,
  `statut_demande` varchar(10) DEFAULT NULL,
  `date_demande` varchar(255) DEFAULT NULL,
  `id_lot` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demandes`
--

INSERT INTO `demandes` (`id_demande`, `mobile_demande`, `montant_demande`, `details_demande`, `type_demande`, `beneficiaire`, `numero_sinistre`, `statut_demande`, `date_demande`, `id_lot`) VALUES
(1, '96321301', '12000', 'skjeriuer', NULL, 'SAC CACAO', 'A158', 'P', '2021-01-28 16:04:14', '6012e07e62710'),
(2, '96321303', '7500', 'pozer0', NULL, 'SAC IGNAME', 'KOI874', 'P', '2021-01-28 16:04:14', '6012e07e62710'),
(3, '96321399', '8000', 'ezakj1500,5', NULL, 'SAC SORGHO', 'LK963', 'P', '2021-01-28 16:04:14', '6012e07e62710'),
(4, '96321388', '69333', 'az75,25', NULL, 'SAC COTON', 'POU789', 'P', '2021-01-28 16:04:14', '6012e07e62710'),
(5, '96321341', '4500', 'erzkljrez1500', NULL, 'SAC BANANE', 'HAT854', 'P', '2021-01-28 16:04:14', '6012e07e62710'),
(6, '96321355', '8933', 'A258kzée', NULL, 'SAC CAFE', 'MLPO8963', 'P', '2021-01-28 16:04:14', '6012e07e62710');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id_ent` int(11) NOT NULL,
  `code_ent` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `ent_rccm` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ent_raison` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `ent_etat` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `ent_contact` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `ent_stat_jur` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `domaine_activite` varchar(250) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Domaine activités ASSURANCE(IARD, VIE) etc',
  `ent_adresse` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ent_date_create` datetime DEFAULT NULL,
  `pays_id` varchar(10) DEFAULT NULL,
  `Ent_Secret_Key` varchar(600) CHARACTER SET latin1 DEFAULT NULL,
  `Ent_Merchand_Id` varchar(250) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id_ent`, `code_ent`, `ent_rccm`, `ent_raison`, `ent_etat`, `ent_contact`, `ent_stat_jur`, `domaine_activite`, `ent_adresse`, `ent_date_create`, `pays_id`, `Ent_Secret_Key`, `Ent_Merchand_Id`) VALUES
(1, '5DA8593812DF8', 'CI- ABJ- 1998- B-229 343', 'Atlantic Assurance', 'A', '20209878', 'SA', 'VIE', 'Abidjan Plateau, Avenue Marchand, Immeuble SCIAM', '2019-10-17 12:06:16', 'CI', 'sk_syca_54679a9b25449b0dbf7aca72c836a75f607370c6', 'BF_5EDE096E1454F'),
(2, '5EDE096E13C46', 'MO2020', 'Atlantic Banque', 'A', '01020301', 'SA', 'SANTE', 'COCODY', '2020-06-08 09:48:30', 'SN', 'sk_syca_54679a9b25449b0dbf7aca72c836a75f607370c6', 'BF_5EDE096E1454F'),
(3, '5DA8593812DF8', 'CI- ABJ- 1998- B-229 343', 'Atlantic Assurance', 'A', '20209878', 'SA', 'VIE', 'Abidjan Plateau, Avenue Marchand, Immeuble SCIAM', '2019-10-17 12:06:16', 'TG', 'sk_syca_54679a9b25449b0dbf7aca72c836a75f607370c6', 'BF_5EDE096E1454F');

-- --------------------------------------------------------

--
-- Structure de la table `lots`
--

CREATE TABLE `lots` (
  `id_lot` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_ajout_lot` varchar(50) DEFAULT NULL,
  `nbre_remb` varchar(50) DEFAULT NULL,
  `validation_1` varchar(50) DEFAULT NULL,
  `validation_2` varchar(50) DEFAULT NULL,
  `date_valid_1` varchar(50) DEFAULT NULL,
  `date_valid_2` varchar(50) DEFAULT NULL,
  `statut_lot` varchar(10) DEFAULT NULL,
  `etat_lot` varchar(10) DEFAULT NULL,
  `type_lot` varchar(50) DEFAULT NULL,
  `ent_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lots`
--

INSERT INTO `lots` (`id_lot`, `user_id`, `date_ajout_lot`, `nbre_remb`, `validation_1`, `validation_2`, `date_valid_1`, `date_valid_2`, `statut_lot`, `etat_lot`, `type_lot`, `ent_fk`) VALUES
('5f7b3fc7508e6', 14, '2020-10-05 15:46:15', '1', NULL, NULL, NULL, NULL, 'P2', 'A', 'vie', 1),
('5f7b405c2a687', 1, '2020-10-05 15:48:45', '1', NULL, NULL, NULL, NULL, 'P1', 'A', 'vie', 1),
('5f7b41431214d', 1, '2020-10-05 15:52:35', '1', NULL, NULL, NULL, NULL, 'P1', 'A', 'vie', 2),
('5f7b422fab493', 1, '2020-10-05 15:56:31', '1', NULL, NULL, NULL, NULL, 'P1', 'A', 'vie', 2),
('5f96de183eee2', 14, '2020-10-26 14:32:57', '1', NULL, NULL, NULL, NULL, 'P1', 'A', NULL, 1),
('5f96de3ee66e4', 14, '2020-10-26 14:33:35', '1', NULL, NULL, NULL, NULL, 'P1', 'A', NULL, 1),
('5f96df4419102', 14, '2020-10-26 14:37:56', '1', NULL, NULL, NULL, NULL, 'P2', 'A', NULL, 1),
('5f96df94bf81d', 14, '2020-10-26 14:39:16', '1', NULL, NULL, NULL, NULL, 'P1', 'A', NULL, 1),
('5f96e12e60888', 14, '2020-10-26 14:46:06', '1', '13', '13', '2020-10-26 15:44:53', '2020-10-26 15:45:28', 'S', 'A', NULL, 1),
('6012e07e62710', 4, '2021-01-28 16:04:15', '6', '4', NULL, '2021-01-28 16:05:19', NULL, 'P1', 'A', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `pays_id` varchar(6) NOT NULL,
  `pays_lib` varchar(50) DEFAULT NULL,
  `pays_indicatif` varchar(10) DEFAULT NULL,
  `pays_etat` varchar(2) DEFAULT NULL,
  `pays_devise` varchar(255) DEFAULT NULL,
  `pays_logo_affiche` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`pays_id`, `pays_lib`, `pays_indicatif`, `pays_etat`, `pays_devise`, `pays_logo_affiche`) VALUES
('CI', 'CÔTE D\'IVOIRE', '225', 'A', 'XOF', 'assets/atlantis_assets/dist/images/CI.png'),
('BJ', 'BENIN', '229', 'A', 'XOF', 'assets/atlantis_assets/dist/images/BJ.png'),
('TG', 'TOGO', '228', 'A', 'XOF', 'assets/atlantis_assets/dist/images/TG.png'),
('SN', 'SENEGAL', '221', 'A', 'XOF', 'assets/atlantis_assets/dist/images/SN.png');

-- --------------------------------------------------------

--
-- Structure de la table `pharmacies`
--

CREATE TABLE `pharmacies` (
  `id_pharm` int(100) NOT NULL,
  `localite_pharm` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `libelle_pharm` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresse_pharm` varchar(1000) CHARACTER SET ascii NOT NULL,
  `latitude_pharm` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `longitude_pharm` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `telephone_pharm` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `etat_pharm` varchar(2) COLLATE latin1_general_ci NOT NULL DEFAULT 'I',
  `date_ajout_pharm` datetime NOT NULL,
  `ville_id` int(100) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Déchargement des données de la table `pharmacies`
--

INSERT INTO `pharmacies` (`id_pharm`, `localite_pharm`, `libelle_pharm`, `adresse_pharm`, `latitude_pharm`, `longitude_pharm`, `telephone_pharm`, `etat_pharm`, `date_ajout_pharm`, `ville_id`) VALUES
(1, 'ABOBO', 'PHARMACIE ABOBO BELLEVILLE', 'Route Du Zoo Carrefour Menuiserie Pres De La Commune Centrale D  abobo', '5.433820', '-3.988437', '01 35 12 24', 'A', '2019-02-08 13:17:26', 0),
(2, 'ABOBO', 'PHARMACIE ABOBO SANTE', 'Face a la Maternite de la Mairie', '5.4237784', '-4.0175844', '24391384', 'A', '2019-02-08 13:17:26', 0),
(3, 'ABOBO', 'PHARMACIE AMINA', 'Abobo Avocatier Pres de la Maternite  Henriette Konan Bedie Avocatier Rue 144', '5.4399265', '-4.0249263', ' 24 39 95 65   07 67 68 98', 'A', '2019-02-08 13:17:26', 0),
(4, 'ABOBO', 'PHARMACIE AVOCATIER MARCHE ', 'Avocatier marche', '5.4407097', '-4.0348455', '24 49 70 19 / 09 30 38 52', 'A', '2019-02-08 13:17:26', 0),
(5, 'ABOBO', 'PHARMACIE AZUR', 'Plateau Dokoui Route du Zoo ', '5.399786', '-4.006335', ' 24 39 46 14', 'A', '2019-02-08 13:17:26', 0),
(6, 'ABOBO', 'PHARMACIE BELLE CITE ', 'Abobo quartier BC  Terminus des woros woros', '5.446630', '-4.020664', ' 24 48 31 52 ', 'A', '2019-02-08 13:17:26', 0),
(7, 'ABOBO', 'PHARMACIE CARRFOUR ANADOR', 'Ront point  Carrefour Anador Route des Rails', '5.336645', '-4.101000', ' 24 48 02 39 / 05 87 51 88 ', 'A', '2019-02-08 13:17:26', 0),
(8, 'ABOBO', 'PHARMACIE DE LA CITE', 'Abobo gare quartier Sogefiha', '5.436942', '-4.015068', '  24 39 03 33', 'A', '2019-02-08 13:17:26', 0),
(9, 'ABOBO', 'PHARMACIE DE LA MAIRIE ABOBO', 'Voie non bitumee  Mairie', ' 5.420087', '-4.015554', '24 39 32 48', 'A', '2019-02-08 13:17:26', 0),
(10, 'ABOBO', 'PHARMACIE DE LA ME', 'Abobo Gare  entre la Mairie et la Gendarmerie  Voie Express Anyama', ' 5.423435', '-4.018521', '24 39 06 02', 'A', '2019-02-08 13:17:26', 0),
(11, 'ABOBO', 'PHARMACIE DE LA PLAQUE II ', 'Abobo Plaque II  non loin Carrefour du Plaque II', ' 5.437270', '-4.006042', ' 24 48 09 90', 'A', '2019-02-08 13:17:26', 0),
(12, 'ABOBO', 'PHARMACIE DE L ETOILE', 'Autoroute d Abobo Abidjan', '5.427175', '-4.019853', ' 24 39 11 54 / 24 39 08 72', 'A', '2019-02-08 13:17:26', 0),
(13, 'ABOBO', 'PHARMACIE DE L HABITAT ', 'A cote du marche de l Habitat', '5.4381535', '-4.0434347', ' 24 39 94 67 ', 'A', '2019-02-08 13:17:26', 0),
(14, 'ABOBO', 'PHARMACIE DIVINE ESPERANCE', 'Abobo Sagbe entre Paroisse Saint Joseph Epoux et Commissariat du 21eme Arrond', '5.424657', '-4.024112', '00225 24 39 78 92', 'A', '2019-02-08 13:17:26', 0),
(15, 'ABOBO', 'PHARMACIE DU CENTRE', 'abobo avocatier voie express anyama pres de l hotel du centre et de l eglise universelle du royaume de Dieu', '5.434145', '-4.026725', '47 85 90 09 / 40 27 54 76', 'A', '2019-02-08 13:17:26', 0),
(16, 'ABOBO', 'PHARMACIE DU DOKUI', 'Rue Hotel Bar Abobote  Abobo', ' 5.399598', '-4.006527', ' 24 39 04 83/47904063', 'A', '2019-02-08 13:17:26', 0),
(17, 'ABOBO', 'PHARMACIE DU MONASTERE', 'Route d Abobo Baoule a 500 m Samake pres du Monastere Sainte Claire', '5.415581', '-4.000753', ' 24 49 63 00', 'A', '2019-02-08 13:17:26', 0),
(18, 'ABOBO', 'PHARMACIE DU RAIL', 'Abobo SAGBE  Derriere Rails', '5.414755', '-4.023829', ' 24 39 01 82 ', 'A', '2019-02-08 13:17:26', 0),
(19, 'ABOBO', 'PHARMACIE D ABOBO BAOULE ', 'En face du 34eme Arrondissement  Quartier Baoule ', '5.420949', '-3.990893', '24.48.27.54', 'A', '2019-02-08 13:17:26', 0),
(20, 'ABOBO', 'PHARMACIE D ABOBOTE', 'Route du Zoo Carrefour Menuiserie pres de la clinique centrale d Abobo', '5.412027', '-4.005915', ' 24 39 69 87', 'A', '2019-02-08 13:17:26', 0),
(21, 'ABOBO', 'PHARMACIE IRYS ', 'Abobo Anoukoua Koute a l entree du village place parking', '5.365424', '-3.9828266', ' 24 48 48 05', 'A', '2019-02-08 13:17:26', 0),
(22, 'ABOBO', 'PHARMACIE KANN SY', 'zone du commissariat du 14eme arrondt face gde mosquee en construction ', '5.4237784', '-4.017579', ' 24 39 12 51 ', 'A', '2019-02-08 13:17:26', 0),
(23, 'ABOBO', ' PHARMACIE KENNEDY', 'Entree Quartier Kennedy Clouetcha a 400 m du Carrefour Samanke', '5.419100', '-4.005506', ' 24 39 43 73 /09 47 56 25', 'A', '2019-02-08 13:17:26', 0),
(24, 'ABOBO', 'PHARMACIE LA PAIX', 'Cocoteraie Anador face au Groupe Scolaire NANTI ', '5.412427', '-4.010293', '24002129', 'A', '2019-02-08 13:17:26', 0),
(25, 'ABOBO', 'PHARMACIE LES 4 SAISONS', 'Carrefour Lycee Adama Sanogo derriere l eglise Sainte Monique', '5.404319', '-4.000902', '225 21 01 11 10', 'A', '2019-02-08 13:17:26', 0),
(26, 'ABOBO', 'PHARMACIE LEON ANGE', 'Abobo face cite policiere en venant terminus des bus 15 et 49', '5.4179572', '-4.0336749', ' 02 50 05 13 / 24 47 75 43', 'A', '2019-02-08 13:17:26', 0),
(27, 'ABOBO', 'PHARMACIE MAGNIFICAT ', 'Route Anyama non loin depot sotra 9 pres du marche de nuit', '5.437306', '-4.030773', ' 24 39 51 67 ', 'A', '2019-02-08 13:17:26', 0),
(28, 'ABOBO', 'PHARMACIE MANZAN', ' Carrefour Samanke carrefour Alepe Vers Abobo Gare', '5.415352', '-4.007099 ', '24 39 67 01', 'A', '2019-02-08 13:17:26', 0),
(29, 'ABOBO', 'PHARMACIE MATENE   ', 'Abobo avocatier entre la dgi et la cie  a 400 m du camp commando', '   5.438515', '-4.020665', ' 24 39 20 32', 'A', '2019-02-08 13:17:26', 0),
(30, 'ABOBO', 'PHARMACIE MIRIA  ', 'Route du zoo carrefour Jean Tailly en face du marche des grossistes', '5.417759', '-4.012002', '24 39 01 09', 'A', '2019-02-08 13:17:26', 0),
(31, 'ABOBO', 'PHARMACIE NOTRE DAME DE FATIMA', 'En face de l Eglise Ste Monique Plateau Dokui', '5.399974', '-4.001543', ' 24 39 59 12 ', 'A', '2019-02-08 13:17:26', 0),
(32, 'ABOBO', 'PHARMACIE N GOHESSE', 'Abobo Plaque I  Entre le Foyer feminin et le Marche de Nuit ', ' 5.435659', '-4.008953', ' 24 39 94 67 /09 84 14 71', 'A', '2019-02-08 13:17:26', 0),
(33, 'ABOBO', 'PHARMACIE PRINCIPALE D ABOBOTE', 'Petit marche d Abobote Pres de la Gare des Taxis Woro woro', ' 5.411068', '-4.000951', '  24-29-12-63 / 24 49 53 22/24 49 54 22', 'A', '2019-02-08 13:17:26', 0),
(34, 'ABOBO', 'PHARMACIE PROVIDENCE', 'quartier banco 1er arret sotra debut autoroute anyama ', '5.348743', '-4.016898', ' 24 49 39 62 ', 'A', '2019-02-08 13:17:26', 0),
(35, 'ABOBO', 'PHARMACIE QUATRE ETAGES', 'En face des 4 Etages et a 100 M de la formation sanitaire d Abobo', ' 5.439257', '-4.008986', '24 39 06 62', 'A', '2019-02-08 13:17:26', 0),
(36, 'ABOBO', 'PHARMACIE ROUTE D AKEIKOI', 'Sur la voie menant a akeikoi Abobo Colatier', '5.444737', '-4.014202', ' 24 39 97 22 / 05 61 64 88', 'A', '2019-02-08 13:17:26', 0),
(37, 'ABOBO', 'PHARMACIE SAINT FRANCOIS XAVIER', 'Abobo Sogefia 300m Du 15 eme Arrondissement', '5.433009', '-4.014863', '24 39 66 02', 'A', '2019-02-08 13:17:26', 0),
(38, 'ABOBO', 'PHARMACIE SOGEFIHA', 'entre commissariat du 15eme et station mobil bus 41 08 49', '5.435168', '-4.012646', '24 01 00 26', 'A', '2019-02-08 13:17:26', 0),
(39, 'ABOBO', 'PHARMACIE SAINT SAUVEUR ', 'Quart  Clouetcha  Carrefour ancienne Boulangerie non loin du Marche de Clouetcha', '5.4369706', '-4.0167621', ' 24 48 47 41 ', 'A', '2019-02-08 13:17:26', 0),
(40, 'ABOBO', 'PHARMACIE SAINTE CROIX', 'Route BC Pres Clinique Medicale de l Etoile Camp Commando', '5.434318', '-4.020429', '24 39 36 73', 'A', '2019-02-08 13:17:26', 0),
(41, 'ABOBO', 'PHARMACIE STE ODILE', 'Non loin de l Alocodrome  Plateau Dokui ', ' 5.396710', ' -4.003938', '24 39 29 97 / 24 39 00 89/59 88 16 07', 'A', '2019-02-08 13:17:26', 0),
(42, 'ABOBO', 'PHARMACIE TEHOUA', 'Sur L autoroute D anyama 500m Apres La Gendarmerie D abobo ', '5.430626', '-4.023613', '47 50 90 90', 'A', '2019-02-08 13:17:26', 0),
(43, 'ABOBO', 'PHARMACIE TIMA  ', 'face au lycee moderne d abobo alignement biao gd marche', '5.427946', '-4.015063', '24 39 08 66', 'A', '2019-02-08 13:17:26', 0),
(44, 'ABOBO', 'PHARMACIE TSACOE ', ' A la Gare Entre le Lycee St Joseph et l Agence Moov', '5.419868', '-4.017989', '24 01 18 20', 'A', '2019-02-08 13:17:26', 0),
(45, 'ABOBO', 'PHARMACIE YARAPHA', 'Carrefour Akeikoi Derriere La Cite Universitaire', '5.438811 ', '-4.013958', '24 49 12 63', 'A', '2019-02-08 13:17:26', 0),
(46, 'ABOBO', 'PHARMACIE MILIE HEVIE  PK 18  ', 'Route D anyama Pk 18 Terminus Bus 76', '5.448178', '-4.049958 ', '24 39 04 00', 'A', '2019-02-08 13:17:26', 0),
(47, 'ABOBO', ' PHARMACIE ROUTE D ANYAMA PK 18 ', ' Route d Anyama Face ex Unicafe  PK 18 Agoueto ', '5.4976557', '-4.053417', '23 55 82 40/77 25 57 30', 'A', '2019-02-08 13:17:26', 0),
(48, 'ABOBO', 'PHARMACIE SAPHIR  PK 18 ', 'Route d Anyama  Derriere Pont  Place Ancienne Boulangerie ', '5.444020', '-4.055152', '24 49 27 76', 'A', '2019-02-08 13:17:26', 0),
(49, 'ABOBO', 'PHARMACIE OLYMPIQUE', 'Abobo ndotre a 300 m du carrefour ndotre route de Yopougon', '5.439198', '-4.068268', ' 02 27 56 13 /  07 89 99 44', 'A', '2019-02-08 13:17:26', 0),
(50, 'ABOBO', 'PHARMACIE SAINT ALEXANDRE', 'PK 18 interieur residences concorde de la SICOGI', '5.450732 ', '-4.049157', '02 02 48 06/59 04 35 32', 'A', '2019-02-08 13:17:26', 0),
(51, 'ABOBODOUME LOCODJORO', 'PHARMACIE CHENAIN', 'Yopougon Jerusalem a 200 m de la Cite SODECI', '5.320142', '-4.043896', '23 50 13 91 ', 'A', '2019-02-08 13:17:26', 0),
(52, 'ABOBODOUME LOCODJORO', 'PHARMACIE DE LOCODJORO', 'Yopougon apres le marche en allant vers abobodoume', '5.336400 ', '-4.042482', '23 45 36 83 ', 'A', '2019-02-08 13:17:26', 0),
(53, 'ABOBODOUME LOCODJORO', 'PHARMACIE D ABOBODOUME', 'Route d Abobodoume 200m des Sapeurs Pompiers Ligne Bus 42  Toits rouges', '5.3198855', '-4.0527578', '23 45 63 00 ', 'A', '2019-02-08 13:17:26', 0),
(54, 'ABOBODOUME LOCODJORO', 'PHARMACIE NOTRE DAME DES VICTOIRES ', 'Mossikro  200 metres du carrefour Locodjoro vers Toits Rouges', '5.352230 ', '-3.980117', '55 60 52 52', 'A', '2019-02-08 13:17:26', 0),
(55, 'ABOBODOUME LOCODJORO', 'PHARMACIE SAINT ETIENNE   ', 'abobodoume face marche poisson carrefour pinasses pres du terminus bus 36 42 44', '5.309766 ', '-4.037701', '23 52 44 44 / 02 82 81 83', 'A', '2019-02-08 13:17:26', 0),
(56, 'ADJAME', 'PHARMACIE ADJAME BRACODI', ' Adjame Gare nord sotra dans la zone de stationnement des gbakas dans le sens Adjame Abobo', '5.361862', '-4.027627', '20 37 54 33', 'A', '2019-02-08 13:17:26', 0),
(57, 'ADJAME', 'PHARMACIE ADJAME LATIN ', 'Prolongement de l avenue 13  pres du petit marche', '5.358954 ', '-4.020031', '20 37 22 29', 'A', '2019-02-08 13:17:26', 0),
(58, 'ADJAME', 'PHARMACIE ADJAME SANTE ', 'Pres de l institut de sante  BD NANGUI ABROGOUA', '5.341196 ', '-4.026478', '20 22 85 44', 'A', '2019-02-08 13:17:26', 0),
(59, 'ADJAME', 'PHARMACIE BOULEVARD DE LA PAIX ', 'Attecoube route de Carena face Sebroko pres station total', '5.340950', '-4.030619', '20 22 20 34', 'A', '2019-02-08 13:17:26', 0),
(60, 'ADJAME', 'PHARMACIE DE LA MAIRIE', 'FACE MAIRIE D ADJAME', '5.343343', ' -4.026101', '20 39 99 99', 'A', '2019-02-08 13:17:26', 0),
(61, 'ADJAME', 'PHARMACIE DE LA MOSQUEE ', 'BD WILLIAM JACOB derriere les rails de la grande mosquee', '5.356576 \n', '-4.028445', '20 37 17 70 ', 'A', '2019-02-08 13:17:26', 0),
(62, 'ADJAME', 'PHARMACIE DE L INDENIE', 'Av Toussaint Louverture SGBCI Indenie  Face cite Policiere \napres les tours Administratives Plateau', '5.338565 ', '-4.023413', '20 21 17 90   ', 'A', '2019-02-08 13:17:26', 0),
(63, 'ADJAME', 'PHARMACIE DE LA PROVIDENCE', 'Petit marche des 220 lgts  face Edipresse ligne bus N 14 09  12 83', '5.348509', '-4.016918', '20 37 55 81 ', 'A', '2019-02-08 13:17:26', 0),
(64, 'ADJAME', 'PHARMACIE DE L  ESPOIR', 'Adjame 220 lgts rue passant devant Edipresse et IMST face cite de l enfance croix rouge 200 m marche Gouro ', '5.349274', '-4.019333', '20 39 08 31 /  07 08 45 12', 'A', '2019-02-08 13:17:26', 0),
(65, 'ADJAME', 'PHARMACIE DES 220 LOGEMENTS', 'Avenue General de Gaulle  face station texaco', '5.354533 ', '-4.018181', '20 37 38 22', 'A', '2019-02-08 13:17:26', 0),
(66, 'ADJAME', 'PHARMACIE DES GARES', ' Boulevard Nangui Abrogoua Entree la RAN et CITELCOM  ', '5.355985 ', '-4.027122', '20 37 26 24', 'A', '2019-02-08 13:17:26', 0),
(67, 'ADJAME', 'PHARMACIE DIBY RENE ', 'Face A La Mairie D adjame', '5.3674593', '-4.0911342', '20 22 48 27 / 20 22 48 11', 'A', '2019-02-08 13:17:26', 0),
(68, 'ADJAME', 'PHARMACIE DU BANCO ', 'Boulevard Nangui Abrogoua Face Service Social Adjame', '5.350397', '-4.025624', ' 20 37 21 69/ 20 37 03 12 ', 'A', '2019-02-08 13:17:26', 0),
(69, 'ADJAME', 'PHARMACIE DU CHATEAU D EAU  ', 'Bd William Jacob En descendant les rails vers la Sodeci', '5.353724 ', '-4.030300', '20 37 11 68', 'A', '2019-02-08 13:17:26', 0),
(70, 'ADJAME', 'PHARMACIE DU FORUM', 'a linterieur du forum des marches dAdjame Mosquee', '5.345184', '-4.026429', '20 38 13 64 ', 'A', '2019-02-08 13:17:26', 0),
(71, 'ADJAME', 'PHARMACIE DU MARCHE', 'Rue des Agouas face au grand marche dAdjame', '5.344947', '-4.026730', '20 37 33 12 ', 'A', '2019-02-08 13:17:26', 0),
(72, 'ADJAME', 'PHARMACIE DU MARCHE GOURO ', 'Adjame marche Gouro a cote de la banque atlantique', '5.349218', '-4.022826', '20 38 83 83 ', 'A', '2019-02-08 13:17:26', 0),
(73, 'ADJAME', 'PHARMACIE DU ROCHER  ', 'Adjame nord  marche bramakote', '5.349056 ', '-4.027860', '20 37 70 63', 'A', '2019-02-08 13:17:26', 0),
(74, 'ADJAME', 'PHARMACIE FRATERNITE', 'Pres Maternite Therese Houphouet Boigny  Quartier Fraternite', '5.349637', '-4.021560', '20 38 12 38', 'A', '2019-02-08 13:17:26', 0),
(75, 'ADJAME', 'PHARMACIE GBEDE', 'Bd Nangui Abrogoua face a la Foire de Chine', ' 5.347534 ', '-4.025590', '20 37 71 58', 'A', '2019-02-08 13:17:26', 0),
(76, 'ADJAME', 'PHARMACIE KANA', 'Adjame habitat extension  pres du petit marche', '5.3585161', '-4.0251875', '20 37 96 49', 'A', '2019-02-08 13:17:26', 0),
(77, 'ADJAME', 'PHARMACIE KORO ', 'Quartier Marie Therese 220 Logements- A cote College Montherlant Batiment B', '5.352914', '-4.020508', ' 07 81 47 18/20 38 68 51', 'A', '2019-02-08 13:17:26', 0),
(78, 'ADJAME', 'PHARMACIE LE BELIER', 'Arret de Bus du Grd Bloc 220 logements entre Liberte et Fraternite', '5.350812   ', '-4.017166', '20 37 12 16 ', 'A', '2019-02-08 13:17:26', 0),
(79, 'ADJAME', ' PHARMACIE MAKISSI', '220 Lgts  place du cinema liberte', '5.354103', '-4.016463', ' 20 37 70 39', 'A', '2019-02-08 13:17:26', 0),
(80, 'ADJAME', 'PHARMACIE QUARTIER EBRIE', 'Face ancienne gare Stiff- Quartier Ebrie Adjame', '5.355587 ', '-4.020748', '20 37 12 56 ', 'A', '2019-02-08 13:17:26', 0),
(81, 'ADJAME', 'PHARMACIE REBOUL', 'Av Reboul 150m des Sapeurs Pompiers de lIndenie BUS 82 81 35  ', '5.342220 ', '-4.022959', '20 37 99 98 ', 'A', '2019-02-08 13:17:26', 0),
(82, 'ADJAME', 'PHARMACIE ROEDDENDT    SCHIBA    ', 'Rue du dispensaire antituberculeux  face marche gouro', '5.3484733', '-4.0256634', '20 37 98 33 ', 'A', '2019-02-08 13:17:26', 0),
(83, 'ADJAME', 'PHARMACIE SAINT MICHEL', 'Oter de l eglise Saint Michel En face du Ceg Harris', '5.346903 ', '-4.022621', '20 37 09 06 ', 'A', '2019-02-08 13:17:26', 0),
(84, 'ADJAME', 'PHARMACIE SARAH  ', 'Route camp Gendarmerie Agban Centre commercial Carine N Couture', '5.357366', '-4.015700', '20 39 03 57 ', 'A', '2019-02-08 13:17:26', 0),
(85, 'ADJAME', 'PHARMACIE SENEVE  ', 'Pres de la gare UTB- Renault Adjame Abidjan', '5.351272', '-4.020967', '20 37 11 04 / 04 13 97 77', 'A', '2019-02-08 13:17:26', 0),
(86, 'ADJAME', 'PHARMACIE SAINTE MARIE D ADJAME', 'Rue Harriste Cote parking payant Derriere Commissariat du 3ieme  ', '5.347041 ', '-4.024689', '20 37 54 44 / 20 37 54 46', 'A', '2019-02-08 13:17:26', 0),
(87, 'ANYAMA', 'PHARMACIE AN NASR', 'Autoroute d Abobo Anyama', '5.503615', '-4.052400', '23 55 98 22  ', 'A', '2019-02-08 13:17:26', 0),
(88, 'ANYAMA', 'PHARMACIE DU CHATEAU ', 'Autoroute d Abobo Anyama', '5.500232 ', '-4.052744', '07 93 46 81/58 17 29 91', 'A', '2019-02-08 13:17:26', 0),
(89, 'ANYAMA', 'PHARMACIE DU MARCHE  ', 'En face de la poste et de la banque atlantique Anyama', '5.495490 ', '-4.051978', '23 55 62 30', 'A', '2019-02-08 13:17:26', 0),
(90, 'ANYAMA', 'PHARMACIE D ANYAMA ', 'Route d anyama', '5.495488  ', '-4.051979', '23 55 94 18 ', 'A', '2019-02-08 13:17:26', 0),
(91, 'ANYAMA', 'PHARMACIE ELIEL', 'Quartier RAN En face de la gare Routiere', '5.480435 ', '-4.052640', '23 55 98 30 / 23 55 62 80  ', 'A', '2019-02-08 13:17:26', 0),
(92, 'ATTECOUBE', 'PHARMACIE BD DE LA PAIX  ', 'boulevard de la paix face sebroko ', '5.4800514', '-4.3338867', '20 22 20 34 / 02 03 76 18 ', 'A', '2019-02-08 13:17:26', 0),
(93, 'ATTECOUBE', 'CHRIST STELLA  ', 'Cite Fairmont  Attecoube apres la Nouvelle Gendarmerie', '5.377581', '-4.088076', '20 37 90 85', 'A', '2019-02-08 13:17:26', 0),
(94, 'ATTECOUBE', 'PHARMACIE DES AGOUAS', ' Face a la boulangerie BBCO', '5.352133 ', '-4.033774', '20 37 10 59', 'A', '2019-02-08 13:17:26', 0),
(95, 'ATTECOUBE', 'PHARMACIE D AGBAN', 'A l entree de la Cite Fairmont Agban Village Adjame ', '5.358766 ', '-4.030485', '20 37 22 30 / 20 37 21 67', 'A', '2019-02-08 13:17:26', 0),
(96, 'ATTECOUBE', 'PHARMACIE D ATTECOUBE ', 'Grand Carrefour d Attecoube Face feux tricolores  Entree bus 04', '5.3500759', '-4.0344756', '08 44 85 95', 'A', '2019-02-08 13:17:26', 0),
(97, 'ATTECOUBE', 'PHARMACIE FATIMA ', 'Carrefour du Pont a la marine Academie de la mer', '5.347443', '-4.038323', ' 20 37 29 10 / 02 56 10 10', 'A', '2019-02-08 13:17:26', 0),
(98, 'ATTECOUBE', 'PHARMACIE MONTANA', 'Entre la Mairie et le Terminus Bus 04 Face ecole Faidherbe ', '5.345891 \n', '-4.034605', '07 36 57 30', 'A', '2019-02-08 13:17:26', 0),
(99, 'ATTECOUBE', 'PHARMACIE REHOBOTH', 'Pres de la Maternite CISCOM  Cite Fairmont 2', '5.3524416', '-4.0359588', '01 07 48 17', 'A', '2019-02-08 13:17:26', 0),
(100, 'ATTECOUBE', 'PHARMACIE DU ROND POINT', 'rond point bidjante gauche', '5.2945874', '-4.0028354', '21 01 59 47 / 07 32 43 76/21 35 73 03', 'A', '2019-02-08 13:17:26', 0),
(101, 'ATTECOUBE', 'PHARMACIE SEBROKO ', 'Pres du Grand marche  Saint Joseph Attecoube', '5.3496316', '-4.0346266', '20 37 67 48', 'A', '2019-02-08 13:17:26', 0),
(102, 'BINGERVILLE', 'PHARMACIE AKRE ALBERT ASSAMOI', 'Nouvelle gare routiere', '5.3532338', '-3.8776556', '22 40 39 37', 'A', '2019-02-08 13:17:26', 0),
(103, 'BINGERVILLE', 'PHARMACIE DU MARCHE', 'En face de la COPEC pres de la Gare', '5.356593', '-3.8830932', '22 40 34 64 ', 'A', '2019-02-08 13:17:26', 0),
(104, 'BINGERVILLE', 'PHARMACIE PRINCIPALE  ', 'Non loin du carrefour marche', '5.3573549', '-3.8880008', '22 40 32 29 ', 'A', '2019-02-08 13:17:26', 0),
(105, 'BINGERVILLE', 'PHARMACIE CARREFOUR SANTE', ' Face Station Corlay au Carrefour sante Akouai Santai ', '5.3613087', '-3.8961634', '75 93 32 18', 'A', '2019-02-08 13:17:26', 0),
(106, 'BINGERVILLE', 'PHARMACIE ST SYLVESTRE ', 'En face du restaurant Coup de Frein Cite Palma route de Bingerville', '5.372685', ' -3.910770', '22 40 27 88', 'A', '2019-02-08 13:17:26', 0),
(107, 'COCODY CENTRE', ' PHARMACIE COMOE ', 'Face Radio Frequence Vie  Cite des arts', '5.347903', '-3.997275', '22 44 28 81', 'A', '2019-02-08 13:17:26', 0),
(108, 'COCODY CENTRE', 'PHARMACIE DE BLOCKAUSS ', 'Terminus Sotra 200 m de L hotel Ivoire', '5.3226718', '-4.002285', '22 48 68 91 ', 'A', '2019-02-08 13:17:26', 0),
(109, 'COCODY CENTRE', 'PHARMACIE DE COCODY', 'Pres du Cash face a la patisserie Abidjanaise ', '5.337068 ', '-4.000109', '22 44 24 95/22 44 03 43', 'A', '2019-02-08 13:17:26', 0),
(110, 'COCODY CENTRE', 'PHARMACIE DE LA CITE COCODY', 'Cocody Danga Face Cite Rouge a la gare Woro Woro de Marcory', '5.337182', '-4.005154', '22 44 63 68', 'A', '2019-02-08 13:17:26', 0),
(111, 'COCODY CENTRE', 'PHARMACIE DE LA CORNICHE ', 'Bd de la Corniche Face Siege Nestle', ' 5.341303 ', '-4.015735', '22 48 73 40 ', 'A', '2019-02-08 13:17:26', 0),
(112, 'COCODY CENTRE', 'PHARMACIE DE LA PIETE ', ' Face a la PMI  COCODY Doyen ', '5.333983 ', '-4.000279', '22 44 45 84/22 43 14 41/57 49 38 61', 'A', '2019-02-08 13:17:26', 0),
(113, 'COCODY CENTRE', 'PHARMACIE CITE  DES ARTS  ', 'Bd de l Universite  Cite des arts', '5.344127 ', '-3.999366', '22 44 93 56', 'A', '2019-02-08 13:17:26', 0),
(114, 'COCODY CENTRE', 'PHARMACIE DU BD DE FRANCE ', 'St Jean Gd marche station Agip', '5.335994 ', '-4.002090', '22 44 74 08', 'A', '2019-02-08 13:17:26', 0),
(115, 'COCODY CENTRE', 'PHARMACIE DU LYCEE TECHNIQUE', 'Route du Lycee Technique pres de la Clinique Goci ', '5.344410 ', '-4.011396', '22 44 60 77', 'A', '2019-02-08 13:17:26', 0),
(116, 'COCODY CENTRE', 'PHARMACIE LES 7 COLONNES ', 'Bd des Martyrs carrefour de la vie face PFO Africa  Cite des arts', '5.344992 ', '-4.002658', '22 44 02 96', 'A', '2019-02-08 13:17:26', 0),
(117, 'COCODY CENTRE', 'PHARMACIE LES MIMOSAS ', ' Entre Carrefour la vie et la Sodefor face a SETACI  Cite des arts', '5.349195 ', '-3.999431', '22 44 32 28', 'A', '2019-02-08 13:17:26', 0),
(118, 'COCODY CENTRE', 'PHARMACIE MERMOZ', 'Av Jean Mermoz  lentree de la Cite Sogefiha  Centre Cocody', '5.340173', '-4.002074', '22 48 74 26   ', 'A', '2019-02-08 13:17:26', 0),
(119, 'COCODY CENTRE', 'PHARMACIE PALM CLUB ', 'Rue du Lycee Technique HOTEL PALM CLUB Boutique N7', '5.353069 ', '-4.002681', '22 44 20 90 ', 'A', '2019-02-08 13:17:26', 0),
(120, 'COCODY CENTRE', 'PHARMACIE ST DOMINIQUE ', 'Cocody en face du lycee international JEAN MERMOZ', '5.339464 ', '-3.997806', '22 48 79 89 ', 'A', '2019-02-08 13:17:26', 0),
(121, 'COCODY CENTRE', 'PHARMACIE ST FRANCOIS DE DANGA  ', 'Prolongement poste Face centre culturel Americain  Danga Cocody', '5.343600', '-4.004050', '22 48 54 08', 'A', '2019-02-08 13:17:26', 0),
(122, 'COCODY CENTRE', 'PHARMACIE ST JEAN  ', 'Bd Latrille face cite Rouge  Centre Cocody ', '5.338585 ', '-4.003892', '22 44 62 49  ', 'A', '2019-02-08 13:17:26', 0),
(123, 'COCODY CENTRE', 'PHARMACIE SAINTE MARIE', 'Bd de France immeuble NSIA A proximite de la maison du parti du PDCI', '5.340250 ', '-3.998829', '22 48 69 20', 'A', '2019-02-08 13:17:26', 0),
(124, 'COCODY II PLATEAUX', 'LES ARCADES', 'Cocody Djibi 8eme Tranche  Terminus du bus express 205', '5.403789 ', '-3.976241', '22 50 62 25 ', 'A', '2019-02-08 13:17:26', 0),
(125, 'COCODY II PLATEAUX', 'PHARMACIE 2 PLATEAUX AGBAN ', 'Entre nouveau marche et hopital des impots sortie Nord Camp de la Gendarmerie Agban', '5.366940', '-4.008054', '22 41 39 90', 'A', '2019-02-08 13:17:26', 0),
(126, 'COCODY II PLATEAUX', 'PHARMACIE 8eme TRANCHE ', 'Carrefour marche Cocovico Route 9eme tranche ', '5.401413 ', '-3.978122', '22 52 34 90 /07 69 54 32', 'A', '2019-02-08 13:17:26', 0),
(127, 'COCODY II PLATEAUX', 'PHARMACIE ANGRE  ', 'A cote du 22e arrondissement  Angre 7e tranche Cocody', '5.399305    ', '-3.991164', '22 42 11 06/22 42 43 19/87 16 15 35', 'A', '2019-02-08 13:17:26', 0),
(128, 'COCODY II PLATEAUX', 'PHARMACIE APPAUL  ', 'cocody 8eme tranche  50 m du college ste camille  interieur centre commercial parkn shop ', '5.394008', ' -3.973916', '22 50 26 40  ', 'A', '2019-02-08 13:17:26', 0),
(129, 'COCODY II PLATEAUX', ' PHARMACIE ARC EN CIEL  ', 'II Plateaux rue des jardins  Carrefour commissariat 12eme arrondt', '5.374509', '-3.990638', '22 41 20 67', 'A', '2019-02-08 13:17:26', 0),
(130, 'COCODY II PLATEAUX', 'PHARMACIE ARUMS', 'Angre 8ie Tranche carrefour BCEAO sur la grande Voie menant a la CNPS', '5.395970 ', '-3.977089', '22 50 66 40 / 07 06 13 94', 'A', '2019-02-08 13:17:26', 0),
(131, 'COCODY II PLATEAUX', 'PHARMACIE BEL HORIZON ', 'Bd Latrille pres du Groupe Scolaire Flamboyant Angre Petro Ivoire  ', '5.406158', '-3.990776', '22 52 24 19/22 52 24 22', 'A', '2019-02-08 13:17:26', 0),
(132, 'COCODY II PLATEAUX', 'PHARMACIE DE LA 7eme TRANCHE', 'Rue L84-L139 II Plateaux 7e tranche', ' 5.391516 ', '-3.988445', '22 52 56 83 ', 'A', '2019-02-08 13:17:26', 0),
(133, 'COCODY II PLATEAUX', 'PHARMACIE DES ALLEES', 'Angre Cafeiers 5 Route du Chateau', '5.409364', '-3.987025', '22 42 14 58', 'A', '2019-02-08 13:17:26', 0),
(134, 'COCODY II PLATEAUX', 'PHARMACIE DES GRACES', 'Route II plateaux non loin de l hopital des impots Williamsville ', '5.373142 ', '-4.007304', '22 41 24 27', 'A', '2019-02-08 13:17:26', 0),
(135, 'COCODY II PLATEAUX', 'PHARMACIE DES HALLES  ', 'Espace Latrille Centre commercial SOCOCE', '5.373525  ', '-3.999188', '22 41 92 94', 'A', '2019-02-08 13:17:26', 0),
(136, 'COCODY II PLATEAUX', 'PHARMACIE DES II PLATEAUX ', 'Bd. Latrille face a la SGBCI Sicogi 2 Plateaux ', '5.370812', '-3.997804', '22 41 36 04', 'A', '2019-02-08 13:17:26', 0),
(137, 'COCODY II PLATEAUX', 'PHARMACIE DES JARDINS', 'Rue des Jardins pres du supermarche HAYAT  Deux plateaux', ' 5.358806', '-3.990021', '22 41 17 70 ', 'A', '2019-02-08 13:17:26', 0),
(138, 'COCODY II PLATEAUX', 'PHARMACIE DES LAUREADES  ', 'En face du stade d Angre  Angre 7e tranche', ' 5.397899  ', '-3.986141', '22 42 48 10 / 05 79 14 03 ', 'A', '2019-02-08 13:17:26', 0),
(139, 'COCODY II PLATEAUX', 'PHARMACIE DIVIN AMOUR  ', ' Bd Latrille Carrefour Macaci Pres de l Eglise du plein Evangile', '5.365661 ', '-3.997324', '22 41 93 38 ', 'A', '2019-02-08 13:17:26', 0),
(140, 'COCODY II PLATEAUX', 'PHARMACIE DIVINE ONCTION', 'Rue J 92 vers le 12eme Arrondissement  Deux plateaux', '5.375114', '-3.996199', '22 41 71 36', 'A', '2019-02-08 13:17:26', 0),
(141, 'COCODY II PLATEAUX', 'PHARMACIE DU BIEN ETRE ', 'Voie de la Djibi entree de Soleil 3  Angre 8e tranche', '5.3910862', '-3.9781451', '22 41 36 04 ', 'A', '2019-02-08 13:17:26', 0),
(142, 'COCODY II PLATEAUX', 'PHARMACIE DU VALLON', 'Rue des Jardins Face a la BSIC non loin de Paul  Deux plateaux', '5.3687245', '-3.9946724', '22 41 35 14 ', 'A', '2019-02-08 13:17:26', 0),
(143, 'COCODY II PLATEAUX', 'PHARMACIE DUNIA ', ' Vers le Commissariat du 30eme Arrondissement ', '5.3744897', '-3.9869421', '22 52 99 99', 'A', '2019-02-08 13:17:26', 0),
(144, 'COCODY II PLATEAUX', 'PHARMACIE ESPACE SANTE', 'Angle Bd Latrille Carrefour Route du Zoo Deux plateaux ', '5.3777923', '-3.9998831', '22 41 21 03/22 41 23 00 ', 'A', '2019-02-08 13:17:26', 0),
(145, 'COCODY II PLATEAUX', ' PHARMACIE ESPERANCE ', ' Derriere le marche et la mosquee de la Djibi ', '5.3778821', '-4.0327141', '22 50 58 98', 'A', '2019-02-08 13:17:26', 0),
(146, 'COCODY II PLATEAUX', 'PHARMACIE FANDASSO', 'Carrefour Batim Angre Cite Fandasso', '5.411602', '-3.9885078', '21 00 18 16', 'A', '2019-02-08 13:17:26', 0),
(147, 'COCODY II PLATEAUX', 'PHARMACIE HERMES', 'Face Cash Center Angre CNPS  Angre 9e tranche', '5.3852915', '-3.9761697', '22 50 10 32 ', 'A', '2019-02-08 13:17:26', 0),
(148, 'COCODY II PLATEAUX', 'PHARMACIE LA DJIBI ', ' Angre Djibi 1 derriere la residence Koriet', '5.4005138', '-3.9831702', '22 52 23 76', 'A', '2019-02-08 13:17:26', 0),
(149, 'COCODY II PLATEAUX', 'PHARMACIE LAS PALMAS ', 'Bd Latrille Carrefour Las Palmas  Deux plateaux Aghien', '5.380593', '-3.9968727', '22 42 14 78 / 22 42 14 79', 'A', '2019-02-08 13:17:26', 0),
(150, 'COCODY II PLATEAUX', 'PHARMACIE LATRILLE', ' II Plateaux sur le Boulevard des Martyrs', '5.3618239', '-3.9997906', '22 41 03 68', 'A', '2019-02-08 13:17:26', 0),
(151, 'COCODY II PLATEAUX', 'PHARMACIE LES 7 LYS ', ' 100m de la residence BADA route d Angre Attoban-carrefour cite ZINSOU ', '5.384194', '-3.9899817', '22 42 63 25/07 83 66 61', 'A', '2019-02-08 13:17:26', 0),
(152, 'COCODY II PLATEAUX', 'PHARMACIE LES PERLES ', 'Bd Latrille Carrefour Opera  Deux plateaux  Les Perles Cocody', '5.3856093', '-3.9962822', '22 42 39 85 ', 'A', '2019-02-08 13:17:26', 0),
(153, 'COCODY II PLATEAUX', 'PHARMACIE LES TULIPES', 'Bd Latrille Espace Santa Maria   Face Mosquee d Aghien', '5.3828808', '-3.9953885', '22 42 50 17 ', 'A', '2019-02-08 13:17:26', 0),
(154, 'COCODY II PLATEAUX', ' PHARMACIE LUMINANCE K  ', 'Angre djibi groupement 4000 batim ci 300 m de la mosquee ', '5.407955', '-3.983751', '22 50 49 26', 'A', '2019-02-08 13:17:26', 0),
(155, 'COCODY II PLATEAUX', 'PHARMACIE NOEMIE ', 'Cocody   II Plateaux  Carrefour les Oscars derriere le glacier les Oscars', '5.394753', '-3.992136', ' 22 52 20 51 / 48 64 67 64', 'A', '2019-02-08 13:17:26', 0),
(156, 'COCODY II PLATEAUX', 'PHARMACIE SAINTE CECILE', 'Face Eglise Ste Cecile  Deux plateaux  Vallon Cocody', '5.3667165', '-3.9983305', '22 42 63 35/22 42 35 17', 'A', '2019-02-08 13:17:26', 0),
(157, 'COCODY II PLATEAUX', 'PHARMACIE SAINTE MARTHE', 'Angre Bd Latrille   carrefour station Petro Ivoire   terminus bus 81 et 82 ', '5.4036527', '-3.9872926', '22 42 33 87/07 93 21 13', 'A', '2019-02-08 13:17:26', 0),
(158, 'COCODY II PLATEAUX', ' PHARMACIE SANTE DE VIE', 'fin de la Rue principale   Cite Sanon   Pres de la Poste  Deux plateaux ', '5.387645 ', '-3.997691', '22 42 05 22 / 06 35 93 05', 'A', '2019-02-08 13:17:26', 0),
(159, 'COCODY II PLATEAUX', 'PHARMACIE ST CHRISTOPHE', 'Non loin de la gare de Woro woro  Angre Petro Ivoire Cocody ', '5.405023 ', '-3.993756', '22 42 55 62', 'A', '2019-02-08 13:17:26', 0),
(160, 'COCODY II PLATEAUX', '  PHARMACIE ST GABRIEL ', 'Bd Latrille   a cote de Bridge Bank Cocody ', '5.388241', '-3.992470', '22 42 58 35/22 42 49 95', 'A', '2019-02-08 13:17:26', 0),
(161, 'COCODY II PLATEAUX', 'PHARMACIE ST GIL', 'Rue des Jardins   face Station Corlay Deux plateaux', '5.356162 ', '-3.991921', '22 41 94 10 / 22 4115 42 ', 'A', '2019-02-08 13:17:26', 0),
(162, 'COCODY II PLATEAUX', ' PHARMACIE STE HARMONY ', 'Non loin du King Cash Djibi Angre Djibi Cocody', '5.403460', '  -3.980957', '22 52 39 97  ', 'A', '2019-02-08 13:17:26', 0),
(163, 'COCODY II PLATEAUX', 'PHARMACIE STE MONIQUE', 'Non loin de la boulangerie Mahou  Angre Mahou', '5.399790 ', '-3.997350', '22 42 01 09  ', 'A', '2019-02-08 13:17:26', 0),
(164, 'COCODY II PLATEAUX', 'PHARMACIE STE TRINITE', 'Route d  Agban   Ligne bus 49  Deux plateaux', '5.368888', '-4.000413', '22 41 68 88/22 41 20 52 ', 'A', '2019-02-08 13:17:26', 0),
(165, 'COCODY II PLATEAUX', 'PHARMACIE AURORE', 'II Plateaux Vallon rue des Jardins 100m avant la Biao et la Station Total', '5.361843', '-3.991184', '22 41 18 08', 'A', '2019-02-08 13:17:26', 0),
(166, 'COCODY II PLATEAUX', ' PHARMACIE NOTRE DAME DE LA VISITATION', 'Face au 12eme Arrondissement  Deux plateaux Vallon Cocody Abidjan', '5.374414', '-3.992700', '41 36 36 92 / 58 60 96 21', 'A', '2019-02-08 13:17:26', 0),
(167, 'COCODY II PLATEAUX', 'PHARMACIE SAINT JOSEPH', 'Rue des Jardins   non loin de la Gourmandine  Deux plateaux ', '5.372374', '-3.990536', '22 41 10 20', 'A', '2019-02-08 13:17:26', 0),
(168, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ANDY', 'Carrefour Leader Price   face station Shell  Anono Cocody', '5.339754 ', '-3.975023', '22 43 25 80 / 08 82 58 58', 'A', '2019-02-08 13:17:26', 0),
(169, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BD MITTERRAND', 'Route de bingerville  carrefour bonoumin km 9 apres carrefour ste famille', '5.358642 ', '-3.964559', '22 47 90 45', 'A', '2019-02-08 13:17:26', 0),
(170, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BELLE EPINE', ' Rue des jardins de la Riviera', '5.3489361', '-3.9829179', '22 43 34 31', 'A', '2019-02-08 13:17:26', 0),
(171, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BETHEL', 'Face Groupe Scolaire Jules Ferry  Riviera Attoban', '5.364253', '-3.979106', '22 43 22 83', 'A', '2019-02-08 13:17:26', 0),
(172, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DE LA RIVIERA 2', 'Au Rond point Riviera II Cocody', '5.353453', '-3.976816', '22 43 28 44/22 43 70 85', 'A', '2019-02-08 13:17:26', 0),
(173, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DU BONHEUR', 'Route du Marche  Riviera Palmeraie', '5.369663', '-3.958971', '22 47 02 88', 'A', '2019-02-08 13:17:26', 0),
(174, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DU GOLF', 'Centre Commercial Sicogi Riviera face aux Caddies Non loin de l Agence Bicici du Golf', '5.339549', '-3.977833', '22 43 14 31', 'A', '2019-02-08 13:17:26', 0),
(175, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LES ELIAS', 'Rue D46 Tour Sicogi et Jardins Carrefour Elias', '5.345071', '-3.978117', '22 43 28 85 ', 'A', '2019-02-08 13:17:26', 0),
(176, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LES ROSIERS', 'Rond poind ecole Kouadio Assahore terminus du bus 210  Programme 3', '5.360526', '-3.967437', '22 49 21 67', 'A', '2019-02-08 13:17:26', 0),
(177, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LIVIE ', 'Entre Dispensaire et Gare des Woro Woro   non loin de la cite ATCI ', '5.353886', '-3.940872', '22 47 48 78', 'A', '2019-02-08 13:17:26', 0),
(178, 'COCODY RIVIERA PALMERAIE', ' PHARMACIE MARTHE ROBIN', 'Cite Sipim 3 face espace Vert  Riviera Palmeraie Cocody', '5.374949', '-3.955910', ' 22 49 24 79 / 58 86 12 38  ', 'A', '2019-02-08 13:17:26', 0),
(179, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE M POUTO ', 'Entre Quartier Ciad Promo Et Cite World City', '5.329214', '-3.948947', '22 43 12 73', 'A', '2019-02-08 13:17:26', 0),
(180, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE PALMERAIE    ', 'Non loin du Rond point et la Sgbci Riviera Palmeraie Cocody', '5.363677', '-3.959575', '22 47 91 19 ', 'A', '2019-02-08 13:17:26', 0),
(181, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE RIVIERA III', 'Route du lycee Francais face lycee Americain', '5.348750 ', '-3.953704', ' 22 47 56 16 /  09 01 01 84 ', 'A', '2019-02-08 13:17:26', 0),
(182, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT ANGE', 'Route d Attoban pres du College Andre Malraux', '5.357448', '-3.978256', '22 43 45 04', 'A', '2019-02-08 13:17:26', 0),
(183, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT ATHANASE', 'Carrefour Anono a cote de ECOBANK  Riviera 2', '5.348210', '-3.975702', '22 43 55 87', 'A', '2019-02-08 13:17:26', 0),
(184, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE FAMILLE', 'Voie centre commercial Peace and Unity face a Cap Nord  Riviera 2', '5.355847', '-3.967406', '22 47 76 76', 'A', '2019-02-08 13:17:26', 0),
(185, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST BERNARD', 'Pres de CIPHARM Cite Attoban  Riviera Attoban Cocody ', '5.367538', '-3.979385', '22 43 65 46/22 43 66 21 ', 'A', '2019-02-08 13:17:26', 0),
(186, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST COME ET DAMIEN  ', 'Voie Lauriers 6  Riviera Bonoumin Cocody', '5.372767', '-3.970624', '22 49 22 49 ', 'A', '2019-02-08 13:17:26', 0),
(187, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT GEORGES D AKOUEDO', 'Faya  Quartier Genie 2000 1er carrefour ', '5.372284', '-3.938016', '22 47 22 38 ', 'A', '2019-02-08 13:17:26', 0),
(188, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT PAUL  ', ' Carrefour sacre coeur cite presse  Riviera Palmeraie', '5.372053', '-3.965089', '22 49 29 01  ', 'A', '2019-02-08 13:17:26', 0),
(189, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST PIERRE DES ROSEES', 'Bd Assouan Arsene face clinique Pantheon  Riviera 3', '5.354068', '-3.957884', '22 47 42 17', 'A', '2019-02-08 13:17:26', 0),
(190, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST PIERRE D ANONO ', 'Zone de la Gare de la Riviera 2  Anono Cocody', '5.346327', '-3.974815', '22 43 90 56', 'A', '2019-02-08 13:17:26', 0),
(191, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT RAPHAEL ARCHANGE', 'Rue Ministre  Riviera Palmeraie Cocody', '5.369811', '-3.955507', '22 49 08 20', 'A', '2019-02-08 13:17:26', 0),
(192, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE AGATHE', 'Bd Arsene Usher Assouan au feu du Commissariat 18eme arrondissement', '5.355548 ', '-3.961338', '22 47 48 19', 'A', '2019-02-08 13:17:26', 0),
(193, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE STE BEATRICE DES ROSIERS', 'Carrefour Palmeraie Sci les Rosiers Face Quick Maket', '5.374048', '-3.961747', '22 47 01 72  ', 'A', '2019-02-08 13:17:26', 0),
(194, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE MARIE DES BEATITUDES  ', 'Avant le 35eme Arrdt en face des Rosiers Programme 1 barriere 6', '5.376986', '-3.963102', '22 49 11 49', 'A', '2019-02-08 13:17:26', 0),
(195, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE ST JOSEPH', 'Rue Ministre face Complexe Sportif  Riviera Palmeraie', '5.376931', '-3.953123', '22 49 39 26', 'A', '2019-02-08 13:17:26', 0),
(196, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BON PASTEUR', 'A cote du college l Ardoise face a la paroisse Bon pasteur  Riviera 3', '5.351267 ', '-3.954986', '22 47 04 05 ', 'A', '2019-02-08 13:17:26', 0),
(197, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE  BONOUMIN', 'Voie menant au College ANDRE MALRHAUX et a l Eglise la Destinee  ', '5.361327', '-3.971151', ' 22 49 49 34', 'A', '2019-02-08 13:17:26', 0),
(198, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE CATLEYAS', 'BD Mitterrand Carrefour Abata', '5.372871', '-3.928670', '22 47 07 79', 'A', '2019-02-08 13:17:26', 0),
(199, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE CEPHAS', 'Route Abatta apres Sicta Oribat ', '5.339757', '-3.922613', '59 06 45 70 /  07 69 48 67', 'A', '2019-02-08 13:17:26', 0),
(200, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE EAU VIVE', 'Route Abatta carrefour supermarche Cash Center Akouedo Attie ', '5.359165', '-3.924998', '22 47 34 45', 'A', '2019-02-08 13:17:26', 0),
(201, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE  LAURIERS ATTOBAN', 'Riviera attoban  derriere leglise st bernard  carrefour des cites lauriers 3 et 4  abri 2000', '5.371861', '-3.979666', '08 43 54 94 /  22 43 90 89', 'A', '2019-02-08 13:17:26', 0),
(202, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE MARIE ESTHER', 'Route du lycee st Viateur Rosiers Programme 4 Riviera Palmeraie', '5.376501', '-3.960389', ' 22 49 53 23 / 22 49 03 30', 'A', '2019-02-08 13:17:26', 0),
(203, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE MONT CARMEL', 'A proximite de lhotel Belle Cote Les Rosiers 5eme programme ', '5.386017', '-3.968862', '22 500 758 / 01 35 08 36', 'A', '2019-02-08 13:17:26', 0),
(204, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE NOTRE DAME DES GRACES', 'Pres de la petite Mosquee  Riviera 2 Cocody', '5.352116 ', '-3.980068', '22 43 61 96', 'A', '2019-02-08 13:17:26', 0),
(205, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE RUE MINISTRE', 'Rue Ministre   au Carrefour AGITEL  Riviera Palmeraie', '5.365671', '-3.957880', '22 49 51 77', 'A', '2019-02-08 13:17:26', 0),
(206, 'KOUMASSI', 'PHARMACIE ANIEL', 'Bd du 7 Decembre pres de l Eglise Saint Etienne Remblais Koumassi ', '5.290445', '-3.962457', '21 28 56 24 / 07 09 07 52', 'A', '2019-02-08 13:17:26', 0),
(207, 'KOUMASSI', 'PHARMACIE BABAKAN', 'Bd Houphouet Boigny apres Commissariat du 20 eme Arret face Mosquee Sangare', '5.3072406', '-3.9433877', '21 28 94 45', 'A', '2019-02-08 13:17:26', 0),
(208, 'KOUMASSI', 'PHARMACIE BENIABIE', 'bd du Caire terminus bus 13 26  33', '5.294169', '-3.955762', ' 21 28 95 25', 'A', '2019-02-08 13:17:26', 0),
(209, 'KOUMASSI', 'PHARMACIE BETHANIE ', 'Route menant du Camp Commando a la Zone Industrielle Bia Sud  Sicogi 1', ' 5.284680 ', '-3.958927', '21 36 62 83 ', 'A', '2019-02-08 13:17:26', 0),
(210, 'KOUMASSI', 'PHARMACIE CLIMBIE ', 'Angle Bd du Caire et Rue Raoul Follereau  face a la Grande Mosquee', '5.2990575', '-3.9544752', ' 21 36 35 46 ', 'A', '2019-02-08 13:17:26', 0),
(211, 'KOUMASSI', 'PHARMACIE DE KOUMASSI    SOUMAH     ', 'Bd du 7 decembre  pres de la formation sanitaire de Koumassi', '5.288686', '-3.968942', '21 36 19 85', 'A', '2019-02-08 13:17:26', 0),
(212, 'KOUMASSI', 'PHARMACIE DE LA BAGOE ', 'Bd du Cameroun Carrefour Station Mobile et Sodeci Marcory face Centre Medical Liring ', '5.395020', '-4.008788', '21 28 64 14 ', 'A', '2019-02-08 13:17:26', 0),
(213, 'KOUMASSI', 'PHARMACIE DE PRODOMO ', 'Pres Terrain Inchallah Bd Cameroun Carrefour Sopim  Prodomo 1', '5.299495', '-3.953142', '21 28 83 43', 'A', '2019-02-08 13:17:26', 0),
(214, 'KOUMASSI', ' PHARMACIE DES SABLES', 'Voie Principale Pres de la Cite Batim Sir  Remblais Koumassi', '5.303955', '-3.962417', '21 28 84 17', 'A', '2019-02-08 13:17:26', 0),
(215, 'KOUMASSI', 'PHARMACIE DU BOULEVARD DU GABON ', '200 m de la clinique la madone face pressing akwaba bus 32 26 23 12', '5.3036448', '-3.9711714', ' 21 36 24 62 ', 'A', '2019-02-08 13:17:26', 0),
(216, 'KOUMASSI', 'PHARMACIE DU CAMPEMENT', 'Carrefour campement Koumassi', ' 5.304852', '-3.945138', '21 36 18 91 ', 'A', '2019-02-08 13:17:26', 0),
(217, 'KOUMASSI', 'PHARMACIE DU CANAL ', 'Entre le Carrefour Gabi feu tricolore et Carrefour Millenium', '5.392923', '-4.012023', '21 36 27 48', 'A', '2019-02-08 13:17:26', 0),
(218, 'KOUMASSI', 'PHARMACIE DU MARAIS', 'Carrefour Quartier Divo Remblais Koumassi ', '5.301704', '-3.957539', '21 36 06 99', 'A', '2019-02-08 13:17:26', 0),
(219, 'KOUMASSI', 'PHARMACIE DU SOLEIL', 'Angle Av 9 Bd du Gabon a 200 m de la Poste Sicogi 1 Koumassi', '5.293071', '-3.970085', '21 36 05 98', 'A', '2019-02-08 13:17:26', 0),
(220, 'KOUMASSI', 'PHARMACIE EHILE', ' Bd du Caire pres du grand marche Ligne bus 13  Sicogi 1', '5.293158', '-3.960232', '21 36 45 44', 'A', '2019-02-08 13:17:26', 0),
(221, 'KOUMASSI', 'PHARMACIE FANNY', 'Terminus Bus 05 et 32 - Aklomiabla Koumassi', '5.310575', '-3.947621', '21 36 09 07 ', 'A', '2019-02-08 13:17:26', 0),
(222, 'KOUMASSI', 'PHARMACIE GAUDO', 'Pres du Kalum Bar Ligne Bus 26 Derriere le college Adjavon Sicogi 3', '5.288468', '-3.963447', '21 36 22 51', 'A', '2019-02-08 13:17:26', 0),
(223, 'KOUMASSI', 'PHARMACIE IROKO', 'Ligne Bus 32 de lecole Mondon  Remblais Koumassi ', '5.294756', '-3.964125', '21 56 43 56  ', 'A', '2019-02-08 13:17:26', 0),
(224, 'KOUMASSI', 'PHARMACIE KAHIRA', 'Bd du Cameroun Ligne bus 05 Paroisse Ste Bernadette ', '5.296651', '-3.967600', '21 36 39 84  ', 'A', '2019-02-08 13:17:26', 0),
(225, 'KOUMASSI', 'PHARMACIE MESANO ', 'Derriere l eglise de Prodomo Terminus 11 a Gauche de leglise Catholique', '5.301235', '-3.947484', '21 36 21 90  ', 'A', '2019-02-08 13:17:26', 0),
(226, 'KOUMASSI', 'PHARMACIE ROC', 'Koumassi Quartier campement a 100m de lepv belier', '5.309355', '-3.938163', '21 36 04 86 / 08 52 58 23 ', 'A', '2019-02-08 13:17:26', 0),
(227, 'KOUMASSI', 'PHARMACIE REGINA', 'Pharmacies Ligne 32  2e Arret  Sogefia Koumassi', '5.298804', '-3.949588', '21 36 68 65', 'A', '2019-02-08 13:17:26', 0),
(228, 'KOUMASSI', 'PHARMACIE SAINT LOUIS', 'Bd du Cameroun Face a EPP Baradji apres zone Remblais entre \nEglise Saint Francois et Eglise Prodomo ligne Bus 05', '5.298234', '-3.959772', '21 36 22 10', 'A', '2019-02-08 13:17:26', 0),
(229, 'KOUMASSI', 'PHARMACIE SAINT ALBERT', 'Grand Marche Face eglise Methodiste Cite des Graces', '5.292504', '-3.957540', '21 28 76 04/ 21 28 75 99', 'A', '2019-02-08 13:17:26', 0),
(230, 'KOUMASSI', 'PHARMACIE SAINT GEORGES', 'Pres du Camp Commando  Sicogi 1', '5.2835441', '-3.9653478', '21 36 03 75 ', 'A', '2019-02-08 13:17:26', 0),
(231, 'KOUMASSI', 'PHARMACIE SAINT LUCIEN', 'En face du depot de la SOTRA Soweto  Sicogi 3', '5.2858919', '-3.9563785', '21 28 84 80/07 57 03 74 ', 'A', '2019-02-08 13:17:26', 0),
(232, 'KOUMASSI', 'PHARMACIE SAINT PAUL ', 'Non loin du Terminus 32 pres de lhotel le Recul Nord Est  Aklomiabla', '5.3100866', '-3.9526434', '21 36 19 28 ', 'A', '2019-02-08 13:17:26', 0),
(233, 'KOUMASSI', 'PHARMACIE SAINT SAUVEUR ', 'Face a lEglise Buisson Ardent Non loin de la station mobil Zone CNPS ', '5.2975075', '-3.9562154', '21 36 69 93 ', 'A', '2019-02-08 13:17:26', 0),
(234, 'KOUMASSI', ' PHARMACIE SAINTE AGATHE ', 'Rue du Canal a cote du College la Colombe', '5.3022187', '-3.9685643', '21 56 51 88 / 21 56 51 89 ', 'A', '2019-02-08 13:17:26', 0),
(235, 'KOUMASSI', 'PHARMACIE SAINTE FOI ', 'Terminus Bus 05  Face Lycee Municipal', '5.3022987', '-3.9751304', '21 56 17 40 / 21 56 19 19', 'A', '2019-02-08 13:17:26', 0),
(236, 'KOUMASSI', 'PHARMACIE DE SOPIM', 'KOUMASSI SOPIM', '5.3022775', '-3.9518541', '21 36 69 32', 'A', '2019-02-08 13:17:26', 0),
(237, 'KOUMASSI', 'PHARMACIE GALILEE', ' Bd Antannarivo a l angle de la rue Geo Andre face Base CIE  Sicogi 1', '5.2829758', '-3.9699322', '', 'A', '2019-02-08 13:17:26', 0),
(238, 'KOUMASSI', 'PHARMACIE INCHALLAH', 'Pres du Carrefour sans fil A 50m du groupe Scolaire Entente ', '5.3011637', '-3.9652947', '21 36 47 01 / 01 53 99 99', 'A', '2019-02-08 13:17:26', 0),
(239, 'KOUMASSI', 'PHARMACIE  M  PIKE', 'Derriere la Mairie  Sicogi 1 Koumassi', '5.2902777', '-3.9707235', '21 36 25 75', 'A', '2019-02-08 13:17:26', 0),
(240, 'KOUMASSI', 'PHARMACIE QUARTIER DIVO', 'Entre le Maquis Calao et le Caniveau  Quartier Divo Koumassi', '5.3023743', '-3.9561111', '46 20 86 59', 'A', '2019-02-08 13:17:26', 0),
(241, 'KOUMASSI', 'PHARMACIE SAINT FRANCOIS', 'Angle Av 9 et Rue 11 Prolongement Bd du Gabon en allant vers Prodomo', '5.2952041', '-3.9620938', '21 36 21 77', 'A', '2019-02-08 13:17:26', 0),
(242, 'MARCORY', ' PHARMACIE ANOUMABO ', 'Face EPP Anoumabo 1er arret bus 03 et 31', '5.3069055', '-3.9898021', '21 26 02 07', 'A', '2019-02-08 13:17:26', 0),
(243, 'MARCORY', 'PHARMACIE CANAAN ', '36 Rue Paul Langevin A 500 m de Mercedes  Zone 4 C', '5.3090766', '-3.955673', '21 25 96 76 ', 'A', '2019-02-08 13:17:26', 0),
(244, 'MARCORY', 'PHARMACIE CYRILLE', 'Angle Bd de Marseille  Bd Giscard d Estaing', '5.3092465', '-3.9556731', '21 25 95 28', 'A', '2019-02-08 13:17:26', 0),
(245, 'MARCORY', 'PHARMACIE DE BIETRY ', 'Bd de Marseille Rue du Canal avant Centre Commercial Holliday Imm SGBCI', '5.2752483', '-3.9805091', '21 24 86 89', 'A', '2019-02-08 13:17:26', 0),
(246, 'MARCORY', 'PHARMACIE DE LA GALERIE', 'Bd Giscard d  Estaing Angle Rue Thomas Edison    Super Hayat    Cap Sud ', '5.2976013', '-3.9883861', '21 35 24 73', 'A', '2019-02-08 13:17:26', 0),
(247, 'MARCORY', 'PHARMACIE DE LA PAIX ', 'Bd de Loraine Face a la SIB et Station Texaco  Marcory  Residentiel', '5.3089559', '-3.9935579', '21 26 30 19 / 21 26 36 89 ', 'A', '2019-02-08 13:17:26', 0),
(248, 'MARCORY', 'PHARMACIE DE LA VIE', 'Bd Achalme Imm Teranga  Residentiel Marcory', '5.3136306', '-3.9951292', '21 28 18 68 ', 'A', '2019-02-08 13:17:26', 0),
(249, 'MARCORY', 'PHARMACIE DE LA ZONE 3  ', 'Rue Clement Ader Centre Commercial Square  Zone 3', '5.2952577', '-3.9894661', '21 35 13 15', 'A', '2019-02-08 13:17:26', 0),
(250, 'MARCORY', 'PHARMACIE DE L AMITIE  ', 'Pharmacies Bd du Cameroun non loin du Grand Marche  Remblais Marcory ', '5.300865', '-3.9789709', '21 26 85 06 / 07 67 46 55 ', 'A', '2019-02-08 13:17:26', 0),
(251, 'MARCORY', 'PHARMACIE DE L INJS', 'Grand portail de l Injs face Station Total', '5.3060004', '-3.9817302', '21 28 10 10  ', 'A', '2019-02-08 13:17:26', 0),
(252, 'MARCORY', 'PHARMACIE DES HIBISCUS', ' Boulevard de Brazzaville Pres de la PMI  Potopoto Marcory', '5.2952844', '-3.9960322', '21 26 31 66  ', 'A', '2019-02-08 13:17:26', 0),
(253, 'MARCORY', 'PHARMACIE DES LAGUNES', 'Rue de la Paix Face a la station Shell  Marcory  Residentiel', '5.3075355', '-3.9967961', '21 26 12 40/21 26 63 80  ', 'A', '2019-02-08 13:17:26', 0),
(254, 'MARCORY', 'PHARMACIE DU BD DE MARSEILLE', 'Bd marseille  Station Shell  Face Lonaci  Zone 3', '5.3075462', '-3.9967961', '21 25 76 25', 'A', '2019-02-08 13:17:26', 0),
(255, 'MARCORY', 'PHARMACIE DU CARREFOUR ', 'Bd VGE Rue du Chevalier de Clieu face BICICI Grand carrefour  Zone 4 A', '5.2997513', '-3.9917262', '21 35 11 66 / 21 35 29 70', 'A', '2019-02-08 13:17:26', 0),
(256, 'MARCORY', 'PHARMACIE DU GRAND MARCHE DE MARCORY', 'Bd Cameroun Rue F133 face Grand Marche  Remblais Marcory', '5.3002477', '-3.9774881', '21 56 90 96  ', 'A', '2019-02-08 13:17:26', 0),
(257, 'MARCORY', 'PHARMACIE DU PETIT MARCHE', 'Rue de l Eglise Sainte Therese en Allant vers la PMI Bus N 32 ', '5.3002744', '-3.9840542', '21 26 24 33 / 21 26 98 08 ', 'A', '2019-02-08 13:17:26', 0),
(258, 'MARCORY', 'PHARMACIE EBATHE', 'Avenue de la Cote d Ivoire  Prolongement PMI', '5.3030947', '-3.9849043', '21 56 77 15 ', 'A', '2019-02-08 13:17:26', 0),
(259, 'MARCORY', 'PHARMACIE ELITE', 'Bd VGE Pres de CI Telecom  Zone 4 C Marcory', '5.295069', '-3.9825399', '21 35 11 19', 'A', '2019-02-08 13:17:26', 0),
(260, 'MARCORY', ' PHARMACIE LE MERIDIEN  ', 'Bd du Cameroun dans le centre commercial Ligne 11  GFCI Marcory', '5.3027071', '-3.9819931', '21 28 27 00 / 07 89 09 85 ', 'A', '2019-02-08 13:17:26', 0),
(261, 'MARCORY', 'PHARMACIE LEILA  ', 'Non loin de l ATCI et de l institut Froebel A 150m du carrefour Dje bi Dje  Sans fil', '5.3117374', '-3.9677586', '21 26 72 94', 'A', '2019-02-08 13:17:26', 0),
(262, 'MARCORY', 'PHARMACIE LES REMBLAIS ', 'Face Commissariat du 26eme arrondissement Alliodan  Sans fil Marcory ', '5.306791', '-3.965807', '21 26 25 40', 'A', '2019-02-08 13:17:26', 0),
(263, 'MARCORY', 'PHARMACIE MARCORY PTT ', 'Bd Cameroun Derriere cours Lamartine 200m de la Poste  Residentiel Marcory', '5.3085006', '-3.9917848', '21 26 98 83 ', 'A', '2019-02-08 13:17:26', 0),
(264, 'MARCORY', 'PHARMACIE MASSARANA   ', 'Angle Bd Gabon et Av de la Cote d ivoire \nContigue a l ex Hotel Massarana Derriere Orca Deco', '5.2992208', '-3.9869927', '21 28 20 48', 'A', '2019-02-08 13:17:26', 0),
(265, 'MARCORY', 'PHARMACIE NOUVELLE PERGOLA ', 'Angle Rue Pierre et Marie Curie et Bd de Marseille apres le cash center', '5.2834548', '-3.9864191', '21 24 48 27 ', 'A', '2019-02-08 13:17:26', 0),
(266, 'MARCORY', 'PHARMACIE P.DIOSCORIDE ', '37 Bd de Marseille  Bietry', '5.2745758', '-3.9812873', '21 35 21 10 ', 'A', '2019-02-08 13:17:26', 0),
(267, 'MARCORY', 'PHARMACIE PERUSIA', 'Rue du Canal Imm Manouchka  Zone 4 C Marcory ', '5.280631', '-3.9789606', '21 35 78 85', 'A', '2019-02-08 13:17:26', 0),
(268, 'MARCORY', 'PHARMACIE PIERRE & MARIE CURIE ', 'Rue Pierre et Marie Curie Centre commercial Francisco pres de Renault  Zone 4 C', '5.2913941', '-3.984796', '21 35 80 30 ', 'A', '2019-02-08 13:17:26', 0),
(269, 'MARCORY', 'PHARMACIE SAINT JOSEPH', ' 6 Rue du 7 Decembre face a AMORE  Zone 4 C', '5.2848056', '-3.9763309', '21 24 45 19/21 24 45 20', 'A', '2019-02-08 13:17:26', 0),
(270, 'MARCORY', 'PHARMACIE SAINTE THERESE', 'Avenue TSF Face a l Eglise  Ste Therese Marcory', '5.3038104', '-3.9899066', '21 26 77 10 ', 'A', '2019-02-08 13:17:26', 0),
(271, 'MARCORY', 'PHARMACIE SAINT ANTOINE ', 'Rue Dr Blanchard Pres du centre Sportif Zirignon  Zone 4 C', '5.2893236', '-3.9802721', '21 35 27 00 / 01 08 69 25', 'A', '2019-02-08 13:17:26', 0),
(272, 'MARCORY', 'PHARMACIE SAINTE MARIE-MAJEURE ', 'Rue du Canal a cote du garage Top auto  Zone 4 C Marcory ', '5.2773253', '-3.9735914', '21 25 54 07 ', 'A', '2019-02-08 13:17:26', 0),
(273, 'MARCORY', 'PHARMACIE SAINTE RUTH', 'Centre Commercial Prima  Zone 4 C Marcory', '5.2953979', '-3.9852932', '21 26 24 33', 'A', '2019-02-08 13:17:26', 0),
(274, 'MARCORY', 'PHARMACIE TIACOH ', 'Bd du Gabon et Carrefour Autoroute  Angle Rue Pierre et Marie Curie', '5.2971923', '-3.9828926', '21 26 28 15', 'A', '2019-02-08 13:17:26', 0),
(275, 'MARCORY', 'PHARMACIE TSF  ', 'Av TSF Non loin du 9eme Arrondissement  Konan Raphael ', '5.306823', '-3.9877796', '21 26 47 82', 'A', '2019-02-08 13:17:26', 0),
(276, 'MARCORY', 'PHARMACIE DU BD GISCARD D  ESTAING', 'Marcory Carrefour Score  Station Texaco', '5.3076155', '-4.0033622', '', 'A', '2019-02-08 13:17:26', 0),
(277, 'MARCORY', 'PHARMACIE ES BIA', 'Bd du Gabon ligne 32 a cote de la Clinique la Madone  Sicogi Marcory', '5.2945172', '-3.9783326', '21 28 08 88', 'A', '2019-02-08 13:17:26', 0);
INSERT INTO `pharmacies` (`id_pharm`, `localite_pharm`, `libelle_pharm`, `adresse_pharm`, `latitude_pharm`, `longitude_pharm`, `telephone_pharm`, `etat_pharm`, `date_ajout_pharm`, `ville_id`) VALUES
(278, 'MARCORY', 'PHARMACIE LANVIA', 'Rue Pierre et Marie Curie en face de Metalux  Zone 4 C', '5.2870023', '-3.9847674', '49 86 82 15 / 21 21 24 00', 'A', '2019-02-08 13:17:26', 0),
(279, 'MARCORY', 'PHARMACIE MAHANDIANA', 'Marcory sicogi zone de l eglise ste Bernadette derriere le marche', '5.3068177', '-3.9723731', '21 26 21 62 / 57 04 86 36', 'A', '2019-02-08 13:17:26', 0),
(280, 'MARCORY', 'PHARMACIE NOTRE DAME D AFRIQUE', '37 Bd de Marseille en face College Notre dame d Afrique  Bietry  Zone 4 C ', '5.2734717', '-3.9776069', '21 25 25 26', 'A', '2019-02-08 13:17:26', 0),
(281, 'PLATEAU', ' PHARMACIE ANIAMAN', 'Rue des Banques Face BMW Imm Aniaman a cote d Alize voyage', '5.320495', '-4.016746', '20 32 12 29/07 07 16 76', 'A', '2019-02-08 13:17:26', 0),
(282, 'PLATEAU', 'PHARMACIE BEL FAM    GDE   ', 'Av Crosson Duplessis en face de MTN Plateau', '5.320256', '-4.013149', '20 33 03 30', 'A', '2019-02-08 13:17:26', 0),
(283, 'PLATEAU', 'PHARMACIE CENTRALE', 'Av Franchet d Esperey Rue Lecoeur Imm BP', '5.322427', '-4.017921', '20 22 41 41', 'A', '2019-02-08 13:17:26', 0),
(284, 'PLATEAU', 'PHARMACIE CHARDY ', 'Av chardy pres station shell  cinema le paris  entre lynx optique et cinema le Paris', '5.323899', '-4.017189', '20 21 66 01 ', 'A', '2019-02-08 13:17:26', 0),
(285, 'PLATEAU', 'PHARMACIE CHICAYA ', ' Avenue Chardy  Immeuble Nour Al Hayat', '5.323520', '-4.018203', '20 21 33 40', 'A', '2019-02-08 13:17:26', 0),
(286, 'PLATEAU', 'PHARMACIE DE L AVENUE NOGUES  ', 'Avenue Nogues  En face de la Banque Sahelo Saharienne', '5.316508', '-4.017948', '20 31 16 10 /12', 'A', '2019-02-08 13:17:26', 0),
(287, 'PLATEAU', 'PHARMACIE DES BANQUES', 'Rue Des Banques Plateau', '5.321182', '-4.017848', '20 32 94 12 ', 'A', '2019-02-08 13:17:26', 0),
(288, 'PLATEAU', 'PHARMACIE DES FINANCES ', 'Bd Clozel Imm Sitarail vers le palais de justice', '5.325898', '-4.020561', ' 20 21 70 61/20 30 39 50    ', 'A', '2019-02-08 13:17:26', 0),
(289, 'PLATEAU', 'PHARMACIE DU BD CARDE', 'Face cercle des Rails', '5.326645', '-4.024263', '20 21 01 08 ', 'A', '2019-02-08 13:17:26', 0),
(290, 'PLATEAU', 'PHARMACIE DU COMMERCE', 'Rue Du Commerce Immeuble L ebrien', '5.317976', '-4.014481', '20 22 31 31', 'A', '2019-02-08 13:17:26', 0),
(291, 'PLATEAU', 'PHARMACIE KORALIE', 'Imm JECEDA Angle Bd de la Republique et Av  Marchand ', '5.325444', '-4.019508', '20 33 88 25', 'A', '2019-02-08 13:17:26', 0),
(292, 'PLATEAU', 'PHARMACIE DU MEMORIAL  ', 'Av  16 Lamblin Imm Signal en face du Patronat ', '5.3253668', '-4.0248749', '20 32 36 90', 'A', '2019-02-08 13:17:26', 0),
(293, 'PLATEAU', 'PHARMACIE DU PALAIS DE JUSTICE ', '55 bd clozel  face palais de justice  pres de la recette municipale', '5.331372 ', '-4.020674', '20 21 95 64', 'A', '2019-02-08 13:17:26', 0),
(294, 'PLATEAU', 'PHARMACIE DU PLATEAU ', 'immeuble botreau roussel face immeuble pyramide et la BCEAO', '5.323267', '-4.016130', '22 20 79 70   ', 'A', '2019-02-08 13:17:26', 0),
(295, 'PLATEAU', 'PHARMACIE DU TERMINUS', 'Rue Treich Lapleine Immeuble Afram Face Gare Sud SOTRA', '5.3310726', '-4.0294612', '20 32 17 15', 'A', '2019-02-08 13:17:26', 0),
(296, 'PLATEAU', 'PHARMACIE DU TIAMA ', 'Angle bd De la republique et av Jesse owens  face hotel tiama', '5.3311152', '-4.0294612', '20 21 34 18', 'A', '2019-02-08 13:17:26', 0),
(297, 'PLATEAU', 'PHARMACIE DU TREFLE', 'Av  Nogues Face Afriland First banque', '5.317900', '-4.016550', '20 22 88 02  ', 'A', '2019-02-08 13:17:26', 0),
(298, 'PLATEAU', 'PHARMACIE LES HARMONIES ', 'non loin du camp militaire galieni  20 m de la gare sotra \nde la cite administrative bd carde face immeuble les harmonies', '5.330816', '-4.024011', '20 22 04 92/ 02 02 04 76', 'A', '2019-02-08 13:17:26', 0),
(299, 'PLATEAU', 'PHARMACIE LES STUDIOS  ', 'Ancien hotel du parc 43 bd de la Republique', '5.3208671', '-4.0200365', '20 21 95 95/20 21 98 98', 'A', '2019-02-08 13:17:26', 0),
(300, 'PLATEAU', 'PHARMACIE LONGCHAMP ', 'Entre  l imprimerie nationale et l hotel ibis Angle avenue Marchand Boulevard Roume', '5.325650', '-4.022654', '20 22 75 98', 'A', '2019-02-08 13:17:26', 0),
(301, 'PLATEAU', 'PHARMACIE MODERNE MAZUET', 'Rue du Commerce cote Gare Sud Imm Nassar', '5.315839', '-4.017241', '20 32 96 64/220 22 21 31', 'A', '2019-02-08 13:17:26', 0),
(302, 'PLATEAU', 'PHARMACIE NIACADIE', 'Rue du Marche en face du siege de la BICICI Plateau ', '5.322457', '-4.018829', '20 21 70 68  ', 'A', '2019-02-08 13:17:26', 0),
(303, 'PLATEAU', 'PHARMACIE PLACE DE LA REPUBLIQUE', 'Av Houdaille pres ancienne Tour EECI ', '5.317801', '-4.017961', '20 21 57 78', 'A', '2019-02-08 13:17:26', 0),
(304, 'PLATEAU', 'PHARMACIE SAINT LAZARE', 'Rue Colomb face Hotel du District non loin de la SGBCI Imm Cash Center ', '5.321197', '-4.019249', '20 33 12 68', 'A', '2019-02-08 13:17:26', 0),
(305, 'PLATEAU', 'PHARMACIE ZAHARA', 'Rue du Commerce  Cote Peyrissac', '5.3213347', '-4.0892576', '20 32 20 28', 'A', '2019-02-08 13:17:26', 0),
(306, 'PLATEAU', 'PHARMACIE DES TOURS', 'av jean paul 2  pres cathedrale st paul  entrees bd angoulvant\n et bd roume  immeuble cic wtc cite administrative', ' 5.331450', '-4.021824', '20 21 91 28', 'A', '2019-02-08 13:17:26', 0),
(307, 'PLATEAU', 'PHARMACIE SAINTE ANNE', '1 Rue lecoeur Face Ambassade de France Plateau ', '5.3208284', '-4.0214053', '20 22 69 00', 'A', '2019-02-08 13:17:26', 0),
(308, 'PORT BOUET VRIDI', 'PHARMACIE AKWABA', 'Aeroport Felix Houphouet boigny', '5.255164', '-3.932827', '21 58 73 15/87 75 13 13', 'A', '2019-02-08 13:17:26', 0),
(309, 'PORT BOUET VRIDI', 'PHARMACIE ATLANTIQUE  ', 'Zone du marche de nuit  Sogefiha PortBouet', '5.255124', '-3.963994', '21 27 95 00', 'A', '2019-02-08 13:17:26', 0),
(310, 'PORT BOUET VRIDI', ' PHARMACIE BALNEAIRE', 'Route de Bassam face entree du 43eme BIMA ', ' 5.256473', '-3.957491', '21 58 07 09 ', 'A', '2019-02-08 13:17:26', 0),
(311, 'PORT BOUET VRIDI', 'PHARMACIE BALTIQUE', 'Face abattoir  Sogefiha PortBouet', '5.259424', '-3.969090', ' 21 27 66 71/08 25 36 26', 'A', '2019-02-08 13:17:26', 0),
(312, 'PORT BOUET VRIDI', 'PHARMACIE DE L OCEAN', 'Av Pacifique Rue des Espadons prolongement de la Boulangerie Boe et CEG ', '5.257269', '-3.961355', '21 27 78 48', 'A', '2019-02-08 13:17:26', 0),
(313, 'PORT BOUET VRIDI', 'PHARMACIE DE PORT-BOUET ', 'Non loin de la Mairie  Sogefiha PortBouet', '5.260538', '-3.964745', '21 27 89 17 ', 'A', '2019-02-08 13:17:26', 0),
(314, 'PORT BOUET VRIDI', 'PHARMACIE DJIGUI', 'Angle Av Atlantique et Av Pacifique Face au Grand Marche 30m du Commissariat ', '5.258156', '-3.964783', '21 27 75 86 ', 'A', '2019-02-08 13:17:26', 0),
(315, 'PORT BOUET VRIDI', 'PHARMACIE DU PHARE   ', 'Av des caraibes Pres du phare Terminus Bus 12  Phare ', '5.252335', '-3.958926', '21 27 73 73 ', 'A', '2019-02-08 13:17:26', 0),
(316, 'PORT BOUET VRIDI', 'PHARMACIE DU WHARF ', ' Quartier derriere wharf route de Grand Bassam pres du depot 8 de la sotra', '5.248129', '-3.944015', '21 27 71 31', 'A', '2019-02-08 13:17:26', 0),
(317, 'PORT BOUET VRIDI', 'PHARMACIE MALON', 'Pres de la grande Mosquee  Sogefiha PortBouet', '5.260431 ', '-3.960893', '21 27 96 14 / 07 90 68 50', 'A', '2019-02-08 13:17:26', 0),
(318, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI SIR', 'Vridi Cite entre la Cite Policiere et l Hopital Municipal de Vridi Port-Bouet ', '5.262563 ', '-3.982806', '21 27 02 60    ', 'A', '2019-02-08 13:17:26', 0),
(319, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI VILLAGE', 'Terminus bus 19 Vridi PortBouet', '5.255915', '-3.997500', '21 27 58 60', 'A', '2019-02-08 13:17:26', 0),
(320, 'PORT BOUET VRIDI', 'PHARMACIE DES PLAGES  ', 'PORT BOUET Adjouffou  Route de Bassam', '5.244134 ', ' -3.922746', '21 27 71 04', 'A', '2019-02-08 13:17:26', 0),
(321, 'PORT BOUET VRIDI', 'PHARMACIE ELOMA   ', 'Port Bouet  Gonzag Terminus du bus 17', '5.240478', '-3.899748', '21 58 60 28', 'A', '2019-02-08 13:17:26', 0),
(322, 'PORT BOUET VRIDI', ' PHARMACIE ROUTE BASSAM', 'Au carrefour du centre commercial Mobibois pres du marche de Jean Folly ', '5.2444822', '-3.9399111', '21 58 63 54 ', 'A', '2019-02-08 13:17:26', 0),
(323, 'PORT BOUET VRIDI', 'PHARMACIE SANTE POUR TOUS ', 'Jean Folly  Carrefour Casier', '5.244613', '-3.913071', '21 58 76 34 / 08 02 47 86 ', 'A', '2019-02-08 13:17:26', 0),
(324, 'PORT BOUET VRIDI', 'PHARMACIE SAINT LOUIS DE GONZAG ', 'Face lycee municipal de Gonzagueville', '5.250245', '-3.900203', '21 58 71 79', 'A', '2019-02-08 13:17:26', 0),
(325, 'PORT BOUET VRIDI', 'PHARMACIE BETHEL', 'Cite Policiere Face QG AKA NANGUI Pres de l Eglise du Reveil  Sogefiha', '5.267678 ', '-3.975362', '21 27 98 12', 'A', '2019-02-08 13:17:26', 0),
(326, 'PORT BOUET VRIDI', 'PHARMACIE  DES ENTREPRISES', ' Rue du Textile face Ancien Toyota premoto 250m environ du tripostal  Vridi ', '5.267160', '-4.002552', '21 27 17 32', 'A', '2019-02-08 13:17:26', 0),
(327, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI PALM-BEACH', 'd Petit bassam sir Carrefour Vridi Cite  Zone Industrielle Vridi ', '5.258790', '-3.982013', '21 27 52 04 ', 'A', '2019-02-08 13:17:26', 0),
(328, 'TREICHVILLE', 'PHARMACIE ALPHA 16  ', 'Av 16 Rue 17 Treichville a cote de Treich Hotel', '5.304859', '-4.009035', '21 25 88 80', 'A', '2019-02-08 13:17:26', 0),
(329, 'TREICHVILLE', 'PHARMACIE BELLEVILLE ', '7 Av sur la voie du marche belleVille', '5.302954', '-3.998566', '21 25 09 32 ', 'A', '2019-02-08 13:17:26', 0),
(330, 'TREICHVILLE', 'PHARMACIE CHEMINOT', 'Avenue 21 Rue 9  Quartier France Amerique Entre Sitarail Et Carrefour ', ' 5.301399 ', '-4.011767', '21 24 09 45 / 21 24 17 81', 'A', '2019-02-08 13:17:26', 0),
(331, 'TREICHVILLE', 'PHARMACIE DE LA BOURSE ', 'Non loin du siege de la CIE SODECI', '5.3121014', '-4.021178', '21 25 01 28', 'A', '2019-02-08 13:17:26', 0),
(332, 'TREICHVILLE', 'PHARMACIE DE L AVENUE 21', 'Rue 21 pres de la Bibliotheque Islamique Carrefour de l Av  21 Bus 05 et 03', '5.303104', '-4.006408', '21 24 83 46', 'A', '2019-02-08 13:17:26', 0),
(333, 'TREICHVILLE', ' PHARMACIE DE L ENTENTE ', 'Pharmacies Angle Av 21 Rue 44  Entente Treichville', '5.307488', '-4.002325', '21 25 26 95  ', 'A', '2019-02-08 13:17:26', 0),
(334, 'TREICHVILLE', ' PHARMACIE DES 3 MOSQUEES', 'dans l avenue 8 Rue 22 Baree A 300 M de la Station Shell Imm Nanan Yamoussou ', '5.308862 ', '-4.007916', '21 24 22 72 / 21 24 21 96 ', 'A', '2019-02-08 13:17:26', 0),
(335, 'TREICHVILLE', 'PHARMACIE DES BRASSEURS', '1 Rue des brasseurs  Face au CHU Entree station Total et Abidjan Continu  Km 4', '5.295843', '-4.002951', '21 25 17 25 / 21 25 31 51', 'A', '2019-02-08 13:17:26', 0),
(336, 'TREICHVILLE', ' PHARMACIE DES QUAIS ', 'Face Grands Moulins Zone portuaire', '5.306633 ', '-4.021562', '21 24 04 94 / 21 24 04 99', 'A', '2019-02-08 13:17:26', 0),
(337, 'TREICHVILLE', 'PHARMACIE DU GRAND MARCHE ', 'Av 8 Angle Rue 12 en face de la Foire de Chine Treichville ', '5.3086958', '-4.017051', '21 24  00 72', 'A', '2019-02-08 13:17:26', 0),
(338, 'TREICHVILLE', 'PHARMACIE DU LEVANT ', 'Rue 12 Imm Charara  Zone 1', '5.302277 ', '-4.010796', '21 24 14 21   ', 'A', '2019-02-08 13:17:26', 0),
(339, 'TREICHVILLE', 'PHARMACIE DU PALAIS DES SPORTS', ' Non loin Palais des sports Prolongement de la Banque BOA  Zone 2  ', '5.297980', '-4.008498', '21 24 02 14 / 21 24 09 83', 'A', '2019-02-08 13:17:26', 0),
(340, 'TREICHVILLE', 'PHARMACIE DU PLAZA', 'v 1 Rue 12 face a la Garde Republicaine', '5.3106688', '-4.0158248', '21 24 03 39/21 24 13 33 ', 'A', '2019-02-08 13:17:26', 0),
(341, 'TREICHVILLE', 'PHARMACIE DU PORT', 'Port de peche  Zone portuaire', '5.289358 ', '-4.008633', '21 25 60 32', 'A', '2019-02-08 13:17:26', 0),
(342, 'TREICHVILLE', 'PHARMACIE D ARRAS ', 'Gare de Bassam', '5.300640', '-4.002856', '21 24 13 29 / 21 24 21 93', 'A', '2019-02-08 13:17:26', 0),
(343, 'TREICHVILLE', 'PHARMACIE FLORA ', ' Av 08 pres du Commissariat du 2eme Arrondissement et du Cinema Vox  ', ' 5.306861 ', '-4.014399', '21 24 05 91', 'A', '2019-02-08 13:17:26', 0),
(344, 'TREICHVILLE', 'PHARMACIE FRANCE AFRIQUE', 'Angle Avenue 2 Rue 5  Face Cite Policiere et Stations Shell et Mobile', '5.308992 ', '-4.016066', ' 21 24 18 19  ', 'A', '2019-02-08 13:17:26', 0),
(345, 'TREICHVILLE', 'PHARMACIE JEANNE D  ARC  ', 'Av 12 Rue 05 Ex Cite RAN Treichville', '5.306972 ', '-3.998234', '21 24 04 74 ', 'A', '2019-02-08 13:17:26', 0),
(346, 'TREICHVILLE', 'PHARMACIE LA GRACE ', 'Avenue 14 Rue 12 Treichville', '5.309961 ', '-4.011581', '21 35 38 79 ', 'A', '2019-02-08 13:17:26', 0),
(347, 'TREICHVILLE', 'PHARMACIE LES ARCHANGES', ' Bd de Marseille derriere le Palais des Sports Rond point CHU  Zone 2', '5.3044946', '-4.0137045', '21 24 11 37 / 21 35 87 66 ', 'A', '2019-02-08 13:17:26', 0),
(348, 'TREICHVILLE', 'PHARMACIE MEGANE', '6 Rue des Brasseurs carrefour Sococe Safca  Zone 3', '5.296868', '-4.000387', '21 25 35 57   ', 'A', '2019-02-08 13:17:26', 0),
(349, 'TREICHVILLE', 'PHARMACIE MEROUANE ', 'Av 16 Rue 25 Face Station Total', '5.334247 ', '-4.045925', '21 24 09 29', 'A', '2019-02-08 13:17:26', 0),
(350, 'TREICHVILLE', 'PHARMACIE MYRIAM DE BIAFRA ', 'Angle Av 2 Rue 29 Bus 74 11 04 Imm Saint Lazare ', '5.3129816', '-4.0084541', ' 21 35 76 84 ', 'A', '2019-02-08 13:17:26', 0),
(351, 'TREICHVILLE', 'PHARMACIE NANAN YAMOUSSO  ', 'Rue 38 Angle Av 13 juste en face Imm Nanan Yamousso ', '5.3079233', '-4.0072632', '21 24 23 15  ', 'A', '2019-02-08 13:17:26', 0),
(352, 'TREICHVILLE', 'PHARMACIE NOTRE DAME ', 'Av 21 Rue 38 en face de la Station Shell', '5.304279', '-4.003990', '21 24 10 60 ', 'A', '2019-02-08 13:17:26', 0),
(353, 'TREICHVILLE', 'PHARMACIE ODE ', 'Av 24 Rue 38 a la Gare de Bassam a cote de l Agence Bicici', '5.3038892', '-4.0061675', '21 25 39 27 ', 'A', '2019-02-08 13:17:26', 0),
(354, 'TREICHVILLE', 'PHARMACIE ROND POINT DU CHU  ', ' Bd de Marseille face Bicici Sud rond point du Chu  Km 4', '5.294973 ', '-4.002717', ' 21 35 73 03 ', 'A', '2019-02-08 13:17:26', 0),
(355, 'TREICHVILLE', 'PHARMACIE ROY  ', 'Place du Marche non loin de la foire de Chine', '5.308687', '-4.012847', '21 24 09 13 ', 'A', '2019-02-08 13:17:26', 0),
(356, 'TREICHVILLE', ' PHARMACIE RUE 12 ', 'Av12 Rue 12 face Bicici Treichville', '5.3082759', '-4.0150362', '21 24 53 91', 'A', '2019-02-08 13:17:26', 0),
(357, 'TREICHVILLE', 'PHARMACIE SAINTE JEANNE  ', 'Entree Marche De Belleville Face Station Petro Ivoire', '5.306059', '-3.998076', '21 24 57 41', 'A', '2019-02-08 13:17:26', 0),
(358, 'TREICHVILLE', 'PHARMACIE SAINTE ADELE  ', 'Facade arriere du Marche Belle Ville face quartier Gbatanikro Face habitat Craonne', '5.304934', '-3.997435', '21 25 47 00 ', 'A', '2019-02-08 13:17:26', 0),
(359, 'TREICHVILLE', ' PHARMACIE DE LA CASERNE', 'Bd VGE face marche de portables quartier Douanes  Cite douane', '5.297401', '-4.013513', '21 25 45 83', 'A', '2019-02-08 13:17:26', 0),
(360, 'TREICHVILLE', 'PHARMACIE IMANN', 'Bd Marseille entre Sari Peugeot et Ets Bernabe', '5.292987 ', '-3.997603', ' 21 25 13 14 /  21 25 13 15', 'A', '2019-02-08 13:17:26', 0),
(361, 'TREICHVILLE', 'PHARMACIE MEROUANE', 'Av 16 Rue 25 Face Station Total Treichville', '5.334247 ', '-4.045925', '21 24 09 29', 'A', '2019-02-08 13:17:26', 0),
(362, 'TREICHVILLE', 'PHARMACIE PALAIS DE LA CULTURE', 'Rue 20 Barre Palais de la culture a la Station Total ', '5.3039159', '4.0127336', '21 24 13 19', 'A', '2019-02-08 13:17:26', 0),
(363, 'WILLIAMSVILLE', 'PHARMACIE AIMESS', 'Williamsville 2  route MACACI face CRS', '5.368957', '-4.025953', '20 37 21 24', 'A', '2019-02-08 13:17:26', 0),
(364, 'WILLIAMSVILLE', 'PHARMACIE DE WILLIAMSVILLE ', 'Pres de la Mosquee de Williamsville', '5.366099', '-4.020579', '20 37 03 45 ', 'A', '2019-02-08 13:17:26', 0),
(365, 'WILLIAMSVILLE', ' PHARMACIE DU PROGRES', 'Pres Terminus 14 espace Tere  Williamsville Adjame', '5.372443', '-4.022628', '20 37 15 25 ', 'A', '2019-02-08 13:17:26', 0),
(366, 'WILLIAMSVILLE', 'PHARMACIE L EMMANUEL ', 'carrefour 2plataeux entre la staion shell et le depot sotra', '5.364903 ', '-4.016436', '20 38 40 17  ', 'A', '2019-02-08 13:17:26', 0),
(367, 'WILLIAMSVILLE', 'PHARMACIE RAPHA  ', 'En allant vers Macaci face Boulangerie BMW  Williamsville', '5.372434', '-4.026636', '20 37 96 88 /68 73 06 38', 'A', '2019-02-08 13:17:26', 0),
(368, 'WILLIAMSVILLE', 'PHARMACIE ST URIEL ', ' Route du Zoo carrefour Paillet Adjame ', '5.372180', '-4.012200', '20 37 23 75  ', 'A', '2019-02-08 13:17:26', 0),
(369, 'YOPOUGON', ' PHARMACIE ANANERAIE  ', 'Ancienne route de Dabou carrefour Oasis', '5.3548058', '-4.1021601', '23 46 72 13/23 46 98 27', 'A', '2019-02-08 13:17:26', 0),
(370, 'YOPOUGON', ' PHARMACIE ARTEMIA  ', 'Face Palais de Justice  Sideci Yopougon', '5.3265673', '-4.0793824', '23 51 68 06', 'A', '2019-02-08 13:17:26', 0),
(371, 'YOPOUGON', ' PHARMACIE BELLE FONTAINE', 'Prolongement Patisserie du Marche  Sicogi', '5.3374472', '-4.0778835', '23 43 11 52', 'A', '2019-02-08 13:17:26', 0),
(372, 'YOPOUGON', ' PHARMACIE CARREFOUR KOWEIT   ', 'Route d Abobodoume face groupe Scolaire Petit Soleil Carrefour Koweit  Toits rouges', '5.322142', '-4.0568304', '23 52 38 54 ', 'A', '2019-02-08 13:17:26', 0),
(373, 'YOPOUGON', ' PHARMACIE CHRIST-ROI  ', 'Carrefour Jean Paul 2  Toits rouges', '5.3305805', '-4.049834', '23 45 44 55', 'A', '2019-02-08 13:17:26', 0),
(374, 'YOPOUGON', 'PHARMACIE AKADJOBA ', 'Carrefour Akadjoba Sideci non loin du Palais de Justice', '5.3245181', '-4.0829743', ' 23 51 82 42 ', 'A', '2019-02-08 13:17:26', 0),
(375, 'YOPOUGON', 'PHARMACIE ALICIA', 'Carrefour Bonikro route de Dabou  Ananeraie ', '5.3515992', '-4.1057556', '23 51 77 98 / 07 08 87 15 ', 'A', '2019-02-08 13:17:26', 0),
(376, 'YOPOUGON', 'PHARMACIE ASSALAM', ' carrefour de l Allocodrome  Niangon Sud', '5.3157648', '-4.0939173', '23 45 40 43/23 45 42 76 ', 'A', '2019-02-08 13:17:26', 0),
(377, 'YOPOUGON', 'PHARMACIE ASSONVON', 'Qt Assavon A 200m du cinema Saguidiba  pres Hotel Assonvon derriere la CNPS', '5.3325474', '-4.0701821', '23 51 82 42', 'A', '2019-02-08 13:17:26', 0),
(378, 'YOPOUGON', 'PHARMACIE AWALE', 'Face au Petit Marche  Selmer', '5.3408082', '-4.0699159', '23 46 01 02   ', 'A', '2019-02-08 13:17:26', 0),
(379, 'YOPOUGON', 'PHARMACIE BATY  ', ' Pres de la Gendarmerie Toits rouges', '5.3273354', '-4.0576583', '23 45 65 59 ', 'A', '2019-02-08 13:17:26', 0),
(380, 'YOPOUGON', 'PHARMACIE BEL AIR ', 'Carrefour Bel Air en face de la SGBCI', '5.346727', '-4.0657545', '23 52 74 43', 'A', '2019-02-08 13:17:26', 0),
(381, 'YOPOUGON', 'PHARMACIE BOISSY', 'Face au Commissariat du 19eme Arrondissement Bus 44 37 38  Toits rouges', '5.3220582', '-4.1108159', '23 45 62 34', 'A', '2019-02-08 13:17:26', 0),
(382, 'YOPOUGON', 'PHARMACIE CAMP MILITAIRE ', 'Camp Militaire  Entre le Groupement des Sapeurs Pompiers et la CNPS', '5.3203522', '-4.0648841', '23 52 62 03  ', 'A', '2019-02-08 13:17:26', 0),
(383, 'YOPOUGON', 'PHARMACIE CARREFOUR BOBY', 'Niangon  Face Base Cie  Entre Stations Petro Ivoire Et Shell', '5.3204082', '-4.0681672', '23 52 93 58/23 51 77 53 ', 'A', '2019-02-08 13:17:26', 0),
(384, 'YOPOUGON', 'PHARMACIE CARREFOUR GHANDI', 'Qrtier Ghandi  Koweit Yopougon', '5.3236316', '-4.0609064', '07 00 75 45', 'A', '2019-02-08 13:17:26', 0),
(385, 'YOPOUGON', 'PHARMACIE CELIA ', 'Sur La Route De Dabou  Carrefour Lycee Technique Banco 2', '5.3488917', '-4.0976593', '23 53 78 76 ', 'A', '2019-02-08 13:17:26', 0),
(386, 'YOPOUGON', 'PHARMACIE CHE N TALE ', 'Bordure de Route voie menant au carrefour Sorbonne non loin de\n la clinique ste Jane Leora et du college Guchanrolain  Ananeraie', '5.3510338', '-4.0989854', '23 51 87 58', 'A', '2019-02-08 13:17:26', 0),
(387, 'YOPOUGON', 'PHARMACIE CHIGATA', 'Route d Abobodoume  200m des Sapeurs Pompiers Ligne Bus 42 Toits rouges', '5.3661302', '-4.0057031', '23 46 99 57', 'A', '2019-02-08 13:17:26', 0),
(388, 'YOPOUGON', 'PHARMACIE CITE MAROC ', 'Yopougon Maroc Zone de lantenne Pres du Supermarche Cash 225', '5.3358324', '-4.0890112', '23 45 87 32 / 23 45 32 42', 'A', '2019-02-08 13:17:26', 0),
(389, 'YOPOUGON', 'PHARMACIE D ANDOKOI ', '1er pont a droite entre Centre Ivoire Couture et College Moderne ', '5.3623102', '-4.0692883', '23 52 43 06 ', 'A', '2019-02-08 13:17:26', 0),
(390, 'YOPOUGON', 'PHARMACIE DE GESCO', 'Quartier Gesco Face au Marche Gesco', '5.3631197', '-4.099836', '23 52 61 75  / 05 99 28 54', 'A', '2019-02-08 13:17:26', 0),
(391, 'YOPOUGON', 'PHARMACIE DE L ANTENNE', 'Face a la CI Telcom Orange  Sideci ', '5.3341301', '-4.0754866', '23 51 88 89 / 07 45 24 14', 'A', '2019-02-08 13:17:26', 0),
(392, 'YOPOUGON', 'PHARMACIE DE LA CITE ', 'Face Sapeur Pompiers  Toits rouges Yopougon ', '5.3349519', '-4.1025341', '23 50 15 27 ', 'A', '2019-02-08 13:17:26', 0),
(393, 'YOPOUGON', 'PHARMACIE DE LA MAIRIE', 'A 200 m de la mairie face maquis Tantie Margot Ligne Bus 30  Selmer', '5.3350368', '-4.1025342', '23 46 60 53 ', 'A', '2019-02-08 13:17:26', 0),
(394, 'YOPOUGON', 'PHARMACIE DE L  AUTOROUTE', 'Gesco Terminus de Bus 34 pres de la Brigade Gendarmerie Autoroute ', '5.3489184', '-4.1042254', ' 23 45 43 70', 'A', '2019-02-08 13:17:26', 0),
(395, 'YOPOUGON', 'PHARMACIE DE L  HOREB ', 'Sideci  Lem 1er lavage', '5.3296215', '-4.0840411', '23 46 94 23', 'A', '2019-02-08 13:17:26', 0),
(396, 'YOPOUGON', 'PHARMACIE DES 3 PONTS', 'Pres centre de sante PMI et Maternite  Sogefiha', '5.3423905', '-4.0823758', '23 53 16 32 ', 'A', '2019-02-08 13:17:26', 0),
(397, 'YOPOUGON', 'PHARMACIE DES ESPERANCES', 'Route Academie de la mer carrefour Lokoa Terminus Bus 39 Niangon a* droite', '5.3228602', '-4.0847718', '23 52 16 39 ', 'A', '2019-02-08 13:17:26', 0),
(398, 'YOPOUGON', 'PHARMACIE DIVINE GRACE ', 'Entree PortBouet II Voie de Dabou ', '5.355407', '-4.0889422', '23 52 23 23 ', 'A', '2019-02-08 13:17:26', 0),
(399, 'YOPOUGON', 'PHARMACIE DU CENACLE ', '3e pont apres la cite policiere BAE Zone CHU', '5.3595833', '-4.0876463', '23 45 15 16', 'A', '2019-02-08 13:17:26', 0),
(400, 'YOPOUGON', 'PHARMACIE DU LAVAGE ', 'Pres du Carrefour Lavage  Sicogi ', '5.3406824', '-4.0746074', ' 23 46 59 02', 'A', '2019-02-08 13:17:26', 0),
(401, 'YOPOUGON', 'PHARMACIE DU MARCHE', 'Pres du Marche de la Sicogi Sicogi', '5.3406931', '-4.0746074', '23 50 95 02 ', 'A', '2019-02-08 13:17:26', 0),
(402, 'YOPOUGON', 'PHARMACIE DU NOUVEAU QUARTIER ', 'Axe Complexe sportif  Caserne des sapeurs pompiers', '5.3387672', '-4.0646578', '23 53 12 15 / 07 94 06 79', 'A', '2019-02-08 13:17:26', 0),
(403, 'YOPOUGON', 'PHARMACIE DU PROGRES', 'Non loin de la station Petro ivoire  Maroc ', '5.3407409', '-4.0899283', '23 46 39 39 ', 'A', '2019-02-08 13:17:26', 0),
(404, 'YOPOUGON', 'PHARMACIE EDEN', 'College Segbe Carrefour Awa  Toits rouges', '5.3227704', '-4.0519408', '23 46 81 38', 'A', '2019-02-08 13:17:26', 0),
(405, 'YOPOUGON', 'PHARMACIE ESTHER ', 'Niangon a droite Academie face a la Cite Cafeier', '5.3230292', '-4.0847718', '23 50 50 23', 'A', '2019-02-08 13:17:26', 0),
(406, 'YOPOUGON', 'PHARMACIE GABRIEL GARE', 'Pres de lecole des Sourds  Wassakara', '5.3553212', '-4.0736103', '23 46 30 03 ', 'A', '2019-02-08 13:17:26', 0),
(407, 'YOPOUGON', 'PHARMACIE JEAN PIERRE ', 'Terminus Bus 39 face Carrefour Ancien Jatak Niangon Sud a droite  ', '5.3221314', '-4.1056452', ' 23 51 47 46  ', 'A', '2019-02-08 13:17:26', 0),
(408, 'YOPOUGON', 'PHARMACIE KENEYA ', 'Yopougon Boulevard Principal', '5.3468525', '-4.0754264', '23 52 70 70 / 07 05 57 93 ', 'A', '2019-02-08 13:17:26', 0),
(409, 'YOPOUGON', 'PHARMACIE KOUTE ', 'Terminus 40  Sideci Yopougon', '5.330387', '-4.076248', '23 46 59 00/ 07 01 10 83 ', 'A', '2019-02-08 13:17:26', 0),
(410, 'YOPOUGON', 'PHARMACIE LA DELIVRANCE', ' Zone Industrielle Yopougon', '5.3687457', '-4.0818658', '23 46 06 33  ', 'A', '2019-02-08 13:17:26', 0),
(411, 'YOPOUGON', 'PHARMACIE LA MAISON BLANCHE ', 'Maison blanche  Banco 2', '5.3475575', '-4.0849801', '23 45 06 66  ', 'A', '2019-02-08 13:17:26', 0),
(412, 'YOPOUGON', 'PHARMACIE LAGOMA ', 'Carrefour Academie face Cite CNPS', '5.323695', '-4.1109527', '23 46 02 15', 'A', '2019-02-08 13:17:26', 0),
(413, 'YOPOUGON', 'PHARMACIE LAOULO', 'Nouveau quartier Nouveau bureau SODECI face clinique medicale LES OLIVIERS', '5.3340243', '-4.0646132', '23 45 16 15', 'A', '2019-02-08 13:17:26', 0),
(414, 'YOPOUGON', 'PHARMACIE LE JOURDAIN', 'Apres la Paroisse Saint Mathieu cite Verte  Niangon Nord', '5.3551238', '-4.1086968', '23 52 27 47', 'A', '2019-02-08 13:17:26', 0),
(415, 'YOPOUGON', 'PHARMACIE LEA   ', 'Non loin de la Mairie Annexe de Niangon  Niangon Nord', '5.3552087', '-4.1086969', '23 45 09 62 / 23 50 23 09 ', 'A', '2019-02-08 13:17:26', 0),
(416, 'YOPOUGON', 'PHARMACIE LEM', 'A 300m du Cinema Saguidiba en face de la Mairie technique  Sideci', '5.3315757', '-4.0793207', '23 52 27 47', 'A', '2019-02-08 13:17:26', 0),
(417, 'YOPOUGON', 'PHARMACIE LES BEATITUDES', 'Nouveau quartier 1er arret du bus 37 Face Station Sape', '5.3405408', '-4.0609764', '23 53 23 17 / 03 30 50 89 ', 'A', '2019-02-08 13:17:26', 0),
(418, 'YOPOUGON', 'PHARMACIE LES ECLUSES', 'Entre depot Sotra et college Minerva  Andokoi', '5.3658704', '-4.0760461', '23 46 84 85 ', 'A', '2019-02-08 13:17:26', 0),
(419, 'YOPOUGON', 'PHARMACIE LES ELYSEES ', 'Non loin de la Coopec  Ananeraie', '5.3437657', '-4.0986201', '23 45 09 62', 'A', '2019-02-08 13:17:26', 0),
(420, 'YOPOUGON', 'PHARMACIE LES OLIVIERS', '1er pont face a la station Shell de l institut des aveugles', '5.3531641', '-4.0662004', '23 45 06 66', 'A', '2019-02-08 13:17:26', 0),
(421, 'YOPOUGON', 'PHARMACIE LOTUS', ' Niangon Sud a gauche Terminus Bus 27', '5.3170879', '-4.0910415', '23 48 09 69 ', 'A', '2019-02-08 13:17:26', 0),
(422, 'YOPOUGON', 'PHARMACIE MAMIE ADJOUA', 'Non loin de la cite Mamie Adjoua Gesco Residentiel', '5.3585502', '-4.0982897', '23 46 27 15', 'A', '2019-02-08 13:17:26', 0),
(423, 'YOPOUGON', 'PHARMACIE MATY ', 'Entre la station Lubafrique et le College Anador  Maroc', '5.3326847', '-4.1056199', '23 46 28 14', 'A', '2019-02-08 13:17:26', 0),
(424, 'YOPOUGON', 'PHARMACIE MICHEL ARCHANGE', 'Cite Verte 100 M DU Terminus 44', '5.3308559', '-4.1153638', '23 45 24 80', 'A', '2019-02-08 13:17:26', 0),
(425, 'YOPOUGON', 'PHARMACIE NANKOKO', 'Entre Sapeurs Pompiers et CNPS  Camp militaire', '5.328118', '-4.0656623', '23 50 02 06', 'A', '2019-02-08 13:17:26', 0),
(426, 'YOPOUGON', 'PHARMACIE NIANGON NORD', ' Entre SICOGI Academie de la Mer et la Cite Verte Bus 44  Niangon Nord', '5.3292993', '-4.1046703', '23 45 15 44', 'A', '2019-02-08 13:17:26', 0),
(427, 'YOPOUGON', 'PHARMACIENIKIBEL    EX NISSI   ', 'Ancien Bel Air avant carrefour college St Louis\n zone du Marche GFCI face Maquis le Pitch SOPIM', '5.3294318', '-4.1375013', '23 00 14 67', 'A', '2019-02-08 13:17:26', 0),
(428, 'YOPOUGON', 'PHARMACIE PALOMA', 'Gare Sable carrefour Auto ecole  Wassakara ', '5.3558437', '-4.0708351', '23 45 07 07 ', 'A', '2019-02-08 13:17:26', 0),
(429, 'YOPOUGON', 'PHARMACIE PHALENES', 'A gauche de Saint Pierre Niangon Sud Yopougon', '5.3233492', '-4.0900053', '23 45 70 75', 'A', '2019-02-08 13:17:26', 0),
(430, 'YOPOUGON', 'PHARMACIE PHENIX', 'Au feu de Banco 2  nouvelle route Port Bouet 2 gare BDF', '5.3478252', '-4.081227', '23 46 57 77', 'A', '2019-02-08 13:17:26', 0),
(431, 'YOPOUGON', 'PHARMACIE PRINCIPALE', 'Yopougon Attie sur Le Bd Principal  Face A Ficgayo arret Bus Station Esso', '5.323439', '-4.1228363', '23 46 62 36', 'A', '2019-02-08 13:17:26', 0),
(432, 'YOPOUGON', 'PHARMACIE ROXANNE', 'Cite CIE  Base CIE Yopougon', '5.3394182', '-4.0915248', '23 50 72 77', 'A', '2019-02-08 13:17:26', 0),
(433, 'YOPOUGON', 'PHARMACIE SAINT ANDRE', 'Apres la Cathedrale St Andre  Sicogi', '5.3423824', '-4.0770145', '23 50 48 79', 'A', '2019-02-08 13:17:26', 0),
(434, 'YOPOUGON', 'PHARMACIE SAINT CLEMENT', 'Face au CHU  Mamie Adjoua Yopougon', '5.3424303', '-4.0923354', '23 51 96 46 ', 'A', '2019-02-08 13:17:26', 0),
(435, 'YOPOUGON', 'PHARMACIE SAINT HERMANN', 'Entree face station Petro Ivoire  Base CIE ', '5.3395141', '-4.0819224', '23 45 24 24 ', 'A', '2019-02-08 13:17:26', 0),
(436, 'YOPOUGON', 'PHARMACIE SAINT LAURENT', 'Camp Militaire  Gare des Gbakas  200m Terminus Sotra  Bus 37', '5.3198092', '-4.0637504', '23 45 28 77', 'A', '2019-02-08 13:17:26', 0),
(437, 'YOPOUGON', 'PHARMACIE SAINT MARTIN ', 'Face College IGES Terminus 42  Sideci', '5.3216879', '-4.0823921', '23 46 47 64', 'A', '2019-02-08 13:17:26', 0),
(438, 'YOPOUGON', 'PHARMACIE SAINT PIERRE', ' Pres de l Ancienne Gare  2eme arret du bus 39 200m Entree Ancienne Mosquee', '5.3401792', '-4.1064084', '23 52 11 31 ', 'A', '2019-02-08 13:17:26', 0),
(439, 'YOPOUGON', 'PHARMACIE SAINTE RITA', 'Niangon Nord non loin de l eglise Ste Rita de Cascia', '5.3274379', '-4.098689', '23 53 08 73', 'A', '2019-02-08 13:17:26', 0),
(440, 'YOPOUGON', 'PHARMACIE SANDRINA', 'Carrefour Ecole Libanaise  Carrefour de la Sorbonne  Ananeraie', '5.3491882', '-4.1015365', '23 45 29 03', 'A', '2019-02-08 13:17:26', 0),
(441, 'YOPOUGON', 'PHARMACIE SCHEKINA', 'Siporex Rue Du Marche Gouro Viande De Brousse ', '5.3526454', '-4.0785674', '23 50 88 56', 'A', '2019-02-08 13:17:26', 0),
(442, 'YOPOUGON', 'PHARMACIE SEGAI', 'Au marche  Wassakara ', '5.3493278', '-4.070054', '23 46 09 00', 'A', '2019-02-08 13:17:26', 0),
(443, 'YOPOUGON', 'PHARMACIE SHALOM', 'Sogefiha Pont Vagabond non loin du Terminus du Bus 47', '5.3395034', '-4.0819224', '23 50 04 47', 'A', '2019-02-08 13:17:26', 0),
(444, 'YOPOUGON', 'PHARMACIE GLORIA', 'A proximite de la station Total Ananeraie a 500 m du carrefour Oasis Anananeraie', '5.3549703', '-4.0968589', '23 45 16 93', 'A', '2019-02-08 13:17:26', 0),
(445, 'YOPOUGON', 'PHARMACIE SIPOREX', 'Pres de la Gare Yopougon Ancienne Route', '5.3555432', '-4.0765989', '23 51 80 41', 'A', '2019-02-08 13:17:26', 0),
(446, 'YOPOUGON', 'PHARMACIE TEREZA', 'Route Niangon * Magasin', '5.3366475', '-4.0860766', '23 45 31 89', 'A', '2019-02-08 13:17:26', 0),
(447, 'YOPOUGON', 'PHARMACIE THEODORA', 'Carrefour a droite Intersection Ligne bus 27 et 39  Niangon Sud', '5.3186391', '-4.0970233', '25 45 08 14', 'A', '2019-02-08 13:17:26', 0),
(448, 'YOPOUGON', 'PHARMACIE TIZRA', 'Toit Rouge vers Offoumou Yapo  Immeuble Ancien Bazar', '5.3298669', '-4.0523502', '23 50 88 91', 'A', '2019-02-08 13:17:26', 0),
(449, 'YOPOUGON', 'PHARMACIE TOITS ROUGE', 'Apres la BAE  Toits rouges', '5.329899', '-4.0523502', '23 45 20 39', 'A', '2019-03-05 17:56:38', 0),
(450, 'YOPOUGON', 'PHARMACIE VALORS', 'Yopougon Face Nouveau Marche de Port Bouet 2', '5.3471102', '-4.0961201', '23 52 03 40', 'A', '2019-03-05 17:56:38', 0),
(451, 'YOPOUGON', 'PHARMACIE WAKOUBOUE', 'Angle Rue du Marche Sicogi face au lavage', '5.3396808', '-4.0758076', '23 52 62 08', 'A', '2019-03-05 17:56:38', 0),
(452, 'YOPOUGON', 'PHARMACIE WASSAKARA', ' Rue Princesse pres du Carrefour cite SIB  Wassakara ', '5.3465312', '-4.0702096', '22 45 22 00', 'A', '2019-03-05 17:56:38', 0),
(453, 'YOPOUGON', 'PHARMACIE WILLIAM PONTY', 'Face College William Ponty', '5.318687', '-4.1123442', '23 46 99 57', 'A', '2019-03-05 17:56:38', 0),
(454, 'YOPOUGON', 'PHARMACIE ZEULAYET', 'Quartier Maroc face a la Mosquee ', '5.3401099', '-4.0998423', '23 46 32 99', 'A', '2019-03-05 17:56:38', 0),
(456, 'YOPOUGON', 'PHARMACIE DE PORT BOUET II ', 'Route Niangon Nord Face Mosquee AL BILAL', '5.3297938', '-4.1518932', '23 46 60 53 / 01 51 67 56', 'A', '2019-03-05 17:56:38', 0),
(457, 'YOPOUGON', 'PHARMACIE DU JUBILE ', 'Yopougon Route De Dabou Face Marche Bagnon', '5.3389579', '-4.113014', '01 05 26 10 /06 37 09 42', 'A', '2019-03-05 17:56:38', 0),
(458, 'YOPOUGON', 'PHARMACIE DU PROGRES ', 'Non loin de la station Petro ivoire  Maroc ', '5.3407409', '-4.0899283', '20 37 15 25', 'A', '2019-03-05 17:56:38', 0),
(459, 'YOPOUGON', ' PHARMACIE GNIMAH', 'Yopougon Port Bouet 2  Pres Ancienne Gare 2eme Arret Du Bus 39 ', '5.351897', '-4.089672', '23 46 54 89 / 01 88 48 86', 'A', '2019-03-05 17:56:38', 0),
(461, 'YOPOUGON', 'PHARMACIE DE LA VIE', 'Ananeraie  Zone rond point du CHU Yopougon', '5.3550759', '-4.0933759', '23 46 66 33 / 58 17 29 19', 'A', '2019-03-05 17:56:38', 0),
(462, ' YOPOUGON', 'PHARMACIE MOAYE', 'Route de Dabou pres du Cimetiere Municipal Ananeraie', '5.342936', '-4.1106171', '07 91 65 53 /  02 50 14 60', 'A', '2019-03-05 17:56:38', 0),
(463, 'YOPOUGON', 'PHARMACIE LOKOA', 'Apres le Lycee Ehivet Gbagbo  Niangon Lokoa ', '5.3168643', '-4.1019245', '23 45 07 80 / 01 25 91 24', 'A', '2019-03-05 17:56:38', 0),
(464, 'YOPOUGON', 'PHARMACIE NOTRE DAME DE LA PAIX', ' km 17 Route Dabou au Marche  Marche Bagnon ', '5.3377861', '-4.1279579', '23 45 61 97', 'A', '2019-03-05 17:56:38', 0),
(465, 'YOPOUGON', 'PHARMACIE SACRE COEUR', ' Nouveau quartier Ancien Garage Zone de la Poste  Pres du Top Pain', '5.336318', '-4.057218', '05 60 59 02 - 02 68 91 98', 'A', '2019-03-05 17:56:38', 0),
(466, 'YOPOUGON', 'PHARMACIE SANTE PLUS', 'Autoroute du Nord pres de la station Shell apres la fourriere  Gesco', '5.366827', '-4.1021839', '23 52 58 66', 'A', '2019-03-05 17:56:38', 0),
(467, 'YOPOUGON', 'PHARMACIE SELMER', 'Carrefour Baron a 100m de la Mairie Axe William Ponty Mairie', '5.3436377', '-4.0707833', '23 46 11 63 /  01 33 07 33', 'A', '2019-03-05 17:56:38', 0),
(468, 'YOPOUGON', 'PHARMACIE SAINT RAPHAEL ', 'Cite Novalim  cite Eeci  non loin de la Clinique Rita de Cascia', '5.3384896', '-4.0847477', '01 82 97 97 /  08 97 11 71', 'A', '2019-03-05 17:56:38', 0),
(469, 'YOPOUGON', 'PHARMACIE SAINTE AUDE', 'Entree Carrefour Siporex et rue Nouveau Goudron  Banco 2', '5.3554802', '-4.0796611', '23 52 28 87 /  07 03 8018', 'A', '2019-03-05 17:56:38', 0),
(470, 'YOPOUGON', 'PHARMACIE SAINTE MARIE', 'Andokoi  En Face du Pont Pieton  A Cote de la Station Total', '5.35557', '-4.1124921', '23 45 24 24', 'A', '2019-03-05 17:56:38', 0),
(471, 'YOPOUGON', 'PHARMACIE AFIA', 'Non loin du lycee jeunes filles  Millionnaire', '5.3458332', '-4.0553629', '07 46 12 50', 'A', '2019-03-05 17:56:38', 0),
(472, 'YOPOUGON', 'PHARMACIE BERAKA', 'Camp Militaire Petit Toit Rouge Carrefour Chapouli', '5.318453', '-4.0669699', '07 77 01 20 /  07 69 66 76', 'A', '2019-03-05 17:56:38', 0),
(474, 'YOPOUGON', 'PHARMACIE NANDY', 'Carrefour Koute par Eglise St Laurent entre le Terminus 40 et  la CNPS', '5.3291341', '-4.0719104', '23 45 51 12 / 01 20 56 62', 'A', '2019-03-05 17:56:38', 0),
(475, 'YOPOUGON', 'PHARMACIE  PENIEL', ' Ananeraie Rond Point Gesco', '5.3573581', '-4.1011619', '23 45 42 82 /  09 75 82 07', 'A', '2019-03-05 17:56:38', 0),
(476, 'COCODY', 'PHARMACIE CITE SIR', 'ABATTA vers la cite SIR', '5.387734', '-3.926920', ' 58 36 18 47', 'A', '2019-03-05 17:56:38', 0),
(477, 'COCODY', 'PHARMACIE ELIEZER', 'RIVIERA FAYA carrefour Akouedo face operation immobiliere ATCI', '5.363589', '-3.936494', '22 45 60 19', 'A', '2019-03-05 17:56:38', 0),
(478, 'COCODY', 'PHARMACIE MAGNIFICAT DES II PLATEAUX', 'carrefour OPERA ADJIEN tourner du cote de la station  PETROCI  100 M DU CASH IVOIRE', '5.385494', '-3.991915', '22 52 53 00', 'A', '2019-03-05 17:56:38', 0),
(479, 'COCODY ', 'PHARMACIE ST VIATEUR', 'Riviera Palmeraie  200 METRES DU ROND POINT ST VIATEUR route  HOPITAL GENERAL D ANGRE', '5.385983', '-3.955555 ', '88 88 04 80', 'A', '2019-03-05 17:56:38', 0),
(480, 'COCODY ', 'PHARMACIE THERA', 'II PLATEAUX 7eme tranche dans le centre commercial TERA', '5.377988', ' -3.988132', '22 42 49 91', 'A', '2019-03-05 17:56:38', 0),
(481, 'ADJAME ', 'PHARMACIE CHRIST MARIE DE PAILLET', 'PAILLET  extention entre le carrefour  HUMMER et la maison blanche des petits', '5.377926', ' -4.014081', '20 37 40 00', 'A', '2019-03-05 17:56:38', 0),
(482, 'ADJAME', 'PHARMACIE DU PROGRES', 'face eglise  ST KIZITO', '5.370142 ', '-4.021507', '20371525', 'A', '2019-03-05 17:56:38', 0),
(483, 'MARCORY', 'PHARMACIE EL OLAM', 'carrefour AMAGOU pres du marche', '5.308674', '-3.971056', '21 26 68 50', 'A', '2019-03-05 17:56:38', 0),
(485, 'YOPOUGON', 'PHARMACIE CITE CIE', 'YOPOUGON voie principale de la cite CIE menant a  PORT BOUET II entre AGENCE CIE de paiement', '5.342270', '-4.089240', '23 46 34 26', 'A', '2019-03-05 17:56:38', 0),
(489, 'ABOBO ', 'PHARMACIE DU DOKUI GDE', 'Au Dessus du Zoo  Station Total face Cie  Plateau Dokui', '5.399512', '-4.006495', '47 90 40 63', 'A', '2019-03-05 17:56:38', 0),
(490, 'ABOBO', 'PHARMACIE LA VIERGE DU SIGNE', 'Abobo N guessankoi derriere la station ORYX', '5.433208 ', '-4.038903', '09 92 42 79/01 12 22 70', 'A', '2019-03-05 17:56:38', 0),
(491, 'ABOBO', 'PHARMACIE DU PETIT MARCHE DU DOKUI', 'Abobo  Entre le Petit Marche  de Dokui et la Mosquee  sur la Voie menant au Mahou', '5.395853', ' -4.000011', '22 42 28 56', 'A', '2019-03-05 17:56:38', 0),
(492, 'ABOBO', 'PHARMACIE RAHMAN', 'Abobo Agbekoi  Pres de l Ecole Nord et l Ageep', '5.426089', '-4.012124 ', '24 00 29 52', 'A', '2019-03-05 17:56:38', 0),
(494, 'ABOBO', 'PHARMACIE YANDI', 'ABOBO face cite policiere en venant du terminus des bus 15 et 49', '5.435179', ' -4.016163', '09 84 14 02', 'A', '2019-03-05 17:56:38', 0),
(495, 'ATTECOUBE', 'PHARMACIE  AGOUA SARL', 'ATTECOUBE face boulangerie BBCO', '5.352037', ' -4.033731', '09 31 27 51', 'A', '2019-03-05 17:56:38', 0),
(497, 'PORT BOUET', 'PHARMACIE DJESSOU DE VRIDI', 'VRIDI 3 quartier ZIMBABWE pres de l EPP VRIDI 3', '5.262566 ', '-3.984027', '21 27 23 15', 'A', '2019-03-05 17:56:38', 0),
(499, 'PORT BOUET', 'PHARMACIE DU CARREFOUR ANCIENNE GENDARMERIE', 'Batiment ancienne gendarmerie entre usine UNICAFE et la morgue D ANYAMA', '5.462863', ' -4.052709', '22 55 64 26', 'A', '2019-03-05 17:56:38', 0),
(500, 'ADJAME', 'PHARMACIE STE MARIE THERESE D ADJAME SARL', 'ADJAME prolongement hotel BANFORA derriere FRATERNITE non loin de la maternite MARIE THERESE HOUPHOUET BOIGNY', '5.347286 ', '-4.020000', ' 56 46 19 45', 'A', '2019-03-05 17:56:38', 0),
(501, 'COCODY', 'PHARMACIE BELLEVUE', 'Angre  8eme Tranche Soleil II  Apres le terminus 205', '5.400504 ', '-3.971170', '22 50 06 24', 'A', '2019-03-05 17:56:38', 0),
(502, 'COCODY', 'PHAMACIE CITES SYNACASSI SARL', 'RIVIERA 3 SYNACASSCI route de MBADON', '5.346815', '-3.945346', '22 00 13 15', 'A', '2019-03-05 17:56:38', 0),
(503, 'COCODY', 'PHARMACIE DU FUTUR', 'Bd Mitterand Immeuble Mariam a cote Socoprix Riviera Palmeraie Triangle zone des impots', ' 5.363420', '-3.957094', '77 33 79 02', 'A', '2019-03-05 17:56:38', 0),
(504, 'COCODY', 'PHARMACIE EPHRATA', 'Prolongement mission EPHRATA RIVIERA CARREFOUR ABATA a gauche sur la route de la cite  SIR', ' 5.383055  ', '-3.931991', '75 37 94 07', 'A', '2019-03-05 17:56:38', 0),
(506, 'COCODY', 'PHARMACIE ST GABRIEL', 'Boulevard Latrille  a 900 M Du Sporting Club En Allant Vers Angre  Face Perles Grises +225 22 42 49 95', '5.388059 ', '-3.992438', '22 42 58 35', 'A', '2019-03-05 17:56:38', 0),
(507, 'COCODY', 'PHARMACIE ST MOISE', 'Cocody 9eme Tranche Abri 2000 sur la Voie Principale entre le rond point Ado et celui avant la Pharmacie Hermes', '5.378403 ', '-3.969366', '22 49 61 36', 'A', '2019-03-05 17:56:38', 0),
(508, 'COCODY', 'PHARMACIE STE CLEMENTINE', 'A 500 metres du nouveau CHU d ANGRE', '5.405370 ', '-3.955792', '52 79 93 90', 'A', '2019-03-05 17:56:38', 0),
(509, 'ABOBO', 'PHARMACIE ACTUELLE', 'Route D ABOBO BAOULE au niveau du carrefour CARREFOUR ANGRE  200 M COLLEGE VICTOR LOBAD sur nvelle route reliant ANGRE A ABOBO', '5.413713 ', '-3.994756', '22 00 41 85', 'A', '2019-03-05 17:56:38', 0),
(510, 'ABOBO', 'PHARMACIE AL FATIH', 'ABOBO SAGBE PALMERAIE  route de  BOCABO non loin du COLLEGE BOCABO', '5.415052', '-4.033514', '02 27 07 13', 'A', '2019-03-05 17:56:38', 0),
(512, 'ABOBO', 'PHARMACIE CARREFOUR NDOTRE', 'ABOBO NDOTRE  GD CARREFOUR NDOTRE  A 20 M DE LA VOIE NON BITUMEE', '5.443620 ', '-4.070814', '40 30 24 31', 'A', '2019-03-05 17:56:38', 0),
(513, 'ABOBO', 'PHARMACIE DE L EMMANUEL', 'Carrefour des deux plateaux station Shell', '5.364871 ', '-4.016447', '20 38 40 17', 'A', '2019-03-05 17:56:38', 0),
(514, 'ABOBO', 'PHARMACIE DU CHATEAU ', 'Autoroute d Abobo  Anyama', '5.489741  ', '-4.051743', '23 55 93 99', 'A', '2019-03-05 17:56:38', 0),
(515, 'ABOBO', 'PHARMACIE FADYL', 'Non loin de la  mosquee ADJA NABINTOU CISSE', '5.424012 ', '-3.997631', '57 19 44 55', 'A', '2019-03-05 17:56:38', 0),
(516, 'ABOBO', 'PHARMACIE NOURAM', 'dokui route du zoo 100 m du carrefour mauritanien dans le sens d abobote face ecole primaire nouvelles alliances', '5.408649 ', '-4.004144', '57 96 41 62', 'A', '2019-03-05 17:56:38', 0),
(518, 'YOPOUGON', 'PHARMACIE INOX', 'YOPOUGONroute de  DABOU carrefour CITE VERTE non loin du marche DJEDJE BAGNON', '5.338801 ', '-4.113802', '23 45 55 94', 'A', '2019-03-05 17:56:38', 0),
(519, 'YOPOUGON', 'PHARMACIE ZONE INDUSTRIELLE', 'YOPOUGON ZONE INDUSTRIELLE entree principale contigue a l AGENCE BICICI', '5.370418', '-4.082128', '23 46 51 08', 'A', '2019-03-05 17:56:38', 0),
(520, 'PORTBOUET', 'pharmacie belleville', 'Quartier BELLE VILLE JEAN FOLLY 100 M carrefour petit marche  carrefour LA LOBO', '5.302943', '-3.998598', '24 38 15 83', 'A', '2019-03-05 17:56:38', 0),
(521, 'ATTECOUBE', 'PHARMACIE DE BLOFODOUME', 'Vers le marche de BROFODOUME', '5.514234 ', '-3.931894', '42623272', 'A', '2019-03-05 17:56:38', 0),
(524, 'ANYAMA', 'pharmacie de la misericorde', 'QUARTIER RAN  ZONE morgue et hotelL KEDJENOU  apres STATION OIL LYBIA', '5.474486 ', '-4.053640', '23 55 64 34', 'A', '2019-03-05 17:56:38', 0),
(525, 'BINGERVILLE', 'PHARMACIE NISSI nouvelle', 'Cite FEH KESSE', '5.377988', ' -3.912448', '22 40 19 04', 'A', '2019-03-05 17:56:38', 0),
(526, 'ABOBO', 'pharmacie abobo clouetcha', 'ABOBO KENNEDY quartier CLOUETCHA carrefour ancienne boulangerie non loin du marche', '5.423774', '-4.002931', '09 50 13 82', 'A', '2019-03-05 17:56:38', 0),
(527, 'ADJAME', 'pharmacie RAPHA', 'En allant vers Macaci  face Boulangerie BMW  Williamsville', '5.372423', '-4.026636', '20 37 96 88', 'A', '2019-03-05 17:56:38', 0),
(528, 'ADJAME', 'pharmacie st emmauel', 'Bd Ge ne ral de Gaulle  Mirador  face Sicogi 30 M DU CARREFOUR', '5.365768 ', '-4.016476', '20 37 98 27', 'A', '2019-03-05 17:56:38', 0),
(529, 'ADJAME', 'pharmacie st uriel', 'paillet national route du zoo abidjan', '5.372223 ', '-4.012157', '20372375', 'A', '2019-03-05 17:56:38', 0),
(530, 'COCODY', 'pharmacie ANALYA', '400 M DU CENTRE HOSPITALIER UNIVERSITAIRE REGIONALE D ANGRE au feu face CITE GESTOCI', '5.403073 ', '-3.961344', '08 95 19 25', 'A', '2019-03-05 17:56:38', 0),
(531, 'COCODY', 'PHARMACIE EMMANUEL', 'ANGRE DJIBI rue COCOVICO  STAR 10', '5.401021 ', '-3.981140', '22 42 56 05', 'A', '2019-03-05 17:56:38', 0),
(532, 'COCODY', 'PHARMACIE LES JARDINS D EDEN', 'RIVIERA 4  route  M BADON cite EDEN face ecole LIBANAISE', '5.328582', ' -3.943270', '79 19 20 04', 'A', '2019-03-05 17:56:38', 0),
(533, 'COCODY', 'PHARMACIE ST MARTIN DE FAYA', 'RIVIERA FAYA prolongement cloture CLOTURE NOUVEAU CAMP MILITAIRE D AKOUEDO APRES CITE GENIE 2000', '5.387986', '-3.941960', '22455496', 'A', '2019-03-05 17:56:38', 0),
(534, 'YOPOUGON', 'PHARMACIE DE LA FELICITE', 'Yopougon Maroc  Carrefour Obama entre Station Shell et Hotel Kimi', '5.339750', '-4.093714', '23 46 16 12', 'A', '2019-03-05 17:56:38', 0),
(535, 'YOPOUGON', 'PHARMACIE JERICO', 'NIANGON NORD ACADEMIE FIN GOUDRON ROUTE DE PAYS BAS', '5.325397 ', '-4.111481', '23 45 62 38', 'A', '2019-03-05 17:56:38', 0),
(536, 'ABOBO', 'PHARMACIE ROUTE DU ZOO', 'Abobo Axe Dokui zoo a 200m des stations Petroci et vidange entree du quartier Dokui Olympe', '5.387460', '-4.007083', '01 14 04 34', 'A', '2019-03-05 17:56:38', 0),
(537, 'ABOBO', 'Pharmacie St Antoine', 'Abobo Sagbe Celeste derriere Rail Taxi gare', '5.417759', '-4.029922', '20 24 77 47', 'A', '2019-03-05 17:56:38', 0),
(538, 'ABOBO', 'Pharmacie Biabou Mosquee', 'Abobo Biabou Moronou route dAlepE a cote de la mosquee Cosim', '5.441558', '-3.983389', '71 51 40 85', 'A', '2019-03-05 17:56:38', 0),
(539, 'Anyama', 'Pharmacie du Carrefour Ancienne Gendarmerie', 'BATIMENT ANCIENNE GENDARMERIE ENTRE USINE UNICAFE ET LA MORGUE DANYAMA', '5.461993', '-4.052868', '22 55 64 26', 'A', '2019-03-05 17:56:38', 0),
(540, 'COCODY', 'PHARMACIE BOAZ', 'Cocody Mpouto Village', '5.328599', '-3.954020', '22 47 73 83', 'A', '2019-03-05 17:56:38', 0),
(541, 'COCODY', 'PHARMACIE CYD NIKO', 'Bd. Mitterand  route de Bingerville  Riviera Faya - Face Petit Portail de la Cite Genie 2000', '5.371333', '-3.934512', '22 47 69 27', 'A', '2019-03-05 17:56:38', 0),
(542, 'COCODY', 'PHARMACIE DU CHATEAU D EAU', 'Angre djibi chateau d  eau', '5.413656', '-3.979945', '22 51 07 49', 'A', '2019-03-05 17:56:38', 0),
(543, 'COCODY', 'PHARMACIE ST JEAN PAUL II', 'RIVIERA BONOUMIN 500 M AU NORD DABIDJAN MALL APRES CARREFOUR DJEDJE MADY UNIVERSITAIRE REGIONALE DANGRE / AU FEU FACE CITE GESTOCI', '5.366386', '-3.969271', '22 49 75 63', 'A', '2019-03-05 17:56:38', 0),
(544, 'COCODY', 'PHARMACIE DES NATIONS', 'ANGRE 7EME TRANCHE / ZONE COMMISSSARIAT DU 22EME ARRONDISSEMENT ENTRE LA SODECI ET LA PAROISSE ST AMBROISE MA VIGNE', '5.397065', '-3.989579', '22 42 23 00', 'A', '2019-03-05 17:56:38', 0),
(545, 'ADJAME', 'PHARMACIE ST MICHEL', 'Avenue 13 50m de LEglise St Michel en face du CEG - Harris', '5.347031', '-4.022754', '20 37 09 06', 'A', '2019-03-05 17:56:38', 0),
(546, 'YOPOUGON', 'PHARMACIE ANADOR', 'YOPOUGON MAROC AU NIVEAU DU CARREFOUR ANADOR / FACE MAQUIS O BAOULE', '5.336709', '-4.101043', '23 46 27 80', 'A', '2019-03-05 17:56:38', 0),
(547, 'YOPOUGON', 'PHARMACIE DE SION', 'YOPOUGON QUARTIER SIDECI TERMINUS 42 A 100 M DIGES', '5.319287', '-4.079770', '59 97 73 72', 'A', '2019-03-05 17:56:38', 0),
(548, 'YOPOUGON', 'PHARMACIE SAINT JOSEPH ACADEMIE', 'YOPOUGON ACADEMIE DES MERS A 50 M AVANT LECOLE ACADEMIE DES MERS', '5.319126', '-4.112950', '23 45 56 97', 'A', '2019-03-05 17:56:38', 0),
(549, 'YOPOUGON', 'PHARMACIE ST ANGE EMMANUEL', 'YOPOUGON ATTIE TERMINUS BUS 47', '5.340446', '-4.083750', '23 46 32 74', 'A', '2019-03-05 17:56:38', 0),
(550, 'YOPOUGON', 'PHARMACIE DU 16EME', 'Non loin de l  ecole EPP SICOGI 7', '5.337449', '-4.071233', '23 50 95 02', 'A', '2019-03-05 17:56:38', 0),
(551, 'YOPOUGON', 'PHARMACIE ZONE MICAO', 'Zone Industrielle - Quartier Micao - Cite HKB', '5.376523', '-4.084840', '23 46 55 98', 'A', '2019-03-05 17:56:38', 0),
(552, 'PORTBOUET', 'PHARMACIE ST LOUIS DE GONZAGUE', 'FACE LYCEE MUNICIPAL DE GONZAG', '5.250213', '-3.900139', '58 09 46 25', 'A', '2019-03-05 17:56:38', 0),
(553, 'KOUMASSI', 'PHARMACIE ST JOSEPH ARTISAN', 'Carrefour Caneton derri ere le lycee municipal', '5.309869', '-3.944214', '21 36 79 91', 'A', '2019-03-05 17:56:38', 0),
(554, 'ABOBO', 'PHARMACIE B S A', 'ABOBO ANOKOUA KOUTE SUR L AXE ABOBO ANYAMA PRES DE LA CIE ET DE NOUVELLE GARE INTERNATIONALE', '5.439426', '-4.040503', '77 81 19 23', 'A', '2019-03-05 17:56:38', 0),
(555, 'ABOBO', 'PHARMACIE DU MARCHE AKEIKOI', 'ABOBO AKEIKOI  ZONE DU MARCHE', '5.449845', '-4.013621', '24 39 00 35', 'A', '2019-03-05 17:56:38', 0),
(556, 'COCODY', 'PHARMACIE EMERAUDE', 'DERRIERE LA STATION TOTAL SOCOCE II PLATEAUX EN FACE DE LA CLINIQUE LE ROCHER', '5.371924', '-4.001385', '21 57 28 51', 'A', '2019-03-05 17:56:38', 0),
(557, 'COCODY', 'PHARMACIE KANNIEN', 'AKOUEDO EXTENSION FIN GOUDRON DERRIERE CITE MUGEFCI', '5.366011', '-3.921893', '22 46 54 68', 'A', '2019-03-05 17:56:38', 0),
(558, 'COCODY', 'PHARMACIE NOTRE DAME DU REFUGE ', 'ROUTE AKOUEDO ENTRE FEU PALMERAIE ET FAYA A 100 M DE LA STATION TOTAL', '5.364407', '-3.952294', '22 47 50 59', 'A', '2019-03-05 17:56:38', 0),
(559, 'KOUMASSI', 'PHARMACIE ABYA', 'Remblais  Rue du Canal  Face Station Petroci', '5.301930', '-3.969518', '21 28 62 36', 'A', '2019-03-05 17:56:38', 0),
(560, 'KOUMASSI', 'PHARMACIE ST AUGUSTE', 'Koumassi Sicogi 1 Carrefour Gelti en allant vers l Ecole La Pepiniere', '5.288222', ' -3.965574', '21 28 58 26', 'A', '2019-03-05 17:56:38', 0),
(563, 'YOPOUGON', 'PHARMACIE CHENAIN', 'Jerusalem route d Abobodoume a 200m de l hotel Gran Maxim', '5.320153', '-4.043928', '23 50 13 78', 'A', '2019-03-05 17:56:38', 0),
(564, 'YOPOUGON', 'PHARMACIE MOKLA', 'Toit Rouge Jean Paul 2 Carrefour G S  Belle Marise 50 m Carrefour Gompci', '5.331312', ' -4.045591', '23 45 00 15', 'A', '2019-03-05 17:56:38', 0),
(565, 'YOPOUGON', 'PHARMACIE BAITY', 'YOPOUGON TOIT ROUGE PRES DE LA GENDARMERIE', '5.327672', '-4.055470', '23 51 77 98', 'A', '2019-03-05 17:56:38', 0),
(566, 'YOPOUGON', 'PHARMACIE ELKANAH', 'YOPOUGON CARREFOUR CANAL  FACE IMMEUBLE CANAL A 450 M DE LA STATION PETRO IVOIRE', '5.335762', '-4.087758', '23 50 88 19', 'A', '2019-03-05 17:56:38', 0),
(568, 'YOPOUGON', 'PHARMACIE CHRIST ROI', '60 M DU Carrefour Jean Paul 2  Carrefour Toit rouge', ' 5.330928', ' -4.047742', '23 53 78 76', 'A', '2019-03-05 17:56:38', 0),
(569, 'YOPOUGON', 'PHARMACIE DE LA DELIVRANCE', 'Zone Industrielle  Pres du Depot de la Sotra Cash Center', '5.369082', ' -4.079613', '23 51 13 79', 'A', '2019-03-05 17:56:38', 0),
(570, 'YOPOUGON', 'PHARMACIE GLORIA EX PHARMACIE SILOE', 'yopougon annaneraie carrefour antenne/ 300m du carrefour CHU et EGLISE CMA /sens EGLISE CMA / carrefour OASIS', '5.355307', '-4.094713', '23 52 84 48/23 52 03 40', 'A', '2019-03-05 17:56:38', 0),
(571, 'YOPOUGON', 'PHARMACIE MOAYE ', 'Niangon Adjame Route de Dabou pres du cimetiere municipal', '5.343146', ' -4.108391', '07 91 65 53', 'A', '2019-03-05 17:56:38', 0),
(572, 'YOPOUGON', 'PHARMACIE SEGAI', 'Wassakara  Face au Marche', '5.352429', '-4.067721', '23 52 21 10', 'A', '2019-03-05 17:56:38', 0),
(573, 'YOPOUGON', 'PHARMACIE SIDECI EXTENSION', 'Sideci Carrefour Mandjo Entre Palais de Justice et le Pont Restaure', '5.323992', '-4.083334', '23 51 23 34', 'A', '2019-03-05 17:56:38', 0),
(574, 'YOPOUGON', 'PHARMACIE ST RAPHAEL', 'Cite Novalim  cite Eeci  non loin de la Clinique Rita de Cascia', '5.342201', '-4.087656', '08 97 11 71', 'A', '2019-03-05 17:56:38', 0),
(575, 'KOUMASSI', 'PHARMACIE BD DU CAMEROUN', 'Koumassi remblais ligne 11 entre les carrefours colombe et 3 ampoules', '5.296937', '-3.964143', '21 36 34 47', 'A', '2019-03-05 17:56:38', 0),
(576, 'KOUMASSI', 'PHARMACIE CHRIST EMILE', 'Quartier divo Carrefour Hopital', ' 5.305521', '-3.948877', '21 36 81 33', 'A', '2019-03-05 17:56:38', 0),
(578, 'COCODY', 'PHARMACIE STE TRINITE', 'CARREFOUR BD LATRILLE / ROUTE DE WILLIAMSVILLE / PRES SUPER MARCHE PRIX CHOC / LIGNE BUS 49', '5.368671', '-4.000382', '22 41 68 88', 'A', '2019-03-05 17:56:38', 0),
(579, 'COCODY', 'PHARMACIE VAL DE GRACE', 'riviera palmeraie / route de l hotel bellecote a 200 m du college le FIGUIER', '5.380300', ' -3.967427', '22 49 93 44', 'A', '2019-03-05 17:56:38', 0);
INSERT INTO `pharmacies` (`id_pharm`, `localite_pharm`, `libelle_pharm`, `adresse_pharm`, `latitude_pharm`, `longitude_pharm`, `telephone_pharm`, `etat_pharm`, `date_ajout_pharm`, `ville_id`) VALUES
(580, 'YOPOUGON', 'PHARMACIE SOURALEY', 'YOPOUGON PORT BOUET II / PRES DU CARREFOUR DU CENTRE SOCIAL DE PORT BOUET II A 200 M DERRIERE LE COLLEGE GUCHANROLAIN', '5.349886', ' -4.094154', '02 81 97 01', 'A', '2019-03-05 17:56:38', 0),
(581, 'PORBOUET', 'PHARMACIE KOFFI KRA', 'GONZAG /CARREFOUR USINE I2T / ENTRE BOULANGERIE / GLACIER AMORE / 200M DE LA VOIE EXPRESS ABIDJAN GRAND-BASSAM', '5.244283', ' -3.895610', '58 00 73 24/22 45 33 99', 'A', '2019-03-05 17:56:38', 0),
(582, 'BINGERVILLE', 'PHARMACIE DE GBAGBA', 'Gbagba petit Paris  Face Eglise Presbyterienne', '5.349403', '-3.885323', '24 38 10 74', 'A', '2019-03-05 17:56:38', 0),
(583, 'ANYAMA', 'PHARMACIE MEYDEBA', 'ANYAMA ENTRE CARREFOUR CISSE ET CARREFOUR SYLLA DANS ZONE CARREFOUR ZONZONKOI', ' 5.483299', ' -4.051297', '23 55 62 44', 'A', '2019-03-05 17:56:38', 0),
(584, 'MARCORY', 'PHARMACIE DU PONT D ANOUMABO', 'Apres le Pont Anoumanbo', '5.305455', ' -3.976231', '21 26 15 11', 'A', '2019-03-05 17:56:38', 0),
(585, 'MARCORY', 'PHARMACIE IMMACULEE', 'MARCORY ZONE 4 C RUE DR CALMETTE A 500M DE PRIMA CENTER ENTRE LE COLLEGE ET LES COURS SEVIGNE', '5.297622', ' -3.979161', '01 30 18 84', 'A', '2019-03-05 17:56:38', 0),
(586, 'MARCORY', 'PHARMACIE KAEMILLA', 'ZONE 4 C / 36 RUE PAUL LANGEVIN / A 500 M DE MERCEDES', '5.276674 ', '-3.967323', '21 57 36 23', 'A', '2019-03-05 17:56:38', 0),
(587, 'TREICHVILLE', 'PHARMACIE STE ADELE', 'Treichville facade arriere du marche de belle ville  face habitat quartier craonne', '5.304913', '-3.997403', '21 25 47 00', 'A', '2019-03-05 17:56:38', 0),
(588, 'ABOBO', 'PHARMACIE MAGNIFICAT', 'ROUTE D ANYAMA NON LOIN DEPOT SOTRA 9 ABOBO AVOCATIER PRES DU MARCHE DE NUIT', '5.437381', '-4.030763', '24 39 51 67', 'A', '2019-03-05 17:56:38', 0),
(589, 'COCODY', 'PHARMACIE CELLULE SAINE', 'COCODY ANGRE AXE 7eme et 8eme TRANCHE APRES NOUVEAU PONT', '5.391735', ' -3.981421', '22 42 64 13', 'A', '2019-03-05 17:56:38', 0),
(590, 'COCODY', 'PHARMACIE DE L OASIS DE PAIX', 'NON LOIN DU CHU DE COCODY / DANS L UNIVERSITE FELIX HOUPHOUET BOIGNY APRES LA GUERITE PREMIER BATIMENT A DROITE', '5.350624', ' -3.986969', '22 48 92 44', 'A', '2019-03-05 17:56:38', 0),
(591, 'YOPOUGON', 'PHARMACIE NIANGON SUD EX PHCIE ASSALAMI', 'Niangon Sud a gauche au carrefour de l Allocodrome', '5.316080', '-4.091718', '23 52 99 30', 'A', '2019-03-05 17:56:38', 0),
(592, 'ABOBO', 'PHARMACIE ABOBO BELLEVILLE', 'Route Du Zoo Carrefour Menuiserie Pres De La Commune Centrale D  abobo', '5.433820', '-3.988437', '01 35 12 24', 'A', '2019-07-11 12:00:19', 1),
(593, 'ABOBO', 'PHARMACIE ABOBO SANTE', 'Face a la Maternite de la Mairie', '5.4237784', '-4.0175844', '24391384', 'A', '2019-07-11 12:00:19', 1),
(594, 'ABOBO', 'PHARMACIE AMINA', 'Abobo Avocatier Pres de la Maternite  Henriette Konan Bedie Avocatier Rue 144', '5.4399265', '-4.0249263', ' 24 39 95 65   07 67 68 98', 'A', '2019-07-11 12:00:19', 1),
(595, 'ABOBO', 'PHARMACIE AVOCATIER MARCHE ', 'Avocatier marche', '5.4407097', '-4.0348455', '24 49 70 19 / 09 30 38 52', 'A', '2019-07-11 12:00:19', 1),
(596, 'ABOBO', 'PHARMACIE AZUR', 'Plateau Dokoui Route du Zoo ', '5.399786', '-4.006335', ' 24 39 46 14', 'A', '2019-07-11 12:00:19', 1),
(597, 'ABOBO', 'PHARMACIE BELLE CITE ', 'Abobo quartier BC  Terminus des woros woros', '5.446630', '-4.020664', ' 24 48 31 52 ', 'A', '2019-07-11 12:00:19', 1),
(598, 'ABOBO', 'PHARMACIE CARREFOUR ANADOR', 'Ront point  Carrefour Anador Route des Rails', '5.336645', '-4.101000', ' 24 48 02 39 / 05 87 51 88 ', 'A', '2019-07-11 12:00:19', 1),
(599, 'ABOBO', 'PHARMACIE DE LA CITE', 'Abobo gare quartier Sogefiha', '5.436942', '-4.015068', '  24 39 03 33', 'A', '2019-07-11 12:00:19', 1),
(600, 'ABOBO', 'PHARMACIE DE LA MAIRIE D ABOBO', 'Voie non bitumee  Mairie', ' 5.420087', '-4.015554', '24 39 32 48', 'A', '2019-07-11 12:00:19', 1),
(601, 'ABOBO', 'PHARMACIE DE LA ME', 'Abobo Gare  entre la Mairie et la Gendarmerie  Voie Express Anyama', ' 5.423435', '-4.018521', '24 39 06 02', 'A', '2019-07-11 12:00:19', 1),
(602, 'ABOBO', 'PHARMACIE DE LA PLAQUE II ', 'Abobo Plaque II  non loin Carrefour du Plaque II', ' 5.437270', '-4.006042', ' 24 48 09 90', 'A', '2019-07-11 12:00:19', 1),
(603, 'ABOBO', 'PHARMACIE DE L ETOILE', 'Autoroute d Abobo Abidjan', '5.427175', '-4.019853', ' 24 39 11 54 / 24 39 08 72', 'A', '2019-07-11 12:00:19', 1),
(604, 'ABOBO', 'PHARMACIE DE L HABITAT ', 'A cote du marche de l Habitat', '5.4381535', '-4.0434347', ' 24 39 94 67 ', 'A', '2019-07-11 12:00:19', 1),
(605, 'ABOBO', 'PHARMACIE DIVINE ESPERANCE', 'Abobo Sagbe entre Paroisse Saint Joseph Epoux et Commissariat du 21eme Arrond', '5.424657', '-4.024112', '00225 24 39 78 92', 'A', '2019-07-11 12:00:19', 1),
(606, 'ABOBO', 'PHARMACIE DU CENTRE', 'abobo avocatier voie express anyama pres de l hotel du centre et de l eglise universelle du royaume de Dieu', '5.434145', '-4.026725', '47 85 90 09 / 40 27 54 76', 'A', '2019-07-11 12:00:19', 1),
(607, 'ABOBO', 'PHARMACIE DU DOKUI', 'Rue Hotel Bar Abobote  Abobo', ' 5.399598', '-4.006527', ' 24 39 04 83/47904063', 'A', '2019-07-11 12:00:19', 1),
(608, 'ABOBO', 'PHARMACIE DU MONASTERE', 'Route d Abobo Baoule a 500 m Samake pres du Monastere Sainte Claire', '5.415581', '-4.000753', ' 24 49 63 00', 'A', '2019-07-11 12:00:19', 1),
(609, 'ABOBO', 'PHARMACIE DU RAIL', 'Abobo SAGBE  Derriere Rails', '5.414755', '-4.023829', ' 24 39 01 82 ', 'A', '2019-07-11 12:00:19', 1),
(610, 'ABOBO', 'PHARMACIE D ABOBO BAOULE ', 'En face du 34eme Arrondissement  Quartier Baoule ', '5.420949', '-3.990893', '24.48.27.54', 'A', '2019-07-11 12:00:19', 1),
(611, 'ABOBO', 'PHARMACIE D ABOBOTE', 'Route du Zoo Carrefour Menuiserie pres de la clinique centrale d Abobo', '5.412027', '-4.005915', ' 24 39 69 87', 'A', '2019-07-11 12:00:19', 1),
(612, 'ABOBO', 'PHARMACIE IRYS ', 'Abobo Anoukoua Koute a l entree du village place parking', '5.365424', '-3.9828266', ' 24 48 48 05', 'A', '2019-07-11 12:00:19', 1),
(613, 'ABOBO', 'PHARMACIE NOUR EX KANN SY', 'zone du commissariat du 14eme arrondt face gde mosquee en construction ', '5.4237784', '-4.017579', ' 24 39 12 51 ', 'A', '2019-07-11 12:00:19', 1),
(614, 'ABOBO', ' PHARMACIE KENNEDY', 'Entree Quartier Kennedy Clouetcha a 400 m du Carrefour Samanke', '5.419100', '-4.005506', ' 24 39 43 73 /09 47 56 25', 'A', '2019-07-11 12:00:19', 1),
(615, 'ABOBO', 'PHARMACIE LA PAIX', 'Cocoteraie Anador face au Groupe Scolaire NANTI ', '5.412427', '-4.010293', '24002129', 'A', '2019-07-11 12:00:19', 1),
(616, 'ABOBO', 'PHARMACIE LES 4 SAISONS', 'Carrefour Lycee Adama Sanogo derriere l eglise Sainte Monique', '5.404319', '-4.000902', '225 21 01 11 10', 'A', '2019-07-11 12:00:19', 1),
(617, 'ABOBO', 'PHARMACIE LEON ANGE', 'Abobo face cite policiere en venant terminus des bus 15 et 49', '5.4179572', '-4.0336749', ' 02 50 05 13 / 24 47 75 43', 'A', '2019-07-11 12:00:19', 1),
(618, 'ABOBO', 'PHARMACIE MAGNIFICAT ', 'Route Anyama non loin depot sotra 9 pres du marche de nuit', '5.437306', '-4.030773', ' 24 39 51 67 ', 'A', '2019-07-11 12:00:19', 1),
(619, 'ABOBO', 'PHARMACIE MANZAN', ' Carrefour Samanke carrefour Alepe Vers Abobo Gare', '5.415352', '-4.007099 ', '24 39 67 01', 'A', '2019-07-11 12:00:19', 1),
(620, 'ABOBO', 'PHARMACIE MATENE   ', 'Abobo avocatier entre la dgi et la cie  a 400 m du camp commando', '   5.438515', '-4.020665', ' 24 39 20 32', 'A', '2019-07-11 12:00:19', 1),
(621, 'ABOBO', 'PHARMACIE MIRIA  ', 'Route du zoo carrefour Jean Tailly en face du marche des grossistes', '5.417759', '-4.012002', '24 39 01 09', 'A', '2019-07-11 12:00:19', 1),
(622, 'ABOBO', 'PHARMACIE NOTRE DAME DE FATIMA', 'En face de l Eglise Ste Monique Plateau Dokui', '5.399974', '-4.001543', ' 24 39 59 12 ', 'A', '2019-07-11 12:00:19', 1),
(623, 'ABOBO', 'PHARMACIE N GOHESSE', 'Abobo Plaque I  Entre le Foyer feminin et le Marche de Nuit ', ' 5.435659', '-4.008953', ' 24 39 94 67 /09 84 14 71', 'A', '2019-07-11 12:00:19', 1),
(624, 'ABOBO', 'PHARMACIE PRINCIPALE D ABOBOTE', 'Petit marche d Abobote Pres de la Gare des Taxis Woro woro', ' 5.411068', '-4.000951', '  24-29-12-63 / 24 49 53 22/24 49 54 22', 'A', '2019-07-11 12:00:19', 1),
(625, 'ABOBO', 'PHARMACIE PROVIDENCE', 'quartier banco 1er arret sotra debut autoroute anyama ', '5.348743', '-4.016898', ' 24 49 39 62 ', 'A', '2019-07-11 12:00:19', 1),
(626, 'ABOBO', 'PHARMACIE QUATRE ETAGES', 'En face des 4 Etages et a 100 M de la formation sanitaire d Abobo', ' 5.439257', '-4.008986', '24 39 06 62', 'A', '2019-07-11 12:00:19', 1),
(627, 'ABOBO', 'PHARMACIE ROUTE D AKEIKOI', 'Sur la voie menant a akeikoi Abobo Colatier', '5.444737', '-4.014202', ' 24 39 97 22 / 05 61 64 88', 'A', '2019-07-11 12:00:19', 1),
(628, 'ABOBO', 'PHARMACIE SAINT FRANCOIS XAVIER', 'Abobo Sogefia 300m Du 15 eme Arrondissement', '5.433009', '-4.014863', '24 39 66 02', 'A', '2019-07-11 12:00:19', 1),
(629, 'ABOBO', 'PHARMACIE SOGEFIHA', 'entre commissariat du 15eme et station mobil bus 41 08 49', '5.435168', '-4.012646', '24 01 00 26', 'A', '2019-07-11 12:00:19', 1),
(630, 'ABOBO', 'PHARMACIE SAINT SAUVEUR ', 'Quart  Clouetcha  Carrefour ancienne Boulangerie non loin du Marche de Clouetcha', '5.4369706', '-4.0167621', ' 24 48 47 41 ', 'A', '2019-07-11 12:00:19', 1),
(631, 'ABOBO', 'PHARMACIE SAINTE CROIX', 'Route BC Pres Clinique Medicale de l Etoile Camp Commando', '5.434318', '-4.020429', '24 39 36 73', 'A', '2019-07-11 12:00:19', 1),
(632, 'ABOBO', 'PHARMACIE STE ODILE', 'Non loin de l Alocodrome  Plateau Dokui ', ' 5.396710', ' -4.003938', '24 39 29 97 / 24 39 00 89/59 88 16 07', 'A', '2019-07-11 12:00:19', 1),
(633, 'ABOBO', 'PHARMACIE TEHOUA', 'Sur L autoroute D anyama 500m Apres La Gendarmerie D abobo ', '5.430626', '-4.023613', '47 50 90 90', 'A', '2019-07-11 12:00:19', 1),
(634, 'ABOBO', 'PHARMACIE TIMA  ', 'face au lycee moderne d abobo alignement biao gd marche', '5.427946', '-4.015063', '24 39 08 66', 'A', '2019-07-11 12:00:19', 1),
(635, 'ABOBO', 'PHARMACIE TSACOE ', ' A la Gare Entre le Lycee St Joseph et l Agence Moov', '5.419868', '-4.017989', '24 01 18 20', 'A', '2019-07-11 12:00:19', 1),
(636, 'ABOBO', 'PHARMACIE YARAPHA', 'Carrefour Akeikoi Derriere La Cite Universitaire', '5.438811 ', '-4.013958', '24 49 12 63', 'A', '2019-07-11 12:00:19', 1),
(637, 'ABOBO', 'PHARMACIE MILIE HEVIE  PK 18  ', 'Route D anyama Pk 18 Terminus Bus 76', '5.448178', '-4.049958 ', '24 39 04 00', 'A', '2019-07-11 12:00:19', 1),
(638, 'ABOBO', ' PHARMACIE ROUTE D ANYAMA ', ' Route d Anyama Face ex Unicafe  PK 18 Agoueto ', '5.4976557', '-4.053417', '23 55 82 40/77 25 57 30', 'A', '2019-07-11 12:00:19', 1),
(639, 'ABOBO', 'PHARMACIE SAFIR  PK 18 ', 'Route d Anyama  Derriere Pont  Place Ancienne Boulangerie ', '5.444020', '-4.055152', '24 49 27 76', 'A', '2019-07-11 12:00:19', 1),
(640, 'ABOBO', 'PHARMACIE OLYMPIQUE', 'Abobo ndotre a 300 m du carrefour ndotre route de Yopougon', '5.439198', '-4.068268', ' 02 27 56 13 /  07 89 99 44', 'A', '2019-07-11 12:00:19', 1),
(641, 'ABOBO', 'PHARMACIE SAINT ALEXANDRE', 'PK 18 interieur residences concorde de la SICOGI', '5.450732 ', '-4.049157', '02 02 48 06/59 04 35 32', 'A', '2019-07-11 12:00:19', 1),
(642, 'ABOBODOUME LOCODJORO', 'PHARMACIE CHENAIN', 'Yopougon Jerusalem a 200 m de la Cite SODECI', '5.320142', '-4.043896', '23 50 13 91 ', 'A', '2019-07-11 12:00:19', 1),
(643, 'ABOBODOUME LOCODJORO', 'PHARMACIE DE LOCODJORO', 'Yopougon apres le marche en allant vers abobodoume', '5.336400 ', '-4.042482', '23 45 36 83 ', 'A', '2019-07-11 12:00:19', 1),
(644, 'ABOBODOUME LOCODJORO', 'PHARMACIE D ABOBODOUME', 'Route d Abobodoume 200m des Sapeurs Pompiers Ligne Bus 42  Toits rouges', '5.3198855', '-4.0527578', '23 45 63 00 ', 'A', '2019-07-11 12:00:19', 1),
(645, 'ABOBODOUME LOCODJORO', 'PHARMACIE NOTRE DAME DES VICTOIRES ', 'Mossikro  200 metres du carrefour Locodjoro vers Toits Rouges', '5.352230 ', '-3.980117', '55 60 52 52', 'A', '2019-07-11 12:00:19', 1),
(646, 'ABOBODOUME LOCODJORO', 'PHARMACIE SAINT ETIENNE   ', 'abobodoume face marche poisson carrefour pinasses pres du terminus bus 36 42 44', '5.309766 ', '-4.037701', '23 52 44 44 / 02 82 81 83', 'A', '2019-07-11 12:00:19', 1),
(647, 'ADJAME', 'PHARMACIE ADJAME BRACODI', ' Adjame Gare nord sotra dans la zone de stationnement des gbakas dans le sens Adjame Abobo', '5.361862', '-4.027627', '20 37 54 33', 'A', '2019-07-11 12:00:19', 1),
(648, 'ADJAME', 'PHARMACIE ADJAME LATIN ', 'Prolongement de l avenue 13  pres du petit marche', '5.358954 ', '-4.020031', '20 37 22 29', 'A', '2019-07-11 12:00:19', 1),
(649, 'ADJAME', 'PHARMACIE ADJAME SANTE ', 'Pres de l institut de sante  BD NANGUI ABROGOUA', '5.341196 ', '-4.026478', '20 22 85 44', 'A', '2019-07-11 12:00:19', 1),
(650, 'ADJAME', 'PHARMACIE BOULEVARD DE LA PAIX ', 'Attecoube route de Carena face Sebroko pres station total', '5.340950', '-4.030619', '20 22 20 34', 'A', '2019-07-11 12:00:19', 1),
(651, 'ADJAME', 'PHARMACIE DE LA MAIRIE', 'FACE MAIRIE D ADJAME', '5.343343', ' -4.026101', '20 39 99 99', 'A', '2019-07-11 12:00:19', 1),
(652, 'ADJAME', 'PHARMACIE DE LA MOSQUEE ', 'BD WILLIAM JACOB derriere les rails de la grande mosquee', '5.356576 \n', '-4.028445', '20 37 17 70 ', 'A', '2019-07-11 12:00:19', 1),
(653, 'ADJAME', 'PHARMACIE DE L INDENIE', 'Av Toussaint Louverture SGBCI Indenie  Face cite Policiere \napres les tours Administratives Plateau', '5.338565 ', '-4.023413', '20 21 17 90   ', 'A', '2019-07-11 12:00:19', 1),
(654, 'ADJAME', 'PHARMACIE DE LA PROVIDENCE', 'Petit marche des 220 lgts  face Edipresse ligne bus N 14 09  12 83', '5.348509', '-4.016918', '20 37 55 81 ', 'A', '2019-07-11 12:00:19', 1),
(655, 'ADJAME', 'PHARMACIE DE L  ESPOIR', 'Adjame 220 lgts rue passant devant Edipresse et IMST face cite de l enfance croix rouge 200 m marche Gouro ', '5.349274', '-4.019333', '20 39 08 31 /  07 08 45 12', 'A', '2019-07-11 12:00:19', 1),
(656, 'ADJAME', 'PHARMACIE DES 220 LOGEMENTS', 'Avenue General de Gaulle  face station texaco', '5.354533 ', '-4.018181', '20 37 38 22', 'A', '2019-07-11 12:00:19', 1),
(657, 'ADJAME', 'PHARMACIE DES GARES', ' Boulevard Nangui Abrogoua Entree la RAN et CITELCOM  ', '5.355985 ', '-4.027122', '20 37 26 24', 'A', '2019-07-11 12:00:19', 1),
(658, 'ADJAME', 'PHARMACIE DIBY RENE ', 'Face A La Mairie D adjame', '5.3674593', '-4.0911342', '20 22 48 27 / 20 22 48 11', 'A', '2019-07-11 12:00:19', 1),
(659, 'ADJAME', 'PHARMACIE DU BANCO ', 'Boulevard Nangui Abrogoua Face Service Social Adjame', '5.350397', '-4.025624', ' 20 37 21 69/ 20 37 03 12 ', 'A', '2019-07-11 12:00:19', 1),
(660, 'ADJAME', 'PHARMACIE DU CHATEAU D EAU  ', 'Bd William Jacob En descendant les rails vers la Sodeci', '5.353724 ', '-4.030300', '20 37 11 68', 'A', '2019-07-11 12:00:19', 1),
(661, 'ADJAME', 'PHARMACIE DU FORUM', 'a linterieur du forum des marches dAdjame Mosquee', '5.345184', '-4.026429', '20 38 13 64 ', 'A', '2019-07-11 12:00:19', 1),
(662, 'ADJAME', 'PHARMACIE DU MARCHE', 'Rue des Agouas face au grand marche dAdjame', '5.344947', '-4.026730', '20 37 33 12 ', 'A', '2019-07-11 12:00:19', 1),
(663, 'ADJAME', 'PHARMACIE DU MARCHE GOURO ', 'Adjame marche Gouro a cote de la banque atlantique', '5.349218', '-4.022826', '20 38 83 83 ', 'A', '2019-07-11 12:00:19', 1),
(664, 'ADJAME', 'PHARMACIE DU ROCHER  ', 'Adjame nord  marche bramakote', '5.349056 ', '-4.027860', '20 37 70 63', 'A', '2019-07-11 12:00:19', 1),
(665, 'ADJAME', 'PHARMACIE FRATERNITE', 'Pres Maternite Therese Houphouet Boigny  Quartier Fraternite', '5.349637', '-4.021560', '20 38 12 38', 'A', '2019-07-11 12:00:19', 1),
(666, 'ADJAME', 'PHARMACIE GBEDE', 'Bd Nangui Abrogoua face a la Foire de Chine', ' 5.347534 ', '-4.025590', '20 37 71 58', 'A', '2019-07-11 12:00:19', 1),
(667, 'ADJAME', 'PHARMACIE KANA', 'Adjame habitat extension  pres du petit marche', '5.3585161', '-4.0251875', '20 37 96 49', 'A', '2019-07-11 12:00:19', 1),
(668, 'ADJAME', 'PHARMACIE KORO ', 'Quartier Marie Therese 220 Logements- A cote College Montherlant Batiment B', '5.352914', '-4.020508', ' 07 81 47 18/20 38 68 51', 'A', '2019-07-11 12:00:19', 1),
(669, 'ADJAME', 'PHARMACIE LE BELIER', 'Arret de Bus du Grd Bloc 220 logements entre Liberte et Fraternite', '5.350812   ', '-4.017166', '20 37 12 16 ', 'A', '2019-07-11 12:00:19', 1),
(670, 'ADJAME', 'PHARMACIE MAKISSI', '220 Lgts  place du cinema liberte', '5.354103', '-4.016463', ' 20 37 70 39', 'A', '2019-07-11 12:00:19', 1),
(671, 'ADJAME', 'PHARMACIE QUARTIER EBRIE', 'Face ancienne gare Stiff- Quartier Ebrie Adjame', '5.355587 ', '-4.020748', '20 37 12 56 ', 'A', '2019-07-11 12:00:19', 1),
(672, 'ADJAME', 'PHARMACIE REBOUL', 'Av Reboul 150m des Sapeurs Pompiers de lIndenie BUS 82 81 35  ', '5.342220 ', '-4.022959', '20 37 99 98 ', 'A', '2019-07-11 12:00:19', 1),
(673, 'ADJAME', 'PHARMACIE ROEDDENDT    SCHIBA    ', 'Rue du dispensaire antituberculeux  face marche gouro', '5.3484733', '-4.0256634', '20 37 98 33 ', 'A', '2019-07-11 12:00:19', 1),
(674, 'ADJAME', 'PHARMACIE SAINT MICHEL', 'Oter de l eglise Saint Michel En face du Ceg Harris', '5.346903 ', '-4.022621', '20 37 09 06 ', 'A', '2019-07-11 12:00:19', 1),
(675, 'ADJAME', 'PHARMACIE SARAH  ', 'Route camp Gendarmerie Agban Centre commercial Carine N Couture', '5.357366', '-4.015700', '20 39 03 57 ', 'A', '2019-07-11 12:00:19', 1),
(676, 'ADJAME', 'PHARMACIE SENEVE  ', 'Pres de la gare UTB- Renault Adjame Abidjan', '5.351272', '-4.020967', '20 37 11 04 / 04 13 97 77', 'A', '2019-07-11 12:00:19', 1),
(677, 'ADJAME', 'PHARMACIE SAINTE MARIE D ADJAME', 'Rue Harriste Cote parking payant Derriere Commissariat du 3ieme  ', '5.347041 ', '-4.024689', '20 37 54 44 / 20 37 54 46', 'A', '2019-07-11 12:00:19', 1),
(678, 'ANYAMA', 'PHARMACIE AN NASR', 'Autoroute d Abobo Anyama', '5.503615', '-4.052400', '23 55 98 22  ', 'A', '2019-07-11 12:00:19', 1),
(679, 'ANYAMA', 'PHARMACIE DU CHATEAU ', 'Autoroute d Abobo Anyama', '5.500232 ', '-4.052744', '07 93 46 81/58 17 29 91', 'A', '2019-07-11 12:00:19', 1),
(680, 'ANYAMA', 'PHARMACIE DU MARCHE  ', 'En face de la poste et de la banque atlantique Anyama', '5.495490 ', '-4.051978', '23 55 62 30', 'A', '2019-07-11 12:00:19', 1),
(681, 'ANYAMA', 'PHARMACIE D ANYAMA ', 'Route d anyama', '5.495488  ', '-4.051979', '23 55 94 18 ', 'A', '2019-07-11 12:00:19', 1),
(682, 'ANYAMA', 'PHARMACIE ELIEL', 'Quartier RAN En face de la gare Routiere', '5.480435 ', '-4.052640', '23 55 98 30 / 23 55 62 80  ', 'A', '2019-07-11 12:00:19', 1),
(683, 'ATTECOUBE', 'PHARMACIE BD DE LA PAIX  ', 'boulevard de la paix face sebroko ', '5.4800514', '-4.3338867', '20 22 20 34 / 02 03 76 18 ', 'A', '2019-07-11 12:00:19', 1),
(684, 'ATTECOUBE', 'CHRIST STELLA  ', 'Cite Fairmont  Attecoube apres la Nouvelle Gendarmerie', '5.377581', '-4.088076', '20 37 90 85', 'A', '2019-07-11 12:00:19', 1),
(685, 'ATTECOUBE', 'PHARMACIE DES AGOUAS', ' Face a la boulangerie BBCO', '5.352133 ', '-4.033774', '20 37 10 59', 'A', '2019-07-11 12:00:19', 1),
(686, 'ATTECOUBE', 'PHARMACIE D AGBAN', 'A l entree de la Cite Fairmont Agban Village Adjame ', '5.358766 ', '-4.030485', '20 37 22 30 / 20 37 21 67', 'A', '2019-07-11 12:00:19', 1),
(687, 'ATTECOUBE', 'PHARMACIE D ATTECOUBE ', 'Grand Carrefour d Attecoube Face feux tricolores  Entree bus 04', '5.3500759', '-4.0344756', '08 44 85 95', 'A', '2019-07-11 12:00:19', 1),
(688, 'ATTECOUBE', 'PHARMACIE FATIMA ', 'Carrefour du Pont a la marine Academie de la mer', '5.347443', '-4.038323', ' 20 37 29 10 / 02 56 10 10', 'A', '2019-07-11 12:00:19', 1),
(689, 'ATTECOUBE', 'PHARMACIE MONTANA', 'Entre la Mairie et le Terminus Bus 04 Face ecole Faidherbe ', '5.345891 \n', '-4.034605', '07 36 57 30', 'A', '2019-07-11 12:00:19', 1),
(690, 'ATTECOUBE', 'PHARMACIE REHOBOTH', 'Pres de la Maternite CISCOM  Cite Fairmont 2', '5.3524416', '-4.0359588', '01 07 48 17', 'A', '2019-07-11 12:00:19', 1),
(691, 'ATTECOUBE', 'PHARMACIE DU ROND POINT', 'rond point bidjante gauche', '5.2945874', '-4.0028354', '21 01 59 47 / 07 32 43 76/21 35 73 03', 'A', '2019-07-11 12:00:19', 1),
(692, 'ATTECOUBE', 'PHARMACIE SEBROKO ', 'Pres du Grand marche  Saint Joseph Attecoube', '5.3496316', '-4.0346266', '20 37 67 48', 'A', '2019-07-11 12:00:19', 1),
(693, 'BINGERVILLE', 'PHARMACIE AKRE ALBERT ASSAMOI', 'Nouvelle gare routiere', '5.3532338', '-3.8776556', '22 40 39 37', 'A', '2019-07-11 12:00:19', 1),
(694, 'BINGERVILLE', 'PHARMACIE DU MARCHE', 'En face de la COPEC pres de la Gare', '5.356593', '-3.8830932', '22 40 34 64 ', 'A', '2019-07-11 12:00:19', 1),
(695, 'BINGERVILLE', 'PHARMACIE PRINCIPALE  ', 'Non loin du carrefour marche', '5.3573549', '-3.8880008', '22 40 32 29 ', 'A', '2019-07-11 12:00:19', 1),
(696, 'BINGERVILLE', 'PHARMACIE CARREFOUR SANTE', ' Face Station Corlay au Carrefour sante Akouai Santai ', '5.3613087', '-3.8961634', '75 93 32 18', 'A', '2019-07-11 12:00:19', 1),
(697, 'BINGERVILLE', 'PHARMACIE ST SYLVESTRE ', 'En face du restaurant Coup de Frein Cite Palma route de Bingerville', '5.372685', ' -3.910770', '22 40 27 88', 'A', '2019-07-11 12:00:19', 1),
(698, 'COCODY CENTRE', ' PHARMACIE COMOE ', 'Face Radio Frequence Vie  Cite des arts', '5.347903', '-3.997275', '22 44 28 81', 'A', '2019-07-11 12:00:19', 1),
(699, 'COCODY CENTRE', 'PHARMACIE DE BLOCKAUSS ', 'Terminus Sotra 200 m de L hotel Ivoire', '5.3226718', '-4.002285', '22 48 68 91 ', 'A', '2019-07-11 12:00:19', 1),
(700, 'COCODY CENTRE', 'PHARMACIE DE COCODY', 'Pres du Cash face a la patisserie Abidjanaise ', '5.337068 ', '-4.000109', '22 44 24 95/22 44 03 43', 'A', '2019-07-11 12:00:19', 1),
(701, 'COCODY CENTRE', 'PHARMACIE DE LA CITE COCODY', 'Cocody Danga Face Cite Rouge a la gare Woro Woro de Marcory', '5.337182', '-4.005154', '22 44 63 68', 'A', '2019-07-11 12:00:19', 1),
(702, 'COCODY CENTRE', 'PHARMACIE DE LA CORNICHE ', 'Bd de la Corniche Face Siege Nestle', ' 5.341303 ', '-4.015735', '22 48 73 40 ', 'A', '2019-07-11 12:00:19', 1),
(703, 'COCODY CENTRE', 'PHARMACIE DE LA PIETE ', ' Face a la PMI  COCODY Doyen ', '5.333983 ', '-4.000279', '22 44 45 84/22 43 14 41/57 49 38 61', 'A', '2019-07-11 12:00:19', 1),
(704, 'COCODY CENTRE', 'PHARMACIE CITE  DES ARTS  ', 'Bd de l Universite  Cite des arts', '5.344127 ', '-3.999366', '22 44 93 56', 'A', '2019-07-11 12:00:19', 1),
(705, 'COCODY CENTRE', 'PHARMACIE DU BD DE FRANCE ', 'St Jean Gd marche station Agip', '5.335994 ', '-4.002090', '22 44 74 08', 'A', '2019-07-11 12:00:19', 1),
(706, 'COCODY CENTRE', 'PHARMACIE DU LYCEE TECHNIQUE', 'Route du Lycee Technique pres de la Clinique Goci ', '5.344410 ', '-4.011396', '22 44 60 77', 'A', '2019-07-11 12:00:19', 1),
(707, 'COCODY CENTRE', 'PHARMACIE LES 7 COLONNES ', 'Bd des Martyrs carrefour de la vie face PFO Africa  Cite des arts', '5.344992 ', '-4.002658', '22 44 02 96', 'A', '2019-07-11 12:00:19', 1),
(708, 'COCODY CENTRE', 'PHARMACIE LES MIMOSAS ', ' Entre Carrefour la vie et la Sodefor face a SETACI  Cite des arts', '5.349195 ', '-3.999431', '22 44 32 28', 'A', '2019-07-11 12:00:19', 1),
(709, 'COCODY CENTRE', 'PHARMACIE MERMOZ', 'Av Jean Mermoz  lentree de la Cite Sogefiha  Centre Cocody', '5.340173', '-4.002074', '22 48 74 26   ', 'A', '2019-07-11 12:00:19', 1),
(710, 'COCODY CENTRE', 'PHARMACIE PALM CLUB ', 'Rue du Lycee Technique HOTEL PALM CLUB Boutique N7', '5.353069 ', '-4.002681', '22 44 20 90 ', 'A', '2019-07-11 12:00:19', 1),
(711, 'COCODY CENTRE', 'PHARMACIE ST DOMINIQUE ', 'Cocody en face du lycee international JEAN MERMOZ', '5.339464 ', '-3.997806', '22 48 79 89 ', 'A', '2019-07-11 12:00:19', 1),
(712, 'COCODY CENTRE', 'PHARMACIE ST FRANCOIS DE DANGA  ', 'Prolongement poste Face centre culturel Americain  Danga Cocody', '5.343600', '-4.004050', '22 48 54 08', 'A', '2019-07-11 12:00:19', 1),
(713, 'COCODY CENTRE', 'PHARMACIE ST JEAN  ', 'Bd Latrille face cite Rouge  Centre Cocody ', '5.338585 ', '-4.003892', '22 44 62 49  ', 'A', '2019-07-11 12:00:19', 1),
(714, 'COCODY CENTRE', 'PHARMACIE SAINTE MARIE', 'Bd de France immeuble NSIA A proximite de la maison du parti du PDCI', '5.340250 ', '-3.998829', '22 48 69 20', 'A', '2019-07-11 12:00:19', 1),
(715, 'COCODY II PLATEAUX', 'LES ARCADES', 'Cocody Djibi 8eme Tranche  Terminus du bus express 205', '5.403789 ', '-3.976241', '22 50 62 25 ', 'A', '2019-07-11 12:00:19', 1),
(716, 'COCODY II PLATEAUX', 'PHARMACIE 2 PLATEAUX AGBAN ', 'Entre nouveau marche et hopital des impots sortie Nord Camp de la Gendarmerie Agban', '5.366940', '-4.008054', '22 41 39 90', 'A', '2019-07-11 12:00:19', 1),
(717, 'COCODY II PLATEAUX', 'PHARMACIE 8EME TRANCHE ', 'Carrefour marche Cocovico Route 9eme tranche ', '5.401413 ', '-3.978122', '22 52 34 90 /07 69 54 32', 'A', '2019-07-11 12:00:19', 1),
(718, 'COCODY II PLATEAUX', 'PHARMACIE ANGRE  ', 'A cote du 22e arrondissement  Angre 7e tranche Cocody', '5.399305    ', '-3.991164', '22 42 11 06/22 42 43 19/87 16 15 35', 'A', '2019-07-11 12:00:19', 1),
(719, 'COCODY II PLATEAUX', 'PHARMACIE APPAUL  ', 'cocody 8eme tranche  50 m du college ste camille  interieur centre commercial parkn shop ', '5.394008', ' -3.973916', '22 50 26 40  ', 'A', '2019-07-11 12:00:19', 1),
(720, 'COCODY II PLATEAUX', ' PHARMACIE ARC EN CIEL  ', 'II Plateaux rue des jardins  Carrefour commissariat 12eme arrondt', '5.374509', '-3.990638', '22 41 20 67', 'A', '2019-07-11 12:00:19', 1),
(721, 'COCODY II PLATEAUX', 'PHARMACIE ARUMS', 'Angre 8ie Tranche carrefour BCEAO sur la grande Voie menant a la CNPS', '5.395970 ', '-3.977089', '22 50 66 40 / 07 06 13 94', 'A', '2019-07-11 12:00:19', 1),
(722, 'COCODY II PLATEAUX', 'PHARMACIE BEL HORIZON ', 'Bd Latrille pres du Groupe Scolaire Flamboyant Angre Petro Ivoire  ', '5.406158', '-3.990776', '22 52 24 19/22 52 24 22', 'A', '2019-07-11 12:00:19', 1),
(723, 'COCODY II PLATEAUX', 'PHARMACIE DE LA 7EME TRANCHE', 'Rue L84-L139 II Plateaux 7e tranche', ' 5.391516 ', '-3.988445', '22 52 56 83 ', 'A', '2019-07-11 12:00:19', 1),
(724, 'COCODY II PLATEAUX', 'PHARMACIE DES ALLEES', 'Angre Cafeiers 5 Route du Chateau', '5.409364', '-3.987025', '22 42 14 58', 'A', '2019-07-11 12:00:19', 1),
(725, 'COCODY II PLATEAUX', 'PHARMACIE DES GRACES', 'Route II plateaux non loin de l hopital des impots Williamsville ', '5.373142 ', '-4.007304', '22 41 24 27', 'A', '2019-07-11 12:00:19', 1),
(726, 'COCODY II PLATEAUX', 'PHARMACIE DES HALLES  ', 'Espace Latrille Centre commercial SOCOCE', '5.373525  ', '-3.999188', '22 41 92 94', 'A', '2019-07-11 12:00:19', 1),
(727, 'COCODY II PLATEAUX', 'PHARMACIE DES II PLATEAUX ', 'Bd. Latrille face a la SGBCI Sicogi 2 Plateaux ', '5.370812', '-3.997804', '22 41 36 04', 'A', '2019-07-11 12:00:19', 1),
(728, 'COCODY II PLATEAUX', 'PHARMACIE DES JARDINS', 'Rue des Jardins pres du supermarche HAYAT  Deux plateaux', ' 5.358806', '-3.990021', '22 41 17 70 ', 'A', '2019-07-11 12:00:19', 1),
(729, 'COCODY II PLATEAUX', 'PHARMACIE DES LAUREADES  ', 'En face du stade d Angre  Angre 7e tranche', ' 5.397899  ', '-3.986141', '22 42 48 10 / 05 79 14 03 ', 'A', '2019-07-11 12:00:19', 1),
(730, 'COCODY II PLATEAUX', 'PHARMACIE DIVIN AMOUR  ', ' Bd Latrille Carrefour Macaci Pres de l Eglise du plein Evangile', '5.365661 ', '-3.997324', '22 41 93 38 ', 'A', '2019-07-11 12:00:19', 1),
(731, 'COCODY II PLATEAUX', 'PHARMACIE DIVINE ONCTION', 'Rue J 92 vers le 12eme Arrondissement  Deux plateaux', '5.375114', '-3.996199', '22 41 71 36', 'A', '2019-07-11 12:00:19', 1),
(732, 'COCODY II PLATEAUX', 'PHARMACIE DU BIEN ETRE ', 'Voie de la Djibi entree de Soleil 3  Angre 8e tranche', '5.3910862', '-3.9781451', '22 41 36 04 ', 'A', '2019-07-11 12:00:19', 1),
(733, 'COCODY II PLATEAUX', 'PHARMACIE DU VALLON', 'Rue des Jardins Face a la BSIC non loin de Paul  Deux plateaux', '5.3687245', '-3.9946724', '22 41 35 14 ', 'A', '2019-07-11 12:00:19', 1),
(734, 'COCODY II PLATEAUX', 'PHARMACIE DUNIA ', ' Vers le Commissariat du 30eme Arrondissement ', '5.3744897', '-3.9869421', '22 52 99 99', 'A', '2019-07-11 12:00:19', 1),
(735, 'COCODY II PLATEAUX', 'PHARMACIE ESPACE SANTE', 'Angle Bd Latrille Carrefour Route du Zoo Deux plateaux ', '5.3777923', '-3.9998831', '22 41 21 03/22 41 23 00 ', 'A', '2019-07-11 12:00:19', 1),
(736, 'COCODY II PLATEAUX', ' PHARMACIE ESPERANCE ', ' Derriere le marche et la mosquee de la Djibi ', '5.3778821', '-4.0327141', '22 50 58 98', 'A', '2019-07-11 12:00:19', 1),
(737, 'COCODY II PLATEAUX', 'PHARMACIE FANDASSO', 'Carrefour Batim Angre Cite Fandasso', '5.411602', '-3.9885078', '21 00 18 16', 'A', '2019-07-11 12:00:19', 1),
(738, 'COCODY II PLATEAUX', 'PHARMACIE HERMES', 'Face Cash Center Angre CNPS  Angre 9e tranche', '5.3852915', '-3.9761697', '22 50 10 32 ', 'A', '2019-07-11 12:00:19', 1),
(739, 'COCODY II PLATEAUX', 'PHARMACIE LA DJIBI ', ' Angre Djibi 1 derriere la residence Koriet', '5.4005138', '-3.9831702', '22 52 23 76', 'A', '2019-07-11 12:00:19', 1),
(740, 'COCODY II PLATEAUX', 'PHARMACIE LAS PALMAS ', 'Bd Latrille Carrefour Las Palmas  Deux plateaux Aghien', '5.380593', '-3.9968727', '22 42 14 78 / 22 42 14 79', 'A', '2019-07-11 12:00:19', 1),
(741, 'COCODY II PLATEAUX', 'PHARMACIE LATRILLE', ' II Plateaux sur le Boulevard des Martyrs', '5.3618239', '-3.9997906', '22 41 03 68', 'A', '2019-07-11 12:00:19', 1),
(742, 'COCODY II PLATEAUX', 'PHARMACIE LES 7 LYS ', ' 100m de la residence BADA route d Angre Attoban-carrefour cite ZINSOU ', '5.384194', '-3.9899817', '22 42 63 25/07 83 66 61', 'A', '2019-07-11 12:00:19', 1),
(743, 'COCODY II PLATEAUX', 'PHARMACIE LES PERLES ', 'Bd Latrille Carrefour Opera  Deux plateaux  Les Perles Cocody', '5.3856093', '-3.9962822', '22 42 39 85 ', 'A', '2019-07-11 12:00:19', 1),
(744, 'COCODY II PLATEAUX', 'PHARMACIE LES TULIPES', 'Bd Latrille Espace Santa Maria   Face Mosquee d Aghien', '5.3828808', '-3.9953885', '22 42 50 17 ', 'A', '2019-07-11 12:00:19', 1),
(745, 'COCODY II PLATEAUX', ' PHARMACIE LUMINANCE K  ', 'Angre djibi groupement 4000 batim ci 300 m de la mosquee ', '5.407955', '-3.983751', '22 50 49 26', 'A', '2019-07-11 12:00:19', 1),
(746, 'COCODY II PLATEAUX', 'PHARMACIE NOEMIE ', 'Cocody   II Plateaux  Carrefour les Oscars derriere le glacier les Oscars', '5.394753', '-3.992136', ' 22 52 20 51 / 48 64 67 64', 'A', '2019-07-11 12:00:19', 1),
(747, 'COCODY II PLATEAUX', 'PHARMACIE SAINTE CECILE', 'Face Eglise Ste Cecile  Deux plateaux  Vallon Cocody', '5.3667165', '-3.9983305', '22 42 63 35/22 42 35 17', 'A', '2019-07-11 12:00:19', 1),
(748, 'COCODY II PLATEAUX', 'PHARMACIE SAINTE MARTHE', 'Angre Bd Latrille   carrefour station Petro Ivoire   terminus bus 81 et 82 ', '5.4036527', '-3.9872926', '22 42 33 87/07 93 21 13', 'A', '2019-07-11 12:00:19', 1),
(749, 'COCODY II PLATEAUX', ' PHARMACIE SANTE DE VIE', 'fin de la Rue principale   Cite Sanon   Pres de la Poste  Deux plateaux ', '5.387645 ', '-3.997691', '22 42 05 22 / 06 35 93 05', 'A', '2019-07-11 12:00:19', 1),
(750, 'COCODY II PLATEAUX', 'PHARMACIE ST CHRISTOPHE', 'Non loin de la gare de Woro woro  Angre Petro Ivoire Cocody ', '5.405023 ', '-3.993756', '22 42 55 62', 'A', '2019-07-11 12:00:19', 1),
(751, 'COCODY II PLATEAUX', '  PHARMACIE ST GABRIEL ', 'Bd Latrille   a cote de Bridge Bank Cocody ', '5.388241', '-3.992470', '22 42 58 35/22 42 49 95', 'A', '2019-07-11 12:00:19', 1),
(752, 'COCODY II PLATEAUX', 'PHARMACIE ST GIL', 'Rue des Jardins   face Station Corlay Deux plateaux', '5.356162 ', '-3.991921', '22 41 94 10 / 22 4115 42 ', 'A', '2019-07-11 12:00:19', 1),
(753, 'COCODY II PLATEAUX', ' PHARMACIE STE HARMONY ', 'Non loin du King Cash Djibi Angre Djibi Cocody', '5.403460', '  -3.980957', '22 52 39 97  ', 'A', '2019-07-11 12:00:19', 1),
(754, 'COCODY II PLATEAUX', 'PHARMACIE STE MONIQUE', 'Non loin de la boulangerie Mahou  Angre Mahou', '5.399790 ', '-3.997350', '22 42 01 09  ', 'A', '2019-07-11 12:00:19', 1),
(755, 'COCODY II PLATEAUX', 'PHARMACIE STE TRINITE', 'Route d  Agban   Ligne bus 49  Deux plateaux', '5.368888', '-4.000413', '22 41 68 88/22 41 20 52 ', 'A', '2019-07-11 12:00:19', 1),
(756, 'COCODY II PLATEAUX', 'PHARMACIE AURORE', 'II Plateaux Vallon rue des Jardins 100m avant la Biao et la Station Total', '5.361843', '-3.991184', '22 41 18 08', 'A', '2019-07-11 12:00:19', 1),
(757, 'COCODY II PLATEAUX', ' PHARMACIE NOTRE DAME DE LA VISITATION', 'Face au 12eme Arrondissement  Deux plateaux Vallon Cocody Abidjan', '5.374414', '-3.992700', '41 36 36 92 / 58 60 96 21', 'A', '2019-07-11 12:00:19', 1),
(758, 'COCODY II PLATEAUX', 'PHARMACIE SAINT JOSEPH', 'Rue des Jardins   non loin de la Gourmandine  Deux plateaux ', '5.372374', '-3.990536', '22 41 10 20', 'A', '2019-07-11 12:00:19', 1),
(759, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ANDY', 'Carrefour Leader Price   face station Shell  Anono Cocody', '5.339754 ', '-3.975023', '22 43 25 80 / 08 82 58 58', 'A', '2019-07-11 12:00:19', 1),
(760, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BD MITTERRAND', 'Route de bingerville  carrefour bonoumin km 9 apres carrefour ste famille', '5.358642 ', '-3.964559', '22 47 90 45', 'A', '2019-07-11 12:00:19', 1),
(761, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DE L IMMACULEE CONCEPTION EX BELLE EPINE', ' Rue des jardins de la Riviera', '5.3489361', '-3.9829179', '22 43 34 31', 'A', '2019-07-11 12:00:19', 1),
(762, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BETHEL', 'Face Groupe Scolaire Jules Ferry  Riviera Attoban', '5.364253', '-3.979106', '22 43 22 83', 'A', '2019-07-11 12:00:19', 1),
(763, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DE LA RIVIERA 2', 'Au Rond point Riviera II Cocody', '5.353453', '-3.976816', '22 43 28 44/22 43 70 85', 'A', '2019-07-11 12:00:19', 1),
(764, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DU BONHEUR', 'Route du Marche  Riviera Palmeraie', '5.369663', '-3.958971', '22 47 02 88', 'A', '2019-07-11 12:00:19', 1),
(765, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE DU GOLF', 'Centre Commercial Sicogi Riviera face aux Caddies Non loin de l Agence Bicici du Golf', '5.339549', '-3.977833', '22 43 14 31', 'A', '2019-07-11 12:00:19', 1),
(766, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LES ELIAS', 'Rue D46 Tour Sicogi et Jardins Carrefour Elias', '5.345071', '-3.978117', '22 43 28 85 ', 'A', '2019-07-11 12:00:19', 1),
(767, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LES ROSIERS', 'Rond poind ecole Kouadio Assahore terminus du bus 210  Programme 3', '5.360526', '-3.967437', '22 49 21 67', 'A', '2019-07-11 12:00:19', 1),
(768, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE LIVIE ', 'Entre Dispensaire et Gare des Woro Woro   non loin de la cite ATCI ', '5.353886', '-3.940872', '22 47 48 78', 'A', '2019-07-11 12:00:19', 1),
(769, 'COCODY RIVIERA PALMERAIE', ' PHARMACIE MARTHE ROBIN', 'Cite Sipim 3 face espace Vert  Riviera Palmeraie Cocody', '5.374949', '-3.955910', ' 22 49 24 79 / 58 86 12 38  ', 'A', '2019-07-11 12:00:19', 1),
(770, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE M POUTO ', 'Entre Quartier Ciad Promo Et Cite World City', '5.329214', '-3.948947', '22 43 12 73', 'A', '2019-07-11 12:00:19', 1),
(771, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE PALMERAIE    ', 'Non loin du Rond point et la Sgbci Riviera Palmeraie Cocody', '5.363677', '-3.959575', '22 47 91 19 ', 'A', '2019-07-11 12:00:19', 1),
(772, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE RIVIERA III', 'Route du lycee Fran?ais face lycee Americain  ', '5.348750 ', '-3.953704', ' 22 47 56 16 /  09 01 01 84 ', 'A', '2019-07-11 12:00:19', 1),
(773, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT ANGE', 'Route d Attoban pres du College Andre Malraux', '5.357448', '-3.978256', '22 43 45 04', 'A', '2019-07-11 12:00:19', 1),
(774, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT ATHANASE', 'Carrefour Anono a cote de ECOBANK  Riviera 2', '5.348210', '-3.975702', '22 43 55 87', 'A', '2019-07-11 12:00:19', 1),
(775, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE FAMILLE', 'Voie centre commercial Peace and Unity face a Cap Nord  Riviera 2', '5.355847', '-3.967406', '22 47 76 76', 'A', '2019-07-11 12:00:19', 1),
(776, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST BERNARD', 'Pres de CIPHARM Cite Attoban  Riviera Attoban Cocody ', '5.367538', '-3.979385', '22 43 65 46/22 43 66 21 ', 'A', '2019-07-11 12:00:19', 1),
(777, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST COME ET DAMIEN  ', 'Voie Lauriers 6  Riviera Bonoumin Cocody', '5.372767', '-3.970624', '22 49 22 49 ', 'A', '2019-07-11 12:00:19', 1),
(778, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT GEORGES D AKOUEDO', 'Faya  Quartier Genie 2000 1er carrefour ', '5.372284', '-3.938016', '22 47 22 38 ', 'A', '2019-07-11 12:00:19', 1),
(779, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT PAUL  ', ' Carrefour sacre coeur cite presse  Riviera Palmeraie', '5.372053', '-3.965089', '22 49 29 01  ', 'A', '2019-07-11 12:00:19', 1),
(780, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST PIERRE DES ROSEES', 'Bd Assouan Arsene face clinique Pantheon  Riviera 3', '5.354068', '-3.957884', '22 47 42 17', 'A', '2019-07-11 12:00:19', 1),
(781, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE ST PIERRE D ANONO ', 'Zone de la Gare de la Riviera 2  Anono Cocody', '5.346327', '-3.974815', '22 43 90 56', 'A', '2019-07-11 12:00:19', 1),
(782, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINT RAPHAEL ARCHANGE', 'Rue Ministre  Riviera Palmeraie Cocody', '5.369811', '-3.955507', '22 49 08 20', 'A', '2019-07-11 12:00:19', 1),
(783, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE AGATHE', 'Bd Arsene Usher Assouan au feu du Commissariat 18eme arrondissement', '5.355548 ', '-3.961338', '22 47 48 19', 'A', '2019-07-11 12:00:19', 1),
(784, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE STE BEATRICE DES ROSIERS', 'Carrefour Palmeraie Sci les Rosiers Face Quick Maket', '5.374048', '-3.961747', '22 47 01 72  ', 'A', '2019-07-11 12:00:19', 1),
(785, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE SAINTE MARIE DES BEATITUDES  ', 'Avant le 35eme Arrdt en face des Rosiers Programme 1 barriere 6', '5.376986', '-3.963102', '22 49 11 49', 'A', '2019-07-11 12:00:19', 1),
(786, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE ST JOSEPH', 'Rue Ministre face Complexe Sportif  Riviera Palmeraie', '5.376931', '-3.953123', '22 49 39 26', 'A', '2019-07-11 12:00:19', 1),
(787, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE BON PASTEUR', 'A cote du college l Ardoise face a la paroisse Bon pasteur  Riviera 3', '5.351267 ', '-3.954986', '22 47 04 05 ', 'A', '2019-07-11 12:00:19', 1),
(788, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE  BONOUMIN', 'Voie menant au College ANDRE MALRHAUX et a l Eglise la Destinee  ', '5.361327', '-3.971151', ' 22 49 49 34', 'A', '2019-07-11 12:00:19', 1),
(789, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE CATLEYAS', 'BD Mitterrand Carrefour Abata', '5.372871', '-3.928670', '22 47 07 79', 'A', '2019-07-11 12:00:19', 1),
(790, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE CEPHAS', 'Route Abatta apres Sicta Oribat ', '5.339757', '-3.922613', '59 06 45 70 /  07 69 48 67', 'A', '2019-07-11 12:00:19', 1),
(791, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE EAU VIVE', 'Route Abatta carrefour supermarche Cash Center Akouedo Attie ', '5.359165', '-3.924998', '22 47 34 45', 'A', '2019-07-11 12:00:19', 1),
(792, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE  LAURIERS ATTOBAN', 'Riviera attoban  derriere leglise st bernard  carrefour des cites lauriers 3 et 4  abri 2000', '5.371861', '-3.979666', '08 43 54 94 /  22 43 90 89', 'A', '2019-07-11 12:00:19', 1),
(793, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE MARIE ESTHER', 'Route du lycee st Viateur Rosiers Programme 4 Riviera Palmeraie', '5.376501', '-3.960389', ' 22 49 53 23 / 22 49 03 30', 'A', '2019-07-11 12:00:19', 1),
(794, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE MONT CARMEL', 'A proximite de lhotel Belle Cote Les Rosiers 5eme programme ', '5.386017', '-3.968862', '22 500 758 / 01 35 08 36', 'A', '2019-07-11 12:00:19', 1),
(795, 'COCODY RIVIERA  PALMERAIE', 'PHARMACIE NOTRE DAME DES GRACES', 'Pres de la petite Mosquee  Riviera 2 Cocody', '5.352116 ', '-3.980068', '22 43 61 96', 'A', '2019-07-11 12:00:19', 1),
(796, 'COCODY RIVIERA PALMERAIE', 'PHARMACIE RUE MINISTRE', 'Rue Ministre   au Carrefour AGITEL  Riviera Palmeraie', '5.365671', '-3.957880', '22 49 51 77', 'A', '2019-07-11 12:00:19', 1),
(797, 'KOUMASSI', 'PHARMACIE ANIEL', 'Bd du 7 Decembre pres de l Eglise Saint Etienne Remblais Koumassi ', '5.290445', '-3.962457', '21 28 56 24 / 07 09 07 52', 'A', '2019-07-11 12:00:19', 1),
(798, 'KOUMASSI', 'PHARMACIE BABAKAN', 'Bd Houphouet Boigny apres Commissariat du 20 eme Arret face Mosquee Sangare', '5.307588', '-3.941285', '21 28 94 45', 'A', '2019-07-11 12:00:19', 1),
(799, 'KOUMASSI', 'PHARMACIE BENIABIE', 'bd du Caire terminus bus 13 26  33', '5.294169', '-3.955762', ' 21 28 95 25', 'A', '2019-07-11 12:00:19', 1),
(800, 'KOUMASSI', 'PHARMACIE BETHANIE ', 'Route menant du Camp Commando a la Zone Industrielle Bia Sud  Sicogi 1', ' 5.284680 ', '-3.958927', '21 36 62 83 ', 'A', '2019-07-11 12:00:19', 1),
(801, 'KOUMASSI', 'PHARMACIE CLIMBIE ', 'Angle Bd du Caire et Rue Raoul Follereau  face a la Grande Mosquee', '5.2990575', '-3.9544752', ' 21 36 35 46 ', 'A', '2019-07-11 12:00:19', 1),
(802, 'KOUMASSI', 'PHARMACIE DE KOUMASSI    SOUMAH     ', 'Bd du 7 decembre  pres de la formation sanitaire de Koumassi', '5.288686', '-3.968942', '21 36 19 85', 'A', '2019-07-11 12:00:19', 1),
(803, 'KOUMASSI', 'PHARMACIE DE LA BAGOE ', 'Bd du Cameroun Carrefour Station Mobile et Sodeci Marcory face Centre Medical Liring ', '5.395020', '-4.008788', '21 28 64 14 ', 'A', '2019-07-11 12:00:19', 1),
(804, 'KOUMASSI', 'PHARMACIE DE PRODOMO ', 'Pres Terrain Inchallah Bd Cameroun Carrefour Sopim  Prodomo 1', '5.299495', '-3.953142', '21 28 83 43', 'A', '2019-07-11 12:00:19', 1),
(805, 'KOUMASSI', ' PHARMACIE DES SABLES', 'Voie Principale Pres de la Cite Batim Sir  Remblais Koumassi', '5.303955', '-3.962417', '21 28 84 17', 'A', '2019-07-11 12:00:19', 1),
(806, 'KOUMASSI', 'PHARMACIE DU BOULEVARD DU GABON ', '200 m de la clinique la madone face pressing akwaba bus 32 26 23 12', '5.3036448', '-3.9711714', ' 21 36 24 62 ', 'A', '2019-07-11 12:00:19', 1),
(807, 'KOUMASSI', 'PHARMACIE DU CAMPEMENT', 'Carrefour campement Koumassi', ' 5.304852', '-3.945138', '21 36 18 91 ', 'A', '2019-07-11 12:00:19', 1),
(808, 'KOUMASSI', 'PHARMACIE DU CANAL ', 'Entre le Carrefour Gabi feu tricolore et Carrefour Millenium', '5.392923', '-4.012023', '21 36 27 48', 'A', '2019-07-11 12:00:19', 1),
(809, 'KOUMASSI', 'PHARMACIE DU MARAIS', 'Carrefour Quartier Divo Remblais Koumassi ', '5.301704', '-3.957539', '21 36 06 99', 'A', '2019-07-11 12:00:19', 1),
(810, 'KOUMASSI', 'PHARMACIE DU SOLEIL', 'Angle Av 9 Bd du Gabon a 200 m de la Poste Sicogi 1 Koumassi', '5.293071', '-3.970085', '21 36 05 98', 'A', '2019-07-11 12:00:19', 1),
(811, 'KOUMASSI', 'PHARMACIE EHILE', ' Bd du Caire pres du grand marche Ligne bus 13  Sicogi 1', '5.293158', '-3.960232', '21 36 45 44', 'A', '2019-07-11 12:00:19', 1),
(812, 'KOUMASSI', 'PHARMACIE FANNY', 'Terminus Bus 05 et 32 - Aklomiabla Koumassi', '5.310575', '-3.947621', '21 36 09 07 ', 'A', '2019-07-11 12:00:19', 1),
(813, 'KOUMASSI', 'PHARMACIE GAUDO', 'Pres du Kalum Bar Ligne Bus 26 Derriere le college Adjavon Sicogi 3', '5.288468', '-3.963447', '21 36 22 51', 'A', '2019-07-11 12:00:19', 1),
(814, 'KOUMASSI', 'PHARMACIE IROKO', 'Ligne Bus 32 de lecole Mondon  Remblais Koumassi ', '5.294756', '-3.964125', '21 56 43 56  ', 'A', '2019-07-11 12:00:19', 1),
(815, 'KOUMASSI', 'PHARMACIE KAHIRA', 'Bd du Cameroun Ligne bus 05 Paroisse Ste Bernadette ', '5.296651', '-3.967600', '21 36 39 84  ', 'A', '2019-07-11 12:00:19', 1),
(816, 'KOUMASSI', 'PHARMACIE MESANO ', 'Derriere l eglise de Prodomo Terminus 11 a Gauche de leglise Catholique', '5.301235', '-3.947484', '21 36 21 90  ', 'A', '2019-07-11 12:00:19', 1),
(817, 'KOUMASSI', 'PHARMACIE ROC', 'Koumassi Quartier campement a 100m de lepv belier', '5.309355', '-3.938163', '21 36 04 86 / 08 52 58 23 ', 'A', '2019-07-11 12:00:19', 1),
(818, 'KOUMASSI', 'PHARMACIE REGINA', 'Pharmacies Ligne 32  2e Arret  Sogefia Koumassi', '5.298804', '-3.949588', '21 36 68 65', 'A', '2019-07-11 12:00:19', 1),
(819, 'KOUMASSI', 'PHARMACIE SAINT LOUIS', 'Bd du Cameroun Face a EPP Baradji apres zone Remblais entre \nEglise Saint Francois et Eglise Prodomo ligne Bus 05', '5.298234', '-3.959772', '21 36 22 10', 'A', '2019-07-11 12:00:19', 1),
(820, 'KOUMASSI', 'PHARMACIE SAINT ALBERT', 'Grand Marche Face eglise Methodiste Cite des Graces', '5.292504', '-3.957540', '21 28 76 04/ 21 28 75 99', 'A', '2019-07-11 12:00:19', 1),
(821, 'KOUMASSI', 'PHARMACIE SAINT GEORGES', 'Pres du Camp Commando  Sicogi 1', '5.2835441', '-3.9653478', '21 36 03 75 ', 'A', '2019-07-11 12:00:19', 1),
(822, 'KOUMASSI', 'PHARMACIE SAINT LUCIEN', 'En face du depot de la SOTRA Soweto  Sicogi 3', '5.2858919', '-3.9563785', '21 28 84 80/07 57 03 74 ', 'A', '2019-07-11 12:00:19', 1),
(823, 'KOUMASSI', 'PHARMACIE SAINT PAUL ', 'Non loin du Terminus 32 pres de lhotel le Recul Nord Est  Aklomiabla', '5.3100866', '-3.9526434', '21 36 19 28 ', 'A', '2019-07-11 12:00:19', 1),
(824, 'KOUMASSI', 'PHARMACIE SAINT SAUVEUR ', 'Face a lEglise Buisson Ardent Non loin de la station mobil Zone CNPS ', '5.2975075', '-3.9562154', '21 36 69 93 ', 'A', '2019-07-11 12:00:19', 1),
(825, 'KOUMASSI', ' PHARMACIE SAINTE AGATHE ', 'Rue du Canal a cote du College la Colombe', '5.3022187', '-3.9685643', '21 56 51 88 / 21 56 51 89 ', 'A', '2019-07-11 12:00:19', 1),
(826, 'KOUMASSI', 'PHARMACIE SAINTE FOI ', 'Terminus Bus 05  Face Lycee Municipal', '5.3022987', '-3.9751304', '21 56 17 40 / 21 56 19 19', 'A', '2019-07-11 12:00:19', 1),
(827, 'KOUMASSI', 'PHARMACIE DE SOPIM', 'KOUMASSI SOPIM', '5.3022775', '-3.9518541', '21 36 69 32', 'A', '2019-07-11 12:00:19', 1),
(828, 'KOUMASSI', 'PHARMACIE GALILEE', ' Bd Antannarivo a l angle de la rue Geo Andre face Base CIE  Sicogi 1', '5.2829758', '-3.9699322', '', 'A', '2019-07-11 12:00:19', 1),
(829, 'KOUMASSI', 'PHARMACIE INCHALLAH', 'Pres du Carrefour sans fil A 50m du groupe Scolaire Entente ', '5.3011637', '-3.9652947', '21 36 47 01 / 01 53 99 99', 'A', '2019-07-11 12:00:19', 1),
(830, 'KOUMASSI', 'PHARMACIE  MPIKE', 'Derriere la Mairie  Sicogi 1 Koumassi', '5.2902777', '-3.9707235', '21 36 25 75', 'A', '2019-07-11 12:00:19', 1),
(831, 'KOUMASSI', 'PHARMACIE QUARTIER DIVO', 'Entre le Maquis Calao et le Caniveau  Quartier Divo Koumassi', '5.3023743', '-3.9561111', '46 20 86 59', 'A', '2019-07-11 12:00:19', 1),
(832, 'KOUMASSI', 'PHARMACIE SAINT FRANCOIS', 'Angle Av 9 et Rue 11 Prolongement Bd du Gabon en allant vers Prodomo', '5.2952041', '-3.9620938', '21 36 21 77', 'A', '2019-07-11 12:00:19', 1),
(833, 'MARCORY', ' PHARMACIE ANOUMABO ', 'Face EPP Anoumabo 1er arret bus 03 et 31', '5.3069055', '-3.9898021', '21 26 02 07', 'A', '2019-07-11 12:00:19', 1),
(834, 'MARCORY', 'PHARMACIE CANAAN ', '36 Rue Paul Langevin A 500 m de Mercedes  Zone 4 C', '5.3090766', '-3.955673', '21 25 96 76 ', 'A', '2019-07-11 12:00:19', 1),
(835, 'MARCORY', 'PHARMACIE CYRILLE', 'Angle Bd de Marseille  Bd Giscard d Estaing', '5.3092465', '-3.9556731', '21 25 95 28', 'A', '2019-07-11 12:00:19', 1),
(836, 'MARCORY', 'PHARMACIE DE BIETRY ', 'Bd de Marseille Rue du Canal avant Centre Commercial Holliday Imm SGBCI', '5.2752483', '-3.9805091', '21 24 86 89', 'A', '2019-07-11 12:00:19', 1),
(837, 'MARCORY', 'PHARMACIE DE LA GALERIE', 'Bd Giscard d  Estaing Angle Rue Thomas Edison    Super Hayat    Cap Sud ', '5.2976013', '-3.9883861', '21 35 24 73', 'A', '2019-07-11 12:00:19', 1),
(838, 'MARCORY', 'PHARMACIE DE LA PAIX ', 'Bd de Loraine Face a la SIB et Station Texaco  Marcory  Residentiel', '5.3089559', '-3.9935579', '21 26 30 19 / 21 26 36 89 ', 'A', '2019-07-11 12:00:19', 1),
(839, 'MARCORY', 'PHARMACIE DE LA VIE', 'Bd Achalme Imm Teranga  Residentiel Marcory', '5.3136306', '-3.9951292', '21 28 18 68 ', 'A', '2019-07-11 12:00:19', 1),
(840, 'MARCORY', 'PHARMACIE DE LA ZONE 3  ', 'Rue Clement Ader Centre Commercial Square  Zone 3', '5.2952577', '-3.9894661', '21 35 13 15', 'A', '2019-07-11 12:00:19', 1),
(841, 'MARCORY', 'PHARMACIE DE L AMITIE  ', 'Pharmacies Bd du Cameroun non loin du Grand Marche  Remblais Marcory ', '5.300865', '-3.9789709', '21 26 85 06 / 07 67 46 55 ', 'A', '2019-07-11 12:00:19', 1),
(842, 'MARCORY', 'PHARMACIE DE L INJS', 'Grand portail de l Injs face Station Total', '5.3060004', '-3.9817302', '21 28 10 10  ', 'A', '2019-07-11 12:00:19', 1),
(843, 'MARCORY', 'PHARMACIE DES HIBISCUS', ' Boulevard de Brazzaville Pres de la PMI  Potopoto Marcory', '5.2952844', '-3.9960322', '21 26 31 66  ', 'A', '2019-07-11 12:00:19', 1),
(844, 'MARCORY', 'PHARMACIE DES LAGUNES', 'Rue de la Paix Face a la station Shell  Marcory  Residentiel', '5.3075355', '-3.9967961', '21 26 12 40/21 26 63 80  ', 'A', '2019-07-11 12:00:19', 1),
(845, 'MARCORY', 'PHARMACIE DU BD DE MARSEILLE', 'Bd marseille  Station Shell  Face Lonaci  Zone 3', '5.3075462', '-3.9967961', '21 25 76 25', 'A', '2019-07-11 12:00:19', 1),
(846, 'MARCORY', 'PHARMACIE DU CARREFOUR ', 'Bd VGE Rue du Chevalier de Clieu face BICICI Grand carrefour  Zone 4 A', '5.2997513', '-3.9917262', '21 35 11 66 / 21 35 29 70', 'A', '2019-07-11 12:00:19', 1),
(847, 'MARCORY', 'PHARMACIE DU GRAND MARCHE DE MARCORY', 'Bd Cameroun Rue F133 face Grand Marche  Remblais Marcory', '5.3002477', '-3.9774881', '21 56 90 96  ', 'A', '2019-07-11 12:00:19', 1),
(848, 'MARCORY', 'PHARMACIE DU PETIT MARCHE', 'Rue de l Eglise Sainte Therese en Allant vers la PMI Bus N 32 ', '5.3002744', '-3.9840542', '21 26 24 33 / 21 26 98 08 ', 'A', '2019-07-11 12:00:19', 1),
(849, 'MARCORY', 'PHARMACIE EBATHE', 'Avenue de la Cote d Ivoire  Prolongement PMI', '5.3030947', '-3.9849043', '21 56 77 15 ', 'A', '2019-07-11 12:00:19', 1),
(850, 'MARCORY', 'PHARMACIE ELITE', 'Bd VGE Pres de CI Telecom  Zone 4 C Marcory', '5.295069', '-3.9825399', '21 35 11 19', 'A', '2019-07-11 12:00:19', 1),
(851, 'MARCORY', ' PHARMACIE LE MERIDIEN  ', 'Bd du Cameroun dans le centre commercial Ligne 11  GFCI Marcory', '5.3027071', '-3.9819931', '21 28 27 00 / 07 89 09 85 ', 'A', '2019-07-11 12:00:19', 1),
(852, 'MARCORY', 'PHARMACIE LEILA  ', 'Non loin de l ATCI et de l institut Froebel A 150m du carrefour Dje bi Dje  Sans fil', '5.3117374', '-3.9677586', '21 26 72 94 ', 'A', '2019-07-11 12:00:19', 1),
(853, 'MARCORY', 'PHARMACIE LES REMBLAIS ', 'Face Commissariat du 26eme arrondissement Alliodan  Sans fil Marcory ', '5.306791', '-3.965807', '21 26 25 40', 'A', '2019-07-11 12:00:19', 1),
(854, 'MARCORY', 'PHARMACIE MARCORY PTT ', 'Bd Cameroun Derriere cours Lamartine 200m de la Poste  Residentiel Marcory', '5.3085006', '-3.9917848', '21 26 98 83 ', 'A', '2019-07-11 12:00:19', 1);
INSERT INTO `pharmacies` (`id_pharm`, `localite_pharm`, `libelle_pharm`, `adresse_pharm`, `latitude_pharm`, `longitude_pharm`, `telephone_pharm`, `etat_pharm`, `date_ajout_pharm`, `ville_id`) VALUES
(855, 'MARCORY', 'PHARMACIE MASSARANA   ', 'Angle Bd Gabon et Av de la Cote d ivoire \nContigue a l ex Hotel Massarana Derriere Orca Deco', '5.2992208', '-3.9869927', '21 28 20 48', 'A', '2019-07-11 12:00:19', 1),
(856, 'MARCORY', 'PHARMACIE NOUVELLE PERGOLA ', 'Angle Rue Pierre et Marie Curie et Bd de Marseille apres le cash center', '5.2834548', '-3.9864191', '21 24 48 27 ', 'A', '2019-07-11 12:00:19', 1),
(857, 'MARCORY', 'PHARMACIE P.DIOSCORIDE ', '37 Bd de Marseille  Bietry', '5.2745758', '-3.9812873', '21 35 21 10 ', 'A', '2019-07-11 12:00:19', 1),
(858, 'MARCORY', 'PHARMACIE PERUSIA', 'Rue du Canal Imm Manouchka  Zone 4 C Marcory ', '5.280631', '-3.9789606', '21 35 78 85', 'A', '2019-07-11 12:00:19', 1),
(859, 'MARCORY', 'PHARMACIE PIERRE ET MARIE CURIE ', 'Rue Pierre et Marie Curie Centre commercial Francisco pres de Renault  Zone 4 C', '5.2913941', '-3.984796', '21 35 80 30 ', 'A', '2019-07-11 12:00:19', 1),
(860, 'MARCORY', 'PHARMACIE SAINT JOSEPH', ' 6 Rue du 7 Decembre face a AMORE  Zone 4 C', '5.2848056', '-3.9763309', '21 24 45 19/21 24 45 20', 'A', '2019-07-11 12:00:19', 1),
(861, 'MARCORY', 'PHARMACIE SAINTE THERESE', 'Avenue TSF Face a l Eglise  Ste Therese Marcory', '5.3038104', '-3.9899066', '21 26 77 10 ', 'A', '2019-07-11 12:00:19', 1),
(862, 'MARCORY', 'PHARMACIE SAINT ANTOINE ', 'Rue Dr Blanchard Pres du centre Sportif Zirignon  Zone 4 C', '5.2893236', '-3.9802721', '21 35 27 00 / 01 08 69 25', 'A', '2019-07-11 12:00:19', 1),
(863, 'MARCORY', 'PHARMACIE SAINTE MARIE-MAJEURE ', 'Rue du Canal a cote du garage Top auto  Zone 4 C Marcory ', '5.2773253', '-3.9735914', '21 25 54 07 ', 'A', '2019-07-11 12:00:19', 1),
(864, 'MARCORY', 'PHARMACIE SAINTE RUTH', 'Centre Commercial Prima  Zone 4 C Marcory', '5.2953979', '-3.9852932', '21 26 24 33', 'A', '2019-07-11 12:00:19', 1),
(865, 'MARCORY', 'PHARMACIE TIACOH ', 'Bd du Gabon et Carrefour Autoroute  Angle Rue Pierre et Marie Curie', '5.2971923', '-3.9828926', '21 26 28 15', 'A', '2019-07-11 12:00:19', 1),
(866, 'MARCORY', 'PHARMACIE TSF  ', 'Av TSF Non loin du 9eme Arrondissement  Konan Raphael ', '5.306823', '-3.9877796', '21 26 47 82', 'A', '2019-07-11 12:00:19', 1),
(867, 'MARCORY', 'PHARMACIE DU BD GISCARD D  ESTAING', 'Marcory Carrefour Score  Station Texaco', '5.3076155', '-4.0033622', '', 'A', '2019-07-11 12:00:19', 1),
(868, 'MARCORY', 'PHARMACIE ES BIA', 'Bd du Gabon ligne 32 a cote de la Clinique la Madone  Sicogi Marcory', '5.2945172', '-3.9783326', '21 28 08 88', 'A', '2019-07-11 12:00:19', 1),
(869, 'MARCORY', 'PHARMACIE LANVIA', 'Rue Pierre et Marie Curie en face de Metalux  Zone 4 C', '5.2870023', '-3.9847674', '49 86 82 15 / 21 21 24 00', 'A', '2019-07-11 12:00:19', 1),
(870, 'MARCORY', 'PHARMACIE MAHANDIANA', 'Marcory sicogi zone de l eglise ste Bernadette derriere le marche', '5.3068177', '-3.9723731', '21 26 21 62 / 57 04 86 36', 'A', '2019-07-11 12:00:19', 1),
(871, 'MARCORY', 'PHARMACIE NOTRE DAME D AFRIQUE', '37 Bd de Marseille en face College Notre dame d Afrique  Bietry  Zone 4 C ', '5.2734717', '-3.9776069', '21 25 25 26', 'A', '2019-07-11 12:00:19', 1),
(872, 'PLATEAU', ' PHARMACIE ANIAMAN', 'Rue des Banques Face BMW Imm Aniaman a cote d Alize voyage', '5.320495', '-4.016746', '20 32 12 29/07 07 16 76', 'A', '2019-07-11 12:00:19', 1),
(873, 'PLATEAU', 'PHARMACIE BEL FAM    GDE   ', 'Av Crosson Duplessis en face de MTN Plateau', '5.320256', '-4.013149', '20 33 03 30', 'A', '2019-07-11 12:00:19', 1),
(874, 'PLATEAU', 'PHARMACIE CENTRALE', 'Av Franchet d Esperey Rue Lecoeur Imm BP', '5.322427', '-4.017921', '20 22 41 41', 'A', '2019-07-11 12:00:19', 1),
(875, 'PLATEAU', 'PHARMACIE CHARDY ', 'Av chardy pres station shell  cinema le paris  entre lynx optique et cinema le Paris', '5.323899', '-4.017189', '20 21 66 01 ', 'A', '2019-07-11 12:00:19', 1),
(876, 'PLATEAU', 'PHARMACIE CHICAYA ', ' Avenue Chardy  Immeuble Nour Al Hayat', '5.323520', '-4.018203', '20 21 33 40', 'A', '2019-07-11 12:00:19', 1),
(877, 'PLATEAU', 'PHARMACIE DE L AVENUE NOGUES  ', 'Avenue Nogues  En face de la Banque Sahelo Saharienne', '5.316508', '-4.017948', '20 31 16 10 /12', 'A', '2019-07-11 12:00:19', 1),
(878, 'PLATEAU', 'PHARMACIE DES BANQUES', 'Rue Des Banques Plateau', '5.321182', '-4.017848', '20 32 94 12 ', 'A', '2019-07-11 12:00:19', 1),
(879, 'PLATEAU', 'PHARMACIE DES FINANCES ', 'Bd Clozel Imm Sitarail vers le palais de justice', '5.325898', '-4.020561', ' 20 21 70 61/20 30 39 50    ', 'A', '2019-07-11 12:00:19', 1),
(880, 'PLATEAU', 'PHARMACIE DU BD CARDE', 'Face cercle des Rails', '5.326645', '-4.024263', '20 21 01 08 ', 'A', '2019-07-11 12:00:19', 1),
(881, 'PLATEAU', 'PHARMACIE DU COMMERCE', 'Rue Du Commerce Immeuble L ebrien', '5.317976', '-4.014481', '20 22 31 31', 'A', '2019-07-11 12:00:19', 1),
(882, 'PLATEAU', 'PHARMACIE KORALIE', 'Imm JECEDA Angle Bd de la Republique et Av  Marchand ', '5.325444', '-4.019508', '20 33 88 25', 'A', '2019-07-11 12:00:19', 1),
(883, 'PLATEAU', 'PHARMACIE DU MEMORIAL  ', 'Av  16 Lamblin Imm Signal en face du Patronat ', '5.3253668', '-4.0248749', '20 32 36 90', 'A', '2019-07-11 12:00:19', 1),
(884, 'PLATEAU', 'PHARMACIE DU PALAIS DE JUSTICE ', '55 bd clozel  face palais de justice  pres de la recette municipale', '5.331372 ', '-4.020674', '20 21 95 64', 'A', '2019-07-11 12:00:19', 1),
(885, 'PLATEAU', 'PHARMACIE DU PLATEAU ', 'immeuble botreau roussel face immeuble pyramide et la BCEAO', '5.323267', '-4.016130', '22 20 79 70   ', 'A', '2019-07-11 12:00:19', 1),
(886, 'PLATEAU', 'PHARMACIE DU TERMINUS', 'Rue Treich Lapleine Immeuble Afram Face Gare Sud SOTRA', '5.3310726', '-4.0294612', '20 32 17 15', 'A', '2019-07-11 12:00:19', 1),
(887, 'PLATEAU', 'PHARMACIE DU TIAMA ', 'Angle bd De la republique et av Jesse owens  face hotel tiama', '5.3311152', '-4.0294612', '20 21 34 18', 'A', '2019-07-11 12:00:19', 1),
(888, 'PLATEAU', 'PHARMACIE DU TREFLE', 'Av  Nogues Face Afriland First banque', '5.317900', '-4.016550', '20 22 88 02  ', 'A', '2019-07-11 12:00:19', 1),
(889, 'PLATEAU', 'PHARMACIE LES HARMONIES ', 'non loin du camp militaire galieni  20 m de la gare sotra \nde la cite administrative bd carde face immeuble les harmonies', '5.330816', '-4.024011', '20 22 04 92/ 02 02 04 76', 'A', '2019-07-11 12:00:19', 1),
(890, 'PLATEAU', 'PHARMACIE LES STUDIOS  ', 'Ancien hotel du parc 43 bd de la Republique', '5.3208671', '-4.0200365', '20 21 95 95/20 21 98 98', 'A', '2019-07-11 12:00:19', 1),
(891, 'PLATEAU', 'PHARMACIE LONGCHAMP ', 'Entre  l imprimerie nationale et l hotel ibis Angle avenue Marchand Boulevard Roume', '5.325650', '-4.022654', '20 22 75 98', 'A', '2019-07-11 12:00:19', 1),
(892, 'PLATEAU', 'PHARMACIE MODERNE MAZUET', 'Rue du Commerce cote Gare Sud Imm Nassar', '5.315839', '-4.017241', '20 32 96 64/220 22 21 31', 'A', '2019-07-11 12:00:19', 1),
(893, 'PLATEAU', 'PHARMACIE NIACADIE', 'Rue du Marche en face du siege de la BICICI Plateau ', '5.322457', '-4.018829', '20 21 70 68  ', 'A', '2019-07-11 12:00:19', 1),
(894, 'PLATEAU', 'PHARMACIE PLACE DE LA REPUBLIQUE', 'Av Houdaille pres ancienne Tour EECI ', '5.317801', '-4.017961', '20 21 57 78', 'A', '2019-07-11 12:00:19', 1),
(895, 'PLATEAU', 'PHARMACIE SAINT LAZARE', 'Rue Colomb face Hotel du District non loin de la SGBCI Imm Cash Center ', '5.321197', '-4.019249', '20 33 12 68', 'A', '2019-07-11 12:00:19', 1),
(896, 'PLATEAU', 'PHARMACIE ZAHARA', 'Rue du Commerce  Cote Peyrissac', '5.3213347', '-4.0892576', '20 32 20 28', 'A', '2019-07-11 12:00:19', 1),
(897, 'PLATEAU', 'PHARMACIE DES TOURS', 'av jean paul 2  pres cathedrale st paul  entrees bd angoulvant\n et bd roume  immeuble cic wtc cite administrative', ' 5.331450', '-4.021824', '20 21 91 28', 'A', '2019-07-11 12:00:19', 1),
(898, 'PLATEAU', 'PHARMACIE SAINTE ANNE', '1 Rue lecoeur Face Ambassade de France Plateau ', '5.3208284', '-4.0214053', '20 22 69 00', 'A', '2019-07-11 12:00:19', 1),
(899, 'PORT BOUET VRIDI', 'PHARMACIE AKWABA', 'Aeroport Felix Houphouet boigny', '5.255164', '-3.932827', '21 58 73 15/87 75 13 13', 'A', '2019-07-11 12:00:19', 1),
(900, 'PORT BOUET VRIDI', 'PHARMACIE ATLANTIQUE  ', 'Zone du marche de nuit  Sogefiha PortBouet', '5.255124', '-3.963994', '21 27 95 00', 'A', '2019-07-11 12:00:19', 1),
(901, 'PORT BOUET VRIDI', ' PHARMACIE BALNEAIRE', 'Route de Bassam face entree du 43eme BIMA ', ' 5.256473', '-3.957491', '21 58 07 09 ', 'A', '2019-07-11 12:00:19', 1),
(902, 'PORT BOUET VRIDI', 'PHARMACIE BALTIQUE', 'Face abattoir  Sogefiha PortBouet', '5.259424', '-3.969090', ' 21 27 66 71/08 25 36 26', 'A', '2019-07-11 12:00:19', 1),
(903, 'PORT BOUET VRIDI', 'PHARMACIE DE L OCEAN', 'Av Pacifique Rue des Espadons prolongement de la Boulangerie Boe et CEG ', '5.257269', '-3.961355', '21 27 78 48', 'A', '2019-07-11 12:00:19', 1),
(904, 'PORT BOUET VRIDI', 'PHARMACIE DE PORT-BOUET ', 'Non loin de la Mairie  Sogefiha PortBouet', '5.260538', '-3.964745', '21 27 89 17 ', 'A', '2019-07-11 12:00:19', 1),
(905, 'PORT BOUET VRIDI', 'PHARMACIE DJIGUI', 'Angle Av Atlantique et Av Pacifique Face au Grand Marche 30m du Commissariat ', '5.258156', '-3.964783', '21 27 75 86 ', 'A', '2019-07-11 12:00:19', 1),
(906, 'PORT BOUET VRIDI', 'PHARMACIE DU PHARE   ', 'Av des caraibes Pres du phare Terminus Bus 12  Phare ', '5.252335', '-3.958926', '21 27 73 73 ', 'A', '2019-07-11 12:00:19', 1),
(907, 'PORT BOUET VRIDI', 'PHARMACIE DU WHARF ', ' Quartier derriere wharf route de Grand Bassam pres du depot 8 de la sotra', '5.248129', '-3.944015', '21 27 71 31', 'A', '2019-07-11 12:00:19', 1),
(908, 'PORT BOUET VRIDI', 'PHARMACIE MALON', 'Pres de la grande Mosquee  Sogefiha PortBouet', '5.260431 ', '-3.960893', '21 27 96 14 / 07 90 68 50', 'A', '2019-07-11 12:00:19', 1),
(909, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI SIR', 'Vridi Cite entre la Cite Policiere et l Hopital Municipal de Vridi Port-Bouet ', '5.262563 ', '-3.982806', '21 27 02 60    ', 'A', '2019-07-11 12:00:19', 1),
(910, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI VILLAGE', 'Terminus bus 19 Vridi PortBouet', '5.255915', '-3.997500', '21 27 58 60', 'A', '2019-07-11 12:00:19', 1),
(911, 'PORT BOUET VRIDI', 'PHARMACIE DES PLAGES  ', 'PORT BOUET Adjouffou  Route de Bassam', '5.244134 ', ' -3.922746', '21 27 71 04', 'A', '2019-07-11 12:00:19', 1),
(912, 'PORT BOUET VRIDI', 'PHARMACIE ELOMA   ', 'Port Bouet  Gonzag Terminus du bus 17', '5.240478', '-3.899748', '21 58 60 28', 'A', '2019-07-11 12:00:19', 1),
(913, 'PORT BOUET VRIDI', ' PHARMACIE ROUTE BASSAM', 'Au carrefour du centre commercial Mobibois pres du marche de Jean Folly ', '5.2444822', '-3.9399111', '21 58 63 54 ', 'A', '2019-07-11 12:00:19', 1),
(914, 'PORT BOUET VRIDI', 'PHARMACIE SANTE POUR TOUS ', 'Jean Folly  Carrefour Casier', '5.244613', '-3.913071', '21 58 76 34 / 08 02 47 86 ', 'A', '2019-07-11 12:00:19', 1),
(915, 'PORT BOUET VRIDI', 'PHARMACIE SAINT LOUIS DE GONZAG ', 'Face lycee municipal de Gonzagueville', '5.250245', '-3.900203', '21 58 71 79', 'A', '2019-07-11 12:00:19', 1),
(916, 'PORT BOUET VRIDI', 'PHARMACIE BETHEL', 'Cite Policiere Face QG AKA NANGUI Pres de l Eglise du Reveil  Sogefiha', '5.267678 ', '-3.975362', '21 27 98 12', 'A', '2019-07-11 12:00:19', 1),
(917, 'PORT BOUET VRIDI', 'PHARMACIE  DES ENTREPRISES', ' Rue du Textile face Ancien Toyota premoto 250m environ du tripostal  Vridi ', '5.267160', '-4.002552', '21 27 17 32', 'A', '2019-07-11 12:00:19', 1),
(918, 'PORT BOUET VRIDI', 'PHARMACIE VRIDI PALM-BEACH', 'd Petit bassam sir Carrefour Vridi Cite  Zone Industrielle Vridi ', '5.258790', '-3.982013', '21 27 52 04 ', 'A', '2019-07-11 12:00:19', 1),
(919, 'TREICHVILLE', 'PHARMACIE ALPHA 16  ', 'Av 16 Rue 17 Treichville a cote de Treich Hotel', '5.304859', '-4.009035', '21 25 88 80', 'A', '2019-07-11 12:00:19', 1),
(920, 'TREICHVILLE', 'PHARMACIE BELLEVILLE ', '7 Av sur la voie du marche belleVille', '5.302954', '-3.998566', '21 25 09 32 ', 'A', '2019-07-11 12:00:19', 1),
(921, 'TREICHVILLE', 'PHARMACIE CHEMINOT', 'Avenue 21 Rue 9  Quartier France Amerique Entre Sitarail Et Carrefour ', ' 5.301399 ', '-4.011767', '21 24 09 45 / 21 24 17 81', 'A', '2019-07-11 12:00:19', 1),
(922, 'TREICHVILLE', 'PHARMACIE DE LA BOURSE ', 'Non loin du siege de la CIE SODECI', '5.3121014', '-4.021178', '21 25 01 28', 'A', '2019-07-11 12:00:19', 1),
(923, 'TREICHVILLE', 'PHARMACIE DE L AVENUE 21', 'Rue 21 pres de la Bibliotheque Islamique Carrefour de l Av  21 Bus 05 et 03', '5.303104', '-4.006408', '21 24 83 46', 'A', '2019-07-11 12:00:19', 1),
(924, 'TREICHVILLE', ' PHARMACIE DE L ENTENTE ', 'Pharmacies Angle Av 21 Rue 44  Entente Treichville', '5.307488', '-4.002325', '21 25 26 95  ', 'A', '2019-07-11 12:00:19', 1),
(925, 'TREICHVILLE', ' PHARMACIE DES 3 MOSQUEES', 'dans l avenue 8 Rue 22 Baree A 300 M de la Station Shell Imm Nanan Yamoussou ', '5.308862 ', '-4.007916', '21 24 22 72 / 21 24 21 96 ', 'A', '2019-07-11 12:00:19', 1),
(926, 'TREICHVILLE', 'PHARMACIE DES BRASSEURS', '1 Rue des brasseurs  Face au CHU Entree station Total et Abidjan Continu  Km 4', '5.295843', '-4.002951', '21 25 17 25 / 21 25 31 51', 'A', '2019-07-11 12:00:19', 1),
(927, 'TREICHVILLE', ' PHARMACIE DES QUAIS ', 'Face Grands Moulins Zone portuaire', '5.306633 ', '-4.021562', '21 24 04 94 / 21 24 04 99', 'A', '2019-07-11 12:00:19', 1),
(928, 'TREICHVILLE', 'PHARMACIE DU GRAND MARCHE ', 'Av 8 Angle Rue 12 en face de la Foire de Chine Treichville ', '5.3086958', '-4.017051', '21 24  00 72', 'A', '2019-07-11 12:00:19', 1),
(929, 'TREICHVILLE', 'PHARMACIE DU LEVANT ', 'Rue 12 Imm Charara  Zone 1', '5.302277 ', '-4.010796', '21 24 14 21   ', 'A', '2019-07-11 12:00:19', 1),
(930, 'TREICHVILLE', 'TREICHVILLE', ' Non loin Palais des sports Prolongement de la Banque BOA  Zone 2  ', '5.297980', '-4.008498', '21 24 02 14 / 21 24 09 83', 'A', '2019-07-11 12:00:19', 1),
(931, 'TREICHVILLE', 'PHARMACIE DU PLAZA', 'v 1 Rue 12 face a la Garde Republicaine', '5.3106688', '-4.0158248', '21 24 03 39/21 24 13 33 ', 'A', '2019-07-11 12:00:19', 1),
(932, 'TREICHVILLE', 'PHARMACIE DU PORT', 'Port de peche  Zone portuaire', '5.289358 ', '-4.008633', '21 25 60 32', 'A', '2019-07-11 12:00:19', 1),
(933, 'TREICHVILLE', 'PHARMACIE D ARRAS ', 'Gare de Bassam', '5.300640', '-4.002856', '21 24 13 29 / 21 24 21 93', 'A', '2019-07-11 12:00:19', 1),
(934, 'TREICHVILLE', 'PHARMACIE FLORA ', ' Av 08 pres du Commissariat du 2eme Arrondissement et du Cinema Vox  ', ' 5.306861 ', '-4.014399', '21 24 05 91', 'A', '2019-07-11 12:00:19', 1),
(935, 'TREICHVILLE', 'PHARMACIE FRANCE AFRIQUE', 'Angle Avenue 2 Rue 5  Face Cite Policiere et Stations Shell et Mobile', '5.308992 ', '-4.016066', ' 21 24 18 19  ', 'A', '2019-07-11 12:00:19', 1),
(936, 'TREICHVILLE', 'PHARMACIE STE JEANNE D  ARC  ', 'Av 12 Rue 05 Ex Cite RAN Treichville', '5.306972 ', '-3.998234', '21 24 04 74 ', 'A', '2019-07-11 12:00:19', 1),
(937, 'TREICHVILLE', 'PHARMACIE LA GRACE ', 'Avenue 14 Rue 12 Treichville', '5.309961 ', '-4.011581', '21 35 38 79 ', 'A', '2019-07-11 12:00:19', 1),
(938, 'TREICHVILLE', 'PHARMACIE LES ARCHANGES', ' Bd de Marseille derriere le Palais des Sports Rond point CHU  Zone 2', '5.3044946', '-4.0137045', '21 24 11 37 / 21 35 87 66 ', 'A', '2019-07-11 12:00:19', 1),
(939, 'TREICHVILLE', 'PHARMACIE MEGANE', '6 Rue des Brasseurs carrefour Sococe Safca  Zone 3', '5.296868', '-4.000387', '21 25 35 57   ', 'A', '2019-07-11 12:00:19', 1),
(940, 'TREICHVILLE', 'PHARMACIE MEROUANE ', 'Av 16 Rue 25 Face Station Total', '5.334247 ', '-4.045925', '21 24 09 29', 'A', '2019-07-11 12:00:19', 1),
(941, 'TREICHVILLE', 'PHARMACIE MYRIAM DE BIAFRA ', 'Angle Av 2 Rue 29 Bus 74 11 04 Imm Saint Lazare ', '5.3129816', '-4.0084541', ' 21 35 76 84 ', 'A', '2019-07-11 12:00:19', 1),
(942, 'TREICHVILLE', 'PHARMACIE NANAN YAMOUSSO  ', 'Rue 38 Angle Av 13 juste en face Imm Nanan Yamousso ', '5.3079233', '-4.0072632', '21 24 23 15  ', 'A', '2019-07-11 12:00:19', 1),
(943, 'TREICHVILLE', 'PHARMACIE NOTRE DAME ', 'Av 21 Rue 38 en face de la Station Shell', '5.304279', '-4.003990', '21 24 10 60 ', 'A', '2019-07-11 12:00:19', 1),
(944, 'TREICHVILLE', 'PHARMACIE ODE ', 'Av 24 Rue 38 a la Gare de Bassam a cote de l Agence Bicici', '5.3038892', '-4.0061675', '21 25 39 27 ', 'A', '2019-07-11 12:00:19', 1),
(945, 'TREICHVILLE', 'PHARMACIE ROND POINT DU CHU  ', ' Bd de Marseille face Bicici Sud rond point du Chu  Km 4', '5.294973 ', '-4.002717', ' 21 35 73 03 ', 'A', '2019-07-11 12:00:19', 1),
(946, 'TREICHVILLE', 'PHARMACIE ROY  ', 'Place du Marche non loin de la foire de Chine', '5.308687', '-4.012847', '21 24 09 13 ', 'A', '2019-07-11 12:00:19', 1),
(947, 'TREICHVILLE', ' PHARMACIE RUE 12 ', 'Av12 Rue 12 face Bicici Treichville', '5.3082759', '-4.0150362', '21 24 53 91', 'A', '2019-07-11 12:00:19', 1),
(948, 'TREICHVILLE', 'PHARMACIE SAINTE JEANNE  ', 'Entree Marche De Belleville Face Station Petro Ivoire', '5.306059', '-3.998076', '21 24 57 41', 'A', '2019-07-11 12:00:19', 1),
(949, 'TREICHVILLE', 'PHARMACIE SAINTE ADELE  ', 'Facade arriere du Marche Belle Ville face quartier Gbatanikro Face habitat Craonne', '5.304934', '-3.997435', '21 25 47 00 ', 'A', '2019-07-11 12:00:19', 1),
(950, 'TREICHVILLE', ' PHARMACIE DE LA CASERNE', 'Bd VGE face marche de portables quartier Douanes  Cite douane', '5.297401', '-4.013513', '21 25 45 83', 'A', '2019-07-11 12:00:19', 1),
(951, 'TREICHVILLE', 'PHARMACIE IMANN', 'Bd Marseille entre Sari Peugeot et Ets Bernabe', '5.292987 ', '-3.997603', ' 21 25 13 14 /  21 25 13 15', 'A', '2019-07-11 12:00:19', 1),
(952, 'TREICHVILLE', 'PHARMACIE PALAIS DE LA CULTURE', 'Rue 20 Barre Palais de la culture a la Station Total ', '5.3039159', '4.0127336', '21 24 13 19', 'A', '2019-07-11 12:00:19', 1),
(953, 'WILLIAMSVILLE', 'PHARMACIE AIME SS', 'Williamsville 2  route MACACI face CRS', '5.368957', '-4.025953', '20 37 21 24', 'A', '2019-07-11 12:00:19', 1),
(954, 'WILLIAMSVILLE', 'PHARMACIE DE WILLIAMSVILLE ', 'Pres de la Mosquee de Williamsville', '5.366099', '-4.020579', '20 37 03 45 ', 'A', '2019-07-11 12:00:19', 1),
(955, 'WILLIAMSVILLE', ' PHARMACIE DU PROGRES', 'Pres Terminus 14 espace Tere  Williamsville Adjame', '5.372443', '-4.022628', '20 37 15 25 ', 'A', '2019-07-11 12:00:19', 1),
(956, 'WILLIAMSVILLE', 'PHARMACIE L EMMANUEL ', 'carrefour 2plataeux entre la staion shell et le depot sotra', '5.364903 ', '-4.016436', '20 38 40 17  ', 'A', '2019-07-11 12:00:19', 1),
(957, 'WILLIAMSVILLE', 'PHARMACIE RAPHA  ', 'En allant vers Macaci face Boulangerie BMW  Williamsville', '5.372434', '-4.026636', '20 37 96 88 /68 73 06 38', 'A', '2019-07-11 12:00:19', 1),
(958, 'WILLIAMSVILLE', 'PHARMACIE ST URIEL ', ' Route du Zoo carrefour Paillet Adjame ', '5.372180', '-4.012200', '20 37 23 75  ', 'A', '2019-07-11 12:00:19', 1),
(959, 'YOPOUGON', ' PHARMACIE ANANERAIE  ', 'Ancienne route de Dabou carrefour Oasis', '5.3548058', '-4.1021601', '23 46 72 13/23 46 98 27', 'A', '2019-07-11 12:00:19', 1),
(960, 'YOPOUGON', ' PHARMACIE ARTEMIA  ', 'Face Palais de Justice  Sideci Yopougon', '5.3265673', '-4.0793824', '23 51 68 06', 'A', '2019-07-11 12:00:19', 1),
(961, 'YOPOUGON', 'PHARMACIE SANTA CATERINA EX-BELLE FONTAINE', 'Prolongement Patisserie du Marche  Sicogi', '5.3374472', '-4.0778835', '77 67 79 80 /02 41 22 41', 'A', '2019-07-11 12:00:19', 1),
(962, 'YOPOUGON', ' PHARMACIE CARREFOUR KOWEIT   ', 'Route d Abobodoume face groupe Scolaire Petit Soleil Carrefour Koweit  Toits rouges', '5.322142', '-4.0568304', '23 52 38 54 ', 'A', '2019-07-11 12:00:19', 1),
(963, 'YOPOUGON', ' PHARMACIE CHRIST-ROI  ', 'Carrefour Jean Paul 2  Toits rouges', '5.3305805', '-4.049834', '23 45 44 55', 'A', '2019-07-11 12:00:19', 1),
(964, 'YOPOUGON', 'PHARMACIE AKADJOBA ', 'Carrefour Akadjoba Sideci non loin du Palais de Justice', '5.3245181', '-4.0829743', ' 23 51 82 42 ', 'A', '2019-07-11 12:00:19', 1),
(965, 'YOPOUGON', 'PHARMACIE ALICIA', 'Carrefour Bonikro route de Dabou  Ananeraie ', '5.3515992', '-4.1057556', '23 51 77 98 / 07 08 87 15 ', 'A', '2019-07-11 12:00:19', 1),
(966, 'YOPOUGON', 'PHARMACIE ASSALAM', ' carrefour de l Allocodrome  Niangon Sud', '5.3157648', '-4.0939173', '23 45 40 43/23 45 42 76 ', 'A', '2019-07-11 12:00:19', 1),
(967, 'YOPOUGON', 'PHARMACIE ASSONVON', 'Qt Assavon A 200m du cinema Saguidiba  pres Hotel Assonvon derriere la CNPS', '5.3325474', '-4.0701821', '23 51 82 42', 'A', '2019-07-11 12:00:19', 1),
(968, 'YOPOUGON', 'PHARMACIE AWALE', 'Face au Petit Marche  Selmer', '5.3408082', '-4.0699159', '23 46 01 02   ', 'A', '2019-07-11 12:00:19', 1),
(969, 'YOPOUGON', 'PHARMACIE BAÏTY  ', ' Pres de la Gendarmerie Toits rouges', '5.3273354', '-4.0576583', '23 45 65 59 ', 'A', '2019-07-11 12:00:19', 1),
(970, 'YOPOUGON', 'PHARMACIE BEL AIR ', 'Carrefour Bel Air en face de la SGBCI', '5.346727', '-4.0657545', '23 52 74 43', 'A', '2019-07-11 12:00:19', 1),
(971, 'YOPOUGON', 'PHARMACIE BOISSY', 'Face au Commissariat du 19eme Arrondissement Bus 44 37 38  Toits rouges', '5.3220582', '-4.1108159', '23 45 62 34', 'A', '2019-07-11 12:00:19', 1),
(972, 'YOPOUGON', 'PHARMACIE NOYA(EX CAMP MILITAIRE)', 'Camp Militaire  Entre le Groupement des Sapeurs Pompiers et la CNPS', '5.3203522', '-4.0648841', '  23 45 06 94', 'A', '2019-07-11 12:00:19', 1),
(973, 'YOPOUGON', 'PHARMACIE CARREFOUR BOBY', 'Niangon  Face Base Cie  Entre Stations Petro Ivoire Et Shell', '5.3204082', '-4.0681672', '23 52 93 58/23 51 77 53 ', 'A', '2019-07-11 12:00:19', 1),
(974, 'YOPOUGON', 'PHARMACIE CARREFOUR GHANDI', 'Qrtier Ghandi  Koweit Yopougon', '5.3236316', '-4.0609064', '07 00 75 45', 'A', '2019-07-11 12:00:19', 1),
(975, 'YOPOUGON', 'PHARMACIE CELIA ', 'Sur La Route De Dabou  Carrefour Lycee Technique Banco 2', '5.3488917', '-4.0976593', '', 'A', '2019-07-11 12:00:19', 1),
(976, 'YOPOUGON', 'PHARMACIE CHE N TALE ', 'Bordure de Route voie menant au carrefour Sorbonne non loin de\n la clinique ste Jane Leora et du college Guchanrolain  Ananeraie', '5.3510338', '-4.0989854', '23 51 87 58', 'A', '2019-07-11 12:00:19', 1),
(977, 'YOPOUGON', 'PHARMACIE CHIGATA', 'Route d Abobodoume  200m des Sapeurs Pompiers Ligne Bus 42 Toits rouges', '5.3661302', '-4.0057031', '23 46 99 57', 'A', '2019-07-11 12:00:19', 1),
(978, 'YOPOUGON', 'PHARMACIE CITE MAROC ', 'Yopougon Maroc Zone de lantenne Pres du Supermarche Cash 225', '5.3358324', '-4.0890112', '23 45 87 32 / 23 45 32 42', 'A', '2019-07-11 12:00:19', 1),
(979, 'YOPOUGON', 'PHARMACIE D ANDOKOI ', '1er pont a droite entre Centre Ivoire Couture et College Moderne ', '5.3623102', '-4.0692883', '23 52 43 06 ', 'A', '2019-07-11 12:00:19', 1),
(980, 'YOPOUGON', 'PHARMACIE DE GESCO', 'Quartier Gesco  Face au Marche* Gesco', '5.3631197', '-4.099836', '23 52 61 75  / 05 99 28 54', 'A', '2019-07-11 12:00:19', 1),
(981, 'YOPOUGON', 'PHARMACIE DE L ANTENNE', 'Face a la CI Telcom Orange  Sideci ', '5.3341301', '-4.0754866', '23 51 88 89 / 07 45 24 14', 'A', '2019-07-11 12:00:19', 1),
(982, 'YOPOUGON', 'PHARMACIE DE LA CITE ', 'Face Sapeur Pompiers  Toits rouges Yopougon ', '5.3349519', '-4.1025341', '23 50 15 27 ', 'A', '2019-07-11 12:00:19', 1),
(983, 'YOPOUGON', 'PHARMACIE DE LA MAIRIE', 'A 200 m de la mairie face maquis Tantie Margot Ligne Bus 30  Selmer', '5.3350368', '-4.1025342', '23 46 60 53 ', 'A', '2019-07-11 12:00:19', 1),
(984, 'YOPOUGON', 'PHARMACIE DE L  AUTOROUTE', 'Gesco Terminus de Bus 34 pres de la Brigade Gendarmerie Autoroute ', '5.3489184', '-4.1042254', ' 23 45 43 70', 'A', '2019-07-11 12:00:19', 1),
(985, 'YOPOUGON', 'PHARMACIE RABBI EX HOREB', 'Sideci  Lem 1er lavage', '5.3296215', '-4.0840411', '23 46 94 23', 'A', '2019-07-11 12:00:19', 1),
(986, 'YOPOUGON', 'PHARMACIE DES 3 PONTS', 'Pres centre de sante PMI et Maternite  Sogefiha', '5.3423905', '-4.0823758', '23 53 16 32 ', 'A', '2019-07-11 12:00:19', 1),
(987, 'YOPOUGON', 'PHARMACIE DES ESPERANCES', 'Route Academie de la mer carrefour Lokoa Terminus Bus 39 Niangon a* droite', '5.3228602', '-4.0847718', '23 52 16 39 ', 'A', '2019-07-11 12:00:19', 1),
(988, 'YOPOUGON', 'PHARMACIE DIVINE GRACE ', 'Entree PortBouet II Voie de Dabou ', '5.355407', '-4.0889422', '23 52 23 23 ', 'A', '2019-07-11 12:00:19', 1),
(989, 'YOPOUGON', 'PHARMACIE DU CENACLE ', '3e pont apres la cite policiere BAE Zone CHU', '5.3595833', '-4.0876463', '23 45 15 16', 'A', '2019-07-11 12:00:19', 1),
(990, 'YOPOUGON', 'PHARMACIE DU LAVAGE ', 'Pres du Carrefour Lavage  Sicogi ', '5.3406824', '-4.0746074', ' 23 46 59 02', 'A', '2019-07-11 12:00:19', 1),
(991, 'YOPOUGON', 'PHARMACIE DU MARCHE', 'Pres du Marche de la Sicogi Sicogi', '5.3406931', '-4.0746074', '23 50 95 02 ', 'A', '2019-07-11 12:00:19', 1),
(992, 'YOPOUGON', 'PHARMACIE DU NOUVEAU QUARTIER ', 'Axe Complexe sportif  Caserne des sapeurs pompiers', '5.3387672', '-4.0646578', '23 53 12 15 / 07 94 06 79', 'A', '2019-07-11 12:00:19', 1),
(993, 'YOPOUGON', 'PHARMACIE DU PROGRES', 'Non loin de la station Petro ivoire  Maroc ', '5.3407409', '-4.0899283', '23 46 39 39 ', 'A', '2019-07-11 12:00:19', 1),
(994, 'YOPOUGON', 'PHARMACIE EDEN', 'College Segbe Carrefour Awa  Toits rouges', '5.3227704', '-4.0519408', '23 46 81 38', 'A', '2019-07-11 12:00:19', 1),
(995, 'YOPOUGON', 'PHARMACIE ESTHER ', 'Niangon a droite Academie face a la Cite Cafeier', '5.3230292', '-4.0847718', '23 50 50 23', 'A', '2019-07-11 12:00:19', 1),
(996, 'YOPOUGON', 'PHARMACIE GABRIEL GARE', 'Pres de lecole des Sourds  Wassakara', '5.3553212', '-4.0736103', '23 46 30 03 ', 'A', '2019-07-11 12:00:19', 1),
(997, 'YOPOUGON', 'PHARMACIE JEAN PIERRE ', 'Terminus Bus 39 face Carrefour Ancien Jatak Niangon Sud a droite  ', '5.3221314', '-4.1056452', ' 23 51 47 46  ', 'A', '2019-07-11 12:00:19', 1),
(998, 'YOPOUGON', 'PHARMACIE KENEYA ', 'Yopougon Boulevard Principal', '5.3468525', '-4.0754264', '23 52 70 70 / 07 05 57 93 ', 'A', '2019-07-11 12:00:19', 1),
(999, 'YOPOUGON', 'PHARMACIE KOUTE ', 'Terminus 40  Sideci Yopougon', '5.330387', '-4.076248', '23 46 59 00/ 07 01 10 83 ', 'A', '2019-07-11 12:00:19', 1),
(1000, 'YOPOUGON', 'PHARMACIE LA DELIVRANCE', ' Zone Industrielle Yopougon', '5.3687457', '-4.0818658', '23 46 06 33  ', 'A', '2019-07-11 12:00:19', 1),
(1001, 'YOPOUGON', 'PHARMACIE LA MAISON BLANCHE ', 'Maison blanche  Banco 2', '5.3475575', '-4.0849801', '23 45 06 66  ', 'A', '2019-07-11 12:00:19', 1),
(1002, 'YOPOUGON', 'PHARMACIE LAGOMA ', 'Carrefour Academie face Cite CNPS', '5.323695', '-4.1109527', '23 46 02 15', 'A', '2019-07-11 12:00:19', 1),
(1003, 'YOPOUGON', 'PHARMACIE LAOULO', 'Nouveau quartier Nouveau bureau SODECI face clinique medicale LES OLIVIERS', '5.3340243', '-4.0646132', '23 45 16 15', 'A', '2019-07-11 12:00:19', 1),
(1004, 'YOPOUGON', 'PHARMACIE LE JOURDAIN', 'Apres la Paroisse Saint Mathieu cite Verte  Niangon Nord', '5.3551238', '-4.1086968', '23 52 27 47', 'A', '2019-07-11 12:00:19', 1),
(1005, 'YOPOUGON', 'PHARMACIE LEA   ', 'Non loin de la Mairie Annexe de Niangon  Niangon Nord', '5.3552087', '-4.1086969', '23 45 09 62 / 23 50 23 09 ', 'A', '2019-07-11 12:00:19', 1),
(1006, 'YOPOUGON', 'PHARMACIE LEM', 'A 300m du Cinema Saguidiba en face de la Mairie technique  Sideci', '5.3315757', '-4.0793207', '23 52 27 47', 'A', '2019-07-11 12:00:19', 1),
(1007, 'YOPOUGON', 'PHARMACIE LES BEATITUDES', 'Nouveau quartier 1er arret du bus 37 Face Station Sape', '5.3405408', '-4.0609764', '23 53 23 17 / 03 30 50 89 ', 'A', '2019-07-11 12:00:19', 1),
(1008, 'YOPOUGON', 'PHARMACIE LES ECLUSES', 'Entre depot Sotra et college Minerva  Andokoi', '5.3658704', '-4.0760461', '23 46 84 85 ', 'A', '2019-07-11 12:00:19', 1),
(1009, 'YOPOUGON', 'PHARMACIE LES ELYSEES ', 'Non loin de la Coopec  Ananeraie', '5.3437657', '-4.0986201', '23 45 09 62', 'A', '2019-07-11 12:00:19', 1),
(1010, 'YOPOUGON', 'PHARMACIE LES OLIVIERS', '1er pont face a la station Shell de l institut des aveugles', '5.3531641', '-4.0662004', '23 45 06 66', 'A', '2019-07-11 12:00:19', 1),
(1011, 'YOPOUGON', 'PHARMACIE LOTUS', ' Niangon Sud a gauche Terminus Bus 27', '5.3170879', '-4.0910415', '23 48 09 69 ', 'A', '2019-07-11 12:00:19', 1),
(1012, 'YOPOUGON', 'PHARMACIE MAMIE ADJOUA', 'Non loin de la cite Mamie Adjoua Gesco Residentiel', '5.3585502', '-4.0982897', '23 46 27 15', 'A', '2019-07-11 12:00:19', 1),
(1013, 'YOPOUGON', 'PHARMACIE MATY ', 'Entre la station Lubafrique et le College Anador  Maroc', '5.3326847', '-4.1056199', '23 46 28 14', 'A', '2019-07-11 12:00:19', 1),
(1014, 'YOPOUGON', 'PHARMACIE MICHEL ARCHANGE', 'Cite Verte 100 M DU Terminus 44', '5.3308559', '-4.1153638', '23 45 24 80', 'A', '2019-07-11 12:00:19', 1),
(1015, 'YOPOUGON', 'PHARMACIE NANKOKO', 'Entre Sapeurs Pompiers et CNPS  Camp militaire', '5.328118', '-4.0656623', '23 50 02 06', 'A', '2019-07-11 12:00:19', 1),
(1016, 'YOPOUGON', 'PHARMACIE NIANGON NORD', ' Entre SICOGI Academie de la Mer et la Cite Verte Bus 44  Niangon Nord', '5.3292993', '-4.1046703', '23 45 15 44', 'A', '2019-07-11 12:00:19', 1),
(1017, 'YOPOUGON', 'PHARMACIE NIKIBEL    EX NISSI   ', 'Ancien Bel Air avant carrefour college St Louis\n zone du Marche GFCI face Maquis le Pitch SOPIM', '5.3294318', '-4.1375013', '23 00 14 67', 'A', '2019-07-11 12:00:19', 1),
(1018, 'YOPOUGON', 'PHARMACIE PALOMA', 'Gare Sable carrefour Auto ecole  Wassakara ', '5.3558437', '-4.0708351', '23 45 07 07 ', 'A', '2019-07-11 12:00:19', 1),
(1019, 'YOPOUGON', 'PHARMACIE PHALENES', 'A gauche de Saint Pierre Niangon Sud Yopougon', '5.3233492', '-4.0900053', '23 45 70 75', 'A', '2019-07-11 12:00:19', 1),
(1020, 'YOPOUGON', 'PHARMACIE PHENIX', 'Au feu de Banco 2  nouvelle route Port Bouet 2 gare BDF', '5.3478252', '-4.081227', '23 46 57 77', 'A', '2019-07-11 12:00:19', 1),
(1021, 'YOPOUGON', 'PHARMACIE PRINCIPALE', 'Yopougon Attie sur Le Bd Principal  Face A Ficgayo arret Bus Station Esso', '5.323439', '-4.1228363', '23 46 62 36', 'A', '2019-07-11 12:00:19', 1),
(1022, 'YOPOUGON', 'PHARMACIE ROXANNE', 'Cite CIE  Base CIE Yopougon', '5.3394182', '-4.0915248', '23 50 72 77', 'A', '2019-07-11 12:00:19', 1),
(1023, 'YOPOUGON', 'PHARMACIE SAINT ANDRE', 'Apres la Cathedrale St Andre  Sicogi', '5.3423824', '-4.0770145', '23 50 48 79', 'A', '2019-07-11 12:00:19', 1),
(1024, 'YOPOUGON', 'PHARMACIE SAINT CLEMENT', 'Face au CHU  Mamie Adjoua Yopougon', '5.3424303', '-4.0923354', '23 51 96 46 ', 'A', '2019-07-11 12:00:19', 1),
(1025, 'YOPOUGON', 'PHARMACIE SAINT HERMANN', 'Entree face station Petro Ivoire  Base CIE ', '5.3395141', '-4.0819224', '23 45 24 24 ', 'A', '2019-07-11 12:00:19', 1),
(1026, 'YOPOUGON', 'PHARMACIE SAINT LAURENT', 'Camp Militaire  Gare des Gbakas  200m Terminus Sotra  Bus 37', '5.3198092', '-4.0637504', '23 45 28 77', 'A', '2019-07-11 12:00:19', 1),
(1027, 'YOPOUGON', 'PHARMACIE SAINT MARTIN ', 'Face College IGES Terminus 42  Sideci', '5.3216879', '-4.0823921', '23 46 47 64', 'A', '2019-07-11 12:00:19', 1),
(1028, 'YOPOUGON', 'PHARMACIE SAINT PIERRE', ' Pres de l Ancienne Gare  2eme arret du bus 39 200m Entree Ancienne Mosquee', '5.3401792', '-4.1064084', '23 52 11 31 ', 'A', '2019-07-11 12:00:19', 1),
(1029, 'YOPOUGON', 'PHARMACIE SAINTE RITA', 'Niangon Nord non loin de l eglise Ste Rita de Cascia', '5.3274379', '-4.098689', '23 53 08 73', 'A', '2019-07-11 12:00:19', 1),
(1030, 'YOPOUGON', 'PHARMACIE SANDRINA', 'Carrefour Ecole Libanaise  Carrefour de la Sorbonne  Ananeraie', '5.3491882', '-4.1015365', '23 45 29 03', 'A', '2019-07-11 12:00:19', 1),
(1031, 'YOPOUGON', 'PHARMACIE SCHEKINA', 'Siporex Rue Du Marche Gouro Viande De Brousse ', '5.3526454', '-4.0785674', '23 50 88 56', 'A', '2019-07-11 12:00:19', 1),
(1032, 'YOPOUGON', 'PHARMACIE SEGAI', 'Au marche  Wassakara ', '5.3493278', '-4.070054', '23 46 09 00', 'A', '2019-07-11 12:00:19', 1),
(1033, 'YOPOUGON', 'PHARMACIE SHALOM', 'Sogefiha Pont Vagabond non loin du Terminus du Bus 47', '5.3395034', '-4.0819224', '23 50 04 47', 'A', '2019-07-11 12:00:19', 1),
(1034, 'YOPOUGON', 'PHARMACIE GLORIA', 'A proximite de la station Total Ananeraie a 500 m du carrefour Oasis Anananeraie', '5.3549703', '-4.0968589', '23 45 16 93', 'A', '2019-07-11 12:00:19', 1),
(1035, 'YOPOUGON', 'PHARMACIE SIPOREX', 'Pres de la Gare Yopougon Ancienne Route', '5.3555432', '-4.0765989', '23 51 80 41', 'A', '2019-07-11 12:00:19', 1),
(1036, 'YOPOUGON', 'PHARMACIE TEREZA', 'Route Niangon * Magasin', '5.3366475', '-4.0860766', '23 45 31 89', 'A', '2019-07-11 12:00:19', 1),
(1037, 'YOPOUGON', 'PHARMACIE THEODORA', 'Carrefour a droite Intersection Ligne bus 27 et 39  Niangon Sud', '5.3186391', '-4.0970233', '25 45 08 14', 'A', '2019-07-11 12:00:19', 1),
(1038, 'YOPOUGON', 'PHARMACIE TIZRA', 'Toit Rouge vers Offoumou Yapo  Immeuble Ancien Bazar', '5.3298669', '-4.0523502', '23 50 88 91', 'A', '2019-07-11 12:00:19', 1),
(1039, 'YOPOUGON', 'PHARMACIE TOITS ROUGE', 'Apres la BAE  Toits rouges', '5.329899', '-4.0523502', '23 45 20 39', 'A', '2019-07-11 12:00:19', 1),
(1040, 'YOPOUGON', 'PHARMACIE VALORS', 'Yopougon Face Nouveau Marche de Port Bouet 2', '5.3471102', '-4.0961201', '23 52 03 40', 'A', '2019-07-11 12:00:19', 1),
(1041, 'YOPOUGON', 'PHARMACIE WAKOUBOUE', 'Angle Rue du Marche Sicogi face au lavage', '5.3396808', '-4.0758076', '23 52 62 08', 'A', '2019-07-11 12:00:19', 1),
(1042, 'YOPOUGON', 'PHARMACIE WASSAKARA', ' Rue Princesse pres du Carrefour cite SIB  Wassakara ', '5.3465312', '-4.0702096', '22 45 22 00', 'A', '2019-07-11 12:00:19', 1),
(1043, 'YOPOUGON', 'PHARMACIE WILLIAM PONTY', 'Face College William Ponty', '5.318687', '-4.1123442', '23 46 99 57', 'A', '2019-07-11 12:00:19', 1),
(1044, 'YOPOUGON', 'PHARMACIE ZEULAYET', 'Quartier Maroc face a la Mosquee ', '5.3401099', '-4.0998423', '23 46 32 99', 'A', '2019-07-11 12:00:19', 1),
(1045, 'YOPOUGON', 'PHARMACIE DE PORT BOUET II ', 'Route Niangon Nord Face Mosquee AL BILAL', '5.3297938', '-4.1518932', '23 46 60 53 / 01 51 67 56', 'A', '2019-07-11 12:00:19', 1),
(1046, 'YOPOUGON', 'PHARMACIE DU JUBILE ', 'Yopougon Route De Dabou Face Marche Bagnon', '5.3389579', '-4.113014', '01 05 26 10 /06 37 09 42', 'A', '2019-07-11 12:00:19', 1),
(1047, 'YOPOUGON', 'PHARMACIE DU PROGRÈS ', 'Non loin de la station Petro ivoire  Maroc ', '5.3407409', '-4.0899283', '20 37 15 25', 'A', '2019-07-11 12:00:19', 1),
(1048, 'YOPOUGON', ' PHARMACIE GNIMAH', 'Yopougon Port Bouet 2  Pres Ancienne Gare 2eme Arret Du Bus 39 ', '5.351897', '-4.089672', '23 46 54 89 / 01 88 48 86', 'A', '2019-07-11 12:00:19', 1),
(1049, 'YOPOUGON', 'PHARMACIE DE LA VIE', 'Ananeraie  Zone rond point du CHU Yopougon', '5.3550759', '-4.0933759', '23 46 66 33 / 58 17 29 19', 'A', '2019-07-11 12:00:19', 1),
(1050, ' YOPOUGON', 'PHARMACIE MOAYE', 'Route de Dabou pres du Cimetiere Municipal Ananeraie', '5.342936', '-4.1106171', '07 91 65 53 /  02 50 14 60', 'A', '2019-07-11 12:00:19', 1),
(1051, 'YOPOUGON', 'PHARMACIE LOKOA', 'Apres le Lycee Ehivet Gbagbo  Niangon Lokoa ', '5.3168643', '-4.1019245', '23 45 07 80 / 01 25 91 24', 'A', '2019-07-11 12:00:19', 1),
(1052, 'YOPOUGON', 'PHARMACIE NOTRE DAME DE LA PAIX', ' km 17 Route Dabou au Marche  Marche Bagnon ', '5.3377861', '-4.1279579', '23 45 61 97', 'A', '2019-07-11 12:00:19', 1),
(1053, 'YOPOUGON', 'PHARMACIE SACRE COEUR', ' Nouveau quartier Ancien Garage Zone de la Poste  Pres du Top Pain', '5.336318', '-4.057218', '05 60 59 02 - 02 68 91 98', 'A', '2019-07-11 12:00:19', 1),
(1054, 'YOPOUGON', 'PHARMACIE SANTE PLUS', 'Autoroute du Nord pres de la station Shell apres la fourriere  Gesco', '5.366827', '-4.1021839', '23 52 58 66', 'A', '2019-07-11 12:00:19', 1),
(1055, 'YOPOUGON', 'PHARMACIE SELMER', 'Carrefour Baron a 100m de la Mairie Axe William Ponty Mairie', '5.3436377', '-4.0707833', '23 46 11 63 /  01 33 07 33', 'A', '2019-07-11 12:00:19', 1),
(1056, 'YOPOUGON', 'PHARMACIE SAINT RAPHAEL ', 'Cite Novalim  cite Eeci  non loin de la Clinique Rita de Cascia', '5.3384896', '-4.0847477', '01 82 97 97 /  08 97 11 71', 'A', '2019-07-11 12:00:19', 1),
(1057, 'YOPOUGON', 'PHARMACIE SAINTE AUDE', 'Entree Carrefour Siporex et rue Nouveau Goudron  Banco 2', '5.3554802', '-4.0796611', '23 52 28 87 /  07 03 8018', 'A', '2019-07-11 12:00:19', 1),
(1058, 'YOPOUGON', 'PHARMACIE SAINTE MARIE', 'Andokoi  En Face du Pont Pieton  A Cote de la Station Total', '5.35557', '-4.1124921', '23 45 24 24', 'A', '2019-07-11 12:00:19', 1),
(1059, 'YOPOUGON', 'PHARMACIE AFIA', 'Non loin du lycee jeunes filles  Millionnaire', '5.3458332', '-4.0553629', '07 46 12 50', 'A', '2019-07-11 12:00:19', 1),
(1060, 'YOPOUGON', 'PHARMACIE BERACA', 'Camp Militaire Petit Toit Rouge Carrefour Chapouli', '5.318453', '-4.0669699', '07 77 01 20 /  07 69 66 76', 'A', '2019-07-11 12:00:19', 1),
(1061, 'YOPOUGON', 'PHARMACIE BETHESDA', 'Entre la Coopec et le College Guchanrolain  Ananeraie', '5.348881', '-4.0976593', '', 'A', '2019-07-11 12:00:19', 1),
(1062, 'YOPOUGON', 'PHARMACIE NANDY', 'Carrefour Koute par Eglise St Laurent entre le Terminus 40 et  la CNPS', '5.3291341', '-4.0719104', '23 45 51 12 / 01 20 56 62', 'A', '2019-07-11 12:00:19', 1),
(1063, 'YOPOUGON', 'PHARMACIE  PENIEL', ' Ananeraie Rond Point Gesco', '5.3573581', '-4.1011619', '23 45 42 82 /  09 75 82 07', 'A', '2019-07-11 12:00:19', 1),
(1064, 'COCODY', 'PHARMACIE CITE SIR', 'ABATTA vers la cite SIR', '5.387734', '-3.926920', ' 58 36 18 47', 'A', '2019-07-11 12:00:19', 1),
(1065, 'COCODY', 'PHARMACIE ELIEZER', 'RIVIERA FAYA carrefour Akouedo face operation immobiliere ATCI', '5.363589', '-3.936494', ' 22 45 60 19', 'A', '2019-07-11 12:00:19', 1),
(1066, 'COCODY', 'PHARMACIE MAGNIFICAT DES II PLATEAUX', 'carrefour OPERA ADJIEN tourner du cote de la station  PETROCI  100 M DU CASH IVOIRE', '5.385494', '-3.991915', '22 52 53 00', 'A', '2019-07-11 12:00:19', 1),
(1067, 'COCODY ', 'PHARMACIE ST VIATEUR', 'Riviera Palmeraie  200 METRES DU ROND POINT ST VIATEUR route  HOPITAL GENERAL D ANGRE', '5.385983', '-3.955555 ', ' 88 88 04 80', 'A', '2019-07-11 12:00:19', 1),
(1068, 'COCODY ', 'PHARMACIE THERA', 'II PLATEAUX 7eme tranche dans le centre commercial TERA', '5.377988', ' -3.988132', '22 42 49 91', 'A', '2019-07-11 12:00:19', 1),
(1069, 'ADJAME ', 'PHARMACIE CHRIST MARIE DE PAILLET', 'PAILLET  extention entre le carrefour  HUMMER et la maison blanche des petits', '5.377926', ' -4.014081', '20 37 40 00', 'A', '2019-07-11 12:00:19', 1),
(1070, 'ADJAME', 'PHARMACIE DU PROGRES', 'face eglise  ST KIZITO', '5.370142 ', '-4.021507', '20371525', 'A', '2019-07-11 12:00:19', 1),
(1071, 'MARCORY', 'PHARMACIE EL OLAM', 'carrefour AMAGOU pres du marche', '5.308674', '-3.971056', ' 21 26 68 50', 'A', '2019-07-11 12:00:19', 1),
(1072, 'YOPOUGON', 'PHARMACIE AZITO', 'Terminus 27 en allant a Azito  Niangon Sud Yopougon', '5.309292', ' -4.087354', '', 'A', '2019-07-11 12:00:19', 1),
(1073, 'YOPOUGON', 'PHARMACIE CITE CIE', 'YOPOUGON voie principale de la cite CIE menant a  PORT BOUET II entre AGENCE CIE de paiement', '5.342270', '-4.089240', '23 46 34 26', 'A', '2019-07-11 12:00:19', 1),
(1074, 'YOPOUGON', 'PHARMACIE PEFINA', 'YOPOUGON carrefour KOUTE par l eglise ST LAURENT entre terminus 40 et la CNPS', '', '', ' 23 45 09 41', 'A', '2019-07-11 12:00:19', 1),
(1075, 'ABOBO', 'PHARMACIE DE NDOTRE', 'Axe  NDOTRE ANYAMA  environ 600 M carrefour NDOTRE', '5.448082 ', '-4.071029', '', 'A', '2019-07-11 12:00:19', 1),
(1076, 'ABOBO', 'PHARMACIE DES GENERATIONS DE MONTEZO', 'MONTEZO quartier residentiel SP ALEPE', '5.486281', ' -3.731712', '', 'A', '2019-07-11 12:00:19', 1),
(1077, 'ABOBO ', 'PHARMACIE DU DOKUI GDE', 'Au Dessus du Zoo  Station Total face Cie  Plateau Dokui', '5.399512', '-4.006495', ' 47 90 40 63', 'A', '2019-07-11 12:00:19', 1),
(1078, 'ABOBO', 'PHARMACIE LA VIERGE DU SIGNE', 'Abobo N guessankoi derriere la station ORYX', '5.433208 ', '-4.038903', '09 92 42 79/01 12 22 70', 'A', '2019-07-11 12:00:19', 1),
(1079, 'ABOBO', 'PHARMACIE DU PETIT MARCHE DU DOKUI', 'Abobo  Entre le Petit Marche  de Dokui et la Mosquee  sur la Voie menant au Mahou', '5.395853', ' -4.000011', ' 22 42 28 56', 'A', '2019-07-11 12:00:19', 1),
(1080, 'ABOBO', 'PHARMACIE RAHMAN', 'Abobo Agbekoi  Pres de l Ecole Nord et l Ageep', '5.426089', '-4.012124 ', '24 00 29 52', 'A', '2019-07-11 12:00:19', 1),
(1081, 'ABOBO', 'PHARMACIE SERVIR', '2eme rond point autoroute ABOBO gare pres de l eglise ST JOSEPH', '', '', ' 24 01 18 20', 'A', '2019-07-11 12:00:19', 1),
(1082, 'ABOBO', 'PHARMACIE YANDI', 'ABOBO face cite policiere en venant du terminus des bus 15 et 49', '5.435179', ' -4.016163', '09 84 14 02', 'A', '2019-07-11 12:00:19', 1),
(1083, 'ATTECOUBE', 'PHARMACIE  AGOUA SARL', 'ATTECOUBE face boulangerie BBCO', '5.352037', ' -4.033731', '09 31 27 51', 'A', '2019-07-11 12:00:19', 1),
(1084, 'PORT BOUET', 'PHARMACIE  CHATELET', 'PORT BOUET GONZAGUE terre rouge immeuble COLONEL', '5.272910', '-3.906209', '86 75 76 57', 'A', '2019-07-11 12:00:19', 1),
(1085, 'PORT BOUET', 'PHARMACIE DJESSOU DE VRIDI', 'VRIDI 3 quartier ZIMBABWE pres de l EPP VRIDI 3', '5.262566 ', '-3.984027', '21 27 23 15', 'A', '2019-07-11 12:00:19', 1),
(1086, 'PORT BOUET', 'PHARMACIE DU CARREFOUR ANCIENNE GENDARMERIE', 'Batiment ancienne gendarmerie entre usine UNICAFE et la morgue D ANYAMA', '5.462863', ' -4.052709', ' 22 55 64 26', 'A', '2019-07-11 12:00:19', 1),
(1087, 'ADJAME', 'PHARMACIE STE MARIE THERESE D ADJAME SARL', 'ADJAME prolongement hotel BANFORA derriere FRATERNITE non loin de la maternite MARIE THERESE HOUPHOUET BOIGNY', '5.347286 ', '-4.020000', ' 56 46 19 45', 'A', '2019-07-11 12:00:19', 1),
(1088, 'COCODY', 'PHARMACIE BELLEVUE', 'Angre  8eme Tranche Soleil II  Apres le terminus 205', '5.400504 ', '-3.971170', '22 50 06 24', 'A', '2019-07-11 12:00:19', 1),
(1089, 'COCODY', 'PHAMACIE CITES SYNACASSI SARL', 'RIVIERA 3 SYNACASSCI route de MBADON', '5.346815', '-3.945346', '22 00 13 15', 'A', '2019-07-11 12:00:19', 1),
(1090, 'COCODY', 'PHARMACIE DU FUTUR', 'Bd Mitterand Immeuble Mariam a cote Socoprix Riviera Palmeraie Triangle zone des impots', ' 5.363420', '-3.957094', '77 33 79 02', 'A', '2019-07-11 12:00:19', 1),
(1091, 'COCODY', 'PHARMACIE EPHRATA', 'Prolongement mission EPHRATA RIVIERA CARREFOUR ABATA a gauche sur la route de la cite  SIR', ' 5.383055  ', '-3.931991', '75 37 94 07', 'A', '2019-07-11 12:00:19', 1),
(1092, 'COCODY', 'PHARMACIE ST GABRIEL', 'Boulevard Latrille  a 900 M Du Sporting Club En Allant Vers Angre  Face Perles Grises +225 22 42 49 95', '5.388059 ', '-3.992438', '22 42 58 35', 'A', '2019-07-11 12:00:19', 1),
(1093, 'COCODY', 'PHARMACIE ST MOISE', 'Cocody 9eme Tranche Abri 2000 sur la Voie Principale entre le rond point Ado et celui avant la Pharmacie Hermes', '5.378403 ', '-3.969366', ' 22 49 61 36', 'A', '2019-07-11 12:00:19', 1),
(1094, 'COCODY', 'PHARMACIE STE CLEMENTINE', 'A 500 metres du nouveau CHU d ANGRE', '5.405370 ', '-3.955792', '52 79 93 90', 'A', '2019-07-11 12:00:19', 1),
(1095, 'ABOBO', 'PHARMACIE ACTUELLE', 'Route D ABOBO BAOULE au niveau du carrefour CARREFOUR ANGRE  200 M COLLEGE VICTOR LOBAD sur nvelle route reliant ANGRE A ABOBO', '5.413713 ', '-3.994756', '22 00 41 85', 'A', '2019-07-11 12:00:19', 1),
(1096, 'ABOBO', 'PHARMACIE AL FATIH', 'ABOBO SAGBE PALMERAIE  route de  BOCABO non loin du COLLEGE BOCABO', '5.415052', '-4.033514', '02 27 07 13', 'A', '2019-07-11 12:00:19', 1),
(1097, 'ABOBO', 'PHARMACIE CARREFOUR NDOTRE', 'ABOBO NDOTRE  GD CARREFOUR NDOTRE  A 20 M DE LA VOIE NON BITUMEE', '5.443620 ', '-4.070814', '40 30 24 31', 'A', '2019-07-11 12:00:19', 1),
(1098, 'ABOBO', 'PHARMACIE DE L EMMANUEL', 'Carrefour des deux plateaux station Shell', '5.364871 ', '-4.016447', '20 38 40 17', 'A', '2019-07-11 12:00:19', 1),
(1099, 'ABOBO', 'PHARMACIE DU CHATEAU ', 'Autoroute d Abobo  Anyama', '5.489741  ', '-4.051743', '23 55 93 99', 'A', '2019-07-11 12:00:19', 1),
(1100, 'ABOBO', 'PHARMACIE FADYL', 'Non loin de la  mosquee ADJA NABINTOU CISSE', '5.424012 ', '-3.997631', '57 19 44 55', 'A', '2019-07-11 12:00:19', 1),
(1101, 'ABOBO', 'PHARMACIE NOURAM', 'dokui route du zoo 100 m du carrefour mauritanien dans le sens d abobote face ecole primaire nouvelles alliances', '5.408649 ', '-4.004144', '57 96 41 62', 'A', '2019-07-11 12:00:19', 1),
(1102, 'YOPOUGON', 'PHARMACIE INOX', 'YOPOUGONroute de  DABOU carrefour CITE VERTE non loin du marche DJEDJE BAGNON', '5.338801 ', '-4.113802', ' 23 45 55 94', 'A', '2019-07-11 12:00:19', 1),
(1103, 'YOPOUGON', 'PHARMACIE ZONE INDUSTRIELLE', 'YOPOUGON ZONE INDUSTRIELLE entree principale contigue a l AGENCE BICICI', '5.370418', '-4.082128', ' 23 46 51 08', 'A', '2019-07-11 12:00:19', 1),
(1104, 'PORTBOUET', 'PHARMACIE BELLEVILLE', 'Quartier BELLE VILLE JEAN FOLLY 100 M carrefour petit marche  carrefour LA LOBO', '5.302943', '-3.998598', ' 24 38 15 83', 'A', '2019-07-11 12:00:19', 1),
(1105, 'ATTECOUBE', 'PHARMACIE DE BROFODOUME', 'Vers le marche de BROFODOUME', '5.514234 ', '-3.931894', '42623272', 'A', '2019-07-11 12:00:19', 1),
(1106, 'PORTBOUET', 'PHARMACIE ST MICHEL', 'PORTBOUET  IRHO  rue carrefour MOTARD pres de  L EPP HOUPHOUET BOIGNY D IRHO', '5.245742', '-3.882387', '59 52 98 74', 'A', '2019-07-11 12:00:19', 1),
(1107, 'ANYAMA', 'PHARMACIE DE LA MISERICORDE', 'QUARTIER RAN  ZONE morgue et hotelL KEDJENOU  apres STATION OIL LYBIA', '5.474486 ', '-4.053640', '23 55 64 34', 'A', '2019-07-11 12:00:19', 1),
(1108, 'BINGERVILLE', 'PHARMACIE NISSI NOUVELLE', 'Cite FEH KESSE', '5.377988', ' -3.912448', '22 40 19 04', 'A', '2019-07-11 12:00:19', 1),
(1109, 'ABOBO', 'PHARMACIE ABOBO CLOUETCHA', 'ABOBO KENNEDY quartier CLOUETCHA carrefour ancienne boulangerie non loin du marche', '5.423774', '-4.002931', '09 50 13 82', 'A', '2019-07-11 12:00:19', 1),
(1110, 'ADJAME', 'PHARMACIE RAPHA', 'En allant vers Macaci  face Boulangerie BMW  Williamsville', '5.372423', '-4.026636', '20 37 96 88', 'A', '2019-07-11 12:00:19', 1),
(1111, 'ADJAME', 'PHARMACIE ST EMMAUEL', 'Bd Ge ne ral de Gaulle  Mirador  face Sicogi 30 M DU CARREFOUR', '5.365768 ', '-4.016476', ' 20 37 98 27', 'A', '2019-07-11 12:00:19', 1),
(1112, 'ADJAME', 'PHARMACIE ST URIEL', 'paillet national route du zoo abidjan', '5.372223 ', '-4.012157', '20372375', 'A', '2019-07-11 12:00:19', 1),
(1113, 'COCODY', 'PHARMACIE ANALYA', '400 M DU CENTRE HOSPITALIER UNIVERSITAIRE REGIONALE D ANGRE au feu face CITE GESTOCI', '5.403073 ', '-3.961344', '08 95 19 25', 'A', '2019-07-11 12:00:19', 1),
(1114, 'COCODY', 'PHARMACIE EMMANUEL', 'ANGRE DJIBI rue COCOVICO  STAR 10', '5.401021 ', '-3.981140', ' 22 42 56 05', 'A', '2019-07-11 12:00:19', 1),
(1115, 'COCODY', 'PHARMACIE LES JARDINS D EDEN', 'RIVIERA 4  route  M BADON cite EDEN face ecole LIBANAISE', '5.328582', ' -3.943270', '79 19 20 04', 'A', '2019-07-11 12:00:19', 1),
(1116, 'COCODY', 'PHARMACIE ST MARTIN DE FAYA', 'RIVIERA FAYA prolongement cloture CLOTURE NOUVEAU CAMP MILITAIRE D AKOUEDO APRES CITE GENIE 2000', '5.387986', '-3.941960', '22455496', 'A', '2019-07-11 12:00:19', 1),
(1117, 'YOPOUGON', 'PHARMACIE DE LA FELICITE', 'Yopougon Maroc  Carrefour Obama entre Station Shell et Hotel Kimi', '5.339750', '-4.093714', '23 46 16 12', 'A', '2019-07-11 12:00:19', 1),
(1118, 'YOPOUGON', 'PHARMACIE JERICO', 'NIANGON NORD ACADEMIE FIN GOUDRON ROUTE DE PAYS BAS', '5.325397 ', '-4.111481', '23 45 62 38', 'A', '2019-07-11 12:00:19', 1),
(1119, 'ABOBO', 'PHARMACIE ROUTE DU ZOO', 'Abobo Axe Dokui zoo a 200m des stations Petroci et vidange entree du quartier Dokui Olympe', '5.387460', '-4.007083', '01 14 04 34', 'A', '2019-07-11 12:00:19', 1),
(1120, 'ABOBO', 'PHARMACIE ST ANTOINE', '?Abobo Sagbe Celeste derriere Rail Taxi gare', '5.417759', '-4.029922', '20 24 77 47', 'A', '2019-07-11 12:00:19', 1),
(1121, 'ABOBO', 'PHARMACIE BIABOU MOSQUEE', '?Abobo Biabou Moronou route dAlepE a cote de la mosquee Cosim', '5.441558', '-3.983389', '71 51 40 85', 'A', '2019-07-11 12:00:19', 1),
(1122, 'Anyama', 'PHARMACIE DU CARREFOUR ANCIENNE GENDARMERIE ', 'BATIMENT ANCIENNE GENDARMERIE ENTRE USINE UNICAFE ET LA MORGUE DANYAMA', '5.461993', '-4.052868', ' 22 55 64 26', 'A', '2019-07-11 12:00:19', 1),
(1123, 'COCODY', 'PHARMACIE BOAZ', 'Cocody Mpouto Village', '5.328599', '-3.954020', '22 47 73 83', 'A', '2019-07-11 12:00:19', 1),
(1124, 'COCODY', 'PHARMACIE CYD NIKO', 'Bd. Mitterand, route de Bingerville, Riviera Faya - Face Petit Portail de la Cit? G?nie 2000', '5.371333', '-3.934512', '22 47 69 27', 'A', '2019-07-11 12:00:19', 1),
(1125, 'COCODY', 'PHARMACIE DU CHÂTEAU D EAU', 'Angr? djibi chateau d\'eau', '5.413656', '-3.979945', '22 51 07 49', 'A', '2019-07-11 12:00:19', 1),
(1126, 'COCODY', 'PHARMACIE ST JEAN PAUL II', 'RIVIERA BONOUMIN 500 M AU NORD D?ABIDJAN MALL APRES CARREFOUR DJEDJE MADY UNIVERSITAIRE REGIONALE D?ANGRE / AU FEU FACE CITE GESTOCI', '5.366386', '-3.969271', '22 49 75 63', 'A', '2019-07-11 12:00:19', 1),
(1127, 'COCODY', 'PHARMACIE DES NATIONS', 'ANGRE 7EME TRANCHE / ZONE COMMISSSARIAT DU 22EME ARRONDISSEMENT ENTRE LA SODECI ET LA PAROISSE ST AMBROISE MA VIGNE', '5.397065', '-3.989579', '22 42 23 00', 'A', '2019-07-11 12:00:19', 1),
(1128, 'ADJAME', 'PHARMACIE ST MICHEL', 'Avenue 13? 50m de L?Eglise St Michel en face du CEG - Harris', '5.347031', '-4.022754', ' 20 37 09 06', 'A', '2019-07-11 12:00:19', 1),
(1129, 'YOPOUGON', 'PHARMACIE ANADOR', 'YOPOUGON MAROC AU NIVEAU DU CARREFOUR ANADOR / FACE MAQUIS ? O BAOULE ?', '5.336709', '-4.101043', '23 46 27 80', 'A', '2019-07-11 12:00:19', 1),
(1130, 'YOPOUGON', 'PHARMACIE DE SION', 'YOPOUGON QUARTIER SIDECI TERMINUS 42 A 100 M D?IGES', '5.319287', '-4.079770', '59 97 73 72', 'A', '2019-07-11 12:00:19', 1),
(1131, 'YOPOUGON', 'PHARMACIE SAINT JOSEPH ACADEMIE', 'YOPOUGON ACADEMIE DES MERS A 50 M AVANT L?ECOLE ACADEMIE DES MERS', '5.319126', '-4.112950', '23 45 56 97', 'A', '2019-07-11 12:00:19', 1),
(1132, 'YOPOUGON', 'PHARMACIE ST ANGE EMMANUEL', 'YOPOUGON ATTIE TERMINUS BUS 47', '5.340446', '-4.083750', '23 46 32 74', 'A', '2019-07-11 12:00:19', 1),
(1133, 'YOPOUGON', 'PHARMACIE DU 16EME', 'Non loin de l\'ecole EPP SICOGI 7', '5.337449', '-4.071233', '23 50 95 02', 'A', '2019-07-11 12:00:19', 1),
(1134, 'YOPOUGON', 'PHARMACIE ZONE MICAO', 'Zone Industrielle - Quartier Micao - Cit? HKB', '5.376523', '-4.084840', ' 23 46 55 98', 'A', '2019-07-11 12:00:19', 1),
(1135, 'PORTBOUET', 'PHARMACIE ST LOUIS DE GONZAGUE', 'FACE LYCEE MUNICIPAL DE GONZAG', '5.250213', '-3.900139', '58 09 46 25', 'A', '2019-07-11 12:00:19', 1),
(1136, 'KOUMASSI', 'PHARMACIE ST JOSEPH ARTISAN', 'Carrefour Caneton derri?re le lyc?e municipal', '5.309869', '-3.944214', '21 36 79 91', 'A', '2019-07-11 12:00:19', 1),
(1137, 'ABOBO', 'PHARMACIE B S A', 'ABOBO ANOKOUA KOUTE SUR L AXE ABOBO ANYAMA PRES DE LA CIE ET DE NOUVELLE GARE INTERNATIONALE', '5.439426', '-4.040503', ' 77 81 19 23', 'A', '2019-07-11 12:00:19', 1);
INSERT INTO `pharmacies` (`id_pharm`, `localite_pharm`, `libelle_pharm`, `adresse_pharm`, `latitude_pharm`, `longitude_pharm`, `telephone_pharm`, `etat_pharm`, `date_ajout_pharm`, `ville_id`) VALUES
(1138, 'ABOBO', 'PHARMACIE DU MARCHE AKEIKOI', 'ABOBO AKEIKOI  ZONE DU MARCHE', '5.449845', '-4.013621', '24 39 00 35', 'A', '2019-07-11 12:00:19', 1),
(1139, 'COCODY', 'PHARMACIE EMERAUDE', 'DERRIERE LA STATION TOTAL SOCOCE II PLATEAUX EN FACE DE LA CLINIQUE LE ROCHER', '5.371924', '-4.001385', '21 57 28 51', 'A', '2019-07-11 12:00:19', 1),
(1140, 'COCODY', 'PHARMACIE KANNIEN', 'AKOUEDO EXTENSION FIN GOUDRON DERRIERE CITE MUGEFCI', '5.366011', '-3.921893', ' 22 46 54 68', 'A', '2019-07-11 12:00:19', 1),
(1141, 'COCODY', 'PHARMACIE NOTRE DAME DU REFUGE ', 'ROUTE AKOUEDO ENTRE FEU PALMERAIE ET FAYA A 100 M DE LA STATION TOTAL', '5.364407', '-3.952294', '22 47 50 59', 'A', '2019-07-11 12:00:19', 1),
(1142, 'KOUMASSI', 'PHARMACIE ABYA', 'Remblais  Rue du Canal  Face Station Petroci', '5.301930', '-3.969518', '21 28 62 36', 'A', '2019-07-11 12:00:19', 1),
(1143, 'KOUMASSI', 'PHARMACIE ST AUGUSTE', 'Koumassi Sicogi 1 Carrefour Gelti en allant vers l Ecole La Pepiniere', '5.288222', ' -3.965574', '21 28 58 26', 'A', '2019-07-11 12:00:19', 1),
(1144, 'KOUMASSI', 'PHARMACIE STE FOI', 'Terminus du Bus 05  Face au Lycee Municipal', '', '', '21 56 17 40', 'A', '2019-07-11 12:00:19', 1),
(1145, 'YOPOUGON', 'PHARMACIE MOKLA', 'Toit Rouge Jean Paul 2 Carrefour G S  Belle Marise 50 m Carrefour Gompci', '5.331312', ' -4.045591', ' 23 45 00 15', 'A', '2019-07-11 12:00:19', 1),
(1146, 'YOPOUGON', 'PHARMACIE BAITY', 'YOPOUGON TOIT ROUGE PRES DE LA GENDARMERIE', '5.327672', '-4.055470', '23 51 77 98', 'A', '2019-07-11 12:00:19', 1),
(1147, 'YOPOUGON', 'PHARMACIE ELKANAH', 'YOPOUGON CARREFOUR CANAL  FACE IMMEUBLE CANAL A 450 M DE LA STATION PETRO IVOIRE', '5.335762', '-4.087758', ' 23 50 88 19', 'A', '2019-07-11 12:00:19', 1),
(1148, 'YOPOUGON', 'PHARMACIE CARREFOUR POMPIER', 'YOP TOIT ROUGE/ FACE SAPEURS POMPIERS-BAE', '', '', ' 78 24 60 88', 'A', '2019-07-11 12:00:19', 1),
(1149, 'YOPOUGON', 'PHARMACIE CHRIST ROI', '60 M DU Carrefour Jean Paul 2  Carrefour Toit rouge', ' 5.330928', ' -4.047742', '23 53 78 76', 'A', '2019-07-11 12:00:19', 1),
(1150, 'YOPOUGON', 'PHARMACIE DE LA DELIVRANCE', 'Zone Industrielle  Pres du Depot de la Sotra Cash Center', '5.369082', ' -4.079613', '23 51 13 79', 'A', '2019-07-11 12:00:19', 1),
(1151, 'YOPOUGON', 'PHARMACIE GLORIA EX PHARMACIE SILOE', 'yopougon annaneraie carrefour antenne/ 300m du carrefour CHU et EGLISE CMA /sens EGLISE CMA / carrefour OASIS', '5.355307', '-4.094713', '23 52 84 48/23 52 03 40', 'A', '2019-07-11 12:00:19', 1),
(1152, 'YOPOUGON', 'PHARMACIE SEGAI', 'Wassakara  Face au Marche', '5.352429', '-4.067721', '23 52 21 10', 'A', '2019-07-11 12:00:19', 1),
(1153, 'YOPOUGON', 'PHARMACIE SIDECI EXTENSION', 'Sideci Carrefour Mandjo Entre Palais de Justice et le Pont Restaure', '5.323992', '-4.083334', '23 51 23 34', 'A', '2019-07-11 12:00:19', 1),
(1154, 'YOPOUGON', 'PHARMACIE ST RAPHAEL', 'Cite Novalim  cite Eeci  non loin de la Clinique Rita de Cascia', '5.342201', '-4.087656', ' 08 97 11 71', 'A', '2019-07-11 12:00:19', 1),
(1155, 'KOUMASSI', 'PHARMACIE BD DU CAMEROUN', 'Koumassi remblais ligne 11 entre les carrefours colombe et 3 ampoules', '5.296937', '-3.964143', '21 36 34 47', 'A', '2019-07-11 12:00:19', 1),
(1156, 'KOUMASSI', 'PHARMACIE CHRIST EMILE', 'Quartier divo Carrefour Hopital', ' 5.305521', '-3.948877', '21 36 81 33', 'A', '2019-07-11 12:00:19', 1),
(1157, 'COCODY', 'PHARMACIE LAURIER 9', 'CARREFOUR ANCIENNE ROUTE DE BINGERVILLE 100 M APRES MOSQUEE DU BONHEUR FACE COMPLOEXE SCOLAIRE DES RESIDENCES LAURIER 8 et 9 / AKOUEDO FAYA', '', '', '22 49 25 20', 'A', '2019-07-11 12:00:19', 1),
(1158, 'COCODY', 'PHARMACIE VAL DE GRACE', 'riviera palmeraie / route de l hotel bellecote a 200 m du college le FIGUIER', '5.380300', ' -3.967427', '22 49 93 44', 'A', '2019-07-11 12:00:19', 1),
(1159, 'YOPOUGON', 'PHARMACIE SOURALEY', 'YOPOUGON PORT BOUET II / PRES DU CARREFOUR DU CENTRE SOCIAL DE PORT BOUET II A 200 M DERRIERE LE COLLEGE GUCHANROLAIN', '5.349886', ' -4.094154', '02 81 97 01', 'A', '2019-07-11 12:00:19', 1),
(1160, 'PORBOUET', 'PHARMACIE KOFFI KRA', 'GONZAG /CARREFOUR USINE I2T / ENTRE BOULANGERIE / GLACIER AMORE / 200M DE LA VOIE EXPRESS ABIDJAN GRAND-BASSAM', '5.244283', ' -3.895610', '58 00 73 24/22 45 33 99', 'A', '2019-07-11 12:00:19', 1),
(1161, 'BINGERVILLE', 'PHARMACIE DE GBAGBA', 'Gbagba petit Paris  Face Eglise Presbyterienne', '5.349403', '-3.885323', '24 38 10 74', 'A', '2019-07-11 12:00:19', 1),
(1162, 'ANYAMA', 'PHARMACIE MEYDEBA', 'ANYAMA ENTRE CARREFOUR CISSE ET CARREFOUR SYLLA DANS ZONE CARREFOUR ZONZONKOI', ' 5.483299', ' -4.051297', '23 55 62 44', 'A', '2019-07-11 12:00:19', 1),
(1163, 'MARCORY', 'PHARMACIE DU PONT D ANOUMABO', 'Apres le Pont Anoumanbo', '5.305455', ' -3.976231', '21 26 15 11', 'A', '2019-07-11 12:00:19', 1),
(1164, 'MARCORY', 'PHARMACIE IMMACULEE', 'MARCORY ZONE 4 C RUE DR CALMETTE A 500M DE PRIMA CENTER ENTRE LE COLLEGE ET LES COURS SEVIGNE', '5.297622', ' -3.979161', '01 30 18 84', 'A', '2019-07-11 12:00:19', 1),
(1165, 'MARCORY', 'PHARMACIE KAMELIA', 'ZONE 4 C / 36 RUE PAUL LANGEVIN / A 500 M DE MERCEDES', '5.276674 ', '-3.967323', '21 57 36 23', 'A', '2019-07-11 12:00:19', 1),
(1166, 'TREICHVILLE', 'PHARMACIE STE ADELE', 'Treichville facade arriere du marche de belle ville  face habitat quartier craonne', '5.304913', '-3.997403', '21 25 47 00', 'A', '2019-07-11 12:00:19', 1),
(1167, 'ABOBO', 'PHARMACIE MAGNIFICAT', 'ROUTE D ANYAMA NON LOIN DEPOT SOTRA 9 ABOBO AVOCATIER PRES DU MARCHE DE NUIT', '5.437381', '-4.030763', '24 39 51 67', 'A', '2019-07-11 12:00:19', 1),
(1168, 'COCODY', 'PHARMACIE CELLULE SAINE', 'COCODY ANGRE AXE 7eme et 8eme TRANCHE APRES NOUVEAU PONT', '5.391735', ' -3.981421', '22 42 64 13', 'A', '2019-07-11 12:00:19', 1),
(1169, 'COCODY', 'PHARMACIE DE L OASIS DE PAIX', 'NON LOIN DU CHU DE COCODY / DANS L UNIVERSITE FELIX HOUPHOUET BOIGNY APRES LA GUERITE PREMIER BATIMENT A DROITE', '5.350624', ' -3.986969', '22 48 92 44', 'A', '2019-07-11 12:00:19', 1),
(1170, 'YOPOUGON', 'PHARMACIE NIANGON SUD EX PHCIE ASSALAMI', 'Niangon Sud a gauche au carrefour de l Allocodrome', '5.316080', '-4.091718', '23 52 99 30', 'A', '2019-07-11 12:00:19', 1),
(1171, 'MARCORY', 'PHARMACIE ROMA D ANOUMABO', 'FACE EPP ANOUMABO 1er ARRET BUS 03  31', '5.308949', '-3.976883', '21 00 29 78', 'A', '2019-07-11 12:00:19', 1),
(1172, 'YOPOUGON', 'PHARMACIE CITE CIE', 'YOPOUGON VOIE PRINCIPALE DE LA CITE CIE MENANT A PORT BOUET II ENTRE AGENCE CIE DE PAIEMENT', '5.343454', '-4.089436', '23 46 34 26', 'A', '2019-07-11 12:00:19', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pharmacies_garde`
--

CREATE TABLE `pharmacies_garde` (
  `id_garde` int(100) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `etat_garde` varchar(2) COLLATE latin1_general_ci NOT NULL DEFAULT 'I',
  `pharm_id` int(100) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Déchargement des données de la table `pharmacies_garde`
--

INSERT INTO `pharmacies_garde` (`id_garde`, `date_debut`, `date_fin`, `etat_garde`, `pharm_id`) VALUES
(1, '2019-03-02', '2019-03-09', 'A', 9),
(2, '2019-03-02', '2019-03-09', 'A', 22),
(3, '2019-03-02', '2019-03-09', 'A', 38),
(4, '2019-03-02', '2019-03-09', 'A', 40),
(5, '2019-03-02', '2019-03-09', 'A', 41),
(6, '2019-03-02', '2019-03-09', 'A', 43),
(7, '2019-03-02', '2019-03-09', 'A', 45),
(8, '2019-03-02', '2019-03-09', 'A', 48),
(9, '2019-03-02', '2019-03-09', 'A', 50),
(10, '2019-03-02', '2019-03-09', 'A', 52),
(11, '2019-03-02', '2019-03-09', 'A', 71),
(12, '2019-03-02', '2019-03-09', 'A', 74),
(13, '2019-03-02', '2019-03-09', 'A', 77),
(14, '2019-03-02', '2019-03-09', 'A', 81),
(15, '2019-03-02', '2019-03-09', 'A', 82),
(16, '2019-03-02', '2019-03-09', 'A', 84),
(17, '2019-03-02', '2019-03-09', 'A', 93),
(18, '2019-03-02', '2019-03-09', 'A', 96),
(19, '2019-03-02', '2019-03-09', 'A', 98),
(20, '2019-03-02', '2019-03-09', 'A', 99),
(21, '2019-03-02', '2019-03-09', 'A', 102),
(22, '2019-03-02', '2019-03-09', 'A', 106),
(23, '2019-03-02', '2019-03-09', 'A', 114),
(24, '2019-03-02', '2019-03-09', 'A', 131),
(25, '2019-03-02', '2019-03-09', 'A', 138),
(26, '2019-03-02', '2019-03-09', 'A', 144),
(27, '2019-03-02', '2019-03-09', 'A', 148),
(28, '2019-03-02', '2019-03-09', 'A', 150),
(29, '2019-03-02', '2019-03-09', 'A', 181),
(30, '2019-03-02', '2019-03-09', 'A', 183),
(31, '2019-03-02', '2019-03-09', 'A', 186),
(32, '2019-03-02', '2019-03-09', 'A', 193),
(33, '2019-03-02', '2019-03-09', 'A', 199),
(34, '2019-03-02', '2019-03-09', 'A', 205),
(35, '2019-03-02', '2019-03-09', 'A', 209),
(36, '2019-03-02', '2019-03-09', 'A', 213),
(37, '2019-03-02', '2019-03-09', 'A', 219),
(38, '2019-03-02', '2019-03-09', 'A', 226),
(39, '2019-03-02', '2019-03-09', 'A', 230),
(40, '2019-03-02', '2019-03-09', 'A', 232),
(41, '2019-03-02', '2019-03-09', 'A', 234),
(42, '2019-03-02', '2019-03-09', 'A', 241),
(43, '2019-03-02', '2019-03-09', 'A', 249),
(44, '2019-03-02', '2019-03-09', 'A', 250),
(45, '2019-03-02', '2019-03-09', 'A', 253),
(46, '2019-03-02', '2019-03-09', 'A', 264),
(47, '2019-03-02', '2019-03-09', 'A', 267),
(48, '2019-03-02', '2019-03-09', 'A', 276),
(49, '2019-03-02', '2019-03-09', 'A', 287),
(50, '2019-03-02', '2019-03-09', 'A', 312),
(51, '2019-03-02', '2019-03-09', 'A', 313),
(52, '2019-03-02', '2019-03-09', 'A', 321),
(53, '2019-03-02', '2019-03-09', 'A', 323),
(54, '2019-03-02', '2019-03-09', 'A', 333),
(55, '2019-03-02', '2019-03-09', 'A', 341),
(56, '2019-03-02', '2019-03-09', 'A', 343),
(57, '2019-03-02', '2019-03-09', 'A', 368),
(58, '2019-03-02', '2019-03-09', 'A', 386),
(59, '2019-03-02', '2019-03-09', 'A', 390),
(60, '2019-03-02', '2019-03-09', 'A', 398),
(61, '2019-03-02', '2019-03-09', 'A', 401),
(62, '2019-03-02', '2019-03-09', 'A', 405),
(63, '2019-03-02', '2019-03-09', 'A', 406),
(64, '2019-03-02', '2019-03-09', 'A', 408),
(65, '2019-03-02', '2019-03-09', 'A', 411),
(66, '2019-03-02', '2019-03-09', 'A', 432),
(67, '2019-03-02', '2019-03-09', 'A', 449),
(68, '2019-03-02', '2019-03-09', 'A', 467),
(69, '2019-03-02', '2019-03-09', 'A', 470),
(70, '2019-03-02', '2019-03-09', 'A', 471),
(71, '2019-03-02', '2019-03-09', 'A', 476),
(72, '2019-03-02', '2019-03-09', 'A', 477),
(73, '2019-03-02', '2019-03-09', 'A', 478),
(74, '2019-03-02', '2019-03-09', 'A', 479),
(75, '2019-03-02', '2019-03-09', 'A', 480),
(77, '2019-03-02', '2019-03-09', 'A', 497),
(79, '2019-03-02', '2019-03-09', 'A', 502),
(80, '2019-03-02', '2019-03-09', 'A', 509),
(81, '2019-03-02', '2019-03-09', 'A', 510),
(82, '2019-03-02', '2019-03-09', 'A', 515),
(83, '2019-03-02', '2019-03-09', 'A', 516),
(84, '2019-03-02', '2019-03-09', 'A', 527),
(85, '2019-03-02', '2019-03-09', 'A', 588),
(86, '2019-03-02', '2019-03-09', 'A', 589),
(87, '2019-03-02', '2019-03-09', 'A', 590),
(88, '2019-03-02', '2019-03-09', 'A', 591),
(89, '2019-07-27', '2019-07-02', 'I', 1),
(90, '2019-07-03', '2019-07-09', 'I', 2),
(91, '2019-07-27', '2019-07-02', 'I', 3),
(92, '2019-07-03', '2019-07-09', 'I', 4),
(93, '2019-07-27', '2019-07-02', 'I', 5),
(94, '2019-07-03', '2019-07-09', 'I', 6),
(95, '2019-07-27', '2019-07-02', 'I', 7),
(96, '2019-07-03', '2019-07-09', 'I', 8),
(97, '2019-07-27', '2019-07-02', 'I', 9);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `code_role` varchar(10) CHARACTER SET utf8 NOT NULL,
  `libelle_role` varchar(150) CHARACTER SET utf8 NOT NULL,
  `etat_role` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id_role`, `code_role`, `libelle_role`, `etat_role`) VALUES
(1, 'SA', 'Super admin', 'A'),
(2, 'MP', 'Manager pays', 'A'),
(3, 'MS', 'Manager simple', 'A'),
(4, 'CP', 'Comptable pays', 'A'),
(5, 'CS', 'Auditeur', 'A'),
(6, 'AP', 'Agent pays', 'A'),
(7, 'AS', 'Agent simple', 'A');

-- --------------------------------------------------------

--
-- Structure de la table `trace`
--

CREATE TABLE `trace` (
  `trace_id` bigint(11) NOT NULL,
  `trace_action` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `trace_date` datetime DEFAULT NULL,
  `user_fk` int(11) DEFAULT NULL,
  `element_fk` int(11) UNSIGNED DEFAULT NULL COMMENT 'L''élément sur lequel l''action est faite',
  `trace_adresse_ip` varchar(100) DEFAULT NULL,
  `trace_complement` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `trace`
--

INSERT INTO `trace` (`trace_id`, `trace_action`, `trace_date`, `user_fk`, `element_fk`, `trace_adresse_ip`, `trace_complement`) VALUES
(1, 'LOGIN', '2020-09-02 20:10:57', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(2, 'LOGOUT', '2020-09-02 22:20:44', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(3, 'LOGIN', '2020-09-02 22:21:07', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(4, 'LOGOUT', '2020-09-02 22:35:07', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(5, 'LOGIN', '2020-09-03 08:55:25', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(6, 'LOGOUT', '2020-09-03 09:17:56', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(7, 'LOGIN', '2020-09-03 09:18:06', 2, NULL, '::1', 'L\'utilisateur se connecte'),
(8, 'LOGOUT', '2020-09-03 09:19:13', 2, NULL, '::1', 'L\'utilisateur se déconnecte'),
(9, 'LOGIN', '2020-09-03 09:19:21', 2, NULL, '::1', 'L\'utilisateur se connecte'),
(10, 'LOGOUT', '2020-09-03 09:20:04', 2, NULL, '::1', 'L\'utilisateur se déconnecte'),
(11, 'LOGIN', '2020-09-03 09:20:09', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(12, 'LOGOUT', '2020-09-03 11:05:21', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(13, 'LOGIN', '2020-09-03 11:08:10', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(14, 'LOGOUT', '2020-09-03 11:13:21', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(15, 'LOGIN', '2020-09-03 11:13:26', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(16, 'LOGIN', '2020-09-03 15:44:53', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(17, 'LOGOUT', '2020-09-03 17:18:15', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(18, 'LOGIN', '2020-09-03 17:18:19', 2, NULL, '::1', 'L\'utilisateur se connecte'),
(19, 'LOGOUT', '2020-09-03 17:22:26', 2, NULL, '::1', 'L\'utilisateur se déconnecte'),
(20, 'LOGIN', '2020-09-03 17:22:30', 2, NULL, '::1', 'L\'utilisateur se connecte'),
(21, 'LOGIN', '2020-09-03 20:27:43', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(22, 'AGENCES', '2020-09-04 09:01:48', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(23, 'AGENCES', '2020-09-04 09:03:21', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(24, 'AGENCES', '2020-09-04 09:03:40', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(25, 'AGENCES', '2020-09-04 09:04:06', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(26, 'AGENCES', '2020-09-04 09:04:11', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(27, 'AGENCES', '2020-09-04 09:05:36', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(28, 'AGENCES', '2020-09-04 09:05:59', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(29, 'AGENCES', '2020-09-04 09:07:26', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(30, 'AGENCES', '2020-09-04 09:07:29', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(31, 'AGENCES', '2020-09-04 09:09:58', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(32, 'LOGOUT', '2020-09-04 09:10:06', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(33, 'LOGIN', '2020-09-04 09:10:10', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(34, 'AGENCES', '2020-09-04 09:10:43', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(35, 'AGENCES', '2020-09-04 09:11:10', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(36, 'AGENCES', '2020-09-04 09:18:36', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(37, 'MODIFIER', '2020-09-04 10:28:22', 1, 1, '::1', 'L\'utilisateur modifie son profil'),
(38, 'MODIFIER', '2020-09-04 11:27:57', 1, 1, '::1', 'L\'utilisateur modifie son profil'),
(39, 'LOGOUT', '2020-09-04 11:29:33', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(40, 'LOGIN', '2020-09-04 11:29:35', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(41, 'AGENCES', '2020-09-04 11:29:44', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(42, 'AGENCES', '2020-09-04 11:34:12', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(43, 'AGENCES', '2020-09-04 12:07:21', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(44, 'AGENCES', '2020-09-04 12:07:27', 1, NULL, '::1', 'L\'utilisateur consulte le menu'),
(45, 'AGENCES', '2020-09-04 12:26:45', 1, NULL, '::1', 'L\'utilisateur consulte le menu agence'),
(46, 'AGENCES', '2020-09-04 12:27:12', 1, NULL, '::1', 'L\'utilisateur consulte le menu agence'),
(47, 'USERS', '2020-09-04 12:27:43', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(48, 'USERS', '2020-09-04 12:32:01', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(49, 'USERS', '2020-09-04 12:32:37', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(50, 'DISABLE', '2020-09-04 12:32:41', 1, 2, '::1', 'L\'utilisateur désactive l\'utilisateur'),
(51, 'USERS', '2020-09-04 12:32:41', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(52, 'ENABLE', '2020-09-04 12:32:45', 1, 2, '::1', 'L\'utilisateur active l\'utilisateur'),
(53, 'USERS', '2020-09-04 12:32:46', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(54, 'DISABLE', '2020-09-04 12:33:17', 1, 2, '::1', 'L\'utilisateur désactive l\'utilisateur'),
(55, 'USERS', '2020-09-04 12:33:17', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(56, 'ENABLE', '2020-09-04 12:33:19', 1, 2, '::1', 'L\'utilisateur active l\'utilisateur'),
(57, 'USERS', '2020-09-04 12:33:19', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(58, 'USERS', '2020-09-04 12:33:26', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(59, 'DISABLE', '2020-09-04 12:33:28', 1, 2, '::1', 'L\'utilisateur désactive l\'utilisateur'),
(60, 'USERS', '2020-09-04 12:33:28', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(61, 'USERS', '2020-09-04 12:38:11', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(62, 'USERS', '2020-09-04 13:23:45', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(63, 'USERS', '2020-09-04 13:24:37', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(64, 'USERS', '2020-09-04 13:26:19', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(65, 'USERS', '2020-09-04 13:27:21', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(66, 'USERS', '2020-09-04 13:29:45', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(67, 'USERS', '2020-09-04 13:29:57', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(68, 'ADDING', '2020-09-04 13:30:19', 1, 4, '::1', 'L\'utilisateur ajoute l\'utilisateur'),
(69, 'USERS', '2020-09-04 13:30:19', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(70, 'ENABLE', '2020-09-04 13:30:53', 1, 2, '::1', 'L\'utilisateur active l\'utilisateur'),
(71, 'USERS', '2020-09-04 13:30:53', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(72, 'USERS', '2020-09-04 13:37:47', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(73, 'USERS', '2020-09-04 13:37:54', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(74, 'USERS', '2020-09-04 13:38:06', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(75, 'MODIFIER', '2020-09-04 13:56:02', 1, 4, '::1', 'L\'utilisateur modifie l\'utilisateur'),
(76, 'USERS', '2020-09-04 13:56:02', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(77, 'USERS', '2020-09-04 13:57:39', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(78, 'LOGIN', '2020-09-04 16:09:27', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(79, 'USERS', '2020-09-04 16:09:29', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(80, 'USERS', '2020-09-04 16:09:33', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(81, 'USERS', '2020-09-04 16:10:25', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(82, 'USERS', '2020-09-04 16:14:05', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(83, 'USERS', '2020-09-04 16:14:57', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(84, 'EXPORT', '2020-09-04 16:31:02', 1, NULL, '::1', 'L\'utilisateur exporte un fichier des transactions'),
(85, 'USERS', '2020-09-04 16:35:14', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(86, 'MODIFIER', '2020-09-04 16:44:01', 1, 1, '::1', 'L\'utilisateur modifie son profil'),
(87, 'LOGOUT', '2020-09-04 16:44:04', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(88, 'LOGIN', '2020-09-04 16:44:07', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(89, 'USERS', '2020-09-04 16:44:16', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(90, 'LOGOUT', '2020-09-04 17:04:35', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(91, 'LOGIN', '2020-09-09 12:25:29', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(92, 'LOGOUT', '2020-09-09 13:06:47', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(93, 'LOGIN', '2021-01-28 09:18:37', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(94, 'LOGIN', '2021-01-28 09:27:50', 1, NULL, '127.0.0.1', 'L\'utilisateur se connecte'),
(95, 'USERS', '2021-01-28 09:33:18', 1, NULL, '127.0.0.1', 'L\'utilisateur consulte le menu users'),
(96, 'USERS', '2021-01-28 09:35:36', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(97, 'LOGOUT', '2021-01-28 09:36:16', 1, NULL, '127.0.0.1', 'L\'utilisateur se déconnecte'),
(98, 'LOGOUT', '2021-01-28 09:40:36', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(99, 'LOGIN', '2021-01-28 09:42:57', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(100, 'LOGOUT', '2021-01-28 09:43:02', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(101, 'LOGIN', '2021-01-28 09:43:13', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(102, 'LOGIN', '2021-01-28 09:47:52', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(103, 'LOGOUT', '2021-01-28 09:48:10', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(104, 'LOGIN', '2021-01-28 10:09:22', 1, NULL, '127.0.0.1', 'L\'utilisateur se connecte'),
(105, 'LOGOUT', '2021-01-28 10:09:28', 1, NULL, '127.0.0.1', 'L\'utilisateur se déconnecte'),
(106, 'LOGIN', '2021-01-28 10:13:14', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(107, 'LOGOUT', '2021-01-28 10:19:03', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(108, 'LOGIN', '2021-01-28 11:28:29', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(109, 'MODIFIER', '2021-01-28 11:34:15', 1, 1, '::1', 'L\'utilisateur modifie son password'),
(110, 'LOGOUT', '2021-01-28 11:34:24', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(111, 'LOGIN', '2021-01-28 11:34:59', 1, NULL, '127.0.0.1', 'L\'utilisateur se connecte'),
(112, 'LOGIN', '2021-01-28 11:53:02', 2, NULL, '::1', 'L\'utilisateur se connecte'),
(113, 'USERS', '2021-01-28 11:53:11', 2, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(114, 'LOGOUT', '2021-01-28 11:53:28', 2, NULL, '::1', 'L\'utilisateur se déconnecte'),
(115, 'LOGIN', '2021-01-28 11:53:45', 1, NULL, '::1', 'L\'utilisateur se connecte'),
(116, 'USERS', '2021-01-28 11:53:52', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(117, 'USERS', '2021-01-28 11:53:52', 1, NULL, '::1', 'L\'utilisateur consulte le menu users'),
(118, 'LOGOUT', '2021-01-28 11:53:58', 1, NULL, '::1', 'L\'utilisateur se déconnecte'),
(119, 'LOGIN', '2021-01-28 11:54:19', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(120, 'LOGOUT', '2021-01-28 12:00:05', 4, NULL, '::1', 'L\'utilisateur se déconnecte'),
(121, 'LOGIN', '2021-01-28 12:00:15', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(122, 'LOGOUT', '2021-01-28 12:01:02', 4, NULL, '::1', 'L\'utilisateur se déconnecte'),
(123, 'LOGIN', '2021-01-28 12:01:13', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(124, 'LOGOUT', '2021-01-28 12:02:10', 4, NULL, '::1', 'L\'utilisateur se déconnecte'),
(125, 'LOGIN', '2021-01-28 12:02:19', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(126, 'LOGOUT', '2021-01-28 12:02:33', 4, NULL, '::1', 'L\'utilisateur se déconnecte'),
(127, 'LOGIN', '2021-01-28 12:02:45', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(128, 'LOGOUT', '2021-01-28 12:02:50', 4, NULL, '::1', 'L\'utilisateur se déconnecte'),
(129, 'LOGIN', '2021-01-28 12:03:07', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(130, 'LOGIN', '2021-01-28 15:48:07', 4, NULL, '::1', 'L\'utilisateur se connecte'),
(131, 'LOGOUT', '2021-01-28 17:49:40', 4, NULL, '::1', 'L\'utilisateur se déconnecte');

-- --------------------------------------------------------

--
-- Structure de la table `transactions`
--

CREATE TABLE `transactions` (
  `id_trans` bigint(11) NOT NULL,
  `reference_syca` varchar(255) NOT NULL,
  `atlantis_ref` varchar(255) NOT NULL,
  `montant_trans` int(11) NOT NULL,
  `mobile_trans` varchar(10) DEFAULT NULL,
  `produit_trans` varchar(250) DEFAULT NULL,
  `assure_trans` varchar(250) DEFAULT NULL,
  `assureID` bigint(100) UNSIGNED DEFAULT NULL,
  `assureMobile` varchar(50) DEFAULT NULL,
  `assure_num_polices` varchar(350) DEFAULT NULL,
  `assure_code_inte` varchar(350) DEFAULT NULL,
  `date_create_trans` datetime NOT NULL,
  `date_maj_statut` datetime NOT NULL,
  `statut_trans` varchar(2) NOT NULL DEFAULT 'P',
  `ent_fk` int(11) UNSIGNED DEFAULT NULL COMMENT 'id de l''entreprise',
  `pays_fk` varchar(10) DEFAULT NULL,
  `NUMEENCA` varchar(100) DEFAULT NULL COMMENT 'Retour api terme',
  `NUMEQUIT` varchar(100) DEFAULT NULL COMMENT 'Retour api terme',
  `MOISTERM` varchar(100) DEFAULT NULL COMMENT 'Retour api terme',
  `DATEEFFE` varchar(100) DEFAULT NULL COMMENT 'Retour api terme',
  `DATEECHE` varchar(100) DEFAULT NULL COMMENT 'Retour api terme'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `transactions`
--

INSERT INTO `transactions` (`id_trans`, `reference_syca`, `atlantis_ref`, `montant_trans`, `mobile_trans`, `produit_trans`, `assure_trans`, `assureID`, `assureMobile`, `assure_num_polices`, `assure_code_inte`, `date_create_trans`, `date_maj_statut`, `statut_trans`, `ent_fk`, `pays_fk`, `NUMEENCA`, `NUMEQUIT`, `MOISTERM`, `DATEEFFE`, `DATEECHE`) VALUES
(1, 'kAfJwbBASWW8MzT', '5f57cb90a53ea', 200, 'INDEFINED', NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-08 20:21:39', '2020-09-08 20:22:42', 'S', NULL, 'CI', NULL, NULL, NULL, NULL, NULL),
(2, 'JnNmDFBve4gzX4Z', '5f57cd60569ce', 400, 'INDEFINED', NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-08 20:29:19', '2020-09-08 20:29:37', 'S', NULL, 'CI', NULL, NULL, NULL, NULL, NULL),
(3, '', 'V_6012DCD5B309F', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-28 15:48:37', '0000-00-00 00:00:00', 'P', 1, 'CI', NULL, NULL, NULL, NULL, NULL),
(4, '', 'V_601BE1665D4D3', 3000000, NULL, 'VIE', 'SABA HAMIDOU', NULL, NULL, NULL, NULL, '2021-02-04 12:58:33', '0000-00-00 00:00:00', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'eWtPM7U7fQxTadM', 'V_601BE39898FE5', 100, 'INDEFINED', 'VIE', 'SABA HAMIDOU', NULL, NULL, NULL, NULL, '2021-02-04 13:07:57', '2021-02-04 13:12:30', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'rkJye6A3byuDYvk', 'V_601BEC7332E31', 100, 'INDEFINED', 'VIE', 'SABA HAMIDOU', 4, NULL, NULL, NULL, '2021-02-04 13:45:53', '2021-02-04 13:46:23', 'S', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Cjb4ufuGsUmVnbR', 'V_601BEDB0AA7B2', 200, 'INDEFINED', 'VIE', 'SABA HAMIDOU', 4, NULL, NULL, NULL, '2021-02-04 13:50:59', '2021-02-04 13:51:37', 'S', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', 'I_601BF06CBF405', 53864, NULL, 'IARD', ' ATLANTIC BUSINESS INTERNATIONAL', NULL, NULL, NULL, NULL, '2021-02-04 14:02:41', '0000-00-00 00:00:00', 'P', NULL, 'CI', NULL, NULL, NULL, NULL, NULL),
(9, '', 'V_601BF1F98E440', 1200, NULL, 'VIE', 'SABA HAMIDOU', 4, NULL, NULL, NULL, '2021-02-04 14:09:18', '0000-00-00 00:00:00', 'P', 1, 'CI', NULL, NULL, NULL, NULL, NULL),
(10, '', 'V_601BF2B72DB05', 1200, NULL, 'VIE', 'SABA HAMIDOU', 4, NULL, NULL, NULL, '2021-02-04 14:12:27', '0000-00-00 00:00:00', 'P', 1, 'CI', NULL, NULL, NULL, NULL, NULL),
(11, '', 'V_601D1F29C6193', 1100, NULL, 'VIE', 'TRAORE MABINTOU', NULL, '22890029922', '', '', '2021-02-05 11:34:21', '0000-00-00 00:00:00', 'P', NULL, '0', NULL, NULL, NULL, NULL, NULL),
(12, '', 'V_601D1F29C786F', 1000, NULL, 'VIE', 'TRAORE MABINTOU', NULL, '22890029922', NULL, NULL, '2021-02-05 11:34:21', '0000-00-00 00:00:00', 'P', NULL, '0', NULL, NULL, NULL, NULL, NULL),
(13, '', 'V_601D24429C04F', 1100, NULL, 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000387', '100', '2021-02-05 10:56:06', '0000-00-00 00:00:00', 'P', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(14, '', 'V_601D24429DCBC', 1000, NULL, 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000388', '100', '2021-02-05 10:56:06', '0000-00-00 00:00:00', 'P', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(15, 'xQeLfN3VGdxLDQV', 'V_601D24BB974B1', 1100, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000387', '100', '2021-02-05 10:58:07', '2021-02-05 10:58:24', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(16, 'xQeLfN3VGdxLDQV', 'V_601D24BB9856B', 1000, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000388', '100', '2021-02-05 10:58:07', '2021-02-05 10:58:24', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(17, 'jqZ3rBtyKx5edh1', 'V_601D2931CC9C8', 1100, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000387', '100', '2021-02-05 11:17:11', '2021-02-05 11:17:31', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(18, 'jqZ3rBtyKx5edh1', 'V_601D2931CE2C8', 1000, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000388', '100', '2021-02-05 11:17:12', '2021-02-05 11:17:31', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(19, 'gEwTw2qhzXjjyWT', 'V_601D2A743ECD1', 1000, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '21000388', '100', '2021-02-05 11:22:31', '2021-02-05 11:34:48', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(20, 'gEwTw2qhzXjjyWT', 'V_601D2A74403DC', 700, 'INDEFINED', 'VIE', 'TRAORE MABINTOU', 3, '22890029922', '24012742', '100', '2021-02-05 11:22:31', '2021-02-05 11:34:49', 'S', 3, 'TG', NULL, NULL, NULL, NULL, NULL),
(21, 'CDUEue33mBmVuL4', 'V_601D2DB2148E2', 5000, 'INDEFINED', 'VIE', 'AFANOU YAWA RUTH', 3, '22890029922', '24012742', '100', '2021-02-05 11:36:21', '2021-02-05 11:36:48', 'S', 3, 'TG', '9202100007', '1000764761', '02/2021', '01/02/21', '28/02/21'),
(22, 'CDUEue33mBmVuL4', 'V_601D2DB215E81', 10000, 'INDEFINED', 'VIE', 'AFANOU YAWA RUTH', 3, '22890029922', '24110439', '100', '2021-02-05 11:36:21', '2021-02-05 11:36:49', 'S', 3, 'TG', '9202100008', '1000752229', '12/2020', '01/12/20', '31/12/20');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nom_user` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `prenoms_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `login_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pass_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_create_user` datetime DEFAULT NULL,
  `user_etat` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `role_fk` int(11) DEFAULT NULL COMMENT 'l''id du privilèges',
  `parent_id` int(11) DEFAULT NULL COMMENT 'id de l''user qui l''a crée',
  `fk_ent` int(11) UNSIGNED DEFAULT NULL COMMENT 'antreprise ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `nom_user`, `prenoms_user`, `login_user`, `pass_user`, `date_create_user`, `user_etat`, `role_fk`, `parent_id`, `fk_ent`) VALUES
(1, 'MAJOR', 'MAJOR', 'abou.konate@sycapay.com', 'fe01ce2a7fbac8fafaed7c982a04e229', '2020-09-01 00:00:00', 'A', 1, NULL, NULL),
(2, 'ASSY', 'HELENE', 'majorabou@gmail.com', 'fe01ce2a7fbac8fafaed7c982a04e229', '2020-09-01 00:00:00', 'A', 2, 1, 1),
(4, 'ASSY', 'HELENE', 'helene.assy@sycapay.com', 'fe01ce2a7fbac8fafaed7c982a04e229', '2020-09-04 13:30:18', 'A', 3, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

CREATE TABLE `villes` (
  `id_ville` int(100) NOT NULL,
  `libelle_ville` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `etat_ville` varchar(2) COLLATE latin1_general_ci NOT NULL DEFAULT 'I',
  `details_ville` varchar(10000) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=COMPACT;

--
-- Déchargement des données de la table `villes`
--

INSERT INTO `villes` (`id_ville`, `libelle_ville`, `etat_ville`, `details_ville`) VALUES
(1, 'Abidjan', 'A', 'Capitale économique'),
(2, 'Yamoussokro', 'I', 'Capitale politique');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affectation`
--
ALTER TABLE `affectation`
  ADD PRIMARY KEY (`id_affectation`);

--
-- Index pour la table `agence`
--
ALTER TABLE `agence`
  ADD PRIMARY KEY (`id_agence`);

--
-- Index pour la table `assures`
--
ALTER TABLE `assures`
  ADD PRIMARY KEY (`id_assure`);

--
-- Index pour la table `demandes`
--
ALTER TABLE `demandes`
  ADD PRIMARY KEY (`id_demande`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id_ent`);

--
-- Index pour la table `lots`
--
ALTER TABLE `lots`
  ADD PRIMARY KEY (`id_lot`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`pays_id`);

--
-- Index pour la table `pharmacies`
--
ALTER TABLE `pharmacies`
  ADD PRIMARY KEY (`id_pharm`);

--
-- Index pour la table `pharmacies_garde`
--
ALTER TABLE `pharmacies_garde`
  ADD PRIMARY KEY (`id_garde`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Index pour la table `trace`
--
ALTER TABLE `trace`
  ADD PRIMARY KEY (`trace_id`);

--
-- Index pour la table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id_trans`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `affectation`
--
ALTER TABLE `affectation`
  MODIFY `id_affectation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `agence`
--
ALTER TABLE `agence`
  MODIFY `id_agence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `assures`
--
ALTER TABLE `assures`
  MODIFY `id_assure` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `demandes`
--
ALTER TABLE `demandes`
  MODIFY `id_demande` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id_ent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `trace`
--
ALTER TABLE `trace`
  MODIFY `trace_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT pour la table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id_trans` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
