/**
 * Created by ADMIN on 07/09/2017.
 */
    /**
     * Traitement du formulaire d'inscription
     */
    $('#form_register').on('submit', function (e) {
        e.preventDefault();
        var url = $('#form_register').attr('action');
        var data = $('#form_register').serialize();
/*        $(this).find('button').addClass('chargement');
        $(this).find('button').prop('disabled', true);*/
        //validation des infos du formulaire
        var $form_modal = $("#modal_register");
        var nom = $('#nom');
        var prenoms = $('#prenoms');
        var email = $('#email');
        var mobile = $('#mobile');
        var password = $("#password");
        var confPassword = $("#conf_password");
        var num_abo = $("#num_abo");
        var result = '';
        var that = this;
        if(nom.val()==''){
            nom.parent().addClass('has-error');
        }else{
            nom.parent().removeClass('has-error');
            nom.parent().addClass('has-success');
            result +='1';
        }
        if(prenoms.val()==''){
            prenoms.parent().addClass('has-error');
        }else{
            prenoms.parent().removeClass('has-error');
            prenoms.parent().addClass('has-success');
            result +='2';
        }
        if(email.val()==''){
            email.parent().addClass('has-error');
        }else{
            email.parent().removeClass('has-error');
            email.parent().addClass('has-success');
            result +='3';
        }
        if(mobile.val()==''){
            mobile.parent().addClass('has-error');
        }else{
            mobile.parent().removeClass('has-error');
            mobile.parent().addClass('has-success');
            result +='4';
        }
        if(password.val()==''){
            password.parent().addClass('has-error');
        }else{
            password.parent().removeClass('has-error');
            password.parent().addClass('has-success');
            result +='5';
        }
        if(confPassword.val()==''){
            confPassword.parent().addClass('has-error');
        }else{
            confPassword.parent().removeClass('has-error');
            confPassword.parent().addClass('has-success');
            result +='6';
        }
        /*if(num_abo.val()=='' || num_abo.val()!=''){
            num_abo.parent().addClass('has-error');
        }else{
            num_abo.parent().removeClass('has-error');
            num_abo.parent().addClass('has-success');
            result +='7';
        }*/
        if(confPassword.val()!==password.val()){
            confPassword.parent().addClass('has-error');
            password.parent().addClass('has-error');
        }else{
            confPassword.parent().removeClass('has-error');
            password.parent().removeClass('has-error');
            confPassword.parent().addClass('has-success');
            password.parent().addClass('has-success');
            result +='7';
        }
        if(result=='1234567'){
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: url,
                data: data,
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    $("button.ladda-button").prop('disabled', false);
                    if(response.success){
                        $form_modal.modal("hide");
                        that.reset();
                        toast_success('Inscription effectuée avec succès');
                    }else{
                        toast_error('Echec, '+response.txt);
                    }
                },
                error: function(jqxhr){
                    console.log(jqxhr.responseText);
                    toast_error(jqxhr.responseText);
                }
            });
        }else{
            toast_error('Champs réquis manquants');
        }
    });

    /**
     * Traitement du formulaire de connexion
     */
    $('#formConnect').on('submit', function (e) {
        e.preventDefault();
        var url = $('#formConnect').attr('action');
        var data = $('#formConnect').serialize();
        var lien = $('input[name=lienDirect]').val();
        var $form_modal = $(this).parent().parent().parent();
        $(this).find('button').addClass('chargement');
        $(this).find('button').prop('disabled', true);

        $.post(url, data)
            .done(function(data, text, jqxhr){
                console.log(jqxhr);
                $("button.ladda-button").removeClass('chargement');
                var data = $.parseJSON(jqxhr.responseText);
                if(data.success){
                    console.log(data.nom);
                    console.log(data.prenom);
                    console.log(data.login);
                    window.location.reload();
                    $("button.ladda-button").prop('disabled', false);
                    $form_modal.removeClass('is-visible');
                    $('#formConnect')[0].reset();
                    $("#blocAuth").addClass('is-hidden');
                    $("#blocDashboard").removeClass('is-hidden');
                    $("#blocDashboard").addClass('is-visible');

                    var blocDashboard = '<button class="btn btn-default btn-sm dropdown-toggle btnBlocDashboard" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    blocDashboard += '<img src="web/assets/images/avatar.png">';
                    blocDashboard += '<span class="nameAbonne">'+data.prenom;
                    blocDashboard += ' '+data.nom;
                    blocDashboard += '</span><span class="caret"></span></button>';
                    blocDashboard += '<ul class="dropdown-menu ulBlocDashboard">';
                    blocDashboard +='<li><a href="">Dashboard</a></li>';
                    blocDashboard += '<li><a href="">Mes transactions</a></li>';
                    blocDashboard += '<li><a class="login" href="'+lien+'index.php/Accueil/deconnexion" id="logout">';
                    blocDashboard += '<input type="hidden" name="sess" value="'+data.login+'">';
                    blocDashboard += 'Déconnexion</a></li></ul>';
                    $(".nameAbonne").html(data.prenom+ ' '+data.nom);
                    //$(this).find('a#log').addClass('chargement');
                    //$("#blocDashboard").html(blocDashboard);

                    $("#blocDashboard").removeClass('is-hidden');
                    $("#blocAuth").addClass('is-hidden');
                    $('#formBuyTicker').show();
                    $('#pay_sc').hide();
                    Materialize.toast(data.txt, 2000, 'green darken-2');

                }else{
                    $("button.ladda-button").prop('disabled', false);
                    Materialize.toast(data.txt, 2000, 'red darken-2');
                }
            })
            .fail(function(jqxhr){
                $("button.ladda-button").prop('disabled', false);
                $("button.ladda-button").removeClass('chargement');
                console.log(jqxhr.dataText);
            })
            .always(function(){
                $("button.ladda-button").prop('disabled', false);
                $("button.ladda-button").removeClass('chargement');
            })
    });

    /**
     * Traitement du lien de deconnexion
     */
    $('#logout').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var sess = $(this).attr('class');
        var lien = $(this).attr('title');
        var data  = {sess: sess};
        var gg = "<div class=\"main-nav right is-visible\" id=\"blocAuth\"><ul><li><a class=\"cd-signin\">Log in</a></li><li>|</li><li><a class=\"cd-signup\">registration</a></li></ul></div>";
        $.post(url, data)
            .done(function(data, text, jqxhr){
                console.log(jqxhr);
                var data = $.parseJSON(jqxhr.responseText);
                if(data.success){
                    toast_success(data.txt);
                    console.log(data.txt);
                    $("body").attr("class", "0");
                    $('#formBuyTicker').hide();
                    $('#pay_sc').show();
                    location.reload();
                }else{
                    toast_error(data.txt);
                }
            })
            .fail(function(jqxhr){
                console.log(jqxhr.dataText);
            })
            .always(function(){
            })
    });

    /**
     * Traitement du formulaire mot de passe oublié executant l'envoi du mail
     */
    $('#formForgotPassword').on('submit', function (e) {
        e.preventDefault();
        var url = $('#formForgotPassword').attr('action');
        var data = $('#formForgotPassword').serialize();
        var $form_modal = $(this).parent().parent().parent(),
            $form_modal_container = $form_modal.find('.cd-user-modal-container'),
            $form_login = $form_modal.find('#cd-login'),
            $form_signup = $form_modal.find('#cd-signup'),
            $form_forgot_password = $form_modal.find('#cd-reset-password'),
            $form_new_password = $form_modal.find('#cd-new-password'),
            $form_modal_tab = $('.cd-switcher');
        $(this).find('button').addClass('chargement');
        $(this).find('button').prop('disabled', true);

        $.post(url, data)
            .done(function(data, text, jqxhr){
                console.log(jqxhr);
                $("button.ladda-button").prop('disabled', false);
                var data = $.parseJSON(jqxhr.responseText);
                $("button.ladda-button").removeClass('chargement');
                if(data.success){
                    console.log(data.txt);
                    Materialize.toast(data.txt, 2000, 'green darken-2');
                    //$form_modal.removeClass('is-visible');
                    $form_login.removeClass('is-selected');
                    $form_signup.removeClass('is-selected');
                    $form_forgot_password.removeClass('is-selected');
                    $form_new_password.addClass('is-selected');
                    $form_modal_container.css("height", "auto");
                }else{
                    Materialize.toast(data.txt, 2000, 'red darken-2');
                }
            })
            .fail(function(jqxhr){
                console.log(jqxhr.dataText);
                $("button.ladda-button").prop('disabled', false);
                $("button.ladda-button").removeClass('chargement');
            })
            .always(function(){
                $("button.ladda-button").prop('disabled', false);
                $("button.ladda-button").removeClass('chargement');
            })
    });

    $('#formCodeVerif').on('submit', function (e) {
        e.preventDefault();
        var url = $('#formCodeVerif').attr('action');
        var data = $('#formCodeVerif').serialize();
        var spinner = "<div class='spinner'></div>";
        $(this).find('button').text('Patientez').button("refresh");
        $(this).find('button').addClass('model-1').button("refresh");
        $(this).find('button').append(spinner).button("refresh");

        $.post(url, data)
            .done(function(data, text, jqxhr){
                console.log(jqxhr);
                var data = $.parseJSON(jqxhr.responseText);
                $("button.ladda-button").removeClass('chargement');
                if(data.success){
                    $("body .modal_codeVerif").removeClass("md-show");
                    console.log(data.txt);
                    Materialize.toast(data.txt, 2000, 'green darken-2');
                    $("body .modal_newPassword").addClass("md-show");
                }else{
                    Materialize.toast(data.txt, 2000, 'red darken-2');
                    $('#btnCodeValidate').text('Valider').button("refresh");
                }
            })
            .fail(function(jqxhr){
                console.log(jqxhr.dataText);
                $('#btnCodeValidate').text('Valider').button("refresh");
            })
            .always(function(){
                $('#btnCodeValidate').text('Valider').button("refresh");
            })
    });

    $('#formNewPassword').on('submit', function (e) {
        e.preventDefault();
        var url = $('#formNewPassword').attr('action');
        var data = $('#formNewPassword').serialize();
        var spinner = "<div class='spinner'></div>";
        var that = this;
        $(that).find('button').text('Réinitialisation en cours').button("refresh");
        $(that).find('button').addClass('model-1').button("refresh");
        $(that).find('button').append(spinner).button("refresh");
        $.post(url, data)
            .done(function(data, text, jqxhr){
                console.log(jqxhr);
                var data = $.parseJSON(jqxhr.responseText);
                $(that).find('button').text('Réinitialiser le mot de passe').button("refresh");
                if(data.success){
                    $("body .modal_newPassword").removeClass("md-show");
                    console.log(data.txt);
                    Materialize.toast(data.txt, 2000, 'green darken-2');
                    $("body .modal_connexion").addClass("md-show");
                    setTimeout(function(){// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 1000);
                }else{
                    Materialize.toast(data.txt, 2000, 'red darken-2');
                    $(that).find('button').text('Réinitialiser le mot de passe').button("refresh");
                }
            })
            .fail(function(jqxhr){
                console.log(jqxhr.dataText);
                $(that).find('button').text('Réinitialiser le mot de passe').button("refresh");
            })
            .always(function(){
                $(that).find('button').text('Réinitialiser le mot de passe').button("refresh");
            })
    });

    /**
     * Traitement du formulaire achat de ticket en mode Online
     */
    $('#form_acheter').on('submit', function (e) {
        e.preventDefault();
        var url = $('#form_acheter').attr('action');
        $(this).find('button').addClass('chargement');
        var montant = $('#montant');
        var meter_number = $('#meter_number');
        var data = {
            montant: montant.val(),
            meter_number: meter_number.val(),
        };
        console.log(data);
        var result = '';
        if(montant.val()==''){
            toast_error("Veuillez saisir un montant correct s'il vous plait");
            montant.addClass('has-error');
        }else{
            montant.removeClass('has-error');
            result ='1';
        }
        if((result == '1')){
            var valeur = parseInt(montant.val());
            $(this).find('button').prop('disabled', true);
            $.post(url, data)
                .done(function(data, text, jqxhr){
/*                    console.log(jqxhr);
                    $("button.ladda-button").prop('disabled', false);
                    $("button.ladda-button").removeClass('chargement');*/
                    var data = $.parseJSON(jqxhr.responseText);
                    if(data.success){
                        console.log(data);

                        var blocConfirm = '<div class="row">';
                        blocConfirm += '<div class="col-md-12">';
                        blocConfirm += '<form action="https://secure.sycapay.com/checkresponsive" method="post" name="sycapayment" id="sycapayment" class="sycapayment">';
                        blocConfirm += '<input type="hidden" name="token" value="'+data.txt+'">';
                        blocConfirm += '<input type="hidden" id="amount" name="amount" value="'+valeur+'">';
                        blocConfirm += '<input type="hidden" name="currency" value="XOF">';
                        blocConfirm += '<input type="hidden" name="urls" value="'+data.urls+'/'+valeur+'/'+'usercontact'+'">';
                        blocConfirm += '<input type="hidden" name="urlc" value="'+data.urlc+'">';
                        blocConfirm += '<input type="hidden" id="commande" name="commande" value="' + data.meter_number +'">';
                        blocConfirm += '<input type="hidden" name="merchandid" value="'+data.merchandid+'">';
                        blocConfirm += '<input type="hidden" name="typpaie" id="typpaie" value="payement">';
                        blocConfirm += '<div class="form-group row">';
                        blocConfirm += '<label for="email" class="col-sm-4 control-label">Numero du compteur : </label>';
                        blocConfirm += '<div class="col-sm-8">';
                        blocConfirm += '<span>'+data.meter_number+'<span>';
                        blocConfirm += '</div>';
                        blocConfirm += '</div>';
                        blocConfirm += '<div class="form-group row">';
                        blocConfirm += '<label for="email" class="col-sm-4 control-label"> Valeur du Ticket : </label>';
                        blocConfirm += '<div class="col-sm-8">';
                        blocConfirm += '<span>'+valeur+' FCFA<span>';
                        blocConfirm += '</div>';
                        blocConfirm += '</div>';
                        blocConfirm += '<div class="form-group">';
                        blocConfirm += '<div class="col-sm-12 btn-zone" style="text-align: center">';
                        blocConfirm += '<button class="btn btn-lg btn-register btn_submit" type="button" id="btn_valider">Confirmer</button>';
                        blocConfirm += '</div>';
                        blocConfirm += '</div>';
                        //blocConfirm += '<span> Frais : '+frais+' FCFA<span>';
                        //blocConfirm += '<a href="" class="cd-popup-close img-replace">Fermer</a>';
                        blocConfirm += '</form>';
                        blocConfirm += '</div>';
                        blocConfirm += '</div>';

                        $('#modal_acheter').modal('hide');
                        $("#modal_confirm").find(".modal-body").html(blocConfirm);
                        //$("#modal_confirm").find("#myModalLabel").text('Confirmez-vous votre achat de '+valeur+' FCFA ?');
                        $('#modal_confirm').modal('show');
                    }else{
                        toast_error(data.txt);
                    }
                })
                .fail(function(jqxhr){
                    $("button.ladda-button").prop('disabled', false);
                    $("button.ladda-button").removeClass('chargement');
                    console.log(jqxhr);
                })
                .always(function(){
                    $('#btn_valider').on('click', function (e) {

                        $('#sycapayment').submit();

                    });
                    $("button.ladda-button").prop('disabled', false);
                    $("button.ladda-button").removeClass('chargement');
                })
        }
    });
